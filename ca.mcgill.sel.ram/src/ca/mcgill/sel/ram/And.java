/**
 */
package ca.mcgill.sel.ram;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>And</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see ca.mcgill.sel.ram.RamPackage#getAnd()
 * @model
 * @generated
 */
public interface And extends Binary {
} // And
