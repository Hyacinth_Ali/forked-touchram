/**
 */
package ca.mcgill.sel.ram;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Annotation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.ram.Annotation#getParameter <em>Parameter</em>}</li>
 *   <li>{@link ca.mcgill.sel.ram.Annotation#getMatter <em>Matter</em>}</li>
 *   <li>{@link ca.mcgill.sel.ram.Annotation#getImport <em>Import</em>}</li>
 * </ul>
 *
 * @see ca.mcgill.sel.ram.RamPackage#getAnnotation()
 * @model
 * @generated
 */
public interface Annotation extends EObject {
	/**
     * Returns the value of the '<em><b>Parameter</b></em>' attribute list.
     * The list contents are of type {@link java.lang.String}.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Parameter</em>' attribute list.
     * @see ca.mcgill.sel.ram.RamPackage#getAnnotation_Parameter()
     * @model
     * @generated
     */
	EList<String> getParameter();

	/**
     * Returns the value of the '<em><b>Matter</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Matter</em>' attribute.
     * @see #setMatter(String)
     * @see ca.mcgill.sel.ram.RamPackage#getAnnotation_Matter()
     * @model
     * @generated
     */
	String getMatter();

	/**
     * Sets the value of the '{@link ca.mcgill.sel.ram.Annotation#getMatter <em>Matter</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Matter</em>' attribute.
     * @see #getMatter()
     * @generated
     */
	void setMatter(String value);

	/**
     * Returns the value of the '<em><b>Import</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Import</em>' reference.
     * @see #setImport(Import)
     * @see ca.mcgill.sel.ram.RamPackage#getAnnotation_Import()
     * @model
     * @generated
     */
	Import getImport();

	/**
     * Sets the value of the '{@link ca.mcgill.sel.ram.Annotation#getImport <em>Import</em>}' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Import</em>' reference.
     * @see #getImport()
     * @generated
     */
	void setImport(Import value);

} // Annotation
