/**
 */
package ca.mcgill.sel.ram;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Minus</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see ca.mcgill.sel.ram.RamPackage#getMinus()
 * @model
 * @generated
 */
public interface Minus extends Binary {
} // Minus
