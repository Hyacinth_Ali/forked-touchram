/**
 */
package ca.mcgill.sel.ram;

import ca.mcgill.sel.core.COREMapping;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Association End Mapping</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see ca.mcgill.sel.ram.RamPackage#getAssociationEndMapping()
 * @model
 * @generated
 */
public interface AssociationEndMapping extends COREMapping<AssociationEnd> {
} // AssociationEndMapping
