/**
 */
package ca.mcgill.sel.ram;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Literal Byte</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.ram.LiteralByte#getValue <em>Value</em>}</li>
 * </ul>
 *
 * @see ca.mcgill.sel.ram.RamPackage#getLiteralByte()
 * @model
 * @generated
 */
public interface LiteralByte extends LiteralSpecification {
	/**
     * Returns the value of the '<em><b>Value</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Value</em>' attribute.
     * @see #setValue(byte)
     * @see ca.mcgill.sel.ram.RamPackage#getLiteralByte_Value()
     * @model required="true"
     * @generated
     */
	byte getValue();

	/**
     * Sets the value of the '{@link ca.mcgill.sel.ram.LiteralByte#getValue <em>Value</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Value</em>' attribute.
     * @see #getValue()
     * @generated
     */
	void setValue(byte value);

} // LiteralByte
