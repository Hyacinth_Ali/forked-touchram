/**
 */
package ca.mcgill.sel.ram;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Ternary</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.ram.Ternary#getCondition <em>Condition</em>}</li>
 *   <li>{@link ca.mcgill.sel.ram.Ternary#getExpressionT <em>Expression T</em>}</li>
 *   <li>{@link ca.mcgill.sel.ram.Ternary#getExpressionF <em>Expression F</em>}</li>
 * </ul>
 *
 * @see ca.mcgill.sel.ram.RamPackage#getTernary()
 * @model abstract="true"
 * @generated
 */
public interface Ternary extends Operator {
	/**
     * Returns the value of the '<em><b>Condition</b></em>' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Condition</em>' containment reference.
     * @see #setCondition(ValueSpecification)
     * @see ca.mcgill.sel.ram.RamPackage#getTernary_Condition()
     * @model containment="true" required="true"
     * @generated
     */
	ValueSpecification getCondition();

	/**
     * Sets the value of the '{@link ca.mcgill.sel.ram.Ternary#getCondition <em>Condition</em>}' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Condition</em>' containment reference.
     * @see #getCondition()
     * @generated
     */
	void setCondition(ValueSpecification value);

	/**
     * Returns the value of the '<em><b>Expression T</b></em>' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Expression T</em>' containment reference.
     * @see #setExpressionT(ValueSpecification)
     * @see ca.mcgill.sel.ram.RamPackage#getTernary_ExpressionT()
     * @model containment="true" required="true"
     * @generated
     */
	ValueSpecification getExpressionT();

	/**
     * Sets the value of the '{@link ca.mcgill.sel.ram.Ternary#getExpressionT <em>Expression T</em>}' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Expression T</em>' containment reference.
     * @see #getExpressionT()
     * @generated
     */
	void setExpressionT(ValueSpecification value);

	/**
     * Returns the value of the '<em><b>Expression F</b></em>' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Expression F</em>' containment reference.
     * @see #setExpressionF(ValueSpecification)
     * @see ca.mcgill.sel.ram.RamPackage#getTernary_ExpressionF()
     * @model containment="true" required="true"
     * @generated
     */
	ValueSpecification getExpressionF();

	/**
     * Sets the value of the '{@link ca.mcgill.sel.ram.Ternary#getExpressionF <em>Expression F</em>}' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Expression F</em>' containment reference.
     * @see #getExpressionF()
     * @generated
     */
	void setExpressionF(ValueSpecification value);

} // Ternary
