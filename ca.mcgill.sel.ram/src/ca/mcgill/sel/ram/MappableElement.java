/**
 */
package ca.mcgill.sel.ram;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Mappable Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.ram.MappableElement#getPartiality <em>Partiality</em>}</li>
 * </ul>
 *
 * @see ca.mcgill.sel.ram.RamPackage#getMappableElement()
 * @model abstract="true"
 * @generated
 */
public interface MappableElement extends NamedElement {
	/**
     * Returns the value of the '<em><b>Partiality</b></em>' attribute.
     * The literals are from the enumeration {@link ca.mcgill.sel.ram.RAMPartialityType}.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Partiality</em>' attribute.
     * @see ca.mcgill.sel.ram.RAMPartialityType
     * @see #setPartiality(RAMPartialityType)
     * @see ca.mcgill.sel.ram.RamPackage#getMappableElement_Partiality()
     * @model
     * @generated
     */
	RAMPartialityType getPartiality();

	/**
     * Sets the value of the '{@link ca.mcgill.sel.ram.MappableElement#getPartiality <em>Partiality</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Partiality</em>' attribute.
     * @see ca.mcgill.sel.ram.RAMPartialityType
     * @see #getPartiality()
     * @generated
     */
	void setPartiality(RAMPartialityType value);

} // MappableElement
