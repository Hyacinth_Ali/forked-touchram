/**
 */
package ca.mcgill.sel.ram;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Mul Div Mod</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.ram.MulDivMod#getOp <em>Op</em>}</li>
 * </ul>
 *
 * @see ca.mcgill.sel.ram.RamPackage#getMulDivMod()
 * @model
 * @generated
 */
public interface MulDivMod extends Binary {
	/**
     * Returns the value of the '<em><b>Op</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Op</em>' attribute.
     * @see #setOp(String)
     * @see ca.mcgill.sel.ram.RamPackage#getMulDivMod_Op()
     * @model
     * @generated
     */
	String getOp();

	/**
     * Sets the value of the '{@link ca.mcgill.sel.ram.MulDivMod#getOp <em>Op</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Op</em>' attribute.
     * @see #getOp()
     * @generated
     */
	void setOp(String value);

} // MulDivMod
