/**
 */
package ca.mcgill.sel.operationmodel.impl;

import ca.mcgill.sel.operationmodel.OmPackage;
import ca.mcgill.sel.operationmodel.ParameterType;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Parameter Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ParameterTypeImpl extends NamedElementImpl implements ParameterType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ParameterTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OmPackage.Literals.PARAMETER_TYPE;
	}

} //ParameterTypeImpl
