package ca.mcgill.sel.operationmodel.util;

import org.eclipse.emf.ecore.EObject;

import ca.mcgill.sel.core.controller.ModelUtil;
import ca.mcgill.sel.operationmodel.OmFactory;
import ca.mcgill.sel.operationmodel.OperationSchema;


/**
 * Helper class with convenient static methods for working with operation model objects.
 * @author hyacinthali
 *
 */
public class OmModelUtil implements ModelUtil {
	
	/**
     * Creates a new operation schema and associated entities
     * @param name - The name of the model
     * @return The new model
     */
    public static OperationSchema createEnvironmentModel(String name) {
    	
    	OperationSchema es = OmFactory.eINSTANCE.createOperationSchema();       
    	es.setName(name);
    	es.setSystemName(name);
        return es;
    }

	@Override
	public EObject createNewEmptyModel(String name) {
		return createEnvironmentModel(name);
	}

}
