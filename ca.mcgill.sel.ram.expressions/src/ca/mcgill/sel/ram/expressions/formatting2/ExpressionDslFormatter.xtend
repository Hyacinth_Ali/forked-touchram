/*
 * generated by Xtext 2.12.0
 */
package ca.mcgill.sel.ram.expressions.formatting2

import ca.mcgill.sel.ram.And
import ca.mcgill.sel.ram.Conditional
import ca.mcgill.sel.ram.Or
import ca.mcgill.sel.ram.expressions.services.ExpressionDslGrammarAccess
import com.google.inject.Inject
import org.eclipse.xtext.formatting2.IFormattableDocument
import org.eclipse.xtext.formatting2.AbstractFormatter2

class ExpressionDslFormatter extends AbstractFormatter2 {
    
    @Inject extension ExpressionDslGrammarAccess
    
    def dispatch void format(Conditional conditional, extension IFormattableDocument document) {
        // TODO: format HiddenRegions around keywords, attributes, cross references, etc. 
        conditional.expressionT.format
        conditional.expressionF.format
        conditional.condition.format
    }

    def dispatch void format(Or or, extension IFormattableDocument document) {
        // TODO: format HiddenRegions around keywords, attributes, cross references, etc. 
        or.getRight.format;
        or.getLeft.format;
    }

    def dispatch void format(And and, extension IFormattableDocument document) {
        // TODO: format HiddenRegions around keywords, attributes, cross references, etc. 
        and.getRight.format;
        and.getLeft.format;
    }
    
    // TODO: TODO: implement for And, LogicalOperator, Equality, Comparison, Shift, Binary, MulDivMod, Not, UnaryMinus,
    // PreIncrementOrDecrement, PostIncrementOrDecrement, Plus, Minus
    
}
