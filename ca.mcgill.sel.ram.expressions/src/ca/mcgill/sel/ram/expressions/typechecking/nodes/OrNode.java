package ca.mcgill.sel.ram.expressions.typechecking.nodes;

import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.ram.ValueSpecification;
import ca.mcgill.sel.ram.expressions.typechecking.TypeChecker;
import ca.mcgill.sel.ram.expressions.typechecking.TypeCheckingUtil;
import ca.mcgill.sel.ram.expressions.typechecking.TypeChecker.Types;

/**
 * A class that contains the typechecking logic for a node representing "OR" Operator..
 * 
 * @author Rohit Verma
 */
public final class OrNode {

    /**
     * Private constructor to prevent unintended instantiation.
     */
    private OrNode() {
    }

    /**
     * Method that implements the typechecking logic for the node representing "OR" Operator.
     * 
     * @param left : left of OrNode
     * @param right : right of OrNode
     * 
     * @return The type that OrNode should return
     */
    public static Types typeCheck(ValueSpecification left, ValueSpecification right) {

        Types shouldReturnType = null;

        // Each operand of the conditional-or operator must be of type boolean or Boolean.

        Types lType = TypeChecker.resolveType(left);
        Types rType = TypeChecker.resolveType(right);

        if (lType != null && rType != null) {
            if (lType.equals(Types.booleanType) && rType.equals(Types.booleanType)) {
                shouldReturnType = Types.booleanType;
            }
        } else {
            if (TypeCheckingUtil.EQUALITY_OPERANDS.contains(lType)) {
                TypeChecker.addIssue(EMFEditUtil.getText(right)
                        + " is not a valid type for ORing"
                        + EMFEditUtil.getText(left));
            } else {
                TypeChecker.addIssue(EMFEditUtil.getText(left)
                        + " is not a valid type for ORing"
                        + EMFEditUtil.getText(right));
            }
        }

        return shouldReturnType;
    }
}
