package ca.mcgill.sel.usecases.ui.views.handler;

import org.mt4j.util.math.Vector3D;

import ca.mcgill.sel.ram.ui.events.listeners.ITapAndHoldListener;
import ca.mcgill.sel.ram.ui.events.listeners.ITapListener;
import ca.mcgill.sel.ram.ui.views.handler.IAbstractViewHandler;
import ca.mcgill.sel.usecases.ui.views.BaseView;
import ca.mcgill.sel.usecases.ui.views.UseCaseDiagramView;

public interface IUseCaseDiagramViewHandler extends IAbstractViewHandler, ITapListener, ITapAndHoldListener {

    boolean handleTapAndHold(UseCaseDiagramView useCaseDiagramView, BaseView<?> target);

    boolean handleDoubleTap(UseCaseDiagramView structuralDiagramView, BaseView<?> target);
    
    void dragAllSelected(UseCaseDiagramView svd, Vector3D directionVector);

}
