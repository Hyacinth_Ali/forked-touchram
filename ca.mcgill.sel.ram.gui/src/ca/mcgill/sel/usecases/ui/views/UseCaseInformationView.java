package ca.mcgill.sel.usecases.ui.views;

import java.util.List;
import java.util.stream.Collectors;

import ca.mcgill.sel.ram.ui.components.RamRectangleComponent;
import ca.mcgill.sel.ram.ui.components.RamTextComponent;
import ca.mcgill.sel.ram.ui.layouts.HorizontalLayout;
import ca.mcgill.sel.ram.ui.layouts.VerticalLayout;
import ca.mcgill.sel.ram.ui.utils.Colors;
import ca.mcgill.sel.ram.ui.utils.Fonts;
import ca.mcgill.sel.ram.ui.utils.Strings;
import ca.mcgill.sel.ram.ui.views.TextView;
import ca.mcgill.sel.usecases.Actor;
import ca.mcgill.sel.usecases.LayoutElement;
import ca.mcgill.sel.usecases.UcPackage;
import ca.mcgill.sel.usecases.UseCase;
import ca.mcgill.sel.usecases.ui.utils.UcModelUtils;
import ca.mcgill.sel.usecases.ui.utils.UseCaseModelHandlerFactory;
import ca.mcgill.sel.usecases.ui.views.handler.IUseCaseInformationViewHandler;

public class UseCaseInformationView extends BaseView<IUseCaseInformationViewHandler> {

    private static final int LABEL_PADDING = 30;
    
    private RamRectangleComponent summaryContainer;
    private RamTextComponent nameLabel;
    
    private RamRectangleComponent levelContainer;
    private RamTextComponent levelLabel;
    private TextView levelField;
    
    private RamRectangleComponent intentionContainer;
    private RamTextComponent intentionLabel;
    private TextView intentionField;
    
    private RamRectangleComponent multiplicityContainer;
    private RamTextComponent multiplicityLabel;
    private TextView multiplicityField;
    
    private RamRectangleComponent generalizationContainer;
    private RamTextComponent generalizationLabel;
    private TextView generalizationField;
    
    private RamRectangleComponent primaryActorContainer;
    private RamTextComponent primaryActorLabel;
    private RamTextComponent primaryActorField;
    
    private RamRectangleComponent secondaryActorContainer;
    private RamTextComponent secondaryActorLabel;
    private RamTextComponent secondaryActorField;
    
    private FlowView mainSuccessScenarioView;
    
    protected UseCaseInformationView(UseCaseDiagramView useCaseDiagramView, UseCase represented,
            LayoutElement layoutElement) {
        super(useCaseDiagramView, represented, layoutElement);
        
        setNoFill(false);
        setNoStroke(false);
        setFillColor(Colors.DEFAULT_ELEMENT_FILL_COLOR);
        setStrokeColor(Colors.DEFAULT_ELEMENT_STROKE_COLOR);
        
        if (layoutElement != null) {
            setLayoutElement(layoutElement);
        }
        
        build();
    }
    
    private void build() {
        UseCase useCase = (UseCase) represented;
        
        summaryContainer = new RamRectangleComponent(new VerticalLayout());
        summaryContainer.setNoStroke(false);
        summaryContainer.setStrokeColor(Colors.DEFAULT_ELEMENT_STROKE_COLOR);
        
        // Name
        nameContainer = new RamRectangleComponent(new HorizontalLayout());
        nameContainer.setNoFill(true);
        nameContainer.setNoStroke(true);
        
        nameLabel = new RamTextComponent(Fonts.FONT_CLASS_NAME_ITALIC);
        nameLabel.setText(Strings.LABEL_USE_CASE_NAME);
        nameLabel.setBufferSize(Cardinal.EAST, LABEL_PADDING);
        nameContainer.addChild(nameLabel);
        
        nameField = new TextView(represented, UcPackage.Literals.NAMED_ELEMENT__NAME);
        nameField.setPlaceholderText(Strings.PH_ENTER_USE_CASE_NAME);
        nameContainer.addChild(nameField);
        summaryContainer.addChild(nameContainer);
        
        // Level
        levelContainer = new RamRectangleComponent(new HorizontalLayout());
        levelContainer.setNoFill(true);
        levelContainer.setNoStroke(true);
        
        levelLabel = new RamTextComponent(Fonts.FONT_CLASS_NAME_ITALIC);
        levelLabel.setText(Strings.LABEL_USE_CASE_LEVEL);
        levelLabel.setBufferSize(Cardinal.EAST, LABEL_PADDING);
        levelContainer.addChild(levelLabel);
        
        levelField = new TextView(represented, UcPackage.Literals.USE_CASE__LEVEL);
        levelField.setPlaceholderText(Strings.PH_SELECT_LEVEL);
        levelContainer.addChild(levelField);
        summaryContainer.addChild(levelContainer);
        
        // Intention
        intentionContainer = new RamRectangleComponent(new HorizontalLayout());
        intentionContainer.setNoFill(true);
        intentionContainer.setNoStroke(true);
        
        intentionLabel = new RamTextComponent(Fonts.FONT_CLASS_NAME_ITALIC);
        intentionLabel.setText(Strings.LABEL_USE_CASE_INTENTION);
        intentionLabel.setBufferSize(Cardinal.EAST, LABEL_PADDING);
        intentionContainer.addChild(intentionLabel);
        
        intentionField = new TextView(represented, UcPackage.Literals.USE_CASE__INTENTION);
        intentionField.setPlaceholderText(Strings.PH_ENTER_INTENTION);
        intentionField.setMaximumWidth(UcModelUtils.getMaxComponentWidth());
        intentionField.setBufferSize(Cardinal.WEST, 0);
        intentionContainer.addChild(intentionField);
        summaryContainer.addChild(intentionContainer);
        
        // Multiplicity
        multiplicityContainer = new RamRectangleComponent(new HorizontalLayout());
        multiplicityContainer.setNoFill(true);
        multiplicityContainer.setNoStroke(true);
        
        multiplicityLabel = new RamTextComponent(Fonts.FONT_CLASS_NAME_ITALIC);
        multiplicityLabel.setText(Strings.LABEL_USE_CASE_MULTIPLICITY);
        multiplicityLabel.setBufferSize(Cardinal.EAST, LABEL_PADDING);
        multiplicityContainer.addChild(multiplicityLabel);
        
        multiplicityField = new TextView(represented, UcPackage.Literals.USE_CASE__MULTIPLICITY);
        multiplicityField.setLayout(new VerticalLayout());
        multiplicityField.setPlaceholderText(Strings.PH_ENTER_MULTIPLICITY);
        multiplicityField.setMaximumWidth(UcModelUtils.getMaxComponentWidth());
        multiplicityField.setBufferSize(Cardinal.WEST, 0);
        multiplicityContainer.addChild(multiplicityField);
        summaryContainer.addChild(multiplicityContainer);        
        
        // Handlers
        ((TextView) nameField).setHandler(UseCaseModelHandlerFactory.INSTANCE.getTextViewHandler());
        levelField.setHandler(UseCaseModelHandlerFactory.INSTANCE.getTextViewHandler());
        intentionField.setHandler(UseCaseModelHandlerFactory.INSTANCE.getTextViewHandler());
        multiplicityField.setHandler(UseCaseModelHandlerFactory.INSTANCE.getTextViewHandler());
        
        // Generalization
        if (useCase.getGeneralization() != null) {
            generalizationContainer = new RamRectangleComponent(new HorizontalLayout());
            generalizationContainer.setNoFill(true);
            generalizationContainer.setNoStroke(true);
            
            generalizationLabel = new RamTextComponent(Fonts.FONT_CLASS_NAME_ITALIC);
            generalizationLabel.setText(Strings.LABEL_USE_CASE_GENERALIZATION);
            generalizationLabel.setBufferSize(Cardinal.EAST, LABEL_PADDING);
            generalizationContainer.addChild(generalizationLabel);
            
            generalizationField = new TextView(represented, UcPackage.Literals.USE_CASE__GENERALIZATION);
            generalizationField.setPlaceholderText(Strings.PH_SELECT_GENERALIZATION);
            generalizationContainer.addChild(generalizationField);
            summaryContainer.addChild(generalizationContainer);
        }        
        
        // Actors
        if (useCase.getPrimaryActor() != null) {
            primaryActorContainer = new RamRectangleComponent(new HorizontalLayout());
            primaryActorContainer.setNoFill(true);
            primaryActorContainer.setNoStroke(true);
            
            primaryActorLabel = new RamTextComponent(Fonts.FONT_CLASS_NAME_ITALIC);
            primaryActorLabel.setText(Strings.LABEL_USE_CASE_PRIMARY_ACTOR);
            primaryActorLabel.setBufferSize(Cardinal.EAST, LABEL_PADDING);
            primaryActorContainer.addChild(primaryActorLabel);
            
            primaryActorField = new TextView(represented, UcPackage.Literals.USE_CASE__PRIMARY_ACTOR);
            primaryActorContainer.addChild(primaryActorField);
            summaryContainer.addChild(primaryActorContainer);
        }
        
        if (useCase.getSecondaryActors().size() > 0) {
            List<Actor> secondaryActors = useCase.getSecondaryActors();
            List<String> actorNames = secondaryActors.stream()
                    .map(a -> a.getName())
                    .collect(Collectors.toList());
            
            secondaryActorContainer = new RamRectangleComponent(new HorizontalLayout());
            secondaryActorContainer.setNoFill(true);
            secondaryActorContainer.setNoStroke(true);
            
            secondaryActorLabel = new RamTextComponent(Fonts.FONT_CLASS_NAME_ITALIC);
            secondaryActorLabel.setText(Strings.LABEL_USE_CASE_SECONDARY_ACTORS);
            secondaryActorLabel.setBufferSize(Cardinal.EAST, LABEL_PADDING);
            secondaryActorContainer.addChild(secondaryActorLabel);
            
            secondaryActorField = new RamTextComponent();
            String secondaryActorText = String.join(", ", actorNames);
            secondaryActorField.setText(secondaryActorText);
            secondaryActorField.setMaximumWidth(UcModelUtils.getMaxComponentWidth());
            secondaryActorField.setBufferSize(Cardinal.WEST, 0);
            secondaryActorContainer.addChild(secondaryActorField);
            summaryContainer.addChild(secondaryActorContainer);            
        }
        
        addChild(summaryContainer);
        mainSuccessScenarioView = new FlowView(
                useCaseDiagramView, useCase, useCase.getMainSuccessScenario(), true, 0);
        addChild(mainSuccessScenarioView);
        mainSuccessScenarioView.setHandler(UseCaseModelHandlerFactory.INSTANCE.getFlowViewHandler()); 
    }
    
    @Override
    public void destroy() {
        // Destroy children
        nameLabel.destroy();
        
        levelContainer.destroy();
        levelLabel.destroy();
        levelField.destroy();
        
        intentionContainer.destroy();
        intentionLabel.destroy();
        intentionField.destroy();
        
        multiplicityContainer.destroy();
        multiplicityLabel.destroy();
        multiplicityField.destroy();
        
        if (generalizationContainer != null) {
            generalizationContainer.destroy();
            generalizationLabel.destroy();
            generalizationField.destroy();    
        }        
        
        if (primaryActorContainer != null) {
            primaryActorContainer.destroy();
            primaryActorLabel.destroy();
            primaryActorField.destroy();   
        }
        
        if (secondaryActorContainer != null) {
            secondaryActorContainer.destroy();
            secondaryActorLabel.destroy();
            secondaryActorField.destroy();    
        }        
        
        summaryContainer.destroy();
        mainSuccessScenarioView.destroy();
        
        super.destroy();
    }

}
