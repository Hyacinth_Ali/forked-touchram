package ca.mcgill.sel.usecases.ui.views.handler;

import ca.mcgill.sel.ram.ui.events.listeners.ActionListener;

public interface IFlowViewHandler extends IBaseViewHandler, ActionListener {

}
