package ca.mcgill.sel.usecases.ui.views.handler.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.mt4j.components.TransformSpace;
import org.mt4j.input.inputProcessors.componentProcessors.tapAndHoldProcessor.TapAndHoldEvent;
import org.mt4j.util.math.Vector3D;

import ca.mcgill.sel.commons.emf.util.EMFModelUtil;
import ca.mcgill.sel.core.COREPerspective;
import ca.mcgill.sel.core.perspective.PerspectiveName;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.RamRectangleComponent;
import ca.mcgill.sel.ram.ui.components.RamSelectorComponent;
import ca.mcgill.sel.ram.ui.components.listeners.AbstractDefaultRamSelectorListener;
import ca.mcgill.sel.ram.ui.components.menu.RamLinkedMenu;
import ca.mcgill.sel.ram.ui.components.navigationbar.NavigationBar;
import ca.mcgill.sel.ram.ui.perspective.controller.PerspectiveControllerFactory;
import ca.mcgill.sel.ram.ui.utils.Icons;
import ca.mcgill.sel.ram.ui.utils.Strings;
import ca.mcgill.sel.usecases.Actor;
import ca.mcgill.sel.usecases.UcPackage;
import ca.mcgill.sel.usecases.UseCase;
import ca.mcgill.sel.usecases.UseCaseModel;
import ca.mcgill.sel.usecases.language.controller.UseCaseControllerFactory;
import ca.mcgill.sel.usecases.language.controller.UseCaseController;
import ca.mcgill.sel.usecases.ui.views.ActorView;
import ca.mcgill.sel.usecases.ui.views.BaseView;
import ca.mcgill.sel.usecases.ui.views.handler.IActorViewHandler;
import ca.mcgill.sel.usecases.util.UcModelUtil;

public class ActorViewHandler extends BaseViewHandler implements IActorViewHandler {

    private static final String ACTION_ACTOR_DELETE_ASSOCIATION = "view.actor.association.delete";
    
    @Override
    public void removeRepresented(BaseView<?> baseView) {
        
        COREPerspective perspective = NavigationBar.getInstance().getCurrentPerspective();
        String currentRoleName = NavigationBar.getInstance().getCurrentLanguageRole();
        switch (perspective.getName()) {
                
        case PerspectiveName.DOMAIN_USECASE_MODEL_PERSPECTIVE:
            PerspectiveControllerFactory.INSTANCE.getDomainUseCaseController().deleteActor(perspective, 
                    currentRoleName, (Actor) baseView.getRepresented());
            break;
            
        case PerspectiveName.DOMAIN_DESIGN_USECASE_PERSPECTIVE:
                PerspectiveControllerFactory.INSTANCE.getDomainDesignUseCaseController().deleteActor(perspective, 
                        currentRoleName, (Actor) baseView.getRepresented());
                break;
                
            default:
                UseCaseControllerFactory.INSTANCE.getUseCaseDiagramController().removeActor(
                        (Actor) baseView.getRepresented());
        }
                        
    }
    
    @Override
    public void initMenu(RamLinkedMenu menu, TapAndHoldEvent event) {
        super.initMenu(menu, event);
        
        ActorView actorView = (ActorView) menu.getLinkedView();
        Actor actor = actorView.getActor();
        UseCaseModel ucm = EMFModelUtil.getRootContainerOfType(actor, UcPackage.Literals.USE_CASE_MODEL);
        List<UseCase> availableUseCases = ucm.getUseCases()
                .stream()
                .filter(useCase -> useCase.getPrimaryActor() == actor || useCase.getSecondaryActors().contains(actor))
                .collect(Collectors.toList());        

        if (availableUseCases.size() > 0) {
            menu.addAction(Strings.MENU_DELETE_ASSOCIATION,  Icons.ICON_MENU_CLEAR_SELECTION, 
                    ACTION_ACTOR_DELETE_ASSOCIATION,
                    this, true);
        }
    }
    
    @Override
    public void actionPerformed(ActionEvent event) {
        String actionCommand = event.getActionCommand();
        RamRectangleComponent pressedButton = (RamRectangleComponent) event.getTarget();
        RamLinkedMenu linkedMenu = (RamLinkedMenu) pressedButton.getParentOfType(RamLinkedMenu.class);
        ActorView actorView = (ActorView) linkedMenu.getLinkedView();
        Actor actor = actorView.getActor();
        
        if (ACTION_ACTOR_DELETE_ASSOCIATION.equals(actionCommand)) {
            Vector3D position = linkedMenu.getPosition(TransformSpace.GLOBAL); 
            linkedMenu.destroy();
            deleteUseCaseAssociation(actor, position);
        } else {
            super.actionPerformed(event);
        }
    }

    @SuppressWarnings("static-method")
    private void deleteUseCaseAssociation(Actor actor, Vector3D menuPosition) {
        List<UseCase> availableUseCases = UcModelUtil.getUseCasesAssociatedToActor(actor);   
        
        RamSelectorComponent<UseCase> selector = 
                new RamSelectorComponent<UseCase>(availableUseCases);
        
        RamApp.getActiveScene().addComponent(selector, menuPosition);
        
        selector.registerListener(new AbstractDefaultRamSelectorListener<UseCase>() {

            @Override
            public void elementSelected(RamSelectorComponent<UseCase> selector, UseCase element) {
                UseCaseController controller = UseCaseControllerFactory.INSTANCE.getUseCaseController();
                if (actor == element.getPrimaryActor()) {
                    controller.clearPrimaryActor(element);
                } else {
                    controller.removeSecondaryActor(actor, element);
                }
                
                selector.destroy();
            }
        });        
    }
}
