package ca.mcgill.sel.usecases.ui.views.handler.impl;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.util.EContentAdapter;
import org.mt4j.components.TransformSpace;
import org.mt4j.input.inputProcessors.componentProcessors.tapAndHoldProcessor.TapAndHoldEvent;
import org.mt4j.util.math.Vector3D;

import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.RamRectangleComponent;
import ca.mcgill.sel.ram.ui.components.RamSelectorComponent;
import ca.mcgill.sel.ram.ui.components.listeners.AbstractDefaultRamSelectorListener;
import ca.mcgill.sel.ram.ui.components.menu.RamLinkedMenu;
import ca.mcgill.sel.ram.ui.utils.Icons;
import ca.mcgill.sel.ram.ui.utils.Strings;
import ca.mcgill.sel.usecases.Actor;
import ca.mcgill.sel.usecases.UcPackage;
import ca.mcgill.sel.usecases.UseCase;
import ca.mcgill.sel.usecases.language.controller.UseCaseControllerFactory;
import ca.mcgill.sel.usecases.language.controller.UseCaseController;
import ca.mcgill.sel.usecases.ui.scenes.DisplayUseCaseModelScene;
import ca.mcgill.sel.usecases.ui.views.BaseView;
import ca.mcgill.sel.usecases.ui.views.UseCaseView;
import ca.mcgill.sel.usecases.ui.views.handler.IUseCaseViewHandler;

public class UseCaseViewHandler extends BaseViewHandler implements IUseCaseViewHandler {

    private static final String ACTION_USE_CASE_GOTO_DETAILVIEW = "view.usecase.detail";
    private static final String ACTION_USE_CASE_DELETE_ASSOCIATION = "view.usecase.association.delete";
    
    @Override
    public void removeRepresented(BaseView<?> baseView) {
        UseCase useCase = (UseCase) baseView.getRepresented();
        UseCaseControllerFactory.INSTANCE.getUseCaseDiagramController().removeUseCase(useCase);
    }
    
    @Override
    public void initMenu(RamLinkedMenu menu, TapAndHoldEvent event) {
        super.initMenu(menu, event);
        menu.addAction(Strings.MENU_GOTO_USECASEDETAIL, Icons.ICON_MENU_USE_CASE_DETAIL,
                ACTION_USE_CASE_GOTO_DETAILVIEW,
                this, true);
        menu.addAction(Strings.MENU_DELETE_ASSOCIATION,  Icons.ICON_MENU_CLEAR_SELECTION, 
                ACTION_USE_CASE_DELETE_ASSOCIATION,
                this, true);
    }
    
    @Override
    public void actionPerformed(ActionEvent event) {
        String actionCommand = event.getActionCommand();
        RamRectangleComponent pressedButton = (RamRectangleComponent) event.getTarget();
        RamLinkedMenu linkedMenu = (RamLinkedMenu) pressedButton.getParentOfType(RamLinkedMenu.class);
        UseCaseView useCaseView = (UseCaseView) linkedMenu.getLinkedView();
        UseCase useCase = useCaseView.getUseCase();
        
        if (ACTION_USE_CASE_GOTO_DETAILVIEW.equals(actionCommand)) {
            goToDetailView(useCase);
        } else if (ACTION_USE_CASE_DELETE_ASSOCIATION.equals(actionCommand)) {
            Vector3D position = linkedMenu.getPosition(TransformSpace.GLOBAL); 
            linkedMenu.destroy();
            deleteActorAssociation(useCase, position);
        } else {
            super.actionPerformed(event);
        }
    }

    @SuppressWarnings("static-method")
    private void goToDetailView(UseCase useCase) {
        final DisplayUseCaseModelScene scene = (DisplayUseCaseModelScene) RamApp.getActiveScene();
        
        if (useCase.getMainSuccessScenario() == null) {
            // Some older models may not have been initialized
            useCase.eAdapters().add(new EContentAdapter() {
                @Override
                public void notifyChanged(Notification notification) {
                    if (notification.getFeature() 
                            == UcPackage.Literals.USE_CASE__MAIN_SUCCESS_SCENARIO) {                         
                        scene.showUseCaseDetail(useCase);
                        useCase.eAdapters().remove(this);
                    }
                }
            });

            UseCaseControllerFactory.INSTANCE.getUseCaseController().createFlows(useCase);
        } else {
            scene.showUseCaseDetail(useCase);    
        }        
    }
    
    @SuppressWarnings("static-method")
    private void deleteActorAssociation(UseCase useCase, Vector3D menuPosition) {
        List<Actor> availableActors = new ArrayList<Actor>();
        if (useCase.getPrimaryActor() != null) {
            availableActors.add(useCase.getPrimaryActor());
        }
        
        availableActors.addAll(useCase.getSecondaryActors());
        
        RamSelectorComponent<Actor> selector = 
                new RamSelectorComponent<Actor>(availableActors);
        
        RamApp.getActiveScene().addComponent(selector, menuPosition);
        
        selector.registerListener(new AbstractDefaultRamSelectorListener<Actor>() {

            @Override
            public void elementSelected(RamSelectorComponent<Actor> selector, Actor element) {
                UseCaseController controller = UseCaseControllerFactory.INSTANCE.getUseCaseController();
                if (element == useCase.getPrimaryActor()) {
                    controller.clearPrimaryActor(useCase);
                } else {
                    controller.removeSecondaryActor(element, useCase);
                }
                
                selector.destroy();
            }
        });        
    }
}
