package ca.mcgill.sel.usecases.ui.views;

import ca.mcgill.sel.ram.ui.utils.Fonts;
import ca.mcgill.sel.ram.ui.utils.Strings;
import ca.mcgill.sel.usecases.Step;
import ca.mcgill.sel.usecases.UcPackage;
import ca.mcgill.sel.usecases.ui.utils.UcModelUtils;
import ca.mcgill.sel.usecases.ui.utils.UseCaseModelHandlerFactory;

public class ContextStepView extends StepView {
    
    private ActorReferenceTextView textField;
    
    protected ContextStepView(UseCaseDiagramView useCaseDiagramView, Step represented) {
        super(useCaseDiagramView, represented);
    }

    @Override
    protected void buildStepView() {        
        textField = new ActorReferenceTextView(represented, UcPackage.Literals.STEP__STEP_TEXT);        
        textField.setPlaceholderText(Strings.PH_ENTER_STEP_DESCRIPTION);
        textField.setFont(Fonts.DEFAULT_FONT_ITALIC);       
        textField.setMaximumWidth(UcModelUtils.getMaxComponentWidth());
        textField.setBufferSize(Cardinal.WEST, 0);
        addChild(textField);
        textField.setHandler(UseCaseModelHandlerFactory.INSTANCE.getActorReferenceTextViewHandler());
    }

}
