package ca.mcgill.sel.usecases.ui.views.handler.impl;

import java.util.ArrayList;
import java.util.List;

import org.mt4j.input.inputProcessors.componentProcessors.tapAndHoldProcessor.TapAndHoldEvent;
import org.mt4j.input.inputProcessors.componentProcessors.tapProcessor.TapEvent;

import ca.mcgill.sel.commons.emf.util.EMFModelUtil;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.RamSelectorComponent;
import ca.mcgill.sel.ram.ui.components.listeners.AbstractDefaultRamSelectorListener;
import ca.mcgill.sel.ram.ui.events.listeners.ITapAndHoldListener;
import ca.mcgill.sel.ram.ui.views.TextView;
import ca.mcgill.sel.ram.ui.views.handler.impl.TextViewHandler;
import ca.mcgill.sel.usecases.UcPackage;
import ca.mcgill.sel.usecases.UseCase;
import ca.mcgill.sel.usecases.UseCaseModel;
import ca.mcgill.sel.usecases.UseCaseReferenceStep;
import ca.mcgill.sel.usecases.language.controller.UseCaseControllerFactory;
import ca.mcgill.sel.usecases.language.controller.StepController;
import ca.mcgill.sel.usecases.ui.scenes.DisplayUseCaseModelScene;
import ca.mcgill.sel.usecases.ui.views.StepView;

public class UseCaseSelectorHandler extends TextViewHandler 
    implements ITapAndHoldListener {
    @Override
    public boolean processTapEvent(TapEvent tapEvent) {
        if (tapEvent.isDoubleTap()) {
            final TextView target = (TextView) tapEvent.getTarget();
            StepView stepView = target.getParentOfType(StepView.class);
            UseCaseReferenceStep step = (UseCaseReferenceStep) stepView.getStep();
            UseCase useCase = EMFModelUtil.getRootContainerOfType(step, UcPackage.Literals.USE_CASE);
            UseCaseModel ucm = EMFModelUtil.getRootContainerOfType(step, UcPackage.Literals.USE_CASE_MODEL);

            List<UseCase> availableUseCases = new ArrayList<UseCase>(ucm.getUseCases());
            availableUseCases.remove(useCase);
            
            RamSelectorComponent<UseCase> selector = new RamSelectorComponent<UseCase>(availableUseCases);

            RamApp.getActiveScene().addComponent(selector, tapEvent.getLocationOnScreen());

            selector.registerListener(new AbstractDefaultRamSelectorListener<UseCase>() {
                @Override
                public void elementSelected(RamSelectorComponent<UseCase> selector, UseCase element) {
                    List<UseCase> includedUseCases = useCase.getIncludedUseCases();
                    if (includedUseCases != null && includedUseCases.contains(element)) {
                        StepController controller = UseCaseControllerFactory.INSTANCE.getStepController();
                        controller.setUseCase(step, element);
                    }
                    
                    setValue(target.getData(), target.getFeature(), element);
                    selector.destroy();
                }
            });            

            return true;
        }

        return false;
    }
    
    @Override
    public boolean processTapAndHoldEvent(TapAndHoldEvent tapAndHoldEvent) {
        if (tapAndHoldEvent.isHoldComplete()) {
            final TextView target = (TextView) tapAndHoldEvent.getTarget();
            final DisplayUseCaseModelScene scene = (DisplayUseCaseModelScene) RamApp.getActiveScene();

            StepView stepView = target.getParentOfType(StepView.class);
            UseCaseReferenceStep step = (UseCaseReferenceStep) stepView.getStep();
            UseCase useCase = step.getUsecase();
            scene.showUseCaseDetail(useCase);
            return true;    
        }
        
        return false;
    }
}
