package ca.mcgill.sel.usecases.ui.views.handler.impl;

import org.mt4j.input.inputProcessors.componentProcessors.dragProcessor.DragEvent;
import org.mt4j.input.inputProcessors.componentProcessors.tapAndHoldProcessor.TapAndHoldEvent;
import org.mt4j.input.inputProcessors.componentProcessors.tapProcessor.TapEvent;

import ca.mcgill.sel.usecases.ui.views.BaseView;
import ca.mcgill.sel.usecases.ui.views.UseCaseInformationView;
import ca.mcgill.sel.usecases.ui.views.UseCaseDetailView;
import ca.mcgill.sel.usecases.ui.views.handler.IUseCaseInformationViewHandler;

public class UseCaseInformationViewHandler extends BaseViewHandler 
    implements IUseCaseInformationViewHandler {

    @Override
    public void removeRepresented(BaseView<?> baseView) {
        // TODO Auto-generated method stub        
    }

    @Override
    public boolean processDragEvent(DragEvent dragEvent) {
        final UseCaseInformationView target = (UseCaseInformationView) dragEvent.getTarget();
        final UseCaseDetailView detailView = target.getParentOfType(UseCaseDetailView.class);
        dragEvent.setTarget(detailView);
        return detailView.getHandler().processDragEvent(dragEvent);
    }

    @Override
    public boolean processTapAndHoldEvent(TapAndHoldEvent tapAndHoldEvent) {
        final UseCaseInformationView target = (UseCaseInformationView) tapAndHoldEvent.getTarget();
        final UseCaseDetailView detailView = target.getParentOfType(UseCaseDetailView.class);
        tapAndHoldEvent.setTarget(detailView);
        return detailView.getHandler().processTapAndHoldEvent(tapAndHoldEvent);
    }

    @Override
    public boolean processTapEvent(TapEvent tapEvent) {
        final UseCaseInformationView target = (UseCaseInformationView) tapEvent.getTarget();
        final UseCaseDetailView detailView = target.getParentOfType(UseCaseDetailView.class);
        tapEvent.setTarget(detailView);
        return detailView.getHandler().processTapEvent(tapEvent);
    }
}
