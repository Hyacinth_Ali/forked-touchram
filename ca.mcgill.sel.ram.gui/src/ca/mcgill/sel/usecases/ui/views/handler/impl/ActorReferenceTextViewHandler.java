package ca.mcgill.sel.usecases.ui.views.handler.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EObject;

import ca.mcgill.sel.commons.emf.util.EMFModelUtil;
import ca.mcgill.sel.ram.ui.views.TextView;
import ca.mcgill.sel.ram.ui.views.handler.impl.TextViewHandler;
import ca.mcgill.sel.usecases.CommunicationStep;
import ca.mcgill.sel.usecases.UcPackage;
import ca.mcgill.sel.usecases.UseCase;
import ca.mcgill.sel.usecases.util.UseCaseTextUtils;

public class ActorReferenceTextViewHandler extends TextViewHandler {
    
    //private static final String THE_SYSTEM = "the system";
    
    @Override
    public void keyboardOpened(TextView textView) {
        updateMessageText(textView);
    }    
    
    @Override
    public void keyboardCancelled(TextView textView) {
        updateMessageText(textView);
    }
    
    @Override
    public boolean shouldDismissKeyboard(TextView textView) {
        // Each communication step must contain:
        // - An actor's name
        // - The words "the system"
        EObject data = textView.getData();
        // Text editing is only available for attributes.
        EAttribute feature = (EAttribute) textView.getFeature();
        String text = textView.getText();
        
        //if (!text.contains(THE_SYSTEM)) {
            // return false;
        //}        
        
        UseCase useCase = EMFModelUtil.getRootContainerOfType(data, UcPackage.Literals.USE_CASE);                
        String textToSave = UseCaseTextUtils.parseActorNames(text, useCase);
        
        if (textToSave.equals(text) && data instanceof CommunicationStep) {
            // No actors were replaced, so we shouldn't save.
            return false;
        }
        
        setValue(data, feature, textToSave);
        
        return true;
    }
    
    @SuppressWarnings("static-method")
    private void updateMessageText(TextView textView) {
        EObject referenceObject = textView.getData();
        String text = textView.getText();
        String displayText = UseCaseTextUtils.replaceActorIds(text, referenceObject);
        textView.setText(displayText);
    }
}
