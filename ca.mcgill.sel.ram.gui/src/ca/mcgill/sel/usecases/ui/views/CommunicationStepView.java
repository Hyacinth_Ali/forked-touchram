package ca.mcgill.sel.usecases.ui.views;

import org.eclipse.emf.common.notify.Notification;

import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.commons.emf.util.EMFModelUtil;
import ca.mcgill.sel.ram.ui.utils.Strings;
import ca.mcgill.sel.ram.ui.views.TextView;
import ca.mcgill.sel.usecases.CommunicationStep;
import ca.mcgill.sel.usecases.Step;
import ca.mcgill.sel.usecases.UcPackage;
import ca.mcgill.sel.usecases.ui.utils.UcModelUtils;
import ca.mcgill.sel.usecases.ui.utils.UseCaseModelHandlerFactory;
import ca.mcgill.sel.usecases.util.UseCaseTextUtils;

public class CommunicationStepView extends StepView {   
    private ActorReferenceTextView messageField;
    private TextView directionField;
    
    protected CommunicationStepView(UseCaseDiagramView useCaseDiagramView, Step represented) {
        super(useCaseDiagramView, represented);
        
        CommunicationStep communicationStep = (CommunicationStep) represented;
        if (communicationStep.getActor() != null) {
            EMFEditUtil.addListenerFor(communicationStep.getActor(), this);    
        }        
    }
    
    @Override
    public void notifyChanged(Notification notification) {
        super.notifyChanged(notification);
        
        CommunicationStep communicationStep = (CommunicationStep) represented;
        if (notification.getNotifier() == represented || (notification.getNotifier() == communicationStep.getActor()
                && notification.getFeature() == UcPackage.Literals.NAMED_ELEMENT__NAME)) {
            updateMessageText();
        }
    }

    @Override
    protected void buildStepView() {        
        messageField = new ActorReferenceTextView(represented, UcPackage.Literals.STEP__STEP_TEXT);
        
        messageField.setPlaceholderText(Strings.PH_ENTER_MESSAGE);
        messageField.setMaximumWidth(UcModelUtils.getMaxComponentWidth());
        messageField.setBufferSize(Cardinal.WEST, 0);
        updateMessageText();
        
        addChild(messageField);
       
        messageField.setHandler(UseCaseModelHandlerFactory.INSTANCE.getActorReferenceTextViewHandler());
        
        directionField = new TextView(represented, UcPackage.Literals.COMMUNICATION_STEP__DIRECTION);                
        addChild(directionField);
        directionField.setHandler(UseCaseModelHandlerFactory.INSTANCE.getTextViewHandler());
    }
    
    @Override
    public void destroy() {
        messageField.destroy();
        directionField.destroy();
        super.destroy();
    }

    private void updateMessageText() {
        Step step = (Step) represented;
        if (!"".equals(step.getStepText()) && step.getActor() != null) {
            String text = UseCaseTextUtils.replaceActorIds(step.getStepText(), step);
            // BACKWARD COMPATIBILITY: set the actor to null now
            // TODO: eventually remove the actor reference (and this code)
            String tokenizedText = step.getStepText().replace(
                    UseCaseTextUtils.ACTOR_TOKEN, EMFModelUtil.getObjectEId(step.getActor()));
            
            text = text.replace(
                    UseCaseTextUtils.ACTOR_TOKEN, step.getActor().getName());
            
            EMFEditUtil.getPropertyDescriptor(represented, UcPackage.Literals.STEP__ACTOR)
                .setPropertyValue(represented, null);
            
            EMFEditUtil.getPropertyDescriptor(represented, UcPackage.Literals.STEP__STEP_TEXT)
                .setPropertyValue(represented, tokenizedText);            
            
            messageField.setText(text);
        }
    }
}
