package ca.mcgill.sel.usecases.ui.views.handler.impl;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.mt4j.input.inputProcessors.componentProcessors.tapProcessor.TapEvent;
import org.mt4j.util.math.Vector3D;

import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.RamRectangleComponent;
import ca.mcgill.sel.ram.ui.components.RamSelectorComponent;
import ca.mcgill.sel.ram.ui.components.RamTextComponent;
import ca.mcgill.sel.ram.ui.components.RamListComponent.Namer;
import ca.mcgill.sel.ram.ui.components.listeners.AbstractDefaultRamSelectorListener;
import ca.mcgill.sel.ram.ui.utils.Colors;
import ca.mcgill.sel.ram.ui.views.TextView;
import ca.mcgill.sel.ram.ui.views.handler.impl.TextViewHandler;
import ca.mcgill.sel.usecases.ConclusionType;
import ca.mcgill.sel.usecases.Flow;
import ca.mcgill.sel.usecases.Step;
import ca.mcgill.sel.usecases.language.controller.UseCaseControllerFactory;
import ca.mcgill.sel.usecases.language.controller.FlowController;
import ca.mcgill.sel.usecases.ui.views.StepNamer;
import ca.mcgill.sel.usecases.util.UcModelUtil;
import ca.mcgill.sel.usecases.util.UseCaseTextUtils;

public class FlowConclusionTextViewHandler extends TextViewHandler {
    private class ConclusionNamer implements Namer<ConclusionType> {        
        @Override
        public RamRectangleComponent getDisplayComponent(ConclusionType element) {
            RamTextComponent view = null;
            view = new RamTextComponent(UseCaseTextUtils.getConclusionTypeText(element));            

            view.setNoFill(false);
            view.setFillColor(Colors.DEFAULT_ELEMENT_FILL_COLOR);
            view.setNoStroke(false);
            return view;
        }

        @Override
        public String getSortingName(ConclusionType element) {
            return Integer.toString(element.getValue());
        }

        @Override
        public String getSearchingName(ConclusionType element) {
            return element.toString();
        }
    }

    @Override
    public boolean processTapEvent(TapEvent tapEvent) {
        if (tapEvent.isDoubleTap()) {
            final TextView target = (TextView) tapEvent.getTarget();           
            Flow flow = (Flow) target.getData();
            
            RamSelectorComponent<ConclusionType> selector = 
                    new RamSelectorComponent<ConclusionType>(ConclusionType.VALUES, new ConclusionNamer());

            RamApp.getActiveScene().addComponent(selector, tapEvent.getLocationOnScreen());

            selector.registerListener(new AbstractDefaultRamSelectorListener<ConclusionType>() {
                @Override
                public void elementSelected(RamSelectorComponent<ConclusionType> selector, ConclusionType element) {
                    FlowController controller = UseCaseControllerFactory.INSTANCE.getFlowController();
                    switch (element) {
                        case FAILURE:
                        case SUCCESS:
                            controller.setConclusionType(flow, element);
                            setConclusionText(target);
                            selector.destroy();
                            break;
                        case STEP:
                            selectConclusionStep(flow, target, selector.getCenterPointGlobal());
                            selector.destroy();
                            break;
                    }                    
                }                
            });            
            
            return true;
        }

        return false;
    }
    
    @SuppressWarnings("static-method")
    private void setConclusionText(TextView textView) {
        Flow flow = (Flow) textView.getData();
        textView.setText(UseCaseTextUtils.getConclusionTypeText(flow));
    }
    
    private void selectConclusionStep(Flow flow, TextView target, Vector3D selectorPosition) {   
        EObject parentFlow = (Flow) flow.eContainer();
        List<Step> availableSteps = new ArrayList<Step>();
        
        while (parentFlow instanceof Flow) {
            availableSteps.addAll(0, UcModelUtil.getExtendableSteps((Flow) parentFlow));
            parentFlow = parentFlow.eContainer();
        }        
        
        RamSelectorComponent<Step> selector = 
                new RamSelectorComponent<Step>(availableSteps, new StepNamer());
        
        selector.setMaximumWidth(200);
        
        RamApp.getActiveScene().addComponent(selector, selectorPosition);
        
        // CHECKSTYLE:IGNORE AnonInnerLength: Okay here.
        selector.registerListener(new AbstractDefaultRamSelectorListener<Step>() {

            @Override
            public void elementSelected(RamSelectorComponent<Step> selector, Step element) {
                FlowController controller = UseCaseControllerFactory.INSTANCE.getFlowController();
                controller.setConclusionStep(flow, element);
                setConclusionText(target);
                selector.destroy();
            }
        });
    }
}
