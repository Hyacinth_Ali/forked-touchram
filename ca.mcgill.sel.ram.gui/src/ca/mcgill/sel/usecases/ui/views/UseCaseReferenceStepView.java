package ca.mcgill.sel.usecases.ui.views;

import ca.mcgill.sel.ram.ui.components.RamTextComponent;
import ca.mcgill.sel.ram.ui.utils.Fonts;
import ca.mcgill.sel.ram.ui.utils.Strings;
import ca.mcgill.sel.ram.ui.views.TextView;
import ca.mcgill.sel.usecases.Step;
import ca.mcgill.sel.usecases.UcPackage;
import ca.mcgill.sel.usecases.ui.utils.UcModelUtils;
import ca.mcgill.sel.usecases.ui.utils.UseCaseModelHandlerFactory;

public class UseCaseReferenceStepView extends StepView {
    private static final String REF_LABEL = "(ref. ";
    
    private ActorReferenceTextView stepDescriptionField;
    private RamTextComponent label;
    private RamTextComponent endLabel;
    private TextView useCaseField;
    
    protected UseCaseReferenceStepView(UseCaseDiagramView useCaseDiagramView, Step represented) {
        super(useCaseDiagramView, represented);
    }

    @Override
    protected void buildStepView() {
        stepDescriptionField = new ActorReferenceTextView(represented, UcPackage.Literals.STEP__STEP_TEXT);
        stepDescriptionField.setPlaceholderText(Strings.PH_ENTER_STEP_DESCRIPTION);
        stepDescriptionField.setMaximumWidth(UcModelUtils.getMaxComponentWidth());
        stepDescriptionField.setBufferSize(Cardinal.WEST, 0);
        addChild(stepDescriptionField);
        
        stepDescriptionField.setHandler(UseCaseModelHandlerFactory.INSTANCE.getActorReferenceTextViewHandler());
        
        label = new RamTextComponent();
        label.setText(REF_LABEL);
        label.setBuffers(0);
        addChild(label);
        
        useCaseField = new TextView(represented, UcPackage.Literals.USE_CASE_REFERENCE_STEP__USECASE);
        useCaseField.setFont(Fonts.DEFAULT_FONT_ITALIC);
        useCaseField.setUnderlined(true);
        addChild(useCaseField);
        useCaseField.setHandler(UseCaseModelHandlerFactory.INSTANCE.getUseCaseSelectorHandler());
        
        endLabel = new RamTextComponent();
        endLabel.setText(")");
        endLabel.setBuffers(0);
        addChild(endLabel);
    }
}
