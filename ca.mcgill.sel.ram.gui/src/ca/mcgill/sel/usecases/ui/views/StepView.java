package ca.mcgill.sel.usecases.ui.views;

import org.eclipse.emf.common.notify.Notification;

import ca.mcgill.sel.commons.emf.util.EMFModelUtil;
import ca.mcgill.sel.ram.ui.components.RamButton;
import ca.mcgill.sel.ram.ui.components.RamTextComponent;
import ca.mcgill.sel.ram.ui.layouts.HorizontalLayout;
import ca.mcgill.sel.usecases.Flow;
import ca.mcgill.sel.usecases.Step;
import ca.mcgill.sel.usecases.UcPackage;
import ca.mcgill.sel.usecases.UseCase;
import ca.mcgill.sel.usecases.ui.views.handler.IStepViewHandler;
import ca.mcgill.sel.usecases.util.UcModelUtil;
import ca.mcgill.sel.usecases.util.UseCaseAssociationType;
import ca.mcgill.sel.usecases.util.UseCaseTextUtils;

public abstract class StepView extends BaseView<IStepViewHandler> {
    /**
     * The event to delete a step from a flow.
     */
    public static final String ACTION_STEP_DELETE = "usecase.flow.step.delete";
    
    /**
     * The label for the numbered section of the step.
     */
    protected RamTextComponent indexLabel;
    
    /**
     * The step number within the flow.
     */
    protected String stepNumber;
    
    /**
     * The button used to delete a step.
     */
    protected RamButton deleteStepButton;
    
    protected StepView(UseCaseDiagramView useCaseDiagramView, Step represented) {
        super(useCaseDiagramView, represented, null);
        
        this.stepNumber = UseCaseTextUtils.getStepNumber(represented);
        
        setLayout(new HorizontalLayout());
        
        buildStepIndex();
        buildStepView();
    }
    
    protected abstract void buildStepView();
    
    public Step getStep() {
        return (Step) represented;
    }
    
    public Flow getFlow() {
        return (Flow) getStep().eContainer();
    }
    
    public void setStepNumber(String stepNumber) {
        this.stepNumber = stepNumber;        
        if (this.stepNumber != null && !"".equals(this.stepNumber)) {
            indexLabel.setText(this.stepNumber);
        }
    }
    
    private void buildStepIndex() {
        indexLabel = new RamTextComponent();
        indexLabel.setEnabled(false);
        
        if (stepNumber != null && !"".equals(stepNumber)) {
            indexLabel.setText(stepNumber);
            indexLabel.setBufferSize(Cardinal.WEST, 10);
        }        
        
        addChild(indexLabel);
    }
    
    @Override
    public void notifyChanged(Notification notification) {
        if (notification.getNotifier() == this.getRepresented()) {
            if (notification.getFeature() == UcPackage.Literals.USE_CASE_REFERENCE_STEP__USECASE) {
                UseCase oldValue = (UseCase) notification.getOldValue();
                UseCase newValue = (UseCase) notification.getNewValue();
                UseCase currentUseCase = EMFModelUtil.getRootContainerOfType(
                        getFlow(), UcPackage.Literals.USE_CASE);                
                UseCaseAssociationType type = UseCaseAssociationType.INCLUDE;
                
                if (oldValue != null && !UcModelUtil.useCaseLinkExists(currentUseCase, oldValue, type)) {
                    useCaseDiagramView.removeUseCaseAssociationView(currentUseCase, oldValue, type);
                }
                if (newValue != null) {                
                    useCaseDiagramView.addUseCaseAssociationView(currentUseCase, newValue, type);
                }
            }
        }
        
    }
}
