package ca.mcgill.sel.usecases.ui.views;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;

import ca.mcgill.sel.ram.ui.views.TextView;
import ca.mcgill.sel.usecases.util.UseCaseTextUtils;

public class ActorReferenceTextView extends TextView {

    public ActorReferenceTextView(EObject data, EStructuralFeature feature) {
        super(data, feature);
        // Set the initial text with the replacements
        String textToReplace = (String) data.eGet(feature);
        if (textToReplace != null) {
            String displayText = UseCaseTextUtils.replaceActorIds(textToReplace, data);
            this.setText(displayText);    
        }
        
    }
    
    @Override
    public void handleNotification(Notification notification) {
        // Do nothing; other places will handle this.
    }
}
