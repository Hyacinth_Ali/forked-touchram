package ca.mcgill.sel.usecases.ui.utils;

import ca.mcgill.sel.ram.ui.views.handler.IDisplaySceneHandler;
import ca.mcgill.sel.ram.ui.views.handler.IRelationshipViewHandler;
import ca.mcgill.sel.ram.ui.views.handler.ITextViewHandler;
import ca.mcgill.sel.ram.ui.views.handler.impl.TextViewHandler;
import ca.mcgill.sel.usecases.ui.scenes.handler.impl.DisplayUseCaseModelSceneHandler;
import ca.mcgill.sel.usecases.ui.views.handler.IActorViewHandler;
import ca.mcgill.sel.usecases.ui.views.handler.INoteViewHandler;
import ca.mcgill.sel.usecases.ui.views.handler.IStepViewHandler;
import ca.mcgill.sel.usecases.ui.views.handler.ISystemBoundaryViewHandler;
import ca.mcgill.sel.usecases.ui.views.handler.IUseCaseDetailViewHandler;
import ca.mcgill.sel.usecases.ui.views.handler.IUseCaseDiagramViewHandler;
import ca.mcgill.sel.usecases.ui.views.handler.IFlowViewHandler;
import ca.mcgill.sel.usecases.ui.views.handler.IUseCaseInformationViewHandler;
import ca.mcgill.sel.usecases.ui.views.handler.IUseCaseViewHandler;
import ca.mcgill.sel.usecases.ui.views.handler.impl.ActorMultiplicityHandler;
import ca.mcgill.sel.usecases.ui.views.handler.impl.ActorReferenceTextViewHandler;
import ca.mcgill.sel.usecases.ui.views.handler.impl.FlowConclusionTextViewHandler;
import ca.mcgill.sel.usecases.ui.views.handler.impl.ActorViewHandler;
import ca.mcgill.sel.usecases.ui.views.handler.impl.ActorUseCaseAssociationViewHandler;
import ca.mcgill.sel.usecases.ui.views.handler.impl.NoteViewHandler;
import ca.mcgill.sel.usecases.ui.views.handler.impl.StepViewHandler;
import ca.mcgill.sel.usecases.ui.views.handler.impl.SystemBoundaryViewHandler;
import ca.mcgill.sel.usecases.ui.views.handler.impl.UseCaseAssociationViewHandler;
import ca.mcgill.sel.usecases.ui.views.handler.impl.UseCaseDetailViewHandler;
import ca.mcgill.sel.usecases.ui.views.handler.impl.UseCaseDiagramViewHandler;
import ca.mcgill.sel.usecases.ui.views.handler.impl.UseCaseSelectorHandler;
import ca.mcgill.sel.usecases.ui.views.handler.impl.FlowViewHandler;
import ca.mcgill.sel.usecases.ui.views.handler.impl.InheritanceAssociationViewHandler;
import ca.mcgill.sel.usecases.ui.views.handler.impl.UseCaseInformationViewHandler;
import ca.mcgill.sel.usecases.ui.views.handler.impl.UseCaseViewHandler;

public final class UseCaseModelHandlerFactory {
    /**
     * The singleton instance of the factory.
     */
    public static final UseCaseModelHandlerFactory INSTANCE = new UseCaseModelHandlerFactory();
    
    private IDisplaySceneHandler useCaseDiagramDisplaySceneHandler;
    private IUseCaseDiagramViewHandler useCaseDiagramViewHandler;
    private ISystemBoundaryViewHandler systemBoundaryViewHandler;
    private IUseCaseViewHandler useCaseViewHandler;
    private IActorViewHandler actorViewHandler;
    private INoteViewHandler noteViewHandler;
    private IRelationshipViewHandler associationViewHandler;
    private IRelationshipViewHandler useCaseAssociationViewHandler;
    private ITextViewHandler noteContentHandler;
    private ITextViewHandler textViewHandler;
    private ITextViewHandler actorMultiplicityViewHandler;
    private IUseCaseDetailViewHandler useCaseDetailViewHandler;
    private IUseCaseInformationViewHandler useCaseInformationViewHandler;
    private IFlowViewHandler flowViewHandler;
    private IStepViewHandler stepViewHandler;
    private ITextViewHandler actorReferenceTextViewHandler;
    private ITextViewHandler useCaseSelectorHandler;
    private ITextViewHandler flowConclusionSelectorHandler;
    private IRelationshipViewHandler inheritanceAssociationViewHandler;
    
    private UseCaseModelHandlerFactory() {      
    }
    
    /**
     * Gets the handler for the use case diagram scene.
     * @return The handler for the scene
     */
    public IDisplaySceneHandler getUseCaseDiagramDisplaySceneHandler() {
        if (this.useCaseDiagramDisplaySceneHandler == null) {
            this.useCaseDiagramDisplaySceneHandler = new DisplayUseCaseModelSceneHandler();
        }
        
        return this.useCaseDiagramDisplaySceneHandler;
    }
    
    /**
     * Gets the handler for the use case diagram view.
     * @return The handler for the view
     */
    public IUseCaseDiagramViewHandler getUseCaseDiagramViewHandler() {
        if (this.useCaseDiagramViewHandler == null) {
            this.useCaseDiagramViewHandler = new UseCaseDiagramViewHandler();
        }
        
        return this.useCaseDiagramViewHandler;
    }
    
    /**
     * Gets the handler for the system boundary box.
     * @return The handler.
     */
    public ISystemBoundaryViewHandler getSystemBoundaryViewHandler() {
        if (this.systemBoundaryViewHandler == null) {
            this.systemBoundaryViewHandler = new SystemBoundaryViewHandler();
        }
        
        return this.systemBoundaryViewHandler;
    }
    
    /**
     * Gets the handler for the note view.
     * @return The handler for the view
     */
    public INoteViewHandler getNoteViewHandler() {
        if (this.noteViewHandler == null) {
            this.noteViewHandler = new NoteViewHandler();
        }
        
        return this.noteViewHandler;
    }
    
    /**
     * Gets the handler for the use case view.
     * @return The handler for the view
     */
    public IUseCaseViewHandler getUseCaseViewHandler() {
        if (this.useCaseViewHandler == null) {
            this.useCaseViewHandler = new UseCaseViewHandler();
        }
        
        return this.useCaseViewHandler;
    }
    
    /**
     * Gets the handler for the actor view.
     * @return The handler for the view
     */
    public IActorViewHandler getActorViewHandler() {
        if (this.actorViewHandler == null) {
            this.actorViewHandler = new ActorViewHandler();
        }
        
        return this.actorViewHandler;
    }
    
    /**
     * Gets the handler for the note content.
     * @return the handler
     */
    public ITextViewHandler getNoteContentHandler() {
        if (this.noteContentHandler == null) {
            this.noteContentHandler = new TextViewHandler();
        }
        
        return this.noteContentHandler;
    }
    
    /**
     * Gets a handler for generic text views.
     * @return the handler
     */
    public ITextViewHandler getTextViewHandler() {
        if (this.textViewHandler == null) {
            this.textViewHandler = new TextViewHandler();
        }
        
        return this.textViewHandler;
    }
    
    /**
     * Gets the actor multiplicity text view handler.
     * @return the handler
     */
    public ITextViewHandler getActorMultiplicityViewHandler() {
        if (this.actorMultiplicityViewHandler == null) {
            this.actorMultiplicityViewHandler = new ActorMultiplicityHandler();
        }
        
        return this.actorMultiplicityViewHandler;
    }
    
    /**
     * Gets the handler for association views.
     * @return the handler
     */
    public IRelationshipViewHandler getAssociationViewHandler() {
        if (this.associationViewHandler == null) {
            this.associationViewHandler = new ActorUseCaseAssociationViewHandler();
        }
        
        return this.associationViewHandler;
    }
    
    /**
     * Gets the handler for use case detail views.
     * @return The handler.
     */
    public IUseCaseDetailViewHandler getUseCaseDetailViewHandler() {
        if (this.useCaseDetailViewHandler == null) {
            this.useCaseDetailViewHandler = new UseCaseDetailViewHandler();
        }
        
        return this.useCaseDetailViewHandler;
    }

    /**
     * Gets the handler for use case summary views.
     * @return The handler.
     */
    public IUseCaseInformationViewHandler getUseCaseInformationViewHandler() {
        if (this.useCaseInformationViewHandler == null) {
            this.useCaseInformationViewHandler = new UseCaseInformationViewHandler();
        }
        
        return this.useCaseInformationViewHandler;
    }
    
    /**
     * Gets the handler for flow views.
     * @return The handler.
     */
    public IFlowViewHandler getFlowViewHandler() {
        if (this.flowViewHandler == null) {
            this.flowViewHandler = new FlowViewHandler();
        }
        
        return this.flowViewHandler;
    }
    
    /**
     * Gets the handler for step views.
     * @return The handler.
     */
    public IStepViewHandler getStepViewHandler() {
        if (this.stepViewHandler == null) {
            this.stepViewHandler = new StepViewHandler();
        }
        
        return this.stepViewHandler;
    }
    
    /**
     * Gets the handler for selecting an actor in a step.
     * @return The handler.
     */
    public ITextViewHandler getActorReferenceTextViewHandler() {
        if (this.actorReferenceTextViewHandler == null) {
            this.actorReferenceTextViewHandler = new ActorReferenceTextViewHandler();
        }
        
        return this.actorReferenceTextViewHandler;
    }
    
    /**
     * Gets the handler for selecting a use case in a step.
     * @return The handler.
     */
    public ITextViewHandler getUseCaseSelectorHandler() {
        if (this.useCaseSelectorHandler == null) {
            this.useCaseSelectorHandler = new UseCaseSelectorHandler();
        }
        
        return this.useCaseSelectorHandler;
    }
    
    /**
     * Gets the handler for selecting a flow conclusion type.
     * @return The handler.
     */
    public ITextViewHandler getFlowConclusionSelectorHandler() {
        if (this.flowConclusionSelectorHandler == null) {
            this.flowConclusionSelectorHandler = new FlowConclusionTextViewHandler();
        }
        
        return this.flowConclusionSelectorHandler;
    }
    
    /**
     * Gets the handler for use case associations.
     * @return The handler.
     */
    public IRelationshipViewHandler getUseCaseAssociationViewHandler() {
        if (this.useCaseAssociationViewHandler == null) {
            this.useCaseAssociationViewHandler = new UseCaseAssociationViewHandler();
        }
        
        return this.useCaseAssociationViewHandler;
    }
    
    /**
     * Gets the handler for use case associations.
     * @return The handler.
     */
    public IRelationshipViewHandler getInheritanceAssociationViewHandler() {
        if (this.inheritanceAssociationViewHandler == null) {
            this.inheritanceAssociationViewHandler = new InheritanceAssociationViewHandler();
        }
        
        return this.inheritanceAssociationViewHandler;
    }
}
