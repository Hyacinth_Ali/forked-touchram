package ca.mcgill.sel.restif.ui.views.handler;

import org.mt4j.util.math.Vector3D;

import ca.mcgill.sel.ram.ui.events.listeners.ITapAndHoldListener;
import ca.mcgill.sel.ram.ui.events.listeners.ITapListener;
import ca.mcgill.sel.ram.ui.views.handler.IAbstractViewHandler;
import ca.mcgill.sel.restif.ui.views.BaseView;
import ca.mcgill.sel.restif.ui.views.RestTreeView;

/**
 * The handler of the {@link RestTreeView}.
 * 
 * @author Bowen
 */
public interface IRestTreeViewHandler extends IAbstractViewHandler, ITapListener, ITapAndHoldListener {
    
    /**
     * Handles a double tap performed on a specific {@link PathFragmentView}.
     * 
     * @param restTreeView - the {@link RestTreeView} that contains the {@link PathFragmentView}
     * @param target - the {@link PathFragmentView} that was double tapped
     * @return true, if the event was handled, false otherwise
     */
    boolean handleDoubleTap(RestTreeView restTreeView, BaseView<?> target);
    
    /**
     * Handles a tap-and-hold performed on a specific {@link PathFragmentView}.
     * 
     * @param restTreeView - the {@link RestTreeView} that contains the {@link PathFragmentView}
     * @param target - the {@link PathFragmentView} that a tap-and-hold was performed on
     * @return true, if the event was handled, false otherwise
     */
    boolean handleTapAndHold(RestTreeView restTreeView, BaseView<?> target);
    
    /**
     * This method should be used to drag all selected path fragments when a drag event is processed on a path 
     * fragment that is selected.
     * 
     * @param restTreeView - the structural view that the drag event was applied to
     * @param translationVect - the translation vector coming from the drag event
     */
    void dragAllSelected(RestTreeView restTreeView, Vector3D translationVect);
}