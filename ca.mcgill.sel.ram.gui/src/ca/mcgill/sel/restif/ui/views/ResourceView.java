package ca.mcgill.sel.restif.ui.views;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.edit.provider.INotifyChangedListener;

import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.RamImageComponent;
import ca.mcgill.sel.ram.ui.components.RamRectangleComponent;
import ca.mcgill.sel.ram.ui.layouts.HorizontalLayout;
import ca.mcgill.sel.ram.ui.utils.Colors;
import ca.mcgill.sel.ram.ui.utils.GraphicalUpdater;
import ca.mcgill.sel.ram.ui.utils.Icons;
import ca.mcgill.sel.restif.AccessMethod;
import ca.mcgill.sel.restif.MethodType;
import ca.mcgill.sel.restif.Resource;
import ca.mcgill.sel.restif.RestIF;
import ca.mcgill.sel.restif.ui.components.ResourceRamButton;
import ca.mcgill.sel.restif.ui.views.handler.RestTreeHandlerFactory;
import ca.mcgill.sel.restif.ui.views.handler.impl.ResourceButtonHandler;

/**
 * The visual representation for a {@link Resource}.
 * 
 * @author Bowen
 */
public class ResourceView extends RamRectangleComponent implements INotifyChangedListener {

    private GraphicalUpdater graphicalUpdater;
    
    private ResourceButtonHandler resourceButtonHandler;

    private Resource resource;
    
    private ResourceRamButton getButton;
    private ResourceRamButton putButton;
    private ResourceRamButton postButton;
    private ResourceRamButton deleteButton;
    
    // This variable represents whether or not the represented Resource is in the RestIF model or not.
    private boolean existInRestIFModel;

    /**
     * Creates a new {@link ResourceView} for the given {@link Resource}.
     * 
     * @param restTreeView - the parent view representing the {@link RestIF} model
     * @param resource - the represented {@link Resource}
     * @param parentView - the direct parent view
     * @param existInRestIFModel - whether or not this {@link Resource} exists in the {@link RestIF} model
     */
    public ResourceView(RestTreeView restTreeView, Resource resource, 
            PathFragmentView parentView, boolean existInRestIFModel) {        
        super(0, 0, parentView.getWidth(), parentView.getHeight());
        
        this.resource = resource;
        this.existInRestIFModel = existInRestIFModel;
        
        setMinimumHeight(parentView.getHeight());
        setMinimumWidth(parentView.getWidth());
        setLayout(new HorizontalLayout());
        setStrokeColor(Colors.DEFAULT_ELEMENT_STROKE_COLOR);
        setNoStroke(false);
        
        EMFEditUtil.addListenerFor(resource, this);
        graphicalUpdater = RamApp.getApplication().getGraphicalUpdaterForModel(restTreeView.getRestIF());
        
        graphicalUpdater.addGUListener(resource, this);

        addAccessMethods();
    }

    /**
     * This method sets the appropriate icon and action command for each access method button,
     * then adds them to this view.
     */
    @SuppressWarnings("static-access")
    private void addAccessMethods() {
        EList<AccessMethod> accessMethods = resource.getAccessmethod();
        boolean getFound = false;
        boolean putFound = false;
        boolean postFound = false;
        boolean deleteFound = false;
        resourceButtonHandler = (ResourceButtonHandler) RestTreeHandlerFactory.INSTANCE.getResourceButtonHandler();

        if (accessMethods != null) {
            for (AccessMethod accessMethod : accessMethods) {
                if (accessMethod.getType() == MethodType.GET) {
                    getFound = true;
                } else if (accessMethod.getType() == MethodType.PUT) {
                    putFound = true;
                } else if (accessMethod.getType() == MethodType.POST) {
                    postFound = true;
                } else {
                    deleteFound = true;
                }
            }
        }

        if (getFound) {
            getButton = new ResourceRamButton(new RamImageComponent(Icons.ICON_MENU_GET,
                    Colors.DEFAULT_ELEMENT_FILL_COLOR), (int) this.getHeight());
            getButton.setActionCommand(resourceButtonHandler.getActionResourceGetRemove());
        } else {
            getButton = new ResourceRamButton(new RamImageComponent(Icons.ICON_MENU_ADD_RESOURCE,
                    Colors.DEFAULT_ELEMENT_FILL_COLOR), (int) this.getHeight());
            getButton.setActionCommand(resourceButtonHandler.getActionResourceGetAdd());
        }
        if (putFound) {
            putButton = new ResourceRamButton(new RamImageComponent(Icons.ICON_MENU_PUT,
                    Colors.DEFAULT_ELEMENT_FILL_COLOR), (int) this.getHeight());
            putButton.setActionCommand(resourceButtonHandler.getActionResourcePutRemove());
        } else {
            putButton = new ResourceRamButton(new RamImageComponent(Icons.ICON_MENU_ADD_RESOURCE,
                    Colors.DEFAULT_ELEMENT_FILL_COLOR), (int) this.getHeight());
            putButton.setActionCommand(resourceButtonHandler.getActionResourcePutAdd());
        }
        if (postFound) {
            postButton = new ResourceRamButton(new RamImageComponent(Icons.ICON_MENU_POST,
                    Colors.DEFAULT_ELEMENT_FILL_COLOR), (int) this.getHeight());
            postButton.setActionCommand(resourceButtonHandler.getActionResourcePostRemove());
        } else {
            postButton = new ResourceRamButton(new RamImageComponent(Icons.ICON_MENU_ADD_RESOURCE,
                    Colors.DEFAULT_ELEMENT_FILL_COLOR), (int) this.getHeight());
            postButton.setActionCommand(resourceButtonHandler.getActionResourcePostAdd());
        }
        if (deleteFound) {
            deleteButton = new ResourceRamButton(new RamImageComponent(Icons.ICON_MENU_DELETE,
                    Colors.DEFAULT_ELEMENT_FILL_COLOR), (int) this.getHeight());
            deleteButton.setActionCommand(resourceButtonHandler.getActionResourceDeleteRemove());
        } else {
            deleteButton = new ResourceRamButton(new RamImageComponent(Icons.ICON_MENU_ADD_RESOURCE,
                    Colors.DEFAULT_ELEMENT_FILL_COLOR), (int) this.getHeight());
            deleteButton.setActionCommand(resourceButtonHandler.getActionResourceDeleteAdd());
        }
        
        getButton.addActionListener(resourceButtonHandler);
        putButton.addActionListener(resourceButtonHandler);
        postButton.addActionListener(resourceButtonHandler);
        deleteButton.addActionListener(resourceButtonHandler);

        addChild(getButton);
        addChild(putButton);
        addChild(postButton);
        addChild(deleteButton);
    }

    @Override
    public void notifyChanged(Notification notification) {
        if (notification.getNotifier() == this.getResource()) {
            AccessMethod accessMethod = null;
            switch (notification.getEventType()) {
                case Notification.ADD:
                    accessMethod = (AccessMethod) notification.getNewValue();

                    if (accessMethod.getType() == MethodType.GET) {
                        addGetResource();
                    } else if (accessMethod.getType() == MethodType.PUT) {
                        addPutResource();
                    } else if (accessMethod.getType() == MethodType.POST) {
                        addPostResource();
                    } else if (accessMethod.getType() == MethodType.DELETE) {
                        addDeleteResource();
                    }
                    break;
                case Notification.REMOVE:
                    accessMethod = (AccessMethod) notification.getOldValue();

                    if (accessMethod.getType() == MethodType.GET) {
                        removeGetResource();
                    } else if (accessMethod.getType() == MethodType.PUT) {
                        removePutResource();
                    } else if (accessMethod.getType() == MethodType.POST) {
                        removePostResource();
                    } else if (accessMethod.getType() == MethodType.DELETE) {
                        removeDeleteResource();
                    }
                    break;
            }
        }
    }

    /**
     * This method changes the get button to have the G icon, and resets the action command.
     */
    private void addGetResource() {
        RamApp.getApplication().invokeLater(new Runnable() {

            @SuppressWarnings("static-access")
            @Override
            public void run() {
                getButton.setIcon(new RamImageComponent(Icons.ICON_MENU_GET,
                        Colors.DEFAULT_ELEMENT_FILL_COLOR));
                getButton.setActionCommand(resourceButtonHandler.getActionResourceGetRemove());
            }
        });
    }

    /**
     * This method changes the get button to have the blank icon, and resets the action command.
     */
    private void removeGetResource() {
        RamApp.getApplication().invokeLater(new Runnable() {

            @SuppressWarnings("static-access")
            @Override
            public void run() {
                getButton.setIcon(new RamImageComponent(Icons.ICON_MENU_ADD_RESOURCE,
                        Colors.DEFAULT_ELEMENT_FILL_COLOR));
                getButton.setActionCommand(resourceButtonHandler.getActionResourceGetAdd());
            }
        });
    }

    /**
     * This method changes the put button to have the PU icon, and resets the action command.
     */
    private void addPutResource() {
        RamApp.getApplication().invokeLater(new Runnable() {

            @SuppressWarnings("static-access")
            @Override
            public void run() {
                putButton.setIcon(new RamImageComponent(Icons.ICON_MENU_PUT,
                        Colors.DEFAULT_ELEMENT_FILL_COLOR));
                putButton.setActionCommand(resourceButtonHandler.getActionResourcePutRemove());
            }
        });
    }

    /**
     * This method changes the put button to have the blank icon, and resets the action command.
     */
    private void removePutResource() {
        RamApp.getApplication().invokeLater(new Runnable() {

            @SuppressWarnings("static-access")
            @Override
            public void run() {
                putButton.setIcon(new RamImageComponent(Icons.ICON_MENU_ADD_RESOURCE,
                        Colors.DEFAULT_ELEMENT_FILL_COLOR));
                putButton.setActionCommand(resourceButtonHandler.getActionResourcePutAdd());
            }
        });
    }

    /**
     * This method changes the post button to have the PO icon, and resets the action command.
     */
    private void addPostResource() {
        RamApp.getApplication().invokeLater(new Runnable() {

            @SuppressWarnings("static-access")
            @Override
            public void run() {
                postButton.setIcon(new RamImageComponent(Icons.ICON_MENU_POST,
                        Colors.DEFAULT_ELEMENT_FILL_COLOR));
                postButton.setActionCommand(resourceButtonHandler.getActionResourcePostRemove());
            }
        });
    }

    /**
     * This method changes the post button to have the blank icon, and resets the action command.
     */
    private void removePostResource() {
        RamApp.getApplication().invokeLater(new Runnable() {

            @SuppressWarnings("static-access")
            @Override
            public void run() {
                postButton.setIcon(new RamImageComponent(Icons.ICON_MENU_ADD_RESOURCE,
                        Colors.DEFAULT_ELEMENT_FILL_COLOR));
                postButton.setActionCommand(resourceButtonHandler.getActionResourcePostAdd());
            }
        });
    }
    
    /**
     * This method changes the delete button to have the D icon, and resets the action command.
     */
    private void addDeleteResource() {
        RamApp.getApplication().invokeLater(new Runnable() {

            @SuppressWarnings("static-access")
            @Override
            public void run() {
                deleteButton.setIcon(new RamImageComponent(Icons.ICON_MENU_DELETE,
                        Colors.DEFAULT_ELEMENT_FILL_COLOR));
                deleteButton.setActionCommand(resourceButtonHandler.getActionResourceDeleteRemove());
            }
        });
    }

    /**
     * This method changes the delete button to have the blank icon, and resets the action command.
     */
    private void removeDeleteResource() {
        RamApp.getApplication().invokeLater(new Runnable() {

            @SuppressWarnings("static-access")
            @Override
            public void run() {
                deleteButton.setIcon(new RamImageComponent(Icons.ICON_MENU_ADD_RESOURCE,
                        Colors.DEFAULT_ELEMENT_FILL_COLOR));
                deleteButton.setActionCommand(resourceButtonHandler.getActionResourceDeleteAdd());
            }
        });
    }

    /**
     * Returns the represented {@link Resource}.
     * 
     * @return resource
     */
    public Resource getResource() {
        return resource;
    }
    
    /**
     * Returns whether or not the represented {@link Resource} exists in the {@link RestIF} model.
     * 
     * @return existInRestIFModel
     */
    public boolean isExistInRestIFModel() {
        return existInRestIFModel;
    }

    /**
     * Sets the existInRestIFModel variable.
     * 
     * @param existInRestIFModel - the given boolean
     */
    public void setExistInRestIFModel(boolean existInRestIFModel) {
        this.existInRestIFModel = existInRestIFModel;
    }

    @Override
    public void destroy() {
        this.destroyAllChildren();
        super.destroy();
    }
}
