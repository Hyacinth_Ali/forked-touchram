package ca.mcgill.sel.ram.ui.views.structural;

import org.mt4j.components.MTComponent;
import org.mt4j.input.inputProcessors.IGestureEventListener;
import org.mt4j.input.inputProcessors.componentProcessors.panProcessor.PanProcessorTwoFingers;
import org.mt4j.input.inputProcessors.componentProcessors.tapAndHoldProcessor.TapAndHoldProcessor;
import org.mt4j.input.inputProcessors.componentProcessors.tapProcessor.TapProcessor;
import org.mt4j.input.inputProcessors.componentProcessors.unistrokeProcessor.UnistrokeUtils.Direction;
import org.mt4j.input.inputProcessors.componentProcessors.unistrokeProcessor.UnistrokeUtils.UnistrokeGesture;
import org.mt4j.input.inputProcessors.componentProcessors.zoomProcessor.ZoomProcessor;
import org.mt4j.util.math.Vector3D;

import ca.mcgill.sel.core.COREModelComposition;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.RamLineComponent;
import ca.mcgill.sel.ram.ui.components.RamRectangleComponent;
import ca.mcgill.sel.ram.ui.components.navigationbar.NavigationBar;
import ca.mcgill.sel.ram.ui.events.MouseWheelProcessor;
import ca.mcgill.sel.ram.ui.events.RightClickDragProcessor;
import ca.mcgill.sel.ram.ui.events.UnistrokeProcessorLeftClick;
import ca.mcgill.sel.ram.ui.scenes.DisplayAspectScene;
import ca.mcgill.sel.ram.ui.scenes.RamAbstractScene;
import ca.mcgill.sel.ram.ui.scenes.handler.IRamAbstractSceneHandler;
import ca.mcgill.sel.ram.ui.utils.Colors;
import ca.mcgill.sel.ram.ui.utils.GUIConstants;
import ca.mcgill.sel.ram.ui.utils.Icons;
import ca.mcgill.sel.ram.ui.utils.Strings;
import ca.mcgill.sel.ram.ui.views.AbstractView;
import ca.mcgill.sel.ram.ui.views.handler.IAbstractViewHandler;
import ca.mcgill.sel.ram.ui.views.structural.handler.impl.GenericSplitViewHandler;
import ca.mcgill.sel.restif.ui.views.PathFragmentView;

/**
 * This is a generic view used for composition and individual view editing by seeing 
 * higher and lower level views at the same time.
 * 
 * This view is a generic modification from {@link CompositionSplitEditingView}
 * 
 * @author Bowen
 * 
 * @param <S> type that extends {@link IRamAbstractSceneHandler} for the inner level scene
 * @param <T> type that extends {@link IRamAbstractSceneHandler} for the outer level scene
 * @param <U> type that extends {@link IAbstractViewHandler} for the inner level view
 * @param <V> type that extends {@link IAbstractViewHandler} for the outer level view
 */
public class GenericSplitView<S extends IRamAbstractSceneHandler,
    T extends IRamAbstractSceneHandler, U extends IAbstractViewHandler,
    V extends IAbstractViewHandler> extends MTComponent {

    // screen width and height
    private int width;
    private int height;
    
    // composition of the generic split view
    private COREModelComposition composition;

    // inner and outer scenes
    private RamAbstractScene<S> displaySceneInnerLevel;
    private RamAbstractScene<T> displaySceneOuterLevel;
    
    // inner and outer views
    private AbstractView<U> innerLevelView;
    private AbstractView<V> outerLevelView;

    // line for splitting the inner and outer views
    private RamLineComponent splitLine;

    // boolean for whether the current orientation of the split view is horizontal
    private boolean isHorizontal;
    
    // This component will consume all the gestures and will do gesture forwarding depending on the conditions.
    private RamRectangleComponent overlay;
    
    // Layer for all view elements. This allows scaling and dragging over the entire screen of the whole view.
    private RamRectangleComponent containerLayer;

    // Layer for unistroke gestures. This sits on top of everything (overlay).
    private MTComponent unistrokeLayer;

    // handler for the generic split view
    private GenericSplitViewHandler<S, T, U, V> handler;
    
    /**
     * Creates a new instance of the generic split view.
     * 
     * @param displaySceneInnerLevel inner level scene
     * @param displaySceneOuterLevel outer level scene
     * @param composition composition for editing mappings
     * @param isHorizontal whether or not the split view is horizontal
     */
    public GenericSplitView(RamAbstractScene<S> displaySceneInnerLevel,
            RamAbstractScene<T> displaySceneOuterLevel, COREModelComposition composition, boolean isHorizontal) {
        super(RamApp.getApplication(), "splitview");
        width = RamApp.getApplication().getWidth();
        height = RamApp.getApplication().getHeight();
        
        // add a button to switch the split orientations between vertical and horizontal
        RamApp.getActiveScene().getMenu().addAction(Strings.MENU_SWITCH_SPLIT_ORIENTATION, 
                Icons.ICON_MENU_SWITCH_SPLIT_ORIENATION, "action.switch.split.orientation",
                RamApp.getActiveScene(), true);
                
        Vector3D locationInnerLevel;
        Vector3D locationOuterLevel;

        this.displaySceneInnerLevel = displaySceneInnerLevel;
        this.displaySceneOuterLevel = displaySceneOuterLevel;
        this.composition = composition;
        this.isHorizontal = isHorizontal;

        // add the navigation bar to the generic split view
        RamApp.getApplication().getCanvas().addChild(NavigationBar.getInstance());
        NavigationBar.getInstance().concernSelectMode();
        
        // extract views from scenes (hard coded but is safe)
        innerLevelView = (AbstractView<U>) displaySceneInnerLevel.getContainerLayer()
                .getChildByIndex(0).getChildByIndex(0);
        outerLevelView = (AbstractView<V>) displaySceneOuterLevel.getContainerLayer()
                .getChildByIndex(0).getChildByIndex(0);
        
        // in case the outer level scene didn't show the outer level view
        // if outer level scene is RAM model
        if (displaySceneOuterLevel instanceof DisplayAspectScene) {
            ((DisplayAspectScene) displaySceneOuterLevel).switchToView(outerLevelView);
        }
        
        // TODO: add else if statements for other models

        // set positions of inner and outer level views and split line depending on vertical or horizontal composition
        if (isHorizontal) {
            locationInnerLevel = new Vector3D(0, 0);
            locationOuterLevel = new Vector3D(0, height / 2);
            splitLine = new RamLineComponent(0, height / 2, width, height / 2);
        } else {
            locationInnerLevel = new Vector3D(-1 * width / 3, 0);
            locationOuterLevel = new Vector3D(width / 2, 0);
            splitLine = new RamLineComponent(width / 2, 0, width / 2, height);
        }
        
        // create an instance of RamRectangleComponent
        // containerLayer is used for adding inner and outer level view components 
        containerLayer = new RamRectangleComponent();
        containerLayer.setName("view container layer");
        
        // add the inner and outer level view and split line to the container layer as children
        containerLayer.addChild(innerLevelView);
        containerLayer.addChild(outerLevelView);
        containerLayer.addChild(splitLine);
        
        // set positions and colors of inner and outer views
        innerLevelView.setPositionGlobal(locationInnerLevel);
        outerLevelView.setPositionGlobal(locationOuterLevel);
        outerLevelView.setNoFill(false);
        outerLevelView.setFillColor(Colors.BACKGROUND_COLOR_OF_LOWER_LEVEL_ASPECT_IN_SPLIT_VIEW);
        
        // adding the overlay to capture all the gestures
        overlay = new RamRectangleComponent(0, 0, width, height);
        
        // add the overlay as a child of containerLayer
        containerLayer.addChild(overlay);
        
        // create an instance of RamRectangleComponent
        // unistrokeLayer is used for adding unistroke components ie. the yellow lines 
        unistrokeLayer = new MTComponent(RamApp.getApplication(), "unistroke drawing layer");
        unistrokeLayer.unregisterAllInputProcessors();
        unistrokeLayer.removeAllGestureEventListeners();
        
        // add containerLayer and unistrokeLayer as children of the generic split view
        addChild(containerLayer);
        addChild(unistrokeLayer);
        
        // create an instance of the handler
        handler = new GenericSplitViewHandler<S, T, U, V>(this);

        // register input processors and gesture listeners
        registerInputProcessorsForOverlay();
        registerGestureListeners(handler);
    }

    /**
     * Actions when the generic split view is exited from.
     */
    @Override
    public void destroy() {
        // remove listeners
        overlay.removeAllGestureEventListeners();
        // do rest
        super.destroy();
    }
    
    /**
     * Returns the outer level scene.
     * 
     * @return the outer level {@link RamAbstractScene}
     */
    public RamAbstractScene<T> getDisplaySceneOuterLevel() {
        return displaySceneOuterLevel;
    }
    
    /**
     * Returns the inner level scene.
     * 
     * @return the inner level {@link RamAbstractScene}
     */
    public RamAbstractScene<S> getDisplaySceneInnerLevel() {
        return displaySceneInnerLevel;
    }
    
    /**
     * Returns the composition of the generic split view.
     * 
     * @return the {@link COREModelComposition}
     */
    public COREModelComposition getComposition() {
        return composition;
    }

    /**
     * Returns the overlay.
     * 
     * @return the {@link RamRectangleComponent}
     */
    public RamRectangleComponent getOverlay() {
        return overlay;
    }
    
    /**
     * Returns the container layer.
     * 
     * @return the {@link RamRectangleComponent}
     */
    public RamRectangleComponent getContainerLayer() {
        return containerLayer;
    }
    
    /**
     * Returns the unistroke layer.
     * 
     * @return the {@link RamRectangleComponent}
     */
    public MTComponent getUnistrokeLayer() {
        return unistrokeLayer;
    }

    /**
     * Returns the handler.
     * 
     * @return the {@link GenericSplitViewHandler}
     */
    public GenericSplitViewHandler<S, T, U, V> getHandler() {
        return handler;
    }

    /**
     * Returns the inner level view.
     * 
     * @return the {@link AbstractView}
     */
    public AbstractView<U> getInnerLevelView() {
        return innerLevelView;
    }

    /**
     * Returns the outer level view.
     * 
     * @return the {@link AbstractView}
     */
    public AbstractView<V> getOuterLevelView() {
        return outerLevelView;
    }
    
    /**
     * Returns whether or not the generic split view is horizontal.
     * 
     * @return the boolean
     */
    public boolean isHorizontal() {
        return isHorizontal;
    }
    
    /**
     * Sets the variable for whether or not the generic split view is horizontal.
     * 
     * @param horizontal whether or not the generic split view is horizontal
     */
    public void setHorizontal(boolean horizontal) {
        this.isHorizontal = horizontal;
    }
    
    /**
     * Returns the split line.
     * 
     * @return the {@link RamLineComponent}
     */
    public RamLineComponent getSplitLine() {
        return splitLine;
    }
    
    /**
     * Sets the split line.
     * 
     * @param splitLine the new split line for the split view
     */
    public void setSplitLine(RamLineComponent splitLine) {
        this.splitLine = splitLine;
    }
    
    /**
     * Registers all gesture listeners to the overlay.
     * 
     * @param listener the handler for the generic split view
     */
    private void registerGestureListeners(IGestureEventListener listener) {
        overlay.addGestureListener(RightClickDragProcessor.class, listener);
        overlay.addGestureListener(MouseWheelProcessor.class, listener);
        overlay.addGestureListener(PanProcessorTwoFingers.class, listener);
        overlay.addGestureListener(ZoomProcessor.class, listener);
        overlay.addGestureListener(TapProcessor.class, listener);
        overlay.addGestureListener(TapAndHoldProcessor.class, listener);
        overlay.addGestureListener(UnistrokeProcessorLeftClick.class, listener);
    }
    
    /**
     * Registers all input processors to the overlay.
     * Add the circle rectangle check triangle question and x templates to the unistroke processor.
     */
    private void registerInputProcessorsForOverlay() {
        RamApp application = RamApp.getApplication();
        
        overlay.registerInputProcessor(new RightClickDragProcessor(application));
        overlay.registerInputProcessor(new MouseWheelProcessor(application));
        overlay.registerInputProcessor(new PanProcessorTwoFingers(application));
        overlay.registerInputProcessor(new ZoomProcessor(application));
        overlay.registerInputProcessor(new TapProcessor(application, GUIConstants.TAP_MAX_FINGER_UP,
                true, GUIConstants.TAP_DOUBLE_TAP_TIME));
        overlay.registerInputProcessor(new TapAndHoldProcessor(application, GUIConstants.TAP_AND_HOLD_DURATION));
        
        UnistrokeProcessorLeftClick up = new UnistrokeProcessorLeftClick(application);
        up.addTemplate(UnistrokeGesture.CIRCLE, Direction.CLOCKWISE);
        up.addTemplate(UnistrokeGesture.CIRCLE, Direction.COUNTERCLOCKWISE);
        up.addTemplate(UnistrokeGesture.RECTANGLE, Direction.CLOCKWISE);
        up.addTemplate(UnistrokeGesture.RECTANGLE, Direction.COUNTERCLOCKWISE);
        up.addTemplate(UnistrokeGesture.CHECK, Direction.CLOCKWISE);
        up.addTemplate(UnistrokeGesture.TRIANGLE, Direction.COUNTERCLOCKWISE);
        up.addTemplate(UnistrokeGesture.TRIANGLE, Direction.CLOCKWISE);
        up.addTemplate(UnistrokeGesture.QUESTION, Direction.CLOCKWISE);
        up.addTemplate(UnistrokeGesture.X, Direction.CLOCKWISE);
        up.addTemplate(UnistrokeGesture.X, Direction.COUNTERCLOCKWISE);
        overlay.registerInputProcessor(up);
    }
    
    /**
     * Clears the unistroke drawings that have been drawn on the unistroke layer.
     */
    public void cleanUnistrokeLayer() {
        MTComponent[] components = unistrokeLayer.getChildren();
        for (MTComponent component : components) {
            component.destroy();
        }
    }
}