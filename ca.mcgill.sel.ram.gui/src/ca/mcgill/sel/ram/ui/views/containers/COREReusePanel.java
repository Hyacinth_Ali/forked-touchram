package ca.mcgill.sel.ram.ui.views.containers;

import java.util.List;

import ca.mcgill.sel.core.COREReuse;
import ca.mcgill.sel.ram.ui.components.RamListComponent.Namer;
import ca.mcgill.sel.ram.ui.components.RamPanelListComponent;
import ca.mcgill.sel.ram.ui.components.RamRectangleComponent;
import ca.mcgill.sel.ram.ui.components.RamTextComponent;
import ca.mcgill.sel.ram.ui.components.listeners.RamListListener;
import ca.mcgill.sel.ram.ui.layouts.HorizontalLayoutVerticallyCentered;
import ca.mcgill.sel.ram.ui.utils.Colors;
import ca.mcgill.sel.ram.ui.utils.Strings;

/**
 * Panel container that visualizes the Reuses of a feature model.
 * 
 * @author arthurls
 */
public class COREReusePanel extends RamPanelListComponent<COREReuse> {
    
    private List<COREReuse> reuses;
    
    /**
     * Creates a new panel with the given horizontal and vertical sticks.
     * 
     * @param horizontalStick The horizontalStick for the panel
     * @param verticalStick The verticalStick for the panel
     * @param reuses List of the concern reuses
     * @param listener The custom listener for that panel
     */
    public COREReusePanel(HorizontalStick horizontalStick, VerticalStick verticalStick, List<COREReuse> reuses, 
            RamListListener<COREReuse> listener) {
        super(5, Strings.LABEL_MODEL_REUSE, horizontalStick, verticalStick);
        
        this.reuses = reuses;
        setNamer(new InternalReusePanelNamer());
        setListener(listener);
        setElements(reuses);
    }

    
    /**
     * Internal Reuse Panel Namer.
     */
    private class InternalReusePanelNamer implements Namer<COREReuse> {

        @Override
        public RamRectangleComponent getDisplayComponent(COREReuse reuse) {
            return createReusePanelComponent(reuse, false);
        }

        @Override
        public String getSortingName(COREReuse reuse) {
            return reuse.getName();
        }

        @Override
        public String getSearchingName(COREReuse reuse) {
            return reuse.getName();
        }
        
        /**
         * Internal method to create an element of the list.
         * @param reuse the reuse we want to display in the list
         * @param noFill 
         * @return the RamRectangle Component
         */
        private RamRectangleComponent createReusePanelComponent(COREReuse reuse, boolean noFill) {
            RamRectangleComponent view = new RamRectangleComponent();
            view.setLayout(new HorizontalLayoutVerticallyCentered());

            // Removes quotes of the message
            RamTextComponent text = new RamTextComponent(reuse.getName());
            text.setPickable(false);

            // Adds spaces in left and right
            final int space = 5;
            view.setBufferSize(Cardinal.WEST, space);
            view.setBufferSize(Cardinal.EAST, space);

            view.addChild(text);

            view.setNoFill(noFill);
            view.setFillColor(Colors.DEFAULT_ELEMENT_FILL_COLOR);
            view.setNoStroke(false);

            return view;
        }
    }
    
    /**
     * Returns the list of reuses contained in the list.
     * @return list of reuses
     */
    public List<COREReuse> getReuses() {
        return reuses;
    }
    
    /**
     * Changes the color of the current selected reuse in the list.
     * Returns true if the reuse given is in the list
     * Returns false if the reuse is not in the list
     * @param reuse to highlight in the list
     * @return list.contains(reuse)
     */
    public boolean setSelectedReuse(COREReuse reuse) {
        if (list.getElementMap().containsKey(reuse)) {
            list.getElementMap().get(reuse).setFillColor(Colors.FEATURE_VIEW_IS_REUSING);
            return true;
        }
        return false;
    }
    
    public void clearSelections() {
        for (COREReuse r : reuses) {
            list.getElementMap().get(r).setFillColor(Colors.DEFAULT_ELEMENT_FILL_COLOR);
        }
    }
    
}
