package ca.mcgill.sel.ram.ui.views.structural;

import java.util.List;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EObject;
import org.mt4j.util.MTColor;

import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.commons.emf.util.EMFModelUtil;
import ca.mcgill.sel.core.COREArtefact;
import ca.mcgill.sel.core.CORECIElement;
import ca.mcgill.sel.core.CorePackage;
import ca.mcgill.sel.core.util.COREArtefactUtil;
import ca.mcgill.sel.ram.Attribute;
import ca.mcgill.sel.ram.Class;
import ca.mcgill.sel.ram.Classifier;
import ca.mcgill.sel.ram.LayoutElement;
import ca.mcgill.sel.ram.Operation;
import ca.mcgill.sel.ram.RAMVisibilityType;
import ca.mcgill.sel.ram.RamPackage;
import ca.mcgill.sel.ram.ui.utils.Colors; 
import ca.mcgill.sel.ram.ui.utils.Fonts;
import ca.mcgill.sel.ram.ui.utils.Strings;
import ca.mcgill.sel.ram.ui.views.MappingCardinalityTextView;
import ca.mcgill.sel.ram.ui.views.TextView;
import ca.mcgill.sel.ram.ui.views.handler.HandlerFactory;
import ca.mcgill.sel.ram.ui.views.structural.handler.IClassViewHandler;

/**
 * This view draws the RAM representation of a Class onto a {@link StructuralDiagramView}. It contains a name field, and
 * two lists for methods and attributes. ClassViews are inspectable and create a {@link ClassInspectorView} when click
 * on.
 *
 * @author vbonnet
 * @author eyildirim
 * @author mschoettle
 */
public class ClassView extends ClassifierView<IClassViewHandler> {

    /**
     * The field for mapping cardinalities.
     */
    protected TextView mappingCardinalityField;

    private Class ramClass;

    /**
     * Whether not public operations should be displayed or not.
     */
    private boolean showAllOperations;

    private COREArtefact artefact;

    /**
     * Creates a new view representing the given class.
     *
     * @param structuralDiagram the {@link StructuralDiagramView} that owns this view
     * @param clazz The RAM class to display.
     * @param layoutElement The position at which to display it.
     */
    public ClassView(StructuralDiagramView structuralDiagram, Class clazz, LayoutElement layoutElement) {
        super(structuralDiagram, clazz, layoutElement);

        showAllOperations = false;
        
        // Add a listener to listen to any partiality change in the core metamodel
        artefact = COREArtefactUtil.getReferencingExternalArtefact(clazz);
        EMFEditUtil.addListenerFor(artefact, this);
    }

    /**
     * Returns the class this view is visualizing.
     *
     * @return the ram {@link Class} this view is displaying.
     */
    public Class getRamClass() {
        return ramClass;
    }

    @Override
    protected void initializeClass(Classifier classifier) {
        ramClass = (Class) classifier;

        for (int i = 0; i < ramClass.getAttributes().size(); i++) {
            Attribute attr = ramClass.getAttributes().get(i);
            addAttributeField(i, attr);
        }

        initializeOperations(classifier);
        collapseOperations();
        updateMappingCardinality();
    }

    @Override
    public void notifyChanged(Notification notification) {
        // handle it first by the super class
        super.notifyChanged(notification);
        // this gets called for every ClassView (as there is only one item provider)
        // therefore an additional check is necessary
        if (notification.getNotifier() == ramClass) {
            if (notification.getFeature() == RamPackage.Literals.MAPPABLE_ELEMENT__PARTIALITY) {
                updateMappingCardinality();
            }
        } else if (notification.getNotifier() == artefact) {
            if (notification.getFeature() == CorePackage.Literals.CORE_ARTEFACT__CI_ELEMENTS) {
                CORECIElement ciElement = (CORECIElement) notification.getNewValue();
                if (ciElement == null) {
                    CORECIElement ciOldElement = (CORECIElement) notification.getOldValue();
                    if (ciOldElement.getModelElement() == ramClass) {
                        updateMappingCardinality();
                    }
                } else if (ciElement.getModelElement() == ramClass) {
                    updateMappingCardinality();
                }
            } 
        }
    }

    /**
     * Returns whether an operation should be displayed or not.
     * It will return true if
     * - showAllOperations is true OR
     * - it is public OR
     * - it is not mapped in an operation mapping OR
     * - it is not used in message views
     *
     * @param operation The operation to show or not
     *
     * @return whether operation should be shown or not
     */
    @Override
    protected boolean shouldShowOperation(Operation operation) {
        // TODO: Add the condition that an operation with a message view should be shown.
        return showAllOperations
                || operation.getVisibility() == RAMVisibilityType.PUBLIC
                || !isMappedTo(operation)
                || EMFModelUtil.referencedInFeature(operation, RamPackage.Literals.MESSAGE__SIGNATURE);
    }
    
    protected boolean isMappedTo(Operation op) {
        COREArtefact art = COREArtefactUtil.getReferencingExternalArtefact(op);
        List<EObject> refs = EMFModelUtil.findCrossReferences(art, op, CorePackage.Literals.CORE_LINK__TO);
        return !refs.isEmpty();
    }

    /**
     * Update the mappingCardinalityField and display it if it wasn't already there.
     * If the {@link CORECIElement} associated do no longer exists, we just remove the mappingCardinalityField.
     */
    private void updateMappingCardinality() {
        CORECIElement ciElement = COREArtefactUtil.getCIElementFor(classifier);

        if (mappingCardinalityField == null && ciElement != null) {

            mappingCardinalityField = new MappingCardinalityTextView(ciElement);
            mappingCardinalityField.setBufferSize(Cardinal.EAST, BUFFER_BOTTOM);
            mappingCardinalityField.setPlaceholderText(Strings.PH_CARDINALITY);
            mappingCardinalityField.setFont(Fonts.FONT_CLASS_NAME_REFERENCE);
            nameContainer.addChild(mappingCardinalityField);

            mappingCardinalityField.setHandler(HandlerFactory.INSTANCE.getMappingCardinalityHandler());
        } else if (ciElement == null && mappingCardinalityField != null) {
            nameContainer.removeChild(mappingCardinalityField);
            mappingCardinalityField = null;
        }
    }

    /**
     * Sets color of the all attribute views to default attribute view color and setNoFill, setNoStroke to true.
     */
    public void setDefaultFillAndStrokeColorForAllAttributes() {
        for (AttributeView attView : attributes.values()) {
            attView.setFillColor(Colors.ATTRIBUTE_VIEW_DEFAULT_FILL_COLOR);
            attView.setStrokeColor(Colors.ATTRIBUTE_VIEW_DEFAULT_STROKE_COLOR);
            attView.setNoFill(true);
            attView.setNoStroke(true);
        }
    }

    /**
     * Sets color of the all operation views to default operation view color and setNoFill, setNoStroke to true.
     */
    public void setDefaultFillAndStrokeColorForAllOperations() {
        for (OperationView opView : operations.values()) {
            opView.setFillColor(Colors.OPERATION_VIEW_DEFAULT_FILL_COLOR);
            opView.setStrokeColor(Colors.OPERATION_VIEW_DEFAULT_STROKE_COLOR);
            opView.setNoFill(true);
            opView.setNoStroke(true);
        }
    }

    /**
     * Sets the fill and stroke color of the view representing the given {@link Attribute}.
     *
     * @param attributeToBeColored the {@link Attribute} whose view should be changed
     * @param colorFill the fill color to set
     * @param colorStroke the stroke color to set
     */
    public void setFillandStrokeColorOfAttribute(Attribute attributeToBeColored,
            MTColor colorFill, MTColor colorStroke) {
        AttributeView attributeView = attributes.get(attributeToBeColored);
        attributeView.setFillColor(colorFill);
        attributeView.setStrokeColor(colorStroke);

        attributeView.setNoFill(false);
        attributeView.setNoStroke(false);
    }

    /**
     * Sets the fill and stroke color of the given operation to the given colors.
     *
     * @param operationToBeColored OperationView of this operation will be colored.
     * @param colorFill fill color of the operation view.
     * @param colorStroke stroke color of the operation view.
     */
    public void setFillandStrokeColorOfOperation(Operation operationToBeColored,
            MTColor colorFill, MTColor colorStroke) {
        OperationView operationView = operations.get(operationToBeColored);
        operationView.setFillColor(colorFill);
        operationView.setStrokeColor(colorStroke);

        operationView.setNoFill(false);
        operationView.setNoStroke(false);
    }

    /**
     * Sets Fill color of the class view to default fill color.
     */
    public void setFillColorToDefault() {
        setFillColor(Colors.CLASS_VIEW_DEFAULT_FILL_COLOR);
    }

    /**
     * Displays the keyboard.
     */
    public void showKeyboard() {
        nameField.showKeyboard();
    }

    /**
     * Clear the name field of this view.
     *
     */
    public void clearNameField() {
        ((TextView) nameField).clearText();
    }

    /**
     * Returns the showAllOperations boolean attribute.
     * 
     * @return the showAllOperations boolean attribute
     */
    public boolean isShowingAllOperations() {
        return this.showAllOperations;
    }

    /**
     * Toggles the showAllOperations attribute.
     */
    public void toggleShowAllOperations() {
        showAllOperations = !showAllOperations;
        collapseOperations();
    }

    /**
     * Displays/Hide operations depending on the value of showAllOperations.
     */
    private void collapseOperations() {
        if (showAllOperations) {
            for (int i = 0; i < classifier.getOperations().size(); i++) {
                Operation operation = classifier.getOperations().get(i);
                if (!this.containsChild(operations.get(operation))) {
                    operationsContainer.addChild(i, operations.get(operation));
                }
            }
        } else {
            for (Operation operation : classifier.getOperations()) {
                if (!shouldShowOperation(operation)) {
                    operationsContainer.removeChild(operations.get(operation));
                }
            }
        }
    }
    
    @Override
    public void destroy() {
        if (artefact != null) {
            EMFEditUtil.removeListenerFor(artefact, this);
        }
        super.destroy();
    }
}
