package ca.mcgill.sel.ram.ui.views.impact;

import ca.mcgill.sel.core.COREImpactNode;
import ca.mcgill.sel.core.COREReuse;
import ca.mcgill.sel.core.COREWeightedLink;
import ca.mcgill.sel.core.CorePackage;
import ca.mcgill.sel.ram.ui.components.RamTextComponent;
import ca.mcgill.sel.ram.ui.views.TextView;

/**
 * This view describes a weighted mapping of a FeatureImpactNodeView.
 *
 * @author Romain
 *
 */
public class WeightedLinkLineView extends AbstractFeatureImpactNodeLineView {

    private TextView concernName;
    private RamTextComponent separator;
    private TextView nodeName;

    /**
     * Create a new {@link TextView} for the name.
     *
     * @param concern the {@link COREReuse} that will be displayed.
     * @param node the {@link COREImpactNode} that will be displayed.
     */
    public void createNameTextView(COREReuse concern, COREImpactNode node) {
        this.removeNameTextView();

        this.concernName = new TextView(concern, CorePackage.Literals.CORE_NAMED_ELEMENT__NAME);
        this.nodeName = new TextView(node, CorePackage.Literals.CORE_NAMED_ELEMENT__NAME);
        this.separator = new RamTextComponent("-");

        this.addChild(concernName);
        this.addChild(separator);
        this.addChild(nodeName);
    }

    /**
     * Create a new {@link TextView} for the weight.
     *
     * @param mapping the {@link COREWeightedLink} that contains the weight.
     */
    public void createWeightTextView(COREWeightedLink mapping) {
        this.removeWeightTextView();

        this.weight = new TextView(mapping, CorePackage.Literals.CORE_WEIGHTED_LINK__WEIGHT);

        if (handler != null) {
            this.weight.registerTapProcessor(handler);
        }

        this.addChild(0, this.weight);
    }

    /**
     * Remove {@link TextView} for the name.
     */
    @Override
    public void removeNameTextView() {
        if (concernName != null && this.containsChild(concernName) && nodeName != null
                && this.containsChild(nodeName)) {
            this.removeChild(concernName);
            this.removeChild(separator);
            this.removeChild(nodeName);
            concernName = null;
            separator = null;
            nodeName = null;
        }
    }

    /**
     * Retrieve {@link TextView} for the name.
     *
     * @return the first {@link TextView}.
     */
    @Override
    public TextView getNameTextView() {
        return this.nodeName;
    }
}
