package ca.mcgill.sel.ram.ui.views.message.handler.impl;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.mt4j.input.inputProcessors.componentProcessors.tapProcessor.TapEvent;
import ca.mcgill.sel.ram.AssignmentStatement;
import ca.mcgill.sel.ram.CombinedFragment;
import ca.mcgill.sel.ram.InteractionOperand;
import ca.mcgill.sel.ram.InteractionOperatorKind;
import ca.mcgill.sel.ram.OpaqueExpression;
import ca.mcgill.sel.ram.Parameter;
import ca.mcgill.sel.ram.ParameterValueMapping;
import ca.mcgill.sel.ram.RamFactory;
import ca.mcgill.sel.ram.StructuralFeature;
import ca.mcgill.sel.ram.Type;
import ca.mcgill.sel.ram.ValueSpecification;
import ca.mcgill.sel.ram.controller.ControllerFactory;
import ca.mcgill.sel.ram.controller.MessageController;
import ca.mcgill.sel.ram.expressions.ParserResult;
import ca.mcgill.sel.ram.expressions.ParserUtil;
import ca.mcgill.sel.ram.expressions.ParserUtil.TypeContainer;
import ca.mcgill.sel.ram.expressions.typechecking.TypeChecker.Types;
import ca.mcgill.sel.ram.ui.views.TextView;
import ca.mcgill.sel.ram.ui.views.handler.impl.TextViewHandler;

/**
 * The default handler for a view representing an Expression.
 * 
 * @author Rohit Verma
 */
public class ExpressionHandler extends TextViewHandler {

    private static final String IGNOREPARSER = "#";
    private static ParserUtil parserUtil = new ParserUtil();

    @Override
    public boolean shouldDismissKeyboard(TextView currentTextView) {
        EObject currentTextViewObject = currentTextView.getData();
        String currentText = currentTextView.getText();
        EStructuralFeature feature = currentTextView.getFeature();

        ValueSpecification value = parse(currentTextViewObject, currentText, feature);

        if (value != null) {
            MessageController controller = ControllerFactory.INSTANCE.getMessageController();
            controller.addExpressionValueSpecification(currentTextViewObject, feature, value);
            return true;
        }

        return false;
    }

    /**
     * This method parses string expression into an ValueSpecification.
     * 
     * @param currentTextViewObject
     *            This object corresponds to a text box in a TouchCORE gui.
     * @param currentText
     *            This is the String that contains expression that needs to be parsed.
     * @param feature
     *            This is the object that represents current class inside of which we are editing a sequence diagram.
     * 
     * @return
     *         ValueSpecification that string corresponds to
     */
    private static ValueSpecification parse(EObject currentTextViewObject, String currentText,
            EStructuralFeature feature) {
        Type expectedType = null;
        ValueSpecification result = null;
        // HACK: Be able to specify value that we don't have parser rules for yet
        if (currentText.startsWith(IGNOREPARSER)) {
            OpaqueExpression opaqueExpression = RamFactory.eINSTANCE.createOpaqueExpression();
            opaqueExpression.setBody(currentText);

            result = opaqueExpression;
        } else if (currentTextViewObject instanceof AssignmentStatement) {

            StructuralFeature structuralFeature =
                    ((AssignmentStatement) currentTextViewObject).getAssignTo();

            if (structuralFeature == null) {
                return result;
            }

            expectedType = structuralFeature != null ? structuralFeature.getType() : null;

            TypeContainer container = new TypeContainer(expectedType);

            result = parse(currentTextViewObject, currentText, container);
        } else if (currentTextViewObject instanceof ParameterValueMapping) {
            Parameter parameter = ((ParameterValueMapping) currentTextViewObject).getParameter();

            expectedType = parameter != null ? parameter.getType() : null;

            TypeContainer container = new TypeContainer(expectedType);

            result = parse(currentTextViewObject, currentText, container);
        } else if (currentTextViewObject instanceof InteractionOperand) {
            CombinedFragment combinedFragment = (CombinedFragment) currentTextViewObject.eContainer();
            InteractionOperatorKind operator = combinedFragment.getInteractionOperator();

            if (operator == InteractionOperatorKind.OPT || operator == InteractionOperatorKind.ALT) {
                TypeContainer container = new TypeContainer(Types.booleanType);

                result = parse(currentTextViewObject, currentText, container);
            } else if (operator == InteractionOperatorKind.CRITICAL) {
                OpaqueExpression opExp = RamFactory.eINSTANCE.createOpaqueExpression();
                opExp.setBody(currentText);
                if ("".equals(currentText) || "this".equals(currentText)) {
                    result = opExp;
                } else {
                    result = parserUtil.checkOpaqueExpression(opExp, currentTextViewObject);
                }
            } else {
                OpaqueExpression opaqueExpression = RamFactory.eINSTANCE.createOpaqueExpression();
                opaqueExpression.setBody(currentText);
                result = opaqueExpression;
            }
        }

        return result;
    }

    /**
     * Used to check if expression parsed and is consistent with expectedType.
     * 
     * @param textViewData
     *            This object is a reference to currentTextView.
     * @param textViewText
     *            This is a string that contains expression that we want to parse.
     * @param typeContainer
     *            It's used to contain different types that type checking is using.
     * @return
     *         boolean flag if expression was parsed successfully
     */
    private static ValueSpecification parse(EObject textViewData, String textViewText,
            TypeContainer typeContainer) {
        ParserResult result = parserUtil.parseAndTypeCheckExpression(textViewText, textViewData, typeContainer);

        if (result != null) {
            if (result.getErrors().isEmpty()) {
                return result.getModel();
            }
        }
        return null;
    }

    @Override
    public boolean processTapEvent(TapEvent tapEvent) {
        if (tapEvent.isDoubleTap()) {
            final TextView target = (TextView) tapEvent.getTarget();
            target.showKeyboard();
            return true;
        }
        return false;
    }

    @Override
    public void keyboardOpened(TextView textView) {
        // Leave current text as is.
    }
}