package ca.mcgill.sel.ram.ui.views.impact;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.notify.Notification;
import org.mt4j.util.MTColor;
import org.mt4j.util.math.Vector3D;

import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.commons.emf.util.EMFModelUtil;
import ca.mcgill.sel.core.COREFeatureImpactNode;
import ca.mcgill.sel.core.COREImpactNode;
import ca.mcgill.sel.core.COREModelReuse;
import ca.mcgill.sel.core.COREWeightedLink;
import ca.mcgill.sel.core.CorePackage;
import ca.mcgill.sel.core.LayoutElement;
import ca.mcgill.sel.ram.ui.components.RamExpendableComponent;
import ca.mcgill.sel.ram.ui.components.RamListComponent;
import ca.mcgill.sel.ram.ui.components.RamListComponent.Namer;
import ca.mcgill.sel.ram.ui.components.RamRectangleComponent;
import ca.mcgill.sel.ram.ui.utils.Colors;
import ca.mcgill.sel.ram.ui.views.TextView;
import ca.mcgill.sel.ram.ui.views.handler.IAbstractViewHandler;
import ca.mcgill.sel.ram.ui.views.impact.handler.HandlerFactoryImpactModel;
import ca.mcgill.sel.ram.ui.views.impact.handler.impl.FeatureImpactViewEditHandler;
import ca.mcgill.sel.ram.ui.views.impact.handler.impl.FeatureImpactViewHandler;

/**
 * Class used to visually representing the {@link COREFeatureImpactNode} in an Impact Model.
 *
 * @author Nishanth
 * @author Romain
 *
 */
public class FeatureImpactView extends ImpactModelElementView<FeatureImpactViewHandler> {

    private static final int NB_MAX_MAPPING_VISIBLE = 5;

    private FeatureImpactNodeTitleView titleRectangle;
    private RamListComponent<COREWeightedLink> scrollableListComponent;
    private RamExpendableComponent expandableComponent;

    /**
     * A namer that display a {@link COREWeightedLink} in a {@link RamRectangleComponent}.
     */
    private Namer<COREWeightedLink> weightedLinkNamer = new Namer<COREWeightedLink>() {
        @Override
        public RamRectangleComponent getDisplayComponent(final COREWeightedLink element) {
            COREModelReuse modelReuse =
                    EMFModelUtil.getRootContainerOfType(element, CorePackage.Literals.CORE_MODEL_REUSE);

            WeightedLinkLineView rectangle = new WeightedLinkLineView();
            rectangle.createWeightTextView(element);
            rectangle.createNameTextView(modelReuse.getReuse(), element.getFrom());

            if (handler instanceof FeatureImpactViewEditHandler) {
                rectangle.setHandler();
            }

            return rectangle;
        }

        @Override
        public String getSortingName(COREWeightedLink element) {
            return element.toString();
        }

        @Override
        public String getSearchingName(COREWeightedLink element) {
            return getSortingName(element);
        }
    };

    /**
     * Constructor called on the Feature Impact View .
     *
     * @param root The root {@link COREImpactNode} of this view
     * @param element - the associated {@link COREFeatureImpactNode}
     * @param impactDiagramView - the current scene
     */
    public FeatureImpactView(COREImpactNode root, COREFeatureImpactNode element,
            ImpactDiagramView<? extends IAbstractViewHandler> impactDiagramView) {
        super(root, element, impactDiagramView);

        super.setNoStroke(true);
        super.setNoFill(true);

        EMFEditUtil.addListenerFor(this.impactModelElement, this);

        List<COREWeightedLink> weightedLinks =
                ((COREFeatureImpactNode) this.impactModelElement).getWeightedLinks();

        // Create title of expendable component
        titleRectangle = new FeatureImpactNodeTitleView();

        // If there is no mappings, hide the weight of the feature and the icons of the expandable component
        if (!weightedLinks.isEmpty()) {
            titleRectangle.createWeightTextView(this.impactModelElement);
        }

        titleRectangle.setNameTextView(this.textView);
        titleRectangle.setNoFill(true);

        // Create hideable component of expendable component
        scrollableListComponent =
                new RamListComponent<COREWeightedLink>(weightedLinks, weightedLinkNamer, true);
        scrollableListComponent.setMaxNumberOfElements(NB_MAX_MAPPING_VISIBLE);

        // Create expendable component
        expandableComponent =
                new RamExpendableComponent(titleRectangle, scrollableListComponent);

        expandableComponent.setNoStroke(false);
        expandableComponent.setNoFill(false);
        expandableComponent.setStrokeColor(Colors.DEFAULT_ELEMENT_STROKE_COLOR);
        expandableComponent.setFillColor(Colors.DEFAULT_ELEMENT_FILL_COLOR);

        this.addChild(expandableComponent);

        // If there is no mappings, hide the weight of the feature and the icons of the expandable component
        if (weightedLinks.isEmpty()) {
            expandableComponent.setIconsVisible(false);
        }
    }

    /**
     * Constructor called on an existing element in the impact model.
     *
     * @param root The root {@link COREImpactNode} of this view
     * @param element the associated {@link COREFeatureImpactNode}
     * @param centerPoint - The point where the view is to be created.
     * @param impactDiagramView The scene associated to this element
     */
    public FeatureImpactView(COREImpactNode root, COREFeatureImpactNode element, Vector3D centerPoint,
            ImpactDiagramView<? extends IAbstractViewHandler> impactDiagramView) {
        this(root, element, impactDiagramView);
        if (centerPoint != null) {
            this.setPositionRelativeToParent(centerPoint);
        }
    }

    /**
     * Constructor called on an existing element in the impact model.
     *
     * @param root The root {@link COREImpactNode} of this view
     * @param element the associated {@link COREFeatureImpactNode}
     * @param layoutElement - The point where the view is to be created.
     * @param imDiagramView The scene associated to this element
     */
    public FeatureImpactView(COREImpactNode root, COREFeatureImpactNode element, LayoutElement layoutElement,
            ImpactDiagramView<? extends IAbstractViewHandler> imDiagramView) {
        this(root, element, imDiagramView);
        if (layoutElement != null) {
            this.setPositionRelativeToParent(new Vector3D(layoutElement.getX(), layoutElement.getY()));
        }
    }

    @Override
    public void handleNotification(Notification notification) {
        if (notification.getFeature() == CorePackage.Literals.CORE_FEATURE_IMPACT_NODE__WEIGHTED_LINKS) {
            COREWeightedLink weightedLink;
            switch (notification.getEventType()) {
                case Notification.ADD:
                    weightedLink = (COREWeightedLink) notification.getNewValue();
                    if (weightedLink.getTo() == this.impactModelElement) {
                        if (!titleRectangle.hasWeightTextView()) {
                            titleRectangle.createWeightTextView(this.impactModelElement);
                        }

                        scrollableListComponent.addElement(weightedLink);
                        expandableComponent.setIconsVisible(true);
                    }
                    break;
                case Notification.REMOVE:
                    weightedLink = (COREWeightedLink) notification.getOldValue();
                    if (weightedLink.getTo() == this.impactModelElement) {
                        scrollableListComponent.removeElement(weightedLink);
                        // If there is no mappings, hide the weight of the feature and the icons of the expandable
                        // component
                        if (scrollableListComponent.size() == 0) {
                            expandableComponent.setIconsVisible(false);
                            titleRectangle.removeWeightTextView();
                        }
                    }
                    break;
            }
        }
        super.handleNotification(notification);
    }

    /**
     * Function used to return the textView associated with the view.
     *
     * @return textView.
     */
    @Override
    public TextView getTextView() {
        return titleRectangle.getNameTextView();
    }

    /**
     * Function used to return the weightedLink associated with the view.
     *
     * @return weightedLinks.
     */
    public Collection<COREWeightedLink> getWeightedLinks() {
        return this.scrollableListComponent.getElementMap().keySet();
    }

    /**
     * Function used to return the map of weightedLink associated with their corresponding view.
     *
     * @return the map of weightedLink associated with their corresponding view.
     */
    public Map<COREWeightedLink, RamRectangleComponent> getWeightedLinkMap() {
        return this.scrollableListComponent.getElementMap();
    }

    @Override
    public void changeColor(MTColor color) {
        this.expandableComponent.getFirstLine().setNoFill(false);
        this.expandableComponent.getFirstLine().setFillColor(color);
        this.expandableComponent.getHideableContainer().setNoFill(false);
        this.expandableComponent.getHideableContainer().setFillColor(color);

        // for (RamRectangleComponent rectangle : this.scrollableListComponent.getElementMap().values()) {
        // rectangle.setNoFill(false);
        // rectangle.setFillColor(color);
        // }
    }

    @Override
    public void changeColorToDefault() {
        this.expandableComponent.getFirstLine().setNoFill(false);
        this.expandableComponent.getFirstLine().setFillColor(Colors.DEFAULT_ELEMENT_FILL_COLOR);
        this.expandableComponent.getHideableContainer().setNoFill(false);
        this.expandableComponent.getHideableContainer().setFillColor(Colors.DEFAULT_ELEMENT_FILL_COLOR);

        // for (RamRectangleComponent rectangle : this.scrollableListComponent.getElementMap().values()) {
        // rectangle.setNoFill(false);
        // rectangle.setFillColor(Colors.DEFAULT_ELEMENT_FILL_COLOR);
        // }
    }

    @Override
    public void destroy() {
        EMFEditUtil.removeListenerFor(this.impactModelElement, this);

        super.destroy();
    }

    @Override
    public void setHandler(FeatureImpactViewHandler handler) {
        // Set the handlers for the TextView (Contribution) and WeightedLink
        if (handler instanceof FeatureImpactViewEditHandler) {
            this.titleRectangle.setHandler();

            for (RamRectangleComponent rectangleComponent : scrollableListComponent.getElementMap().values()) {
                if (rectangleComponent instanceof WeightedLinkLineView) {
                    WeightedLinkLineView weightedLinkLineView =
                            (WeightedLinkLineView) rectangleComponent;

                    weightedLinkLineView.setHandler();
                }
            }

            scrollableListComponent.registerTapAndHoldProcessor();
            scrollableListComponent
                    .registerListener(HandlerFactoryImpactModel.INSTANCE.getWeightedLinkEditHandler());
        } else {
            // The handler is for the select Mode. So we set the handler in select mode for the weightedLink.
            scrollableListComponent.registerTapAndHoldProcessor();
            scrollableListComponent
                    .registerListener(HandlerFactoryImpactModel.INSTANCE.getWeightedLinkSelectHandler());
        }

        super.setHandler(handler);
    }
}
