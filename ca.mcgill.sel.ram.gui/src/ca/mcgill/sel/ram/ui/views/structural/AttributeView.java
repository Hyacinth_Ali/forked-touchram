package ca.mcgill.sel.ram.ui.views.structural;

import org.eclipse.emf.common.notify.Notification;
import org.mt4j.components.MTComponent;
import org.mt4j.util.font.IFont;

import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.commons.emf.util.EMFModelUtil;
import ca.mcgill.sel.core.COREArtefact;
import ca.mcgill.sel.core.CORECIElement;
import ca.mcgill.sel.core.CorePackage;
import ca.mcgill.sel.core.util.COREArtefactUtil;
import ca.mcgill.sel.core.util.COREModelUtil;
import ca.mcgill.sel.ram.Aspect;
import ca.mcgill.sel.ram.Attribute;
import ca.mcgill.sel.ram.RAMVisibilityType;
import ca.mcgill.sel.ram.RamPackage;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.RamRectangleComponent;
import ca.mcgill.sel.ram.ui.components.RamTextComponent;
import ca.mcgill.sel.ram.ui.components.listeners.UINotifyChangedListener;
import ca.mcgill.sel.ram.ui.layouts.HorizontalLayoutVerticallyCentered;
import ca.mcgill.sel.ram.ui.utils.Colors;
import ca.mcgill.sel.ram.ui.utils.Fonts;
import ca.mcgill.sel.ram.ui.utils.GraphicalUpdater;
import ca.mcgill.sel.ram.ui.utils.Strings;
import ca.mcgill.sel.ram.ui.views.MappingCardinalityTextView;
import ca.mcgill.sel.ram.ui.views.TextView;
import ca.mcgill.sel.ram.ui.views.handler.HandlerFactory;
import ca.mcgill.sel.ram.ui.views.handler.IHandled;
import ca.mcgill.sel.ram.ui.views.structural.handler.IAttributeViewHandler;

/**
 * Shows the type and name of an attribute. Displays the visibility of an attribute always as private.
 * 
 * @author mschoettle
 */
public class AttributeView extends RamRectangleComponent implements UINotifyChangedListener,
        IHandled<IAttributeViewHandler> {

    /**
     * The field for mapping cardinalities.
     */
    protected TextView mappingCardinalityField;
    
    private RamTextComponent visibilityField;
    private TextView nameField;
    private TextView typeField;

    private Attribute attribute;

    private GraphicalUpdater graphicalUpdater;

    private IAttributeViewHandler handler;
    
    private COREArtefact artefact;

    /**
     * Creates an attribute view for the given {@link Attribute}.
     * 
     * @param classView
     *            the {@link ClassView} which contains this view.
     * @param attribute
     *            the {@link Attribute} that should be represented
     */
    public AttributeView(ClassifierView<?> classView, Attribute attribute) {
        this.attribute = attribute;

        String symbol = (attribute.getVisibility() == RAMVisibilityType.PUBLIC) 
                ? Strings.SYMBOL_PUBLIC : Strings.SYMBOL_PROTECTED;
        visibilityField = new RamTextComponent(symbol);
        visibilityField.setBufferSize(Cardinal.SOUTH, 0);
        addChild(visibilityField);

        typeField = new TextView(attribute, RamPackage.Literals.ATTRIBUTE__TYPE);
        typeField.setBufferSize(Cardinal.SOUTH, 0);
        typeField.setBufferSize(Cardinal.WEST, 0);
        addChild(typeField);
        typeField.setHandler(HandlerFactory.INSTANCE.getTextViewHandler());

        nameField = new TextView(attribute, RamPackage.Literals.NAMED_ELEMENT__NAME);
        nameField.setBufferSize(Cardinal.SOUTH, 0);
        nameField.setBufferSize(Cardinal.WEST, 0);
        nameField.setUniqueName(true);
        addChild(nameField);
        nameField.setHandler(HandlerFactory.INSTANCE.getAttributeNameHandler());

        setLayout(new HorizontalLayoutVerticallyCentered(0));

        setUnderlined(attribute.isStatic());
        updateStyle();

        EMFEditUtil.addListenerFor(attribute, this);
        Aspect aspect = EMFModelUtil.getRootContainerOfType(attribute, RamPackage.Literals.ASPECT);
        graphicalUpdater = RamApp.getApplication().getGraphicalUpdaterForModel(aspect);
        graphicalUpdater.addGUListener(attribute, this);
        
        // Add a listener to listen to any partiality change in the core metamodel
        artefact = COREArtefactUtil.getReferencingExternalArtefact(attribute);
        EMFEditUtil.addListenerFor(artefact, this);
        
        updateMappingCardinality();
    }
    
    @Override
    public void destroy() {
        super.destroy();

        graphicalUpdater.removeGUListener(attribute, this);
        EMFEditUtil.removeListenerFor(attribute, this);
        
        if (artefact != null) {
            EMFEditUtil.removeListenerFor(artefact, this);
        }
    }

    /**
     * Returns the {@link Attribute} that this view represents.
     * 
     * @return the {@link Attribute} represented by this view
     */
    public Attribute getAttribute() {
        return attribute;
    }

    @Override
    public IAttributeViewHandler getHandler() {
        return handler;
    }

    @Override
    public void handleNotification(Notification notification) {
        // this gets called for every view (as there is only one item provider)
        // therefore an additional check is necessary
        if (notification.getNotifier() == attribute) {
            if (notification.getFeature() == RamPackage.Literals.STRUCTURAL_FEATURE__STATIC) {
                if (notification.getEventType() == Notification.SET) {
                    boolean newValue = notification.getNewBooleanValue();

                    setUnderlined(newValue);
                }
                // TODO Re-add this after we can check for reference
            //} else if (notification.getFeature() == CorePackage.Literals.CORE_MODEL_ELEMENT__REFERENCE) {
            //    updateStyle();
            } else if (notification.getFeature() == RamPackage.Literals.MAPPABLE_ELEMENT__PARTIALITY) {
                updateMappingCardinality();
            } 
        } else if (notification.getNotifier() == artefact) {
            if (notification.getFeature() == CorePackage.Literals.CORE_ARTEFACT__CI_ELEMENTS) {
                CORECIElement ciElement = (CORECIElement) notification.getNewValue();
                if (ciElement == null) {
                    CORECIElement ciOldElement = (CORECIElement) notification.getOldValue();
                    if (ciOldElement.getModelElement() == attribute) {
                        updateMappingCardinality();
                    }
                } else if (ciElement.getModelElement() == attribute) {
                    updateMappingCardinality();
                }
            } 
        }
    }

    @Override
    public void setUnderlined(boolean underlined) {
        for (int i = 1; i < getChildCount(); i++) {
            MTComponent child = getChildByIndex(i);
            RamRectangleComponent text = (RamRectangleComponent) child;
            text.setUnderlined(underlined);
        }
    }

    @Override
    public void setHandler(IAttributeViewHandler handler) {
        this.handler = handler;
    }

    /**
     * Returns the name field view.
     * 
     * @return the name field view
     */
    public TextView getNameField() {
        return nameField;
    }

    /**
     * Updates the style of this view depending on whether the attribute is a reference or not.
     */
    @Override
    protected void updateStyle() {
        IFont font;
        
        if (COREModelUtil.isReference(attribute)) {
            font = Fonts.REFERENCE_FONT;
            setNoFill(false);
            setFillColor(Colors.ATTRIBUTE_VIEW_REFERENCE_FILL_COLOR);
        } else {
            font = Fonts.DEFAULT_FONT;
            setNoFill(false);
            setFillColor(Colors.ATTRIBUTE_VIEW_DEFAULT_FILL_COLOR);
        }
        
        visibilityField.setFont(font);
        typeField.setFont(font);
        nameField.setFont(font);
    }

    /**
     * Update the mappingCardinalityField and display it if it wasn't already there.
     * If the {@link CORECIElement} associated do no longer exists, we just remove the mappingCardinalityField.
     */
    private void updateMappingCardinality() {
        CORECIElement ciElement = COREArtefactUtil.getCIElementFor(attribute);

        if (mappingCardinalityField == null && ciElement != null) {

            mappingCardinalityField = new MappingCardinalityTextView(ciElement);
            mappingCardinalityField.setBufferSize(Cardinal.SOUTH, 0);
            mappingCardinalityField.setBufferSize(Cardinal.WEST, 0);
            mappingCardinalityField.setPlaceholderText(Strings.PH_CARDINALITY);
            mappingCardinalityField.setFont(Fonts.FONT_CLASS_NAME_REFERENCE);
            addChild(mappingCardinalityField);

            mappingCardinalityField.setHandler(HandlerFactory.INSTANCE.getMappingCardinalityHandler());
        } else if (ciElement == null && mappingCardinalityField != null) {
            removeChild(mappingCardinalityField);
            mappingCardinalityField = null;
        }
    }
    
}
