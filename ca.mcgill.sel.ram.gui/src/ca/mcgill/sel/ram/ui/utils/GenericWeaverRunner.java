package ca.mcgill.sel.ram.ui.utils;

import org.eclipse.emf.ecore.EObject;
import org.mt4j.sceneManagement.transition.SlideTransition;

import ca.mcgill.sel.core.COREExternalArtefact;
import ca.mcgill.sel.core.COREModelComposition;
import ca.mcgill.sel.core.weaver.COREWeaver;
import ca.mcgill.sel.core.weaver.util.WeaverListener;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.RamPopup;
import ca.mcgill.sel.ram.ui.components.RamPopup.PopupType;
import ca.mcgill.sel.ram.ui.components.navigationbar.NavigationBar;
import ca.mcgill.sel.ram.ui.scenes.SceneCreationAndChangeFactory;
import ca.mcgill.sel.ram.ui.scenes.SceneCreationAndChangeFactory.CurrentMode;

/**
 * The awesome WeaverRunner.
 * 
 * @author mschoettle
 * @author oalam
 * @author Joerg
 */
public final class GenericWeaverRunner implements WeaverListener<EObject> {

    /**
     * The mode that should be used to weave.
     */
    public enum WeaveMode {
        /**
         * All declared compositions will be woven into the artefact and model.
         */
        ALL,

        /**
         * Only one composition will be woven into the artefact and model.
         */
        SINGLE,
    };

    private static GenericWeaverRunner instance;

    private RamPopup weavePopup;
    private COREModelComposition composition;
    private COREExternalArtefact baseArtefact;
    private EObject baseModel;

    /**
     * Prevent composition.
     */
    private GenericWeaverRunner() {
    }

    /**
     * Get the singleton instance of WeaverRunner.
     *
     * @return the instance.
     */
    public static GenericWeaverRunner getInstance() {
        if (instance == null) {
            instance = new GenericWeaverRunner();
        }
        return instance;
    }

    /**
     * Runs the weaver in the given mode.
     *
     * @param weaveMode mode to use for weaving.
     */
    private void runWeaver(final WeaveMode weaveMode) {
        final EObject base = baseArtefact.getRootModelElement();
        this.baseModel = base;
        base.eSetDeliver(false);
        new Thread(new Runnable() {
            @Override
            public void run() {
                // calling the java weaver
                COREWeaver cweaver = COREWeaver.getInstance();
                switch (weaveMode) {
                    case ALL:
                        // cweaver.weaveModels(new ArrayList<Aspect>(), WeaverRunner.this);
                        break;
                    case SINGLE:
                        cweaver.weaveSingle(baseArtefact, composition, GenericWeaverRunner.this);
                        break;
                }
            }
        }).start();

    }

   /**
     * Weave only a single aspect into the current one.
     * 
     * @param base the base model that contains the composition that should be woven
     * @param currentComposition
     *            the model composition of the aspect that should be woven into the current one
     */
    public void weaveSingle(COREExternalArtefact base, COREModelComposition currentComposition) {
        if (currentComposition != null) {
            this.baseArtefact = base;
            this.composition = currentComposition;
            runWeaver(WeaveMode.SINGLE);
            
            NavigationBar.getInstance().weavingInterfaceMode();
        }
    }

    @Override
    public void weavingStarted() {
        if (weavePopup != null) {
            weavePopup.destroy();
        }
        RamApp.getApplication().invokeLater(new Runnable() {
            @Override
            public void run() {
                weavePopup = new RamPopup(Strings.POPUP_WEAVING, true);
                RamApp.getActiveScene().displayPopup(weavePopup);
            }
        });
    }

    @Override
    public void weavingFinished(final COREExternalArtefact resultArtefact, EObject resultModel) {
        if (resultModel == null) {
            finalizeWeaving();
            return;
        }
        // display the model to the user; transition to the left
        RamApp.getApplication().invokeLater(new Runnable() {
            @Override
            public void run() {
                finalizeWeaving();
                RamApp.getActiveScene().setTransition(new SlideTransition(RamApp.getApplication(), 700, true));
                SceneCreationAndChangeFactory.getFactory().navigateToModel(resultModel, CurrentMode.EDIT);
            }
        });
    }

    @Override
    public void weavingFailed(Exception e) {
        // Get error message
        final StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(Strings.POPUP_ERROR_WEAVING);

        if (e != null && e.getLocalizedMessage() != null) {
            stringBuffer.append(" " + e.getLocalizedMessage());
            e.printStackTrace();
        }

        RamApp.getApplication().invokeLater(new Runnable() {
            @Override
            public void run() {
                finalizeWeaving();
                RamApp.getActiveScene().displayPopup(stringBuffer.toString(), PopupType.ERROR);
            }
        });

    }

    /**
     * Destroy the "weaving..." popup and reactivate notifications for the base model.
     */
    private void finalizeWeaving() {
        baseModel.eSetDeliver(true);
        if (weavePopup != null) {
            weavePopup.destroy();
            weavePopup = null;
        }
    }

}
