package ca.mcgill.sel.ram.ui.scenes.handler.impl;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.mt4j.sceneManagement.transition.SlideTransition;

import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.core.COREArtefact;
import ca.mcgill.sel.core.COREConcern;
import ca.mcgill.sel.core.COREConfiguration;
import ca.mcgill.sel.core.COREExternalArtefact;
import ca.mcgill.sel.core.COREFeature;
import ca.mcgill.sel.core.COREFeatureModel;
import ca.mcgill.sel.core.COREModelExtension;
import ca.mcgill.sel.core.COREPerspective;
import ca.mcgill.sel.core.COREScene;
import ca.mcgill.sel.core.CoreFactory;
import ca.mcgill.sel.core.controller.COREControllerFactory;
import ca.mcgill.sel.core.controller.ImpactModelElementController;
import ca.mcgill.sel.core.util.COREArtefactUtil;
import ca.mcgill.sel.core.util.COREModelUtil;
import ca.mcgill.sel.core.util.CoreResourceImpl;
import ca.mcgill.sel.core.weaver.COREWeaver;
import ca.mcgill.sel.ram.Aspect;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.ConfirmPopup;
import ca.mcgill.sel.ram.ui.scenes.DisplayConcernEditScene;
import ca.mcgill.sel.ram.ui.scenes.SceneCreationAndChangeFactory;
import ca.mcgill.sel.ram.ui.scenes.SceneCreationAndChangeFactory.CurrentMode;
import ca.mcgill.sel.ram.ui.scenes.handler.IConcernEditSceneHandler;
import ca.mcgill.sel.ram.ui.utils.GUIConstants;
import ca.mcgill.sel.ram.ui.utils.ImpactModelUtil;
import ca.mcgill.sel.ram.ui.utils.Strings;
import ca.mcgill.sel.ram.util.RAMModelUtil;

/**
 * Handler implementation to catch actions on the concern scene in edit mode.
 *
 * @author Nishanth
 */
public class ConcernEditSceneHandler extends DefaultRamSceneHandler implements IConcernEditSceneHandler {

    @Override
    public void switchToHome(final DisplayConcernEditScene scene) {
        boolean isSaveNeeded = EMFEditUtil.getCommandStack(scene.getConcern()).isSaveNeeded();
        if (isSaveNeeded) {
            scene.showCloseConfirmPopup(scene, new ConfirmPopup.SelectionListener() {

                @Override
                public void optionSelected(int selectedOption) {
                    if (selectedOption == ConfirmPopup.YES_OPTION) {
                        save(scene.getConcern());
                        doSwitchToHome(scene);
                    } else if (selectedOption == ConfirmPopup.NO_OPTION) {
                        doSwitchToHome(scene);
                    }
                }
            });
        } else {
            doSwitchToHome(scene);
        }
    }

    /**
     * Performs the switching to home. Unloads the resources and switches the scene to the background scene.
     *
     * @param scene the current scene
     */
    @SuppressWarnings("static-method")
    private void doSwitchToHome(DisplayConcernEditScene scene) {
        scene.unLoadAllResources();
        scene.setTransition(new SlideTransition(RamApp.getApplication(), 700, false));
        scene.getApplication().switchToBackground(scene);
        scene.getApplication().destroySceneAfterTransition(scene);
    }

    @Override
    public void createImpactModel(final DisplayConcernEditScene scene) {
        final COREConcern concern = scene.getConcern();

        scene.setTransition(new SlideTransition(RamApp.getApplication(), 700, true));

        if (concern.getImpactModel() == null) {
            COREControllerFactory.INSTANCE.getConcernController().createImpactModel(concern,
                ImpactModelUtil.getUniqueGoalName(null), scene.getWidth() / 2, GUIConstants.ROOT_GOAL_INITIAL_HEIGHT);
        } else {
            ImpactModelElementController controller = COREControllerFactory.INSTANCE.getImpactModelElementController();
            controller.createRootImpactModelElement(concern.getImpactModel(), ImpactModelUtil.getUniqueGoalName(
                    concern.getImpactModel()), RamApp.getApplication().getWidth() / 2, GUIConstants
                    .ROOT_GOAL_INITIAL_HEIGHT);
        }
    }

    @Override
    public void createAspect(DisplayConcernEditScene scene) {

        Aspect aspect = RAMModelUtil.createAspect(Strings.DEFAULT_ASPECT_NAME, scene.getConcern());

        COREControllerFactory.INSTANCE.getFeatureController().addArtefactToConcern(scene.getConcern(),
                COREArtefactUtil.getReferencingExternalArtefact(aspect));

        scene.setTransition(new SlideTransition(RamApp.getApplication(), 700, true));

        SceneCreationAndChangeFactory.getFactory().navigateToModel(aspect, null);

    }

    @Override
    public void deleteModel(DisplayConcernEditScene scene, COREArtefact model) {
        COREConcern concern = scene.getConcern();

        // Remove aspect from the concern
        concern.getArtefacts().remove(model);

        // Remove realizations for this aspect
        System.out.println("This should never be called");
//        for (COREFeature feature : concern.getFeatureModel().getFeatures()) {
//            if (feature.getRealizedBy().contains(model)) {
//                feature.getRealizedBy().remove(model);
//            }
//        }

        // Remove instantiations for this aspect
        for (COREArtefact currentModel : concern.getArtefacts()) {
            List<COREModelExtension> toRemove = new ArrayList<>();
            
            for (COREModelExtension modelExtension : currentModel.getModelExtensions()) {
                if (modelExtension.getSource() == model) {
                    toRemove.add(modelExtension);
                }
            }
            
            currentModel.getModelExtensions().removeAll(toRemove);
        }

        // Ensure that the model is in a resource, only then delete it.
        if (model.eResource() != null) {
            File fileToBeDeleted = new File(model.eResource().getURI().toFileString());
            fileToBeDeleted.delete();
        }

        COREModelUtil.unloadEObject(model);

        // save everything
        save(scene.getConcern());
    }
    
    @Override
    public void weaveSelectedFeatures(COREConcern c, COREPerspective p, List<COREFeature> features) {
        // TODO Auto-generated method stub
        COREConfiguration configuration = CoreFactory.eINSTANCE.createCOREConfiguration();
        configuration.getSelected().addAll(features);
        COREExternalArtefact newArtefact = COREWeaver.getInstance().weaveConcern(c, configuration);
        
        COREScene newScene = CoreFactory.eINSTANCE.createCOREScene();
        newScene.setPerspectiveName(p.getName());
        newArtefact.setScene(newScene);
        c.getTemporaryArtefacts().add(newArtefact);
        
        RamApp.getActiveScene().setTransition(new SlideTransition(RamApp.getApplication(), 700, true));
        SceneCreationAndChangeFactory.getFactory().navigateToModel(newArtefact.getRootModelElement(), CurrentMode.EDIT);
    }

}
