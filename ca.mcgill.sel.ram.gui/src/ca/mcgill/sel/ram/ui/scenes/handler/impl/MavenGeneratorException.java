package ca.mcgill.sel.ram.ui.scenes.handler.impl;

public class MavenGeneratorException extends RuntimeException {

    /** 
     * Serialization id.
     */
    private static final long serialVersionUID = 1L;

    MavenGeneratorException(String cause) {
        super(cause);
    }
}
