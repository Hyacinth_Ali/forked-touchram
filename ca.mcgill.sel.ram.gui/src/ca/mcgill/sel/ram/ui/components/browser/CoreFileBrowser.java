package ca.mcgill.sel.ram.ui.components.browser;

import java.io.File;

import org.eclipse.emf.common.util.URI;

import ca.mcgill.sel.commons.emf.util.ResourceManager;
import ca.mcgill.sel.core.COREConcern;
import ca.mcgill.sel.core.util.COREModelUtil;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.RamPopup.PopupType;
import ca.mcgill.sel.ram.ui.components.browser.RamFileBrowser.RamFileBrowserType;
import ca.mcgill.sel.ram.ui.components.browser.interfaces.CoreFileBrowserListener;
import ca.mcgill.sel.ram.ui.components.browser.interfaces.RamFileBrowserListener;
import ca.mcgill.sel.ram.ui.utils.GUIConstants;
import ca.mcgill.sel.ram.ui.utils.Strings;
import ca.mcgill.sel.ram.util.Constants;

/**
 * Utility class to load a core file with a RamFileBrowser.
 *
 * @author tdimeco
 */
public final class CoreFileBrowser {

    private static File lastSharedFolder = new File(GUIConstants.DIRECTORY_MODELS).getAbsoluteFile();

    /**
     * Private constructor.
     */
    private CoreFileBrowser() {
    }

    /**
     * Load a core file with the file browser.
     * Once the file is selected, it will either load the concern from the reusable concern library
     * or locally.
     *
     * @param listener The callback to handle the loaded core file
     * @param getLocalConcern Whether to get the concern locally or in the reusable concern library
     */
    public static void loadCoreFile(final CoreFileBrowserListener listener, final boolean getLocalConcern) {
        RamFileBrowser browser = new RamFileBrowser(RamFileBrowserType.LOAD, Constants.CORE_FILE_EXTENSION,
                lastSharedFolder);
        // TODO: Threaded loading can cause MT4J to crash during drawAndUpdate
        // browser.setCallbackThreaded(true);
        browser.addFileBrowserListener(new RamFileBrowserListener() {

            @Override
            public void fileSelected(File file, RamFileBrowser fileBrowser) {
                try {
                    if (file.canRead()) {
                        lastSharedFolder = file.getParentFile();
                        COREConcern concern;
                        if (getLocalConcern) {
                            concern = COREModelUtil.getLocalConcern(URI.createFileURI(file.toString()),
                                    RamApp.getActiveAspectScene().getArtefact().getCoreConcern().eResource().getURI());
                        } else {
                            concern = (COREConcern) ResourceManager.loadModel(file);
                        }
                        listener.concernLoaded(concern);
                    }
                    // CHECKSTYLE:IGNORE IllegalCatch FOR 1 LINES: Need to catch RuntimeException
                } catch (Exception e) {
                    RamApp.getActiveScene().displayPopup(Strings.POPUP_ERROR_FILE_LOAD_SYNTAX, PopupType.ERROR);
                    e.printStackTrace();
                }
            }
        });
        browser.display();
    }

    /**
     * Sets the initial folder that is displayed first by the file browser.
     * The path needs to be a directory and readable.
     *
     * @param path the new path
     */
    public static void setInitialFolder(String path) {
        File newPath = new File(path);

        if (newPath.isDirectory() && newPath.canRead()) {
            lastSharedFolder = newPath;
        }
    }
}
