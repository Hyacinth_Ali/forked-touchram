package ca.mcgill.sel.ram.ui.perspective;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EObject;

import ca.mcgill.sel.classdiagram.CdmPackage;
import ca.mcgill.sel.core.COREPerspective;
import ca.mcgill.sel.environmentmodel.Actor;
import ca.mcgill.sel.environmentmodel.EmPackage;
import ca.mcgill.sel.environmentmodel.EnvironmentModel;
import ca.mcgill.sel.environmentmodel.MessageDirection;
import ca.mcgill.sel.environmentmodel.language.controller.EnvironmentModelControllerFactory;
import ca.mcgill.sel.usecases.UcPackage;
/**
 * This class contains methods, each calls a corresponding environment model language action to create elements.
 * @author hyacinth
 *
 */
public class EnvironmentModelFacade {
	
	/**
	 * Main facade action to create an actor in an environment model.
	 * 
	 * @param perspective
	 * @param metaclass
	 * @param otherOwner
	 * @param toCreateRoleName
	 * @param name
	 * @param lowerBound
	 * @param upperBound
	 * @param communicationUpperBound
	 * @param communicationLowerBound
	 * @return
	 */
	public static EObject createEMActorAndOthers(COREPerspective perspective, EObject metaclass, EObject otherOwner,
			String toCreateRoleName, String name, int lowerBound, int upperBound, int communicationUpperBound,
			int communicationLowerBound) {
		
		EObject newElement = null;
		
		if (metaclass.equals(EmPackage.eINSTANCE.getActor())) {
			EnvironmentModelFacade.createActor(perspective, toCreateRoleName, otherOwner, name, 0, 10, 0, 10);
		} else if (metaclass.equals(UcPackage.eINSTANCE.getActor())) {
			// TODO: zero is used because environment model editor doesn't exist. This needs to be updated after creating the editor.
			newElement = UseCaseFacade.createActor(perspective, toCreateRoleName, otherOwner, name, 0, 0);
		} else if (metaclass.equals(CdmPackage.eINSTANCE.getClass_())) {
			newElement = ClassDiagramFacade.createClass(perspective, otherOwner, toCreateRoleName, name, false, 0, 0);
		}

		return newElement;
	}

	/**
	 * Main facade action to create a message in an environment model.
	 * 
	 * @param perspective
	 * @param metaclass
	 * @param owner
	 * @param toCreateRoleName
	 * @param name
	 * @param em
	 * @param messageDirection
	 * @return
	 */
	public static EObject createEMMessageAndOthers(COREPerspective perspective, EObject metaclass, EObject owner,
			String toCreateRoleName, String name, EnvironmentModel em, MessageDirection messageDirection) {
		
		EObject newElement = null;
		
		if (metaclass.equals(EmPackage.eINSTANCE.getMessage())) {
			EnvironmentModelControllerFactory.INSTANCE.getEnvironmentModelController().createMessage(em, (Actor) owner, name, messageDirection);
		} else if (metaclass.equals(UcPackage.eINSTANCE.getActor())) {
			// TODO: zero is used because environment model editor doesn't exist. This needs to be updated after creating the editor.
			newElement = UseCaseFacade.createActor(perspective, toCreateRoleName, owner, name, 0, 0);
		} else if (metaclass.equals(CdmPackage.eINSTANCE.getClass_())) {
			newElement = ClassDiagramFacade.createClass(perspective, owner, toCreateRoleName, name, false, 0, 0);
		}

		return newElement;
	}
	
	/**
	 * Creates a new actor in environment model.
	 * @param perspective
	 * @param roleName - the role name of the model to contain the new element.
	 * @param otherOwner - the container of the corresponding element to be mapped with the new actor.
	 * @param name - the name of the new actor element
	 * @return
	 */
	protected static EObject createActor(COREPerspective perspective, String roleName, EObject otherOwner, String name, int lowerBound, int upperBound, 
    		int communicationLowerBound, int communicationUpperBound) {
		EObject actorOwner = FacadeAction.getOwner(perspective, otherOwner, roleName);
        
        if (actorOwner == null) {
            return null;
        }

        EnvironmentModel ownerActor = (EnvironmentModel) actorOwner;
        List<EObject> elements = new ArrayList<EObject>();
        List<EObject> initialElements = new ArrayList<EObject>();
        initialElements.addAll(ownerActor.getActors());
        EnvironmentModelControllerFactory.INSTANCE.getEnvironmentModelController().createActor(ownerActor, name, lowerBound, upperBound, communicationLowerBound, communicationUpperBound);

        return elements.get(0);
	}
	
	/**
	 * Creates a new message in environment model.
	 * @param perspective 
	 * @param roleName - the role name of the model to contain the new element.
	 * @param otherOwner - the container of the corresponding element to be mapped with the new actor.
	 * @param name - the name of the new message
	 * @return
	 */
	protected static EObject createMessage(COREPerspective perspective, String roleName, EnvironmentModel em, EObject otherOwner, String name, MessageDirection messageDirection) {
		EObject owner = FacadeAction.getOwner(perspective, otherOwner, roleName);
        
        if (owner == null) {
            return null;
        }

        Actor messageOwner = (Actor) owner;
        List<EObject> elements = new ArrayList<EObject>();
        List<EObject> initialElements = new ArrayList<EObject>();
        initialElements.addAll(messageOwner.getMessages());
        EnvironmentModelControllerFactory.INSTANCE.getEnvironmentModelController().createMessage(em, messageOwner, name, messageDirection);

        return elements.get(0);
	}

}
