package ca.mcgill.sel.ram.ui.perspective.controller;

/**
 * The class represents all the possible exceptions that may occur due to perspective actions.
 * @author Hyacinth Ali
 *
 */
public class PerspectiveException extends RuntimeException {
    
    private static final long serialVersionUID = -5633915762703837868L;
    
    public PerspectiveException(String errorMessage) {
        super(errorMessage);
    }

}
