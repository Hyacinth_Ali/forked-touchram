package ca.mcgill.sel.ram.ui.perspective;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.emf.ecore.EObject;

import ca.mcgill.sel.classdiagram.CdmPackage;
import ca.mcgill.sel.classdiagram.ClassDiagram;
import ca.mcgill.sel.core.COREConcern;
import ca.mcgill.sel.core.COREExternalArtefact;
import ca.mcgill.sel.core.COREFeature;
import ca.mcgill.sel.core.CORELanguageAction;
import ca.mcgill.sel.core.CORELanguageElement;
import ca.mcgill.sel.core.CORELanguageElementMapping;
import ca.mcgill.sel.core.COREModelElementMapping;
import ca.mcgill.sel.core.COREPerspective;
import ca.mcgill.sel.core.CORERelationship;
import ca.mcgill.sel.core.COREScene;
import ca.mcgill.sel.core.MappingEnd;
import ca.mcgill.sel.core.perspective.COREPerspectiveUtil;
import ca.mcgill.sel.core.util.COREArtefactUtil;
import ca.mcgill.sel.ram.ui.perspective.controller.BasePerspectiveController;
import ca.mcgill.sel.usecases.UcPackage;
import ca.mcgill.sel.usecases.UseCase;
import ca.mcgill.sel.usecases.UseCaseModel;

/**
 * This class provides generic helper methods for a perspective designer.
 * for java invoke method: value of first parameter should be null if the method is a static method otherwise,
 * use the object of the controller class
 * The algorithm in this class doesn't support controller with overloaded methods and methods with more than ten
 * arguments.
 * 
 * @author hyacinthali
 *
 */
public class PerspectiveHelper {

    /**
     * Singleton instance variable.
     */
    private static PerspectiveHelper instance;

    /**
     * The single instance variable getter.
     * 
     * @return the instance
     */
    public static PerspectiveHelper getInstance() {
        if (instance == null) {
            instance = new PerspectiveHelper();
        }
        return instance;
    } 

    @SuppressWarnings("static-method")
    private Object createOtherElement(Class<?> controllerClass, Method m, List<Object> parameters)
            throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, InstantiationException,
            NoSuchMethodException, SecurityException {
        Object elementOther = null;
        switch (parameters.size()) {
            case 0:
                elementOther = m.invoke(controllerClass.getConstructor().newInstance(), null);
                break;
            case 1:
                elementOther = m.invoke(controllerClass.getConstructor().newInstance(), parameters.get(0));
                break;

            case 2:
                elementOther = m.invoke(controllerClass.getConstructor().newInstance(), parameters.get(0),
                        parameters.get(1));
                break;

            case 3:
                elementOther = m.invoke(controllerClass.getConstructor().newInstance(), parameters.get(0),
                        parameters.get(1), parameters.get(2));
                break;

            case 4:
                elementOther = m.invoke(controllerClass.getConstructor().newInstance(), parameters.get(0),
                        parameters.get(1), parameters.get(2), parameters.get(3));
                break;

            case 5:
                elementOther = m.invoke(controllerClass.getConstructor().newInstance(), parameters
                        .get(0), parameters.get(1), parameters.get(2), parameters.get(3),
                        parameters
                                .get(4));
                break;

            case 6:
                elementOther = m.invoke(controllerClass.getConstructor().newInstance(), parameters.get(0),
                        parameters.get(1), parameters.get(2), parameters.get(3), parameters.get(4),
                        parameters.get(5));
                break;

            case 7:
                elementOther = m.invoke(controllerClass.getConstructor().newInstance(), parameters.get(0),
                        parameters.get(1), parameters.get(2), parameters.get(3), parameters.get(4),
                        parameters.get(5), parameters.get(6));
                break;

            case 8:
                elementOther = m.invoke(controllerClass.getConstructor().newInstance(), parameters.get(0),
                        parameters.get(1), parameters.get(2), parameters.get(3), parameters.get(4),
                        parameters.get(5), parameters.get(6),
                        parameters.get(7));
                break;

            case 9:
                elementOther = m.invoke(controllerClass.getConstructor().newInstance(), parameters.get(0),
                        parameters.get(1), parameters.get(2), parameters.get(3), parameters.get(4),
                        parameters.get(5), parameters.get(6), parameters.get(7), parameters.get(8));
                break;

            case 10:
                elementOther = m.invoke(controllerClass.getConstructor().newInstance(), parameters.get(0),
                        parameters.get(1), parameters.get(2), parameters.get(3), parameters.get(4),
                        parameters.get(5), parameters.get(6), parameters.get(7), parameters.get(8),
                        parameters.get(9));
                break;

            default:
                elementOther = null;
        }
        return elementOther;
    }

    /**
     * Creates other element and possible multiple mappings.
     * 
     * @param perspective
     * @param mappingType
     * @param element model element of interest
     * @param numberOfMappings number of mappings to create
     * @param parameters
     * @throws ClassNotFoundException
     * @throws InvocationTargetException
     * @throws IllegalArgumentException
     * @throws IllegalAccessException
     * @throws SecurityException
     * @throws NoSuchMethodException
     * @throws InstantiationException
     * @return elementOther
     */
    public EObject createOtherElementsAndMappings(COREPerspective perspective, CORELanguageElementMapping mappingType,
            EObject element, int numberOfMappings, List<Object> parameters)
            throws ClassNotFoundException, IllegalAccessException, IllegalArgumentException,
            InvocationTargetException, InstantiationException, NoSuchMethodException, SecurityException {

        EObject elementOther = null;
        CORELanguageAction createAction  = COREPerspectiveUtil.INSTANCE.getCreateLanguageAction(perspective, element);
        Class<?> controllerClass = Class.forName(createAction.getClassQualifiedName());
        Method[] methods = controllerClass.getMethods();

        for (int count = 0; count < numberOfMappings; count++) {
            outerloop: for (Method m : methods) {
                if (m.getName().equals(createAction.getMethodName())) {
                    elementOther = (EObject) createOtherElement(controllerClass, m, parameters);
                    break outerloop;
//                    if (elementOther != null) {
//                        COREPerspectiveUtil.INSTANCE.createMapping(perspective, element, (EObject) elementOther, 
//                                mappingType);
//                        break outerloop;
//                    }
                    
                }
            }
        }
        return elementOther;
    }

    /**
     * Creates corresponding class diagram and the associated mapping.
     * 
     * @param perspective of the model
     * @param feature of the model
     * @param roleName of the class diagram to create.
     * @param externalModel existing model to be mapped with new class diagram.
     * @return TODO
     */
    public ClassDiagram createClassDiagramAndMapping(COREPerspective perspective, COREFeature feature, EObject externalModel,
            String roleName) {

        EObject classDiagram = null;

        COREExternalArtefact artefact = COREArtefactUtil.getReferencingExternalArtefact(externalModel);
        COREConcern concern = artefact.getCoreConcern();
        COREScene scene = artefact.getScene();

        classDiagram = BasePerspectiveController.createNewDesignModel(concern, scene, feature,
                perspective, roleName);
        
        CORELanguageElementMapping mappingType = null;
        for (CORELanguageElementMapping type : perspective.getMappings()) {
            EObject fromLanguageElement = type.getMappingEnds().get(0).getLanguageElement().getLanguageElement();
            EObject toLanguageElement = type.getMappingEnds().get(1).getLanguageElement().getLanguageElement();
            EObject cdmMetaclass = CdmPackage.eINSTANCE.getClassDiagram();
            if (fromLanguageElement.equals(cdmMetaclass) && toLanguageElement.equals(externalModel.eClass())) {
                mappingType = type;
                break;
            } else if (fromLanguageElement.equals(externalModel.eClass()) && toLanguageElement.equals(cdmMetaclass)) {
                mappingType = type;
                break;
            }
        }
        if (mappingType != null) {
            COREPerspectiveUtil.INSTANCE.createMapping(perspective, externalModel, classDiagram, mappingType);
        }
        
        return (ClassDiagram) classDiagram;
    }

    /**
     * Creates corresponding use case diagram and the associated mapping.
     * 
     * @param perspective of the use case diagram
     * @param feature of the use case diagram
     * @param externalModel existing model to be mapped with to be created use case diagram
     * @param roleName of the use case diagram to create.
     * @return TODO
     */
    public UseCaseModel createUseCaseDiagramAndMapping(COREPerspective perspective, COREFeature feature,
            EObject externalModel, String roleName) {

        EObject useCaseDiagram = null;

        COREExternalArtefact artefact = COREArtefactUtil.getReferencingExternalArtefact(externalModel);
        COREConcern concern = artefact.getCoreConcern();
        COREScene scene = artefact.getScene();

        useCaseDiagram = BasePerspectiveController.createNewDesignModel(concern, scene, feature,
                perspective, roleName);
        CORELanguageElementMapping mappingType = null;
        for (CORELanguageElementMapping type : perspective.getMappings()) {
            EObject fromLanguageElement = type.getMappingEnds().get(0).getLanguageElement().getLanguageElement();
            EObject toLanguageElement = type.getMappingEnds().get(1).getLanguageElement().getLanguageElement();
            EObject useCase = UcPackage.eINSTANCE.getUseCaseModel();
            if (fromLanguageElement.equals(externalModel.eClass()) && toLanguageElement.equals(useCase)) {
                mappingType = type;
                break;
            } else if (fromLanguageElement.equals(useCase) && toLanguageElement.equals(externalModel.eClass())) {
                mappingType = type;
                break;
            }
        }
        if (mappingType != null) {
            COREPerspectiveUtil.INSTANCE.createMapping(perspective, externalModel, useCaseDiagram, mappingType);
        }
        
        return (UseCaseModel) useCaseDiagram;
    }
    
    /**
     * Creates model element mapping {@link COREModelElementMapping} between two root model elements.
     * @param perspective - the perspective of the system.
     * @param firstModel - one of the root model element.
     * @param firstRoleName from model role name
     * @param secondModel - second root model element.
     * @param secondRoleName to model role name.
     */
    public void createRootElementMapping(COREPerspective perspective, EObject firstModel, String firstRoleName, 
            EObject secondModel, String secondRoleName) {
        CORELanguageElementMapping mappingType = null;
        for (CORELanguageElementMapping type : perspective.getMappings()) {
            MappingEnd firstMappingEnd = type.getMappingEnds().get(0);
            MappingEnd secondMappingEnd = type.getMappingEnds().get(1);
            EObject firstLanguageElement = firstMappingEnd.getLanguageElement().getLanguageElement();
            EObject secondLanguageElement = secondMappingEnd.getLanguageElement().getLanguageElement();
            if (firstLanguageElement.equals(firstModel.eClass()) && firstMappingEnd.getRoleName().equals(firstRoleName)
                    && secondLanguageElement.equals(secondModel.eClass()) 
                    && secondMappingEnd.getRoleName().equals(secondRoleName)) {
                mappingType = type;
                break;
            } else if (firstLanguageElement.equals(secondModel.eClass()) 
                    && firstMappingEnd.getRoleName().equals(secondRoleName) 
                    && secondLanguageElement.equals(firstModel.eClass())
                    && secondMappingEnd.getRoleName().equals(firstRoleName)) {
                mappingType = type;
                break;
            }
        }
        if (mappingType != null) {
            COREPerspectiveUtil.INSTANCE.createMapping(perspective, secondModel, firstModel, mappingType);
        }
    }

    /**
     * Creates one mapping only if other element is not mapped otherwise create other element and mapping.
     * 
     * @param perspective
     * @param mappingType
     * @param elementHere
     * @param roleName
     * @param languageElement
     * @param parameters
     * @throws IllegalAccessException
     * @throws IllegalArgumentException
     * @throws InvocationTargetException
     * @throws ClassNotFoundException
     * @throws SecurityException
     * @throws NoSuchMethodException
     * @throws InstantiationException
     */
    public void createFirstMappingOrElementAndMapping(COREPerspective perspective,
            CORELanguageElementMapping mappingType, EObject elementHere, String roleName,
            CORELanguageElement languageElement, List<Object> parameters)
            throws IllegalAccessException, IllegalArgumentException, InvocationTargetException,
            ClassNotFoundException, InstantiationException, NoSuchMethodException, SecurityException {
        // Check if a corresponding element exists, either mapped or not
        Object elementOther = null;
//        elementOther = QueryAction.INSTANCE.findCorrespondingElementByName(scene, elementHere, roleName);
//        if (elementOther == null || COREPerspectiveUtil.INSTANCE.getMappings(mappingType,
//                (EObject) elementOther).size() > 0) {
//            Class<?> controllerClass = Class.forName(languageElement.getCreateClassQualifiedName());
//            Method[] methods = controllerClass.getMethods();
//            outerloop: for (Method m : methods) {
//                if (m.getName().equals(languageElement.getCreateMethodName())) {
//                    elementOther = createOtherElement(controllerClass, m, parameters);
//                    if (elementOther != null) {
//                        COREPerspectiveUtil.INSTANCE.createMapping(perspective, elementHere, (EObject) elementOther, 
//                                mappingType);
//                        break outerloop;
//                    }
//                }
//            }
//        }
    }

    // TODO implement the method
    public void createMappingOrElementAndMapping() {

    }

    // TODO implement the method
    public void createFirstMappingsAndElementsAndMappings() {

    }

    // TODO implement the method
    public void createMappingsAndElementsAndMappings() {

    }

    public Set<CORELanguageElementMapping> getLanguageElementMappings(COREPerspective perspective,
            EObject languageElement) {
        Set<CORELanguageElementMapping> mappings = new HashSet<CORELanguageElementMapping>();
        for (CORELanguageElementMapping mappingType : perspective.getMappings()) {
            for (MappingEnd mappingEnd : mappingType.getMappingEnds()) {
                if (mappingEnd.getLanguageElement().equals(languageElement)) {
                    mappings.add(mappingType);
                }
            }
        }

        return mappings;
    }

}
