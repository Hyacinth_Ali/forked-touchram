package ca.mcgill.sel.ram.ui.perspective;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.eclipse.emf.ecore.EObject;

import ca.mcgill.sel.core.COREPerspective;
import ca.mcgill.sel.ram.ui.perspective.controller.PerspectiveException;
import ca.mcgill.sel.usecases.UseCaseModel;
import ca.mcgill.sel.usecases.language.controller.UseCaseControllerFactory;
import ca.mcgill.sel.usecases.language.controller.UseCaseModelController;

/**
 * This class contains methods, each calls a corresponding use case language
 * action to create new element.
 * 
 * @author hyacinth
 *
 */
public class UseCaseFacade {

	/**
	 * Creates a new actor.
	 * 
	 * @param perspective
	 * @param roleName - the role name of the model to contain the new actor
	 * @param otherOwner - the container of the corresponding element to be
	 * mapped with the new actor.
	 * @param name
	 * @param x
	 * @param y
	 * @return the new element.
	 */
	protected static EObject createActor(COREPerspective perspective, String roleName, EObject otherOwner, String name,
			float x, float y) {

		EObject actorOwner = FacadeAction.getOwner(perspective, otherOwner, roleName);

		if (actorOwner == null) {
			return null;
		}

		UseCaseModel ownerActor = (UseCaseModel) actorOwner;
		List<EObject> elements = new ArrayList<EObject>();
		List<EObject> initialElements = new ArrayList<EObject>();
		initialElements.addAll(ownerActor.getActors());
		UseCaseControllerFactory.INSTANCE.getUseCaseDiagramController().createNewActor(ownerActor, name, x, y);
		elements.addAll(ownerActor.getActors());
		elements.removeAll(initialElements);

		return elements.get(0);

	}

	public static EObject createActor(COREPerspective perspective, String toCreateRoleName,
			HashMap<Object, Object> parameters) {
		
		String roleName = String.valueOf(parameters.get("roleName"));
		EObject otherOwner = (EObject) parameters.get("owner");
		EObject actorOwner = FacadeAction.getOwner(perspective, otherOwner, roleName);

		if (actorOwner == null) {
			return null;
		}

		UseCaseModel ownerActor = (UseCaseModel) actorOwner;
		List<EObject> elements = new ArrayList<EObject>();
		List<EObject> initialElements = new ArrayList<EObject>();

		Class<UseCaseModelController> controllerClass = UseCaseModelController.class;
		Method createActorMethod = null;
		try {
			createActorMethod = controllerClass.getMethod("createActor", UseCaseModel.class, String.class, float.class, float.class);
		} catch (NoSuchMethodException | SecurityException e) {
			throw new PerspectiveException("Error: The method create actor can't be retrieved");
		}
		Object owner = parameters.get("owner");
		Object name = parameters.get("name");
		Object x = parameters.get("x");
		Object y = parameters.get("y");
		UseCaseModelController useCaseModelController = UseCaseControllerFactory.INSTANCE.getUseCaseDiagramController();
		
		initialElements.addAll(ownerActor.getActors());
		if (createActorMethod != null) {
			try {
				createActorMethod.invoke(useCaseModelController, owner, name, x, y);
			} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
				throw new PerspectiveException("Error: The method create actor can't be invoked");
			}
		}
		
		elements.addAll(ownerActor.getActors());
		elements.removeAll(initialElements);

		return elements.get(0);
	}

}
