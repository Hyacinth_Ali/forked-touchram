package ca.mcgill.sel.ram.ui.perspective.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.ecore.EObject;

import ca.mcgill.sel.classdiagram.ClassDiagram;
import ca.mcgill.sel.classdiagram.Classifier;
import ca.mcgill.sel.classdiagram.language.controller.ControllerFactory;
import ca.mcgill.sel.core.COREExternalArtefact;
import ca.mcgill.sel.core.CORELanguageElementMapping;
import ca.mcgill.sel.core.COREModelElementMapping;
import ca.mcgill.sel.core.COREPerspective;
import ca.mcgill.sel.core.COREScene;
import ca.mcgill.sel.core.MappingEnd;
import ca.mcgill.sel.core.perspective.ActionType;
import ca.mcgill.sel.core.perspective.COREPerspectiveUtil;
import ca.mcgill.sel.core.perspective.TemplateType;
import ca.mcgill.sel.core.util.COREArtefactUtil;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.perspective.FacadeAction;
import ca.mcgill.sel.ram.ui.perspective.QueryAction;
import ca.mcgill.sel.usecases.Actor;
import ca.mcgill.sel.usecases.UseCaseModel;
import ca.mcgill.sel.usecases.language.controller.UseCaseControllerFactory;

/**
 * Handles the creation of model elements in the domain use case perspective.
 * Also, the perspective actions ensure that consistency rules are maintained.
 * 
 * @author hyacinthali
 *
 */
public class DomainUseCaseController {
	
	/**
	 * A hash map between each elements to be deleted and the corresponding role name.
	 */
	private Map<EObject, String> deleteElements;
	/**
	 * A list to contain mappings to be deleted.
	 */
	private List<COREModelElementMapping> deleteMappings;

	/**
	 * Creates a new instance of {@link DomainDesignModelController}.
	 */
	public DomainUseCaseController() {

	}

	/**
	 * Creates a class in a domain use case perspective.
	 * 
	 * @param perspective
	 * @param currentRole
	 * @param owner
	 * @param name
	 * @param dataType
	 * @param x
	 * @param y
	 */
	public void createNewClass(COREPerspective perspective, String currentRole, ClassDiagram owner, String name,
			boolean dataType, float x, float y) {
		// a list to contain all the newly created classes
		List<EObject> elements = new ArrayList<EObject>();

		// a list to contain all the existing classes
		List<EObject> initialElements = new ArrayList<EObject>();
		initialElements.addAll(owner.getClasses());

		// primary language action to create a new class
		ControllerFactory.INSTANCE.getClassDiagramController().createNewClass(owner, name, dataType, false, x, y);
		elements.addAll(owner.getClasses());
		elements.removeAll(initialElements);
		EObject newClass = elements.get(0);

		// get the external artefact of the class model
		COREExternalArtefact artefact = COREArtefactUtil.getReferencingExternalArtefact(owner);
		COREScene scene = artefact.getScene();

		// save the recent changes
		BasePerspectiveController.saveModel(scene);

		try {
			createClassOthers(perspective, scene, newClass, currentRole, owner, name, dataType, x, y);
		} catch (PerspectiveException e) {
			RamApp.getActiveScene().displayPopup(e.getMessage());
		}

	}

	/**
	 * This is a helper method to create other model elements based on the
	 * existing mappings of a Class metaclass.
	 * 
	 * @param perspective
	 * @param element
	 * @param currentRoleName is the role name of the new created element, i.e.,
	 * the element that was created before the recursive method is called.
	 * @param owner
	 * @param name
	 * @param dataType
	 * @param x
	 * @param y
	 */
	private void createClassOthers(COREPerspective perspective, COREScene scene, EObject element,
			String currentRoleName, EObject owner, String name, boolean dataType, float x, float y)
			throws PerspectiveException {

		// get all the language element mappings of the already created model
		// element
		List<CORELanguageElementMapping> mappingTypes = COREPerspectiveUtil.INSTANCE.getMappingTypes(perspective,
				element.eClass(), currentRoleName);
		for (CORELanguageElementMapping mappingType : mappingTypes) {
			List<COREModelElementMapping> mappings = COREPerspectiveUtil.INSTANCE.getMappings(mappingType, element);

			// other role names, i.e., excluding the current role
			List<String> roleNames = COREPerspectiveUtil.INSTANCE.getOtherRoleNames(mappingType, element,
					currentRoleName);

			// for binary mapping
			String toCreateRoleName = roleNames.get(0);

			// the metaclass of the element to be created.
			EObject metaclass = COREPerspectiveUtil.INSTANCE
					.getOtherMetaclasses(mappingType, currentRoleName, element.eClass()).get(0);

			EObject other = null;
			boolean otherExist = true;
			switch (TemplateType.INSTANCE.getCreateType(mappingType, currentRoleName)) {
			// All the "Can..." create actions are suspended because TouchCORE
			// doesn't support interaction with the user while the code is
			// executing.

			// C1.2
			case CREATE_OR_USE_NON_MAPPED:
				// C6.2
				// the algorithm of C1.2 is used, because only one corresponding
				// element can be created or used for now
			case CREATE_OR_USE_NON_MAPPED_AT_LEAST_ONE:
				if (mappings.size() == 0) {
					// find the corresponding element, can return new or old
					// element.
					// Hence, naming must be unique
					other = QueryAction.INSTANCE.findCorrespondingElementByName(scene, element, toCreateRoleName);

					// create other element if the corresponding element is null
					// or
					// mapped.
					if (other == null || COREPerspectiveUtil.INSTANCE.getMappings(mappingType, element).size() > 0) {
						otherExist = false;
						other = FacadeAction.createClassAndOthers(perspective, metaclass, owner, toCreateRoleName, name,
								dataType, x, y);

					}
					COREPerspectiveUtil.INSTANCE.createMapping(perspective, element, other, mappingType);

					BasePerspectiveController.saveModel(scene);
					// stop the recursion if other element exists.
					if (!otherExist) {
						createClassOthers(perspective, scene, other, toCreateRoleName, other.eContainer(), name,
								dataType, x, y);
					}
					otherExist = true;

				}
				break;
			// C2.1
			// with this case, only newly created element can be returned as a
			// corresponding element (in the case of cyclic dependency)
			case CREATE:
				// C4.1
				// the algorithm of C2.1 is used, because only one corresponding
				// element can be created for now
				// with this case, only newly created element can be returned as
				// a
				// corresponding element (in the case of cyclic dependency)
			case CREATE_AT_LEAST_ONE:
				// C8.1
				// the algorithm of C2.1 is used, because only one corresponding
				// element can be created for now. Also,
				// with this case, either the newly created or old element can
				// be returned as a
				// corresponding element (in the case of cyclic dependency)
			case CREATE_OR_USE_AT_LEAST_ONE:
				// C4.2
				// the algorithm of C2.1 is used, because only one corresponding
				// element can be created for now. Also,
				// with this case, either the newly created or old element can
				// be returned as a
				// corresponding element (in the case of cyclic dependency)
			case CREATE_OR_USE:

				if (mappings.size() == 0) {

					// find the corresponding element, can return new or old
					// element.
					other = QueryAction.INSTANCE.findCorrespondingElementByName(scene, element, toCreateRoleName);

					if (other == null) {
						otherExist = false;
						other = FacadeAction.createClassAndOthers(perspective, metaclass, owner, toCreateRoleName, name,
								dataType, x, y);
					}

					COREPerspectiveUtil.INSTANCE.createMapping(perspective, element, other, mappingType);

					// save the recent changes
					BasePerspectiveController.saveModel(scene);
					// stop the recursion if other element exists.
					if (!otherExist) {
						createClassOthers(perspective, scene, other, toCreateRoleName, other.eContainer(), name,
								dataType, x, y);
					}
					otherExist = true;
				}
				break;
			}
		}
	}

	/**
	 * Creates an actor in a domain use case perspective.
	 * 
	 * @param perspective
	 * @param currentRole
	 * @param owner
	 * @param name
	 * @param x
	 * @param y
	 */
	public void createNewActor(COREPerspective perspective, String currentRole, UseCaseModel owner, String name,
			float x, float y) {
		// a list to contain all the newly created actors
		List<EObject> elements = new ArrayList<EObject>();

		// a list to contain all the existing actors
		List<EObject> initialElements = new ArrayList<EObject>();
		initialElements.addAll(owner.getActors());

		UseCaseControllerFactory.INSTANCE.getUseCaseDiagramController().createNewActor(owner, name, x, y);
		elements.addAll(owner.getActors());
		elements.removeAll(initialElements);

		EObject newActor = elements.get(0);

		// get the external artefact of the class model
		COREExternalArtefact artefact = COREArtefactUtil.getReferencingExternalArtefact(owner);
		COREScene scene = artefact.getScene();

		// save the recent changes
		BasePerspectiveController.saveModel(scene);

		try {
			createActorOthers(newActor, scene, perspective, currentRole, owner, name, x, y);
		} catch (PerspectiveException e) {
			RamApp.getActiveScene().displayPopup(e.getMessage());
		}

	}

	/*
	 * COREPerspectiveUtil.INSTANCE.createMapping(perspective, element, other,
	 * mappingType); createActorOthers(other, scene, perspective,
	 * toCreateRoleName, other.eContainer(), name, x, y);
	 */

	/**
	 * This is a helper method to create other model elements based on the
	 * existing mappings of an Actor metaclass.
	 * 
	 * @param element
	 * @param scene TODO
	 * @param perspective
	 * @param currentRole
	 * @param owner
	 * @param name
	 * @param x
	 * @param y
	 */
	private void createActorOthers(EObject element, COREScene scene, COREPerspective perspective,
			String currentRoleName, EObject owner, String name, float x, float y) throws PerspectiveException {

		// get all the language element mappings of the already created model
		// element
		List<CORELanguageElementMapping> mappingTypes = COREPerspectiveUtil.INSTANCE.getMappingTypes(perspective,
				element.eClass(), currentRoleName);
		for (CORELanguageElementMapping mappingType : mappingTypes) {
			List<COREModelElementMapping> mappings = COREPerspectiveUtil.INSTANCE.getMappings(mappingType, element);

			// other role names, i.e., excluding the current role
			List<String> roleNames = COREPerspectiveUtil.INSTANCE.getOtherRoleNames(mappingType, element,
					currentRoleName);

			// for binary mapping
			String toCreateRoleName = roleNames.get(0);

			// the metaclass of the element to be created.
			EObject metaclass = COREPerspectiveUtil.INSTANCE
					.getOtherMetaclasses(mappingType, currentRoleName, element.eClass()).get(0);

			EObject other = null;
			boolean otherExist = true;
			switch (TemplateType.INSTANCE.getCreateType(mappingType, currentRoleName)) {
			// All the "Can..." create actions are suspended because TouchCORE
			// doesn't support interaction with the user while the code is
			// executing.

			// C1.2
			case CREATE_OR_USE_NON_MAPPED:
				// C6.2
				// the algorithm of C1.2 is used, because only one corresponding
				// element can be created or used for now
			case CREATE_OR_USE_NON_MAPPED_AT_LEAST_ONE:
				if (mappings.size() == 0) {
					// find the corresponding element, can return new or old
					// element.
					// Hence, naming must be unique
					other = QueryAction.INSTANCE.findCorrespondingElementByName(scene, element, toCreateRoleName);

					// create other element if the corresponding element is null
					// or
					// mapped.
					if (other == null || COREPerspectiveUtil.INSTANCE.getMappings(mappingType, element).size() > 0) {
						otherExist = false;
						other = FacadeAction.createUseCaseActorAndOthers(perspective, metaclass, owner, toCreateRoleName, name, x,
								y);

					}
					COREPerspectiveUtil.INSTANCE.createMapping(perspective, element, other, mappingType);

					BasePerspectiveController.saveModel(scene);
					// stop the recursion if other element exists.
					if (!otherExist) {
						createActorOthers(other, scene, perspective, toCreateRoleName, other.eContainer(), name, x, y);
					}
					otherExist = true;

				}
				break;
			// C2.1
			// with this case, only newly created element can be returned as a
			// corresponding element (in the case of cyclic dependency)
			case CREATE:
				// C4.1
				// the algorithm of C2.1 is used, because only one corresponding
				// element can be created for now
				// with this case, only newly created element can be returned as
				// a
				// corresponding element (in the case of cyclic dependency)
			case CREATE_AT_LEAST_ONE:
				// C8.1
				// the algorithm of C2.1 is used, because only one corresponding
				// element can be created for now. Also,
				// with this case, either the newly created or old element can
				// be returned as a
				// corresponding element (in the case of cyclic dependency)
			case CREATE_OR_USE_AT_LEAST_ONE:
				// C4.2
				// the algorithm of C2.1 is used, because only one corresponding
				// element can be created for now. Also,
				// with this case, either the newly created or old element can
				// be returned as a
				// corresponding element (in the case of cyclic dependency)
			case CREATE_OR_USE:

				if (mappings.size() == 0) {

					// find the corresponding element, can return new or old
					// element.
					other = QueryAction.INSTANCE.findCorrespondingElementByName(scene, element, toCreateRoleName);

					if (other == null) {
						otherExist = false;
						other = FacadeAction.createUseCaseActorAndOthers(perspective, metaclass, owner, toCreateRoleName, name, x,
								y);
					}

					COREPerspectiveUtil.INSTANCE.createMapping(perspective, element, other, mappingType);

					// save the recent changes
					BasePerspectiveController.saveModel(scene);
					// stop the recursion if other element exists.
					if (!otherExist) {
						createActorOthers(other, scene, perspective, toCreateRoleName, other.eContainer(), name, x, y);
					}
					otherExist = true;
				}
				break;
			}
		}
	}

	public void deleteClassifier(COREPerspective perspective, String currentRole, Classifier classifier) {
		deleteElements = new HashMap<EObject, String>();
		deleteMappings = new ArrayList<COREModelElementMapping>();
		deleteClassifierAndOthers(perspective, currentRole, classifier);
		
		for (int i = 0; i < deleteMappings.size(); i++) {
			BasePerspectiveController.removeMapping(deleteMappings.get(i));
		}
		
		for (Map.Entry<EObject, String> set : deleteElements.entrySet()) {
			EObject element = set.getKey();
			String role = set.getValue();
			FacadeAction.deleteModelElement(element);
		}
		
	}

	private void deleteClassifierAndOthers(COREPerspective perspective, String currentRole, EObject element) {

		List<COREModelElementMapping> mappings = COREPerspectiveUtil.INSTANCE.getMappings(element);
		
		// delete the element
		// FacadeAction.deleteClassifierAndOthers(perspective, currentRole, element);
		// add the element to be deleted
		deleteElements.put(element, currentRole);

		for (COREModelElementMapping mapping : mappings) {
			// return if the mapping is already added to be deleted
			if (deleteMappings.contains(mapping)) {
				return;
			}
			EObject other = COREPerspectiveUtil.INSTANCE.getOtherElement(mapping, element);
			ActionType deleteType = null;
			CORELanguageElementMapping mappingType = COREPerspectiveUtil.INSTANCE.getMappingType(perspective, mapping);

			List<String> roleNames = COREPerspectiveUtil.INSTANCE.getOtherRoleNames(mappingType, element, currentRole);

			// for binary mapping
			String otherRoleName = roleNames.get(0);

			for (MappingEnd mappingEnd : mappingType.getMappingEnds()) {
				if (mappingEnd.getRoleName().equals(currentRole)) {
					deleteType = TemplateType.INSTANCE.getDeleteType(mappingEnd);
					break;
				}
			}

			//PerspectiveBaseController.removeMapping(mapping);
			deleteMappings.add(mapping);

			if (deleteType == null) {
				return;
			}

			switch (deleteType) {

			case DELETE_OTHERS:
				deleteClassifierAndOthers(perspective, otherRoleName, other);
				break;

			case DELETE_SINGLEMAPPED:
				List<COREModelElementMapping> otherMappings = COREPerspectiveUtil.INSTANCE.getMappings(mappingType,
						element);
				if (otherMappings.size() == 0) {
					deleteClassifierAndOthers(perspective, otherRoleName, other);
				}
				break;
			}
		}
	}

	public void deleteActor(COREPerspective perspective, String currentRole, Actor actor) {
		
		deleteElements = new HashMap<EObject, String>();
		deleteMappings = new ArrayList<COREModelElementMapping>();
		
		deleteActorAndOthers(perspective, currentRole, actor);
		
		for (int i = 0; i < deleteMappings.size(); i++) {
			BasePerspectiveController.removeMapping(deleteMappings.get(i));
		}
		
		for (Map.Entry<EObject, String> set : deleteElements.entrySet()) {
			EObject element = set.getKey();
			String role = set.getValue();
			FacadeAction.deleteModelElement(element);
		}
	}

	private void deleteActorAndOthers(COREPerspective perspective, String currentRole, EObject element) {

		List<COREModelElementMapping> mappings = COREPerspectiveUtil.INSTANCE.getMappings(element);
		
		// FacadeAction.deleteActorAndOthers(perspective, currentRole, element);
		// add the element to be deleted
		deleteElements.put(element, currentRole);

		// Traditional for loop is used here to avoid
		// ConcurrentModificationException
		for (COREModelElementMapping mapping: mappings) {
			EObject other = COREPerspectiveUtil.INSTANCE.getOtherElement(mapping, element);
			ActionType deleteType = null;
			CORELanguageElementMapping mappingType = COREPerspectiveUtil.INSTANCE.getMappingType(perspective, mapping);

			for (MappingEnd mappingEnd : mappingType.getMappingEnds()) {
				if (mappingEnd.getRoleName().equals(currentRole)) {
					deleteType = TemplateType.INSTANCE.getDeleteType(mappingEnd);
					break;
				}
			}

//			PerspectiveBaseController.removeMapping(mapping);
			deleteMappings.add(mapping);

			switch (deleteType) {

			case DELETE_OTHERS:
				deleteActorAndOthers(perspective, currentRole, other);
				break;

			case DELETE_SINGLEMAPPED:
				List<COREModelElementMapping> otherMappings = COREPerspectiveUtil.INSTANCE.getMappings(mappingType,
						element);
				if (otherMappings.size() == 0) {
					deleteActorAndOthers(perspective, currentRole, other);
				}
				break;
			}
		}

	}

}
