package ca.mcgill.sel.ram.ui.perspective;

import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;

import ca.mcgill.sel.classdiagram.CdmPackage;
import ca.mcgill.sel.classdiagram.Class;
import ca.mcgill.sel.classdiagram.ClassDiagram;
import ca.mcgill.sel.classdiagram.Classifier;
import ca.mcgill.sel.classdiagram.ui.scenes.DisplayClassDiagramScene;
import ca.mcgill.sel.classdiagram.ui.scenes.handler.impl.DisplayClassDiagramSceneHandler;
import ca.mcgill.sel.core.COREArtefact;
import ca.mcgill.sel.core.COREExternalArtefact;
import ca.mcgill.sel.core.COREScene;
import ca.mcgill.sel.core.util.COREArtefactUtil;
import ca.mcgill.sel.ram.ui.RamApp;

public class QueryAction {

    public static final QueryAction INSTANCE = new QueryAction();

    private boolean createOtherElement = true;
    private boolean userOption = false;

    QueryAction() {

    }

    /**
     * Check either to create other element. This should after asking for user's opinion.
     * 
     * @return the createOtherElement
     */
    public boolean isCreateOtherElement() {
        return createOtherElement;
    }

    /**
     * Sets the value of either creating other model element based on the input of the user.
     * 
     * @param createOtherElement the createOtherElement to set
     */
    public void setCreateOtherElement(boolean createOtherElement) {
        this.createOtherElement = createOtherElement;
    }

    /**
     * @return the userOption
     */
    public boolean isUserOption() {
        return userOption;
    }

    /**
     * @param userOption the userOption to set
     */
    public void setUserOption(boolean userOption) {
        this.userOption = userOption;
    }

    public List<EObject> findCorrespondingElements() {
        // TODO to be implemented
        return null;
    }

    public int askZeroOrOne() {
        // if (RamApp.getActiveScene() instanceof DisplayClassDiagramScene) {
        // DisplayClassDiagramScene scene = (DisplayClassDiagramScene) RamApp.getActiveScene();
        // DisplayClassDiagramSceneHandler displayClassDiagramSceneHandler = new DisplayClassDiagramSceneHandler();
        // displayClassDiagramSceneHandler.showCreateOtherModelConfirmPopup(scene);
        //
        // }
        
//        Thread perspectiveThread = new Thread(new Runnable() {
//            @Override
//            public void run() {
//                if (RamApp.getActiveScene() instanceof DisplayClassDiagramScene) {
//                    DisplayClassDiagramScene scene = (DisplayClassDiagramScene) RamApp.getActiveScene();
//                    DisplayClassDiagramSceneHandler displayClassDiagramSceneHandler =
//                            new DisplayClassDiagramSceneHandler();
//                    displayClassDiagramSceneHandler.showCreateOtherModelConfirmPopup(scene);
//                    System.out.println(Thread.currentThread().getName());
//                }
//            }
//        }, "Perspective Controller Thread");
//        perspectiveThread.start();
//        System.out.println("TEST");

//        RamApp.getApplication().invokeLater(new Runnable() {
//
//            @Override
//            public void run() {
//                if (RamApp.getActiveScene() instanceof DisplayClassDiagramScene) {
//                    DisplayClassDiagramScene scene = (DisplayClassDiagramScene) RamApp.getActiveScene();
//                    DisplayClassDiagramSceneHandler displayClassDiagramSceneHandler =
//                            new DisplayClassDiagramSceneHandler();
//                    displayClassDiagramSceneHandler.showCreateOtherModelConfirmPopup(scene);
//                    System.out.println(Thread.currentThread().getName());
//                }
//            }
//        });
//
//        System.out.println(Thread.currentThread().getName());
//        Thread thread = Thread.currentThread();
//        try {
//            Thread.sleep(5000);
//        } catch (InterruptedException e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//        }
//        System.out.println(thread.getName());
//        System.out.println(thread.getId());

        // TODO

        return 1;
    }

    public void askMany(EObject languageElement) {

    }

    public int askAtLeastOne() {
        
        return 1;

    }

    /**
     * Finds a corresponding element by using identifiable properties, e.g., a corresponding class with the same name.
     *
     * @param element of interest
     * @param roleName
     * @return correspondingElement
     */
    @SuppressWarnings("static-method")
    private EObject findCorrespondingClassByName(EObject element, String roleName) {
        EObject correspondingElement = null;
        COREExternalArtefact artefact = COREArtefactUtil.getReferencingExternalArtefact(element);
        if (artefact != null) {
            COREScene scene = artefact.getScene();
            List<COREArtefact> artefacts = scene.getArtefacts().get(roleName);
            if (artefacts != null) {
                for (COREArtefact a : artefacts) {
                    if (a instanceof COREExternalArtefact) {
                        COREExternalArtefact extArtefact = (COREExternalArtefact) a;
                        EObject rootModelElement = extArtefact.getRootModelElement();
                        if (element instanceof Class) {
                            if (rootModelElement instanceof ClassDiagram) {
                                ClassDiagram model = (ClassDiagram) rootModelElement;
                                for (Classifier c : model.getClasses()) {
                                    if (c instanceof Class && c.getName().equals(((Class) element).getName())) {
                                        correspondingElement = c;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return correspondingElement;
    }
    
    public EObject findCorrespondingElementByName(EObject metaclass, EObject currentElement, List<EObject> elements) {
        EObject correspondingElement = null;
        String currentName = null;
        if (currentElement instanceof Class) {
            Class currentClazz = (Class) currentElement;
            currentName = currentClazz.getName();
        }
        for (EObject element : elements) {
            if (element instanceof Class) {
                Class clazz = (Class) element;
                String name = clazz.getName();
                if (name.equals(currentName) && !element.equals(currentElement)) {
                    correspondingElement = element;
                }
            }
        }
        return correspondingElement;
    }

    /**
     * Finds a corresponding element by using identifiable properties, e.g., a corresponding class with the same name.
     * @param scene - the scene of the model
     * @param element of interest
     * @param roleName - language role of the corresponding model
     * 
     * @return correspondingElement
     */
    public EObject findCorrespondingElementByName(COREScene scene, EObject element, String roleName) {
        EObject correspondingElement = null;
        
        if (scene != null) {
        	List<COREArtefact> artefacts = scene.getArtefacts().get(roleName);
            if (artefacts != null) {
                outer_loop: for (COREArtefact a : artefacts) {
                    if (a instanceof COREExternalArtefact) {
                        COREExternalArtefact extArtefact = (COREExternalArtefact) a;
                        EObject rootModelElement = extArtefact.getRootModelElement();
                        // get all contents of the root model element
                        EList<EObject> rootContents =
                                rootModelElement.eContents();
                        Iterator<EObject> contents = rootContents.iterator();
                        while (contents.hasNext()) {
                            EObject rootContent = contents.next();
                            // assumption, only instances of EClass are supported
                            if (!(rootContent.eClass() instanceof EClass)) {
                                continue;
                            }
                            EClass contentEClass = rootContent.eClass();
                            for (EAttribute attr : contentEClass.getEAllAttributes()) {
                                // CHALLENGE: EMF can only, generically, return the values of direct attributes.
                                // Hence, the values of inherited attributes can't be accessed by using reflection
                                // This can be solved if the language metamodel has a common metaclass (NamedElement).
                                if (attr.getName().equals("name")) {
                                    String rootContentName = (String) rootContent.eGet(CdmPackage.eINSTANCE
                                            .getNamedElement_Name());
                                    String elementName = (String) element.eGet(CdmPackage.eINSTANCE
                                            .getNamedElement_Name());
                                    if (rootContentName.equals(elementName)) {
                                        correspondingElement = rootContent;
                                        break outer_loop;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        
        return correspondingElement;
    }

}
