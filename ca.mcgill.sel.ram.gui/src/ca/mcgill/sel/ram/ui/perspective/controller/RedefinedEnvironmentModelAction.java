package ca.mcgill.sel.ram.ui.perspective.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.ecore.EObject;

import ca.mcgill.sel.core.COREExternalArtefact;
import ca.mcgill.sel.core.CORELanguageElementMapping;
import ca.mcgill.sel.core.COREModelElementMapping;
import ca.mcgill.sel.core.COREPerspective;
import ca.mcgill.sel.core.COREScene;
import ca.mcgill.sel.core.perspective.COREPerspectiveUtil;
import ca.mcgill.sel.core.perspective.TemplateType;
import ca.mcgill.sel.core.util.COREArtefactUtil;
import ca.mcgill.sel.environmentmodel.Actor;
import ca.mcgill.sel.environmentmodel.EnvironmentModel;
import ca.mcgill.sel.environmentmodel.MessageDirection;
import ca.mcgill.sel.environmentmodel.language.controller.EnvironmentModelControllerFactory;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.perspective.EnvironmentModelFacade;
import ca.mcgill.sel.ram.ui.perspective.QueryAction;

public class RedefinedEnvironmentModelAction {
	

	/**
	 * List to track newly created elements.
	 */
	private List<EObject> newElements = new ArrayList<EObject>();

	/**
	 * A hash map between each elements to be deleted and the corresponding role name.
	 */
	private Map<EObject, String> deleteElements;
	
	/**
	 * A list to contain mappings to be deleted.
	 */
	private List<COREModelElementMapping> deleteMappings;

	/**
	 * Creates a new instance of {@link RedefinedOperationModelAction}.
	 */
	protected RedefinedEnvironmentModelAction() {

	}

	/**
	 * Creates a new actor in an operation model.
	 * 
	 * @param perspective
	 * @param currentRole
	 * @param owner
	 * @param name
	 * @param lowerBound
	 * @param upperBound
	 * @param communicationUpperBound
	 * @param communicationLowerBound
	 */
	public void createActor(COREPerspective perspective, String currentRole, EnvironmentModel owner, String name, int lowerBound, int upperBound, 
    		int communicationUpperBound, int communicationLowerBound) {
		// a list to contain all the newly created actors
		List<EObject> elements = new ArrayList<EObject>();

		// a list to contain all the existing actors
		List<EObject> initialElements = new ArrayList<EObject>();
		initialElements.addAll(owner.getActors());

		// primary language action to create a new actor
		EnvironmentModelControllerFactory.INSTANCE.getEnvironmentModelController().createActor(owner, name, lowerBound, upperBound, communicationLowerBound, communicationUpperBound);
		elements.addAll(owner.getActors());
		
		// remove old elements
		elements.removeAll(initialElements);

		// get the external artefact of the class model
		COREExternalArtefact artefact = COREArtefactUtil.getReferencingExternalArtefact(owner);
		COREScene scene = artefact.getScene();

		// save the recent changes
		BasePerspectiveController.saveModel(scene);
		
		// iterate over all the newly created elements in the container
		for (EObject newElement : elements) {
			try {
				createActorOthers(perspective, scene, newElement, currentRole, owner, name, lowerBound, upperBound, communicationUpperBound, communicationLowerBound);
			} catch (PerspectiveException e) {
				RamApp.getActiveScene().displayPopup(e.getMessage());
			}
			
			newElements.clear();
		}

	}

	/**
	 * This is a helper method to create other model elements, because a new actor is created and needs to be mapped with other new elements.
	 * @param perspective
	 * @param scene
	 * @param element
	 * @param currentRoleName
	 * @param owner
	 * @param name
	 * @param lowerBound
	 * @param upperBound
	 * @param communicationUpperBound
	 * @param communicationLowerBound
	 * @throws PerspectiveException
	 */
	private void createActorOthers(COREPerspective perspective, COREScene scene, EObject element,
			String currentRoleName, EObject owner, String name, int lowerBound, int upperBound, 
    		int communicationUpperBound, int communicationLowerBound)
			throws PerspectiveException {

		// get all the language element mappings of the already created model
		// element
		List<CORELanguageElementMapping> mappingTypes = COREPerspectiveUtil.INSTANCE.getMappingTypes(perspective,
				element.eClass(), currentRoleName);
		for (CORELanguageElementMapping mappingType : mappingTypes) {
			List<COREModelElementMapping> mappings = COREPerspectiveUtil.INSTANCE.getMappings(mappingType, element);

			// other role names, i.e., excluding the current role
			List<String> roleNames = COREPerspectiveUtil.INSTANCE.getOtherRoleNames(mappingType, element,
					currentRoleName);

			// for binary mapping
			String toCreateRoleName = roleNames.get(0);

			// the metaclass of the element to be created.
			EObject metaclass = COREPerspectiveUtil.INSTANCE
					.getOtherMetaclasses(mappingType, currentRoleName, element.eClass()).get(0);

			EObject other = null;
			boolean otherExist = true;
			switch (TemplateType.INSTANCE.getCreateType(mappingType, currentRoleName)) {
			// All the "Can..." create actions are suspended because TouchCORE
			// doesn't support interaction with the user while the code is
			// executing.

			// C1.2
			case CREATE_OR_USE_NON_MAPPED:
				// C6.2
				// the algorithm of C1.2 is used, because only one corresponding
				// element can be created or used for now
			case CREATE_OR_USE_NON_MAPPED_AT_LEAST_ONE:
				if (mappings.size() == 0) {
					// find the corresponding element, can return new or old
					// element.
					// Hence, naming must be unique
					other = QueryAction.INSTANCE.findCorrespondingElementByName(scene, element, toCreateRoleName);

					// create other element if the corresponding element is null
					// or
					// mapped.
					if (other == null || COREPerspectiveUtil.INSTANCE.getMappings(mappingType, element).size() > 0) {
						otherExist = false;
//						other = EnvironmentModelFacade.createOMActorAndOthers(perspective, metaclass, owner, toCreateRoleName, name,
//								lowerBound, upperBound, communicationUpperBound, communicationLowerBound);

					}
					COREPerspectiveUtil.INSTANCE.createMapping(perspective, element, other, mappingType);

					BasePerspectiveController.saveModel(scene);
					// stop the recursion if other element exists.
					if (!otherExist) {
						createActorOthers(perspective, scene, other, toCreateRoleName, other.eContainer(), name,
								lowerBound, upperBound, communicationUpperBound, communicationLowerBound);
					}
					otherExist = true;

				}
				break;
			// C2.1
			// with this case, only newly created element can be returned as a
			// corresponding element (in the case of cyclic dependency)
			case CREATE:
				// C4.1
				// the algorithm of C2.1 is used, because only one corresponding
				// element can be created for now
				// with this case, only newly created element can be returned as
				// a
				// corresponding element (in the case of cyclic dependency)
			case CREATE_AT_LEAST_ONE:
				// C8.1
				// the algorithm of C2.1 is used, because only one corresponding
				// element can be created for now. Also,
				// with this case, either the newly created or old element can
				// be returned as a
				// corresponding element (in the case of cyclic dependency)
			case CREATE_OR_USE_AT_LEAST_ONE:
				// C4.2
				// the algorithm of C2.1 is used, because only one corresponding
				// element can be created for now. Also,
				// with this case, either the newly created or old element can
				// be returned as a
				// corresponding element (in the case of cyclic dependency)
			case CREATE_OR_USE:

				if (mappings.size() == 0) {

					// find the corresponding element, can return new or old
					// element.
					other = QueryAction.INSTANCE.findCorrespondingElementByName(scene, element, toCreateRoleName);

					if (other == null) {
						otherExist = false;
//						other = EnvironmentModelFacade.createOMActorAndOthers(perspective, metaclass, owner, toCreateRoleName, name,
//								lowerBound, upperBound, communicationUpperBound, communicationLowerBound);
					}

					COREPerspectiveUtil.INSTANCE.createMapping(perspective, element, other, mappingType);

					// save the recent changes
					BasePerspectiveController.saveModel(scene);
					// stop the recursion if other element exists.
					if (!otherExist) {
						createActorOthers(perspective, scene, other, toCreateRoleName, other.eContainer(), name,
								lowerBound, upperBound, communicationUpperBound, communicationLowerBound);
					}
					otherExist = true;
				}
				break;
			}
		}
	}
	
	/**
	 * Creates a new actor in an operation model.
	 * 
	 * @param perspective
	 * @param currentRole
	 * @param owner
	 * @param name
	 * @param lowerBound
	 * @param upperBound
	 * @param communicationUpperBound
	 * @param communicationLowerBound
	 */
	public void createMessage(COREPerspective perspective, String currentRole, Actor owner, String name, EnvironmentModel em, MessageDirection messageDirection) { 
		// a list to contain all the newly created messages
		List<EObject> elements = new ArrayList<EObject>();

		// a list to contain all the existing messages
		List<EObject> initialElements = new ArrayList<EObject>();
		initialElements.addAll(owner.getMessages());

		// primary language action to create a new message
		EnvironmentModelControllerFactory.INSTANCE.getEnvironmentModelController().createMessage(em, owner, name, messageDirection);
		elements.addAll(owner.getMessages());
		
		// remove old elements
		elements.removeAll(initialElements);

		// get the external artefact of the class model
		COREExternalArtefact artefact = COREArtefactUtil.getReferencingExternalArtefact(owner);
		COREScene scene = artefact.getScene();

		// save the recent changes
		BasePerspectiveController.saveModel(scene);
		
		// iterate over all the newly created elements in the container
		for (EObject newElement : elements) {
			try {
				createMessageAndOthers(perspective, scene, newElement, currentRole, owner, name, em, messageDirection);
			} catch (PerspectiveException e) {
				RamApp.getActiveScene().displayPopup(e.getMessage());
			}
			
			newElements.clear();
		}

	}

	/**
	 * This is a helper method to create other model elements, because a new actor is created and needs to be mapped with other new elements.
	 * @param perspective
	 * @param scene
	 * @param element
	 * @param currentRoleName
	 * @param owner
	 * @param name
	 * @param lowerBound
	 * @param upperBound
	 * @param communicationUpperBound
	 * @param communicationLowerBound
	 * @throws PerspectiveException
	 */
	private void createMessageAndOthers(COREPerspective perspective, COREScene scene, EObject element,
			String currentRoleName, EObject owner, String name, EnvironmentModel em, MessageDirection messageDirection)
			throws PerspectiveException {

		// get all the language element mappings of the already created model
		// element
		List<CORELanguageElementMapping> mappingTypes = COREPerspectiveUtil.INSTANCE.getMappingTypes(perspective,
				element.eClass(), currentRoleName);
		for (CORELanguageElementMapping mappingType : mappingTypes) {
			List<COREModelElementMapping> mappings = COREPerspectiveUtil.INSTANCE.getMappings(mappingType, element);

			// other role names, i.e., excluding the current role
			List<String> roleNames = COREPerspectiveUtil.INSTANCE.getOtherRoleNames(mappingType, element,
					currentRoleName);

			// for binary mapping
			String toCreateRoleName = roleNames.get(0);

			// the metaclass of the element to be created.
			EObject metaclass = COREPerspectiveUtil.INSTANCE
					.getOtherMetaclasses(mappingType, currentRoleName, element.eClass()).get(0);

			EObject other = null;
			boolean otherExist = true;
			switch (TemplateType.INSTANCE.getCreateType(mappingType, currentRoleName)) {
			// All the "Can..." create actions are suspended because TouchCORE
			// doesn't support interaction with the user while the code is
			// executing.

			// C1.2
			case CREATE_OR_USE_NON_MAPPED:
				// C6.2
				// the algorithm of C1.2 is used, because only one corresponding
				// element can be created or used for now
			case CREATE_OR_USE_NON_MAPPED_AT_LEAST_ONE:
				if (mappings.size() == 0) {
					// find the corresponding element, can return new or old
					// element.
					// Hence, naming must be unique
					other = QueryAction.INSTANCE.findCorrespondingElementByName(scene, element, toCreateRoleName);

					// create other element if the corresponding element is null
					// or
					// mapped.
					if (other == null || COREPerspectiveUtil.INSTANCE.getMappings(mappingType, element).size() > 0) {
						otherExist = false;
						other = EnvironmentModelFacade.createEMMessageAndOthers(perspective, metaclass, owner, toCreateRoleName, name, em, messageDirection);

					}
					COREPerspectiveUtil.INSTANCE.createMapping(perspective, element, other, mappingType);

					BasePerspectiveController.saveModel(scene);
					// stop the recursion if other element exists.
					if (!otherExist) {
						createMessageAndOthers(perspective, scene, other, toCreateRoleName, other.eContainer(), name, em, messageDirection);
					}
					otherExist = true;

				}
				break;
			// C2.1
			// with this case, only newly created element can be returned as a
			// corresponding element (in the case of cyclic dependency)
			case CREATE:
				// C4.1
				// the algorithm of C2.1 is used, because only one corresponding
				// element can be created for now
				// with this case, only newly created element can be returned as
				// a
				// corresponding element (in the case of cyclic dependency)
			case CREATE_AT_LEAST_ONE:
				// C8.1
				// the algorithm of C2.1 is used, because only one corresponding
				// element can be created for now. Also,
				// with this case, either the newly created or old element can
				// be returned as a
				// corresponding element (in the case of cyclic dependency)
			case CREATE_OR_USE_AT_LEAST_ONE:
				// C4.2
				// the algorithm of C2.1 is used, because only one corresponding
				// element can be created for now. Also,
				// with this case, either the newly created or old element can
				// be returned as a
				// corresponding element (in the case of cyclic dependency)
			case CREATE_OR_USE:

				if (mappings.size() == 0) {

					// find the corresponding element, can return new or old
					// element.
					other = QueryAction.INSTANCE.findCorrespondingElementByName(scene, element, toCreateRoleName);

					if (other == null) {
						otherExist = false;
						other = EnvironmentModelFacade.createEMMessageAndOthers(perspective, metaclass, owner, toCreateRoleName, name, em, messageDirection);
					}

					COREPerspectiveUtil.INSTANCE.createMapping(perspective, element, other, mappingType);

					// save the recent changes
					BasePerspectiveController.saveModel(scene);
					// stop the recursion if other element exists.
					if (!otherExist) {
						createMessageAndOthers(perspective, scene, other, toCreateRoleName, other.eContainer(), name, em, messageDirection);
					}
					otherExist = true;
				}
				break;
			}
		}
	}

}
