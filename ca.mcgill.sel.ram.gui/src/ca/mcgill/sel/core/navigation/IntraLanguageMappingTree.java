package ca.mcgill.sel.core.navigation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.edit.provider.INotifyChangedListener;
import org.mt4j.components.MTComponent;

import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.commons.emf.util.EMFModelUtil;
import ca.mcgill.sel.core.COREArtefact;
import ca.mcgill.sel.core.COREExternalArtefact;
import ca.mcgill.sel.core.CorePackage;
import ca.mcgill.sel.core.InterLanguageMapping;
import ca.mcgill.sel.core.IntraLanguageMapping;
import ca.mcgill.sel.core.NavigationMapping;
import ca.mcgill.sel.core.navigation.IntraLanguageMappingTree.ILMNode;
import ca.mcgill.sel.core.util.COREArtefactUtil;
import ca.mcgill.sel.ram.ui.components.RamExpendableComponent;
import ca.mcgill.sel.ram.ui.components.RamListComponent;
import ca.mcgill.sel.ram.ui.components.RamRectangleComponent;
import ca.mcgill.sel.ram.ui.components.RamTextComponent;
import ca.mcgill.sel.ram.ui.components.navigationbar.NavigationBar;
import ca.mcgill.sel.ram.ui.components.navigationbar.NavigationBarMenu;
import ca.mcgill.sel.ram.ui.components.navigationbar.NavigationBarMenuElement;
import ca.mcgill.sel.ram.ui.components.navigationbar.NavigationBarMenuElement.TextViewNavBarElement;

public class IntraLanguageMappingTree {
    
    public class ILMNode {
        
        private EObject nodeModelElement;
        private NavigationMapping ilm;
        private List<ILMNode> children;
        private ILMNode parentNode;
        private List<RamRectangleComponent> nodeComponents;
        private IntraLanguageMappingTree ilmTree;
        private int nodeDepth;
        private String interMappingName;
        
//        private Map<EList<EObject>, Integer> listsToWatch;
        
        public ILMNode(EObject nodeElement, NavigationMapping parentILM, IntraLanguageMappingTree tree, int depth) {
            nodeModelElement = nodeElement;
            ilm = parentILM;
            children = new ArrayList<ILMNode>();
//            listsToWatch = new HashMap<EList<EObject>, Integer>();
            nodeComponents = new ArrayList<RamRectangleComponent>();
            ilmTree = tree;
            nodeDepth = depth;
        }
        
        public List<ILMNode> getChildren() {
            return children;
        }
        
        public EObject getNodeModelElement() {
            return nodeModelElement;
        }
        public NavigationMapping getILM() {
            return ilm;
        }
        
//        public Map<EList<EObject>, Integer> getListsToWatch() {
//            return listsToWatch;
//        }
        public List<RamRectangleComponent> getNodeComponents() {
            return nodeComponents;
        }
        
        public void setParentNode(ILMNode parent) {
            this.parentNode = parent;
        }
        public ILMNode getParentNode() {
            return parentNode;
        }
        
        public IntraLanguageMappingTree getIntraLanguageMappingTree() {
            return ilmTree;
        }
        
        public String getInterMappingName() {
            return interMappingName;
        }
        
        public void setInterMappingName(String name) {
            interMappingName = name;
        }
        
    }
    
    private ILMNode rootNode;
    private List<NavigationBarMenuElement<?>> elems;
    private NavigationBarMenu menu;
    
    public IntraLanguageMappingTree(EObject rootModelElement) {
        rootNode = new ILMNode(rootModelElement, null, this, 0);
        elems = new ArrayList<NavigationBarMenuElement<?>>();
    }
    
    public ILMNode getRootNode() {
        return rootNode;
    }
    public EObject getRootModelElement() {
        return rootNode.getNodeModelElement();
    }
    
    public List<NavigationBarMenuElement<?>> getNavigationBarMenuElements() {
        return elems;
    }
    
    public void setNavigationBarMenu(NavigationBarMenu menuToSet) {
        menu = menuToSet;
    }
    public NavigationBarMenu getMenu() {
        return menu;
    }
    
    /**
     * Should find a node for a given eobject if it exists.
     * @param modelElement the eobject which will be searched for
     * @return the node if it exists
     */
    public ILMNode findObjectNode(EObject modelElement) {
        List<ILMNode> nodesToSearch = new ArrayList<ILMNode>();
        nodesToSearch.add(rootNode);
        
        while (nodesToSearch.size() > 0) {
            ILMNode currentNode = nodesToSearch.get(0);
            if (currentNode.getNodeModelElement().equals(modelElement)) {
                return currentNode;
            } else {
                List<ILMNode> currentChildren = currentNode.getChildren();
                for (ILMNode child : currentChildren) {
                    nodesToSearch.add(child);
                }
            }
            nodesToSearch.remove(currentNode);
        }
        return null;
        
    }
    
    /** 
     * Should add an element to the tree in the given position. Will return null if there is no element with the parent
     * eobject as its node model element. An exception is made in the case of a COREExternalArtefact in order to 
     * facilitate model extensions and reuses (this was the only way I could think to do that in a streamlined way).
     * @param modelElementToAdd element that will be the base of the node to be added
     * @param parentModelElement model element of the parent element
     * @param parentILM ilm of the node to be created
     * @return the created node
     */
    public ILMNode addNode(EObject modelElementToAdd, EObject parentModelElement, NavigationMapping parentILM) {
        ILMNode parentNode = findObjectNode(parentModelElement);
        if (parentNode == null) {
            if (parentModelElement instanceof COREExternalArtefact) {
                parentNode = rootNode;
                ILMNode addedNode = new ILMNode(modelElementToAdd, parentILM, this, parentNode.nodeDepth + 1);
                parentNode.getChildren().add(addedNode);
                addedNode.parentNode = parentNode;
                return addedNode;
            }
            return null;
        } else {
            ILMNode addedNode = new ILMNode(modelElementToAdd, parentILM, this, parentNode.nodeDepth + 1);
            parentNode.getChildren().add(addedNode);
            addedNode.parentNode = parentNode;
            return addedNode;
        }
    }
    
    /**
     * Should obtain and return a list of all of the nodes within the tree.
     * @return all of the nodes in the tree.
     */
    public List<ILMNode> getAllNodes() {
        List<ILMNode> allNodes = new ArrayList<ILMNode>();
        
        List<ILMNode> nodesToTraverse = new ArrayList<ILMNode>();
        nodesToTraverse.add(this.rootNode);
        allNodes.add(this.rootNode);
        
        while (nodesToTraverse.size() != 0) {
            ILMNode curNode = nodesToTraverse.get(0);
            List<ILMNode> childrenNodes = curNode.getChildren();
            for (ILMNode childNode : childrenNodes) {
                if (!allNodes.contains(childNode)) {
                    allNodes.add(childNode);
                    nodesToTraverse.add(childNode);
                }
            }
            nodesToTraverse.remove(0);
        }
        
        return allNodes;
    }
    
    /** 
     * Should check if a node already exists with a given eobject as its base.
     * @param objToBeAdded eobject which will be searched for
     * @return true if already in tree, false if not
     */
    private boolean checkIfAlreadyAdded(EObject objToBeAdded) {
        for (ILMNode node : getAllNodes()) {
            EObject curNodeObject = node.getNodeModelElement();
            if (objToBeAdded.equals(curNodeObject)) {
                return true;
            }
        }
        return false;
    }
    
    /**
     * Should add the listeners to the pertinant lists for a node.
     * @param node the node to add listeners to
     */
    private void addListsForElement(ILMNode node) {
        EObject nodeObject = node.nodeModelElement;
        EClass nodeClass = nodeObject.eClass();
        
        
        for (IntraLanguageMapping curMapping : NavigationMappingHelper.getInstance().getAllILM()) {
            if (curMapping.getFrom().equals(nodeClass)) {
                findEList(node, curMapping);
            } else {
                EList<EClass> objectSuperClasses = nodeClass.getESuperTypes();
                for (EClass superType : objectSuperClasses) {
                    if (curMapping.getFrom().equals(superType)) {
                        findEList(node, curMapping);
                    }
                }
            }
        }
    }
    
    
    /**
     * Should add a node if one does not already exist within the tree structure for a given eobject and name. Will 
     * place the node into the structure within the nav bar with different options depending on if the parent is an 
     * expendable element or a text view and how many children the parent element already has.
     * @param node the parent node which the child will be added below
     * @param objToAdd the object for which a node will be created
     * @param ilm the mapping which the list is listening to 
     * @param mappingName the name of the mapping
     */
    protected void checkMappingForAddition(ILMNode node, EObject objToAdd, NavigationMapping ilm, 
            String mappingName) {
        boolean alreadyInTree = checkIfAlreadyAdded(objToAdd);
        ILMNode addedNode = addNode(objToAdd, node.getNodeModelElement(), ilm);
        if (!alreadyInTree) {
            addListsForElement(addedNode);
        }
        if (ilm instanceof InterLanguageMapping) {
            addedNode.setInterMappingName(mappingName);
        }
        boolean addedToMenu = false;
        
        RamRectangleComponent compToRemove = null;
        RamRectangleComponent compToAdd = null;
        
        for (RamRectangleComponent r : node.getNodeComponents()) {
            boolean reuse = false;
            if (ilm instanceof IntraLanguageMapping) {
                reuse = ((IntraLanguageMapping) ilm).isReuse();
            }
            if (r instanceof RamExpendableComponent) {
                TextViewNavBarElement navElementForNewNode = this.getNavigationBarMenuElements().get(
                        0).createTextViewNavBarElement(objToAdd, node.getNodeModelElement(), reuse);
                String currentElementName = EMFModelUtil.getNameAttribute(objToAdd);
                if (currentElementName == null) {
                    currentElementName = objToAdd.toString();
                }
                navElementForNewNode.setName(currentElementName);
                addedNode.nodeComponents.add(navElementForNewNode);
                if (ilm instanceof IntraLanguageMapping) {
                    if (r.getName().equals(((IntraLanguageMapping) ilm).getName())) {
                        RamRectangleComponent parentHidableComp = ((RamExpendableComponent) r
                                ).getHidableComponent();
                        parentHidableComp.addChild(navElementForNewNode);
                        addedToMenu = true;
                    } else {
                        RamRectangleComponent parentHidableComp = ((RamExpendableComponent) r
                                ).getHidableComponent();
                        for (MTComponent child : parentHidableComp.getChildren()) {
                            if (child instanceof RamExpendableComponent) {
                                String parentExpendableName = mappingName;
                                if (child.getName().equals(parentExpendableName)) {
                                    if (child instanceof RamExpendableComponent) {
                                        RamRectangleComponent uncleComponent = ((RamExpendableComponent) child
                                                ).getHidableComponent();
                                        uncleComponent.addChild(navElementForNewNode);
                                        addedToMenu = true;
                                    }
                                }
                                
                            }
                        }
                    }
                }
                if (!addedToMenu) {
                    String parentExpendableName = mappingName;
                    
                    RamTextComponent labelComponent = new RamTextComponent(parentExpendableName + ": ");
                    
                    List<EObject> emptyList = new ArrayList<EObject>();
                    RamListComponent<EObject> innerElementList = new RamListComponent<EObject>(emptyList);
                    
                    innerElementList.addChild(navElementForNewNode);
                    
                    RamRectangleComponent parentHidableComp = ((RamExpendableComponent) r
                            ).getHidableComponent();
                    
                    RamExpendableComponent innerComp = new RamExpendableComponent(labelComponent, 
                            innerElementList);
                    innerComp.getFirstLine().setNoFill(true);
                    innerComp.getFirstLine().setNoStroke(true);
                    innerComp.setNoFill(true);
                    innerComp.setNoStroke(true);
                    innerComp.setName(parentExpendableName);
                    
                    parentHidableComp.addChild(innerComp);
                }
                
            } else if (r instanceof TextViewNavBarElement) {
                String parentExpendableName = mappingName;
                
                RamTextComponent labelComponent = new RamTextComponent(parentExpendableName + ": ");
                
                TextViewNavBarElement navElementForNewNode = this.getNavigationBarMenuElements().get(
                        0).createTextViewNavBarElement(objToAdd, node.getNodeModelElement(), reuse);
                String currentElementName = EMFModelUtil.getNameAttribute(objToAdd);
                if (currentElementName == null) {
                    currentElementName = objToAdd.toString();
                }
                navElementForNewNode.setName(currentElementName);
                
                List<EObject> emptyList = new ArrayList<EObject>();
                RamListComponent<EObject> innerElementList = new RamListComponent<EObject>(emptyList);
                
                innerElementList.addChild(navElementForNewNode);
                
                RamExpendableComponent innerComp = new RamExpendableComponent(labelComponent, innerElementList);
                innerComp.getFirstLine().setNoFill(true);
                innerComp.getFirstLine().setNoStroke(true);
                innerComp.setNoFill(true);
                innerComp.setNoStroke(true);
                
                innerComp.setName(parentExpendableName);
                
                List<EObject> emptyListOuter = new ArrayList<EObject>();
                RamListComponent<EObject> nodeComponentList = new RamListComponent<EObject>(emptyListOuter);
                
                nodeComponentList.addChild(innerComp);
                
                RamExpendableComponent expendable = new RamExpendableComponent(r, nodeComponentList);
                expendable.getFirstLine().setNoFill(true);
                expendable.getFirstLine().setNoStroke(true);
                expendable.setNoFill(true);
                expendable.setNoStroke(true);
                
                RamRectangleComponent grandparentComponent = node.getParentNode().getNodeComponents().get(0);
                if (grandparentComponent instanceof RamExpendableComponent) {
                    RamRectangleComponent gpList = ((RamExpendableComponent) grandparentComponent
                            ).getHidableComponent();
                    gpList.removeChild(r);
                    gpList.addChild(expendable);
                }
                addedToMenu = true;
                
                addedNode.nodeComponents.add(navElementForNewNode);
            
                compToRemove = r;
                compToAdd = expendable;
            }
        }
        
        if (!addedToMenu) {
            String childName = null;
            if (ilm instanceof IntraLanguageMapping) {
                childName = ((IntraLanguageMapping) ilm).getName();
            } else {
                childName = addedNode.interMappingName;
            }
            if (!menu.checkForDuplicateNode(rootNode, ilm)) {
                
                menu.addMenuElementGeneric(childName, rootNode, NavigationBar.getInstance().getGenericNamerInner(), 
                        null, ilm);
            }
        }
        if (compToRemove != null) {
            node.getNodeComponents().remove(compToRemove);
        }
        if (compToAdd != null) {
            node.getNodeComponents().add(compToAdd);
        }
    }
    
    /**
     * Gets the name for a given node based on the node's object.
     * @param node the node in question
     * @return the name of the node
     */
    private String getNameForNode(ILMNode node) {
        EObject nodeObject = node.getNodeModelElement();
        
        String elementName = EMFModelUtil.getNameAttribute(nodeObject);
        
        if (elementName == null) {
            elementName = nodeObject.toString();
        }
        return elementName;
    }
    
    /**
     * Removes a node from the tree and should destroy all children as well by disconnecting them.
     * @param node node to destroy
     */
    private void removeNode(ILMNode node) {
        node.children = null;
        node.getParentNode().children.remove(node);
    }
    
    /**
     * Should remove a child from a given RamRectangleComponent if that child's name is equal to the given name that
     * is input.
     * @param elementName the name to be searched for
     * @param compForRemoval the component to search through
     * @return whether or not an element has been removed.
     */
    private boolean removeChildFromHidableComponent(String elementName, RamRectangleComponent compForRemoval) {
        MTComponent [] childComponents = compForRemoval.getChildren();
        MTComponent childToRemove = null;
        
        boolean nodeRemoved = false;
        
        for (MTComponent child : childComponents) {
            if (child.getName().equals(elementName)) {
                childToRemove = child;
                nodeRemoved = true;
            }
        }
        
        if (childToRemove != null) {
            compForRemoval.removeChild(childToRemove);
        }
        return nodeRemoved;
    }
    
    /** 
     * Should remove an element from both the tree and the nav bar view when it has been deleted in some way.
     * @param node the parent node which will have its child removed
     * @param objToRemove the object which has been removed from the scene
     * @param ilm the mapping which the list is related to
     */
    protected void removeElementFromTree(ILMNode node, EObject objToRemove, IntraLanguageMapping ilm) {
        List<ILMNode> nodesToRemove = new ArrayList<ILMNode>();
        for (ILMNode childNode : node.getChildren()) {
            if (objToRemove.equals(childNode.getNodeModelElement())) {
                String currentElementName = getNameForNode(childNode);
                String parentNodeName = getNameForNode(node);
                for (RamRectangleComponent r : node.getNodeComponents()) {
                    if (r instanceof RamExpendableComponent) {
                        RamRectangleComponent parentHidable = ((RamExpendableComponent) r).getHidableComponent();
                        if (r.getName().equals(ilm.getName())) {
                            boolean removed = removeChildFromHidableComponent(currentElementName, parentHidable);
                            if (removed) {
                                nodesToRemove.add(childNode);
                            }
                        } else if (r.getName().equals(parentNodeName)) {
                            MTComponent [] parentHidableChildren = parentHidable.getChildren();
                            String parentTotalName = parentNodeName + " " + ilm.getName();
                            for (MTComponent parentHChild : parentHidableChildren) {
                                if (parentHChild.getName().equals(parentTotalName)) {
                                    if (parentHChild instanceof RamExpendableComponent) {
                                        RamRectangleComponent hidableComponentForRemoval = ((RamExpendableComponent) 
                                                parentHChild).getHidableComponent();
                                        boolean removed = removeChildFromHidableComponent(currentElementName, 
                                                hidableComponentForRemoval);
                                        if (removed) {
                                            nodesToRemove.add(childNode);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        for (ILMNode nodeToRemove : nodesToRemove) {
            removeNode(nodeToRemove);
        }
    }
    
    private void performAddNotification(Notification notification, EObject currentObj, ILMNode node, EReference hop, 
            IntraLanguageMapping mapping) {
        if (notification.getFeature().equals(hop) && notification.getNotifier().equals(currentObj)) {
            if (notification.getNewValue() instanceof List) {
                List<?> newVal = (List<?>) notification.getNewValue();
                for (Object o : newVal) {
                    if (o instanceof EObject) {
                        checkMappingForAddition(node, (EObject) o, mapping, hop.getName());
                    }
                }   
            } else if (notification.getNewValue() instanceof EObject) {
                checkMappingForAddition(node, (EObject) notification.getNewValue(), mapping, hop.getName());
            }
        }
    }
    
    private void performRemoveNotification(Notification notification, EObject currentObj, ILMNode node, EReference hop, 
            IntraLanguageMapping mapping) {
        if (notification.getFeature().equals(hop) && notification.getNotifier().equals(
                currentObj)) {
            if (notification.getOldValue() instanceof EObject) {
                removeElementFromTree(node, (EObject) notification.getOldValue(), mapping);
            }
        }
    }
    
    public void findEList(ILMNode node, IntraLanguageMapping mapping) {
        EObject nodeObject = node.getNodeModelElement();

        EList<EReference> hops = mapping.getHops();
        
        List<EObject> currentObjects = new ArrayList<EObject>();
        
        currentObjects.add(nodeObject);
        
        int numberOfHopsMade = 0;
        
        System.out.println(node.getNodeModelElement().eClass() + "  " + mapping.getFrom());
        
        while (numberOfHopsMade < hops.size()) {
            EReference hop = hops.get(numberOfHopsMade);
            numberOfHopsMade++;
            List<EObject> currentResponses = new ArrayList<EObject>();
            for (EObject currentObj : currentObjects) {
                EList<EObject> responses = NavigationMappingHelper.getInstance().performHop(currentObj, hop);
                if (responses == null) {
                    return;
                }
                if (!responses.isEmpty()) {
                    currentResponses.addAll(responses); 
                }
                if (numberOfHopsMade == hops.size()) {
//                    int size = responses.size();
//                    node.listsToWatch.put(responses, size);
                    EMFEditUtil.addListenerFor(currentObj, new INotifyChangedListener() {
                        @Override
                        public void notifyChanged(Notification notification) {
                            if (notification.getEventType() == Notification.ADD) {
                                performAddNotification(notification, currentObj, node, hop, mapping);
                            } else if (notification.getEventType() == Notification.REMOVE) {
                                performRemoveNotification(notification, currentObj, node, hop, mapping);
                            }

                            
                        }
                    });
                }
            }
            
            currentObjects = currentResponses;
        }
        
    }
    
    /**
     * Should take each of the nodes within the tree and for each of the mappings relevant to that node's EObect should
     * associate a listener to that node so that the system will keep track of any additions to the list.
     * @param mappings the mappings for the system.
     */
    public void makeChildMappingLists(List<IntraLanguageMapping> mappings) {
        
        Map<EObject, ILMNode> mapOfEObjects = new HashMap<EObject, ILMNode>();
        
        for (ILMNode node : getAllNodes()) {
            if (mapOfEObjects.containsKey(node.getNodeModelElement())) {
                ILMNode oldNodeForEObject = mapOfEObjects.get(node.getNodeModelElement());
                if (node.nodeDepth < oldNodeForEObject.nodeDepth) {
                    mapOfEObjects.replace(node.getNodeModelElement(), node);
                }
            } else {
                mapOfEObjects.put(node.getNodeModelElement(), node);
            }
        }
        
        List<ILMNode> nodesWithLowestDepth = new ArrayList<ILMNode>();
        for (EObject obj : mapOfEObjects.keySet()) {
            nodesWithLowestDepth.add(mapOfEObjects.get(obj));
        }

        for (ILMNode node : nodesWithLowestDepth) {
            EObject nodeObject = node.nodeModelElement;
            EClass nodeClass = nodeObject.eClass();
            
            
            for (IntraLanguageMapping curMapping : mappings) {
                if (curMapping.getFrom().equals(nodeClass)) {
                    findEList(node, curMapping);
                } else if (node == rootNode && curMapping.isReuse()) {
                    COREExternalArtefact extArtefact = COREArtefactUtil.getReferencingExternalArtefact(
                            node.getNodeModelElement());
                    if (extArtefact != null) {
                        if (curMapping.getFrom().equals(CorePackage.eINSTANCE.getCOREArtefact())) {
                            if (nodeClass instanceof COREArtefact) {
                                findEList(node, curMapping);
                            }
                        }
                    }
                } else {
                    EList<EClass> objectSuperClasses = nodeClass.getESuperTypes();
                    for (EClass superType : objectSuperClasses) {
                        if (curMapping.getFrom().equals(superType)) {
                            findEList(node, curMapping);
                        }
                    }
                }
            }
        }
    }
    
    /**
     * Finds the node within the tree with the lowest that has as its EObject the EObject input to the method.
     * Lowest depth here meaning closest to the root node.
     * @param obj the object which the node should have
     * @return the node closest to the root with the relavent EObject.
     */
    protected ILMNode getNodeWithLowestDepth(EObject obj) {
        ILMNode lowestNodeWithEObject = null;
        for (ILMNode node : getAllNodes()) {
            if (node.getNodeModelElement().equals(obj)) {
                if (lowestNodeWithEObject == null) {
                    lowestNodeWithEObject = node;
                } else if (lowestNodeWithEObject.nodeDepth > node.nodeDepth) {
                    lowestNodeWithEObject = node;
                }
            }
        }
        return lowestNodeWithEObject;
    }
    
    public void printTreeForTesting() {
        ILMNode curNode = rootNode;
        int curDepth = 0;
        List<ILMNode> listOfNodesToPrint = new ArrayList<ILMNode>();
        listOfNodesToPrint.add(curNode);
        while (listOfNodesToPrint.size() != 0) {
            ILMNode node = listOfNodesToPrint.get(0);
            listOfNodesToPrint.addAll(node.children);
            if (node.nodeDepth == curDepth) {
                System.out.print(node.getNodeModelElement() + "         ");
            } else {
                curDepth++;
                System.out.println();
                System.out.print(node.getNodeModelElement() + "         ");
            }
            listOfNodesToPrint.remove(0);
        }
        System.out.println();
        
    }

}
