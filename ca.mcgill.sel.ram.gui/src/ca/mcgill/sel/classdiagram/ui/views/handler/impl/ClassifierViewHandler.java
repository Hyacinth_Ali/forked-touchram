package ca.mcgill.sel.classdiagram.ui.views.handler.impl;

import java.util.List;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EObject;
import org.mt4j.input.inputProcessors.componentProcessors.tapAndHoldProcessor.TapAndHoldEvent;

import ca.mcgill.sel.classdiagram.language.controller.ControllerFactory;
import ca.mcgill.sel.classdiagram.ui.views.BaseView;
import ca.mcgill.sel.classdiagram.ui.views.ClassifierView;
import ca.mcgill.sel.classdiagram.ui.views.handler.IClassifierViewHandler;
import ca.mcgill.sel.core.COREPerspective;
import ca.mcgill.sel.core.perspective.COREPerspectiveUtil;
import ca.mcgill.sel.core.perspective.PerspectiveName;
import ca.mcgill.sel.ram.ui.components.RamRectangleComponent;
import ca.mcgill.sel.ram.ui.components.menu.RamLinkedMenu;
import ca.mcgill.sel.ram.ui.components.navigationbar.NavigationBar;
import ca.mcgill.sel.ram.ui.perspective.controller.PerspectiveControllerFactory;
import ca.mcgill.sel.ram.ui.utils.Icons;
import ca.mcgill.sel.ram.ui.utils.Strings;
import ca.mcgill.sel.ram.ui.views.handler.ILinkedMenuListener;

/**
 * The default handler for a {@link ClassifierView}. Handlers for Sub-classes of {@link ca.mcgill.sel.ram.Classifier}
 * may use the default behavior of this handler.
 * 
 * @author mschoettle
 * @author yhattab
 */
public abstract class ClassifierViewHandler extends BaseViewHandler implements IClassifierViewHandler,
        ILinkedMenuListener {

    /**
     * The action to add a new operation.
     */
    protected static final String ACTION_OPERATION_ADD = "view.class.operation.add";
    /**
     * The action to add a new constructor.
     */
    protected static final String ACTION_CONSTRUCTOR_ADD = "view.class.constructor.add";

    /**
     * The sub menu id which contains actions related to operation.
     */
    protected static final String SUBMENU_OPERATION = "sub.operation";

    @Override
    public void actionPerformed(ActionEvent event) {
        String actionCommand = event.getActionCommand();
        RamRectangleComponent pressedButton = (RamRectangleComponent) event.getTarget();
        RamLinkedMenu linkedMenu = (RamLinkedMenu) pressedButton.getParentOfType(RamLinkedMenu.class);
        if (linkedMenu != null) {

            ClassifierView<?> clazz = (ClassifierView<?>) linkedMenu.getLinkedView();

            if (ACTION_OPERATION_ADD.equals(actionCommand)) {
                createOperation(clazz);
            } else if (ACTION_CONSTRUCTOR_ADD.equals(actionCommand)) {
                createConstructor(clazz);
            }

        }
        super.actionPerformed(event);
    }

    @Override
    public List<EObject> getEObjectToListenForUpdateMenu(RamRectangleComponent rectangle) {
        return super.getEObjectToListenForUpdateMenu(rectangle);
    }

    @Override
    public void updateMenu(RamLinkedMenu menu, Notification notification) {
        super.updateMenu(menu, notification);
    }

    @Override
    public void initMenu(RamLinkedMenu menu, TapAndHoldEvent event) {
        super.initMenu(menu, event);
        COREPerspective perspective = NavigationBar.getInstance().getCurrentPerspective();
        String currentLanguageRole = NavigationBar.getInstance().getCurrentLanguageRole();
        if (PerspectiveControllerFactory.INSTANCE.getActionValidator()
                .canCreateOperation(perspective, currentLanguageRole)) {
            menu.addSubMenu(1, SUBMENU_OPERATION);
            menu.addAction(Strings.MENU_OPERATION_ADD, Icons.ICON_MENU_ADD_OPERATION, ACTION_OPERATION_ADD, this,
                    SUBMENU_OPERATION, true);
            menu.addAction(Strings.MENU_CONSTRUCTOR_ADD, Icons.ICON_MENU_ADD_CONSTRUCTOR, ACTION_CONSTRUCTOR_ADD, this,
                    SUBMENU_OPERATION, true);
        }
    }

    @Override
    public void removeRepresented(BaseView<?> baseView) {
        COREPerspective perspective = NavigationBar.getInstance().getCurrentPerspective();
        String currentRoleName = NavigationBar.getInstance().getCurrentLanguageRole();
        switch (perspective.getName()) {
//            case PerspectiveName.DOMAIN_MODEL_PERSPECTIVE:
//            case PerspectiveName.DESIGN_MODEL_PERSPECTIVE:
//                PerspectiveControllerFactory.INSTANCE.getCdmPerspectiveController()
//                        .removeClassifier(baseView.getClassifier());
//                break;
//            case PerspectiveName.DOMAIN_DESIGN_MODEL_PERSPECTIVE:
//                PerspectiveControllerFactory.INSTANCE.getDomainDesignModelController().deleteClassifier(
//                        perspective, currentRoleName, baseView.getClassifier());
//                break;
                
            case PerspectiveName.DOMAIN_USECASE_MODEL_PERSPECTIVE:
                PerspectiveControllerFactory.INSTANCE.getDomainUseCaseController().deleteClassifier(
                        perspective, currentRoleName, baseView.getClassifier());
                break;
                
            case PerspectiveName.DOMAIN_DESIGN_USECASE_PERSPECTIVE:
                PerspectiveControllerFactory.INSTANCE.getDomainDesignUseCaseController().deleteClassifier(
                        perspective, currentRoleName, baseView.getClassifier());
                break;
                
            default:
                ControllerFactory.INSTANCE.getClassDiagramController().removeClassifier(baseView.getClassifier());
        }
    }

}
