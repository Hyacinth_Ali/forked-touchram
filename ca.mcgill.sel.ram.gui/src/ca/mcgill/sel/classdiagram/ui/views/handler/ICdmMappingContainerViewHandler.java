package ca.mcgill.sel.classdiagram.ui.views.handler;

import ca.mcgill.sel.classdiagram.CDAttributeMapping;
import ca.mcgill.sel.classdiagram.CDClassifierMapping;
import ca.mcgill.sel.classdiagram.CDEnumLiteralMapping;
import ca.mcgill.sel.classdiagram.CDEnumMapping;
import ca.mcgill.sel.classdiagram.CDOperationMapping;
import ca.mcgill.sel.classdiagram.CDParameterMapping;
import ca.mcgill.sel.ram.AttributeMapping;
import ca.mcgill.sel.ram.ClassifierMapping;
import ca.mcgill.sel.ram.EnumLiteralMapping;
import ca.mcgill.sel.ram.EnumMapping;
import ca.mcgill.sel.ram.OperationMapping;
import ca.mcgill.sel.ram.ParameterMapping;

/**
 * This interface is implemented by handlers that handle events related to mappings.
 * 
 * @author eyildirim
 */
public interface ICdmMappingContainerViewHandler {
    
    /**
     * Adds a new {@link AttributeMapping}.
     * 
     * @param classifierMapping the current classifier mapping
     */
    void addAttributeMapping(CDClassifierMapping classifierMapping);
    
    /**
     * Adds a new {@link EnumLiteralMapping}.
     * 
     * @param enumMapping the current enum mapping
     */
    void addEnumLiteralMapping(CDEnumMapping enumMapping);
    
    /**
     * Adds a new {@link OperationMapping}.
     * 
     * @param classifierMapping the current classifier mapping
     */
    void addOperationMapping(CDClassifierMapping classifierMapping);
    
    /**
     * Adds a new {@link ParameterMapping}.
     * 
     * @param operationMapping the current operation mapping
     */
    void addParameterMapping(CDOperationMapping operationMapping);
    
    /**
     * Deletes the given {@link AttributeMapping}.
     * 
     * @param attributeMapping the attribute mapping to delete
     */
    void deleteAttributeMapping(CDAttributeMapping attributeMapping);
    
    /**
     * Deletes the given {@link EnumLiteralMapping}.
     * 
     * @param enumLiteralMapping the enum literal mapping to delete
     */
    void deleteEnumLiteralMapping(CDEnumLiteralMapping enumLiteralMapping);
    
   /**
     * Deletes the give {@link ClassifierMapping}.
     * 
     * @param classifierMapping the classifier mapping to delete
     */
    void deleteClassifierMapping(CDClassifierMapping classifierMapping);
    
    /**
     * Deletes the give {@link EnumMapping}.
     * 
     * @param enumMapping the enum mapping to delete
     */
    void deleteEnumMapping(CDEnumMapping enumMapping);
    
   /**
     * Deletes the given {@link OperationMapping}.
     * 
     * @param operationMapping the operation mapping to delete
     */
    void deleteOperationMapping(CDOperationMapping operationMapping);
    
    /**
     * Deletes the given {@link ParameterMapping}.
     * 
     * @param parameterMapping the parameter mapping to delete
     */
    void deleteParameterMapping(CDParameterMapping parameterMapping);
}
