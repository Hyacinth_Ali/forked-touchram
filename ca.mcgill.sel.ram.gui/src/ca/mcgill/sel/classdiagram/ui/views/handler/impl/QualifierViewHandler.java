package ca.mcgill.sel.classdiagram.ui.views.handler.impl;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.mt4j.input.inputProcessors.componentProcessors.tapProcessor.TapEvent;

import ca.mcgill.sel.classdiagram.AssociationEnd;
import ca.mcgill.sel.classdiagram.Type;
import ca.mcgill.sel.classdiagram.language.controller.ControllerFactory;
import ca.mcgill.sel.classdiagram.ui.views.QualifierView;
import ca.mcgill.sel.ram.ui.views.handler.impl.TextViewHandler;

/**
 * The default handler for a Qualifier Text View. It allows selecting another qualifier type
 * 
 * @author yhattab
 * */

public class QualifierViewHandler extends TextViewHandler {
    
    /**
     * Sets the value of the text.
     * 
     * @param data the object containing the feature
     * @param feature the feature of which the value should be changed
     * @param value the new value of the feature
     */
    @Override
    protected void setValue(EObject data, EStructuralFeature feature, Object value) {
        ControllerFactory.INSTANCE.getAssociationController().setQualifier((AssociationEnd) 
                feature, (Type) value);
        
    }

    @Override
    public boolean processTapEvent(TapEvent tapEvent) {
        if (tapEvent.isDoubleTap()) {
            QualifierView qualifierView = (QualifierView) tapEvent.getTarget();
            QualifierView.selectQualifier(qualifierView.getAssociationEnd(), tapEvent.getLocationOnScreen());
            return true;
        }

        return false;
    }
}
