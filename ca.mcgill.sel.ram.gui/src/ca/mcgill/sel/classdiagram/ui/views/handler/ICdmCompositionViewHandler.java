package ca.mcgill.sel.classdiagram.ui.views.handler;

import ca.mcgill.sel.core.COREModelComposition;
import ca.mcgill.sel.ram.ui.events.listeners.ITapListener;
import ca.mcgill.sel.ram.ui.views.CompositionView;

/**
 * This interface can be implemented by a handler which handles the operations of a composition view  for the
 * classdiagram language.
 * 
 * @author joerg
 */
public interface ICdmCompositionViewHandler extends ITapListener {
    
    /**
     * This function is used to add a classifier mapping view.
     * 
     * @param composition the composition
     */
    void addClassifierMapping(COREModelComposition composition);
    
    /**
     * This function is used to add an enum mapping view.
     * 
     * @param composition the composition
     */
    void addEnumMapping(COREModelComposition composition);
    
   /**
     * Hides all mapping details.
     * 
     * @param compositionView the composition view
     */
    void hideMappingDetails(CompositionView compositionView);
    
    /**
     * Loads the extended or reused classdiagram and displays it.
     * 
     * @param myCompositionView the composition
     */
    void showExternalModelOfComposition(CompositionView myCompositionView);
    
    /**
     * Shows all classifier mapping details.
     * 
     * @param compositionView the composition view
     */
    void showMappingDetails(CompositionView compositionView);
    
    /**
     * Switches to the split view for mapping specifications.
     * 
     * @param myCompositionView the composition
     */
    void switchToSplitView(CompositionView myCompositionView);
    
}
