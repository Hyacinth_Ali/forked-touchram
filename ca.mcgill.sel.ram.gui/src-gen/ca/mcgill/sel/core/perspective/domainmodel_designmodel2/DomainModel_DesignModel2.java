package ca.mcgill.sel.core.perspective.domainmodel_designmodel2;

import java.io.IOException;
import java.util.List;

import org.eclipse.emf.common.util.URI;

import ca.mcgill.sel.commons.ResourceUtil;
import ca.mcgill.sel.commons.emf.util.AdapterFactoryRegistry;
import ca.mcgill.sel.commons.emf.util.ResourceManager;
import ca.mcgill.sel.core.COREArtefact;
import ca.mcgill.sel.core.COREConcern;
import ca.mcgill.sel.core.COREExternalLanguage;
import ca.mcgill.sel.core.COREPerspective;
import ca.mcgill.sel.core.CoreFactory;
import ca.mcgill.sel.core.CorePackage;
import ca.mcgill.sel.core.provider.CoreItemProviderAdapterFactory;
import ca.mcgill.sel.core.util.COREModelUtil;
import ca.mcgill.sel.core.util.CoreResourceFactoryImpl;
import ca.mcgill.sel.ram.ui.utils.ResourceUtils;

/**
 * This is the base class for creating and then saving a perspective. To instantiate and then save
 * the bnew poerspective, just run the class as a regular java class..
 * 
 * @author Hyacinth Ali
 *
 *@generated
 *
 */
public class DomainModel_DesignModel2 {
    
 public static void main(String[] args) {
        
        // Initialize ResourceManager
        ResourceManager.initialize();
    
        // Initialize CORE packages.
        CorePackage.eINSTANCE.eClass();
    
        // Register resource factories
        ResourceManager.registerExtensionFactory("core", new CoreResourceFactoryImpl());
    
        // Initialize adapter factories
        AdapterFactoryRegistry.INSTANCE.addAdapterFactory(CoreItemProviderAdapterFactory.class);
        
        ResourceUtils.loadLibraries();
       
        // create a perspective
        COREConcern perspectiveConcern = COREModelUtil.createConcern("Perspective Design");
        
        COREPerspective perspective = CoreFactory.eINSTANCE.createCOREPerspective();
        perspective.setName("Perspective Design");
        
        //Add perspective to the concern
        perspectiveConcern.getArtefacts().add(perspective);
        

        // Add existing external languages, if any
        List<String> languages = ResourceUtil.getResourceListing("models/testlanguages/", ".core");
        if (languages != null) {
        	for (String l : languages) {
        		// load existing languages
        		URI fileURI = URI.createURI(l);
        		COREConcern languageConcern = (COREConcern) ResourceManager.loadModel(fileURI);
        		for (COREArtefact a : languageConcern.getArtefacts()) {
        			if (a instanceof COREExternalLanguage) {
        				COREExternalLanguage existingLanguage = (COREExternalLanguage) a;
        				if (existingLanguage.getName().equals("CDM2")) {
        					perspective.getLanguages().put("Domain_Model", existingLanguage);
        				} 
        			}
        		}
        	}
        	for (String l : languages) {
        		// load existing languages
        		URI fileURI = URI.createURI(l);
        		COREConcern languageConcern = (COREConcern) ResourceManager.loadModel(fileURI);
        		for (COREArtefact a : languageConcern.getArtefacts()) {
        			if (a instanceof COREExternalLanguage) {
        				COREExternalLanguage existingLanguage = (COREExternalLanguage) a;
        				if (existingLanguage.getName().equals("CDM2")) {
        					perspective.getLanguages().put("Design_Model", existingLanguage);
        				} 
        			}
        		}
        	}

        }
        
        // initialize perspective with perspective actions and mappings
        DomainModel_DesignModel2Specification.initializePerspective(perspective);
        
        String fileName = "/Users/hyacinthali/git/touchram/ca.mcgill.sel.ram/resources/models/testperspectives/"
           + "DomainModel_DesignModel2_Perspective";
        
        try {
            ResourceManager.saveModel(perspectiveConcern, fileName.concat("." + "core"));
        } catch (IOException e) {
            // Shouldn't happen.
            e.printStackTrace();
        } 
   }
}

