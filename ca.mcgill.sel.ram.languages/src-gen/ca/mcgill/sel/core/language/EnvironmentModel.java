package ca.mcgill.sel.core.language;

import java.io.IOException;
import org.eclipse.emf.ecore.EObject;

import ca.mcgill.sel.commons.emf.util.AdapterFactoryRegistry;
import ca.mcgill.sel.commons.emf.util.ResourceManager;
import ca.mcgill.sel.core.util.CoreResourceFactoryImpl;
import ca.mcgill.sel.core.provider.CoreItemProviderAdapterFactory;
import ca.mcgill.sel.ram.ui.utils.ResourceUtils;
import ca.mcgill.sel.core.util.COREModelUtil;

import ca.mcgill.sel.core.*;

import ca.mcgill.sel.environmentmodel.*;

public class EnvironmentModel {

    public static void main (String[] args) {

        // Initialize ResourceManager
        ResourceManager.initialize();

        // Initialize CORE packages.
        CorePackage.eINSTANCE.eClass();

        // Register resource factories
        ResourceManager.registerExtensionFactory("core", new CoreResourceFactoryImpl());

        // Initialize adapter factories
        AdapterFactoryRegistry.INSTANCE.addAdapterFactory(CoreItemProviderAdapterFactory.class);

        ResourceUtils.loadLibraries();

        createLanguage();
    }

    /**
     * This method registers existing language (with its details) in TouchCORE.
     *
     * @author Hyacinth Ali
     * @return the class diagram {@link COREExternalLanguage}
     *
     * @generated
     */
    public static COREExternalLanguage createLanguage() {

    // create a language concern
    COREConcern langConcern = COREModelUtil.createConcern("EnvironmentModel");

    COREExternalLanguage language = CoreFactory.eINSTANCE.createCOREExternalLanguage();
    language.setName("EnvironmentModel");
    language.setNsURI("http://cs.mcgill.ca/sel/em/1.0");
    language.setResourceFactory("ca.mcgill.sel.environmentmodel.util.EmResourceFactoryImpl");
    language.setAdapterFactory("ca.mcgill.sel.environmentmodel.provider.EmItemProviderAdapterFactory");
    language.setWeaverClassName("");
    language.setFileExtension("em");
    language.setModelUtilClassName("ca.mcgill.sel.environmentmodel.util.EmModelUtil");

    createLanguageElements(language);

    createLanguageActions(language);

    langConcern.getArtefacts().add(language);

    String language1FileName = "/Users/hyacinthali/workspace/TouchCORE2/touchram/ca.mcgill.sel.ram/resources/models/testlanguages/"
            + "EnvironmentModel";

     try {
         ResourceManager.saveModel(langConcern, language1FileName.concat("." + "core"));
     } catch (IOException e) {
         // Shouldn't happen.
         e.printStackTrace();
     }

     return language;
    }

    private static void createLanguageElements(COREExternalLanguage language) {

        // create classdiagram core language element
        CORELanguageElement environmentModelElement = createCORELanguageElement(language, EmPackage.eINSTANCE.getEnvironmentModel());


        // create classdiagram core language element
        CORELanguageElement actorTypeElement = createCORELanguageElement(language, EmPackage.eINSTANCE.getActorType());

        // create nested element
        CORELanguageElement actorTypeName = CoreFactory.eINSTANCE.createCORELanguageElement();
        actorTypeName.setName("name");
        actorTypeName.setLanguageElement(EmPackage.eINSTANCE.getNamedElement_Name());
        
        actorTypeElement.getNestedElements().add(actorTypeName);
        actorTypeName.setOwner(actorTypeElement);
        

        // create classdiagram core language element
        CORELanguageElement actorElement = createCORELanguageElement(language, EmPackage.eINSTANCE.getActor());

        // create nested element
        CORELanguageElement actorName = CoreFactory.eINSTANCE.createCORELanguageElement();
        actorName.setName("name");
        actorName.setLanguageElement(EmPackage.eINSTANCE.getNamedElement_Name());
        
        actorElement.getNestedElements().add(actorName);
        actorName.setOwner(actorElement);
        

        // create classdiagram core language element
        CORELanguageElement messageTypeElement = createCORELanguageElement(language, EmPackage.eINSTANCE.getMessageType());

        // create nested element
        CORELanguageElement messageTypeName = CoreFactory.eINSTANCE.createCORELanguageElement();
        messageTypeName.setName("name");
        messageTypeName.setLanguageElement(EmPackage.eINSTANCE.getNamedElement_Name());
        
        messageTypeElement.getNestedElements().add(messageTypeName);
        messageTypeName.setOwner(messageTypeElement);
        

        // create classdiagram core language element
        CORELanguageElement messageElement = createCORELanguageElement(language, EmPackage.eINSTANCE.getMessage());



    }

    /**
    * This method creates an instance of {@link CORELanguageElement} for a given language {@link COREExternalLanguage}
    * and its existing language element.
    * @param language - the language in question.
    * @param languageElement - the existing language element.
    * @return the new instance of {@link CORELanguageElement}
    *
    * @generated
    */
    private static CORELanguageElement createCORELanguageElement(COREExternalLanguage language,
            EObject languageElement) {

        // create core language element
        CORELanguageElement coreLanguageElement = CoreFactory.eINSTANCE.createCORELanguageElement();
        coreLanguageElement.setLanguageElement(languageElement);
        language.getLanguageElements().add(coreLanguageElement);

        return coreLanguageElement;
    }

    /**
    * This method creates language actions, which can be manipulated by the perspectives
    * which reuse the language.
    *
    * @author Hyacinth Ali
    * @param language - the language
    *
    * @generated
    */
    private static void createLanguageActions(COREExternalLanguage language) {

    }
}
