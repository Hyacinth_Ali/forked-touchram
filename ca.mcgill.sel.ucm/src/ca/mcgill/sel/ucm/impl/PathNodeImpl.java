/**
 */
package ca.mcgill.sel.ucm.impl;

import ca.mcgill.sel.ucm.ComponentRef;
import ca.mcgill.sel.ucm.NodeConnection;
import ca.mcgill.sel.ucm.PathNode;
import ca.mcgill.sel.ucm.UCMPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Path Node</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link ca.mcgill.sel.ucm.impl.PathNodeImpl#getSucc <em>Succ</em>}</li>
 *   <li>{@link ca.mcgill.sel.ucm.impl.PathNodeImpl#getPred <em>Pred</em>}</li>
 *   <li>{@link ca.mcgill.sel.ucm.impl.PathNodeImpl#getCompRef <em>Comp Ref</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class PathNodeImpl extends UCMModelElementImpl implements PathNode {
    /**
     * The cached value of the '{@link #getSucc() <em>Succ</em>}' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getSucc()
     * @generated
     * @ordered
     */
    protected EList<NodeConnection> succ;

    /**
     * The cached value of the '{@link #getPred() <em>Pred</em>}' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getPred()
     * @generated
     * @ordered
     */
    protected EList<NodeConnection> pred;

    /**
     * The cached value of the '{@link #getCompRef() <em>Comp Ref</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getCompRef()
     * @generated
     * @ordered
     */
    protected ComponentRef compRef;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected PathNodeImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return UCMPackage.Literals.PATH_NODE;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EList<NodeConnection> getSucc() {
        if (succ == null) {
            succ = new EObjectWithInverseResolvingEList<NodeConnection>(NodeConnection.class, this, UCMPackage.PATH_NODE__SUCC, UCMPackage.NODE_CONNECTION__SOURCE);
        }
        return succ;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EList<NodeConnection> getPred() {
        if (pred == null) {
            pred = new EObjectWithInverseResolvingEList<NodeConnection>(NodeConnection.class, this, UCMPackage.PATH_NODE__PRED, UCMPackage.NODE_CONNECTION__TARGET);
        }
        return pred;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public ComponentRef getCompRef() {
        if (compRef != null && compRef.eIsProxy()) {
            InternalEObject oldCompRef = (InternalEObject)compRef;
            compRef = (ComponentRef)eResolveProxy(oldCompRef);
            if (compRef != oldCompRef) {
                if (eNotificationRequired())
                    eNotify(new ENotificationImpl(this, Notification.RESOLVE, UCMPackage.PATH_NODE__COMP_REF, oldCompRef, compRef));
            }
        }
        return compRef;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public ComponentRef basicGetCompRef() {
        return compRef;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public NotificationChain basicSetCompRef(ComponentRef newCompRef, NotificationChain msgs) {
        ComponentRef oldCompRef = compRef;
        compRef = newCompRef;
        if (eNotificationRequired()) {
            ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, UCMPackage.PATH_NODE__COMP_REF, oldCompRef, newCompRef);
            if (msgs == null) msgs = notification; else msgs.add(notification);
        }
        return msgs;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public void setCompRef(ComponentRef newCompRef) {
        if (newCompRef != compRef) {
            NotificationChain msgs = null;
            if (compRef != null)
                msgs = ((InternalEObject)compRef).eInverseRemove(this, UCMPackage.COMPONENT_REF__NODES, ComponentRef.class, msgs);
            if (newCompRef != null)
                msgs = ((InternalEObject)newCompRef).eInverseAdd(this, UCMPackage.COMPONENT_REF__NODES, ComponentRef.class, msgs);
            msgs = basicSetCompRef(newCompRef, msgs);
            if (msgs != null) msgs.dispatch();
        }
        else if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, UCMPackage.PATH_NODE__COMP_REF, newCompRef, newCompRef));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @SuppressWarnings("unchecked")
    @Override
    public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case UCMPackage.PATH_NODE__SUCC:
                return ((InternalEList<InternalEObject>)(InternalEList<?>)getSucc()).basicAdd(otherEnd, msgs);
            case UCMPackage.PATH_NODE__PRED:
                return ((InternalEList<InternalEObject>)(InternalEList<?>)getPred()).basicAdd(otherEnd, msgs);
            case UCMPackage.PATH_NODE__COMP_REF:
                if (compRef != null)
                    msgs = ((InternalEObject)compRef).eInverseRemove(this, UCMPackage.COMPONENT_REF__NODES, ComponentRef.class, msgs);
                return basicSetCompRef((ComponentRef)otherEnd, msgs);
        }
        return super.eInverseAdd(otherEnd, featureID, msgs);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case UCMPackage.PATH_NODE__SUCC:
                return ((InternalEList<?>)getSucc()).basicRemove(otherEnd, msgs);
            case UCMPackage.PATH_NODE__PRED:
                return ((InternalEList<?>)getPred()).basicRemove(otherEnd, msgs);
            case UCMPackage.PATH_NODE__COMP_REF:
                return basicSetCompRef(null, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case UCMPackage.PATH_NODE__SUCC:
                return getSucc();
            case UCMPackage.PATH_NODE__PRED:
                return getPred();
            case UCMPackage.PATH_NODE__COMP_REF:
                if (resolve) return getCompRef();
                return basicGetCompRef();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @SuppressWarnings("unchecked")
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case UCMPackage.PATH_NODE__SUCC:
                getSucc().clear();
                getSucc().addAll((Collection<? extends NodeConnection>)newValue);
                return;
            case UCMPackage.PATH_NODE__PRED:
                getPred().clear();
                getPred().addAll((Collection<? extends NodeConnection>)newValue);
                return;
            case UCMPackage.PATH_NODE__COMP_REF:
                setCompRef((ComponentRef)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case UCMPackage.PATH_NODE__SUCC:
                getSucc().clear();
                return;
            case UCMPackage.PATH_NODE__PRED:
                getPred().clear();
                return;
            case UCMPackage.PATH_NODE__COMP_REF:
                setCompRef((ComponentRef)null);
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case UCMPackage.PATH_NODE__SUCC:
                return succ != null && !succ.isEmpty();
            case UCMPackage.PATH_NODE__PRED:
                return pred != null && !pred.isEmpty();
            case UCMPackage.PATH_NODE__COMP_REF:
                return compRef != null;
        }
        return super.eIsSet(featureID);
    }

} //PathNodeImpl
