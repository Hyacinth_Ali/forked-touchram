package ca.mcgill.sel.usecases.language.controller;

/**
 * A factory to obtain controllers for use case diagrams
 * @author rlanguay
 *
 */
public class UseCaseControllerFactory {
    /**
     * The singleton instance of this factory.
     */
    public static final UseCaseControllerFactory INSTANCE = new UseCaseControllerFactory();
    
    private ActorController actorController;
    private UseCaseController useCaseController;
    private UseCaseModelController useCaseDiagramController;
    private FlowController flowController;
    private StepController stepController;
        
    /**
     * Creates a new instance of {@link UseCaseControllerFactory}.
     */
    private UseCaseControllerFactory() {
        
    }
    
    /**
     * Returns the controller for {@link ca.mcgill.sel.usecases.UseCaseDiagram}s.
     * 
     * @return the controller for {@link ca.mcgill.sel.usecases.UseCaseDiagram}s
     */
    public UseCaseModelController getUseCaseDiagramController() {
        if (useCaseDiagramController == null) {
            useCaseDiagramController = new UseCaseModelController();
        }
        
        return useCaseDiagramController;
    }
    
    /**
     * Returns the controller for {@link ca.mcgill.sel.usecases.Actor}s.
     * 
     * @return the controller for {@link ca.mcgill.sel.usecases.Actor}s.
     */
    public ActorController getActorController() {
        if (actorController == null) {
            actorController = new ActorController();
        }
        
        return actorController;
    }
    
    /**
     * Returns the controller for {@link ca.mcgill.sel.usecases.UseCase}s.
     * 
     * @return the controller for {@link ca.mcgill.sel.usecases.UseCase}s.
     */
    public UseCaseController getUseCaseController() {
        if (useCaseController == null) {
            useCaseController = new UseCaseController();
        }
        
        return useCaseController;
    }
    
    /**
     * Returns the controller for {@link ca.mcgill.sel.usecases.Flow}s.
     * 
     * @return the controller for {@link ca.mcgill.sel.usecases.Flow}s.
     */
    public FlowController getFlowController() {
        if (flowController == null) {
            flowController = new FlowController();
        }
        
        return flowController;
    }
    
    /**
     * Returns the controller for {@link ca.mcgill.sel.usecases.step}s.
     * 
     * @return the controller for {@link ca.mcgill.sel.usecases.step}s.
     */
    public StepController getStepController() {
        if (stepController == null) {
            stepController = new StepController();
        }
        
        return stepController;
    }
}
