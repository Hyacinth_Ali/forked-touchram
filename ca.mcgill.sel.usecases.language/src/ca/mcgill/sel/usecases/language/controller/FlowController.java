package ca.mcgill.sel.usecases.language.controller;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.command.CompoundCommand;
import org.eclipse.emf.edit.command.AddCommand;
import org.eclipse.emf.edit.command.MoveCommand;
import org.eclipse.emf.edit.command.RemoveCommand;
import org.eclipse.emf.edit.command.SetCommand;
import org.eclipse.emf.edit.domain.EditingDomain;

import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.commons.emf.util.EMFModelUtil;
import ca.mcgill.sel.usecases.BoundedFlow;
import ca.mcgill.sel.usecases.CommunicationStep;
import ca.mcgill.sel.usecases.ConclusionType;
import ca.mcgill.sel.usecases.Condition;
import ca.mcgill.sel.usecases.ContextStep;
import ca.mcgill.sel.usecases.ContextType;
import ca.mcgill.sel.usecases.Direction;
import ca.mcgill.sel.usecases.Flow;
import ca.mcgill.sel.usecases.SpecificFlow;
import ca.mcgill.sel.usecases.Step;
import ca.mcgill.sel.usecases.UcFactory;
import ca.mcgill.sel.usecases.UcPackage;
import ca.mcgill.sel.usecases.UseCase;
import ca.mcgill.sel.usecases.UseCaseReferenceStep;
import ca.mcgill.sel.usecases.util.AlternateFlowComparator;
import ca.mcgill.sel.usecases.util.UcModelUtil;

public class FlowController extends BaseController {
    public void addCommunicationStep(Flow flow, Direction direction) {
        CommunicationStep step = UcFactory.eINSTANCE.createCommunicationStep();
        step.setDirection(direction);
        
        doAdd(flow, UcPackage.Literals.FLOW__STEPS, step);
    }
    
    public void addContextStep(Flow flow, ContextType contextType) {
        ContextStep step = UcFactory.eINSTANCE.createContextStep();
        step.setType(contextType);
        
        doAdd(flow, UcPackage.Literals.FLOW__STEPS, step);
    }
    
    public void addUseCaseReferenceStep(Flow flow, UseCase useCase) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(flow);
        
        CompoundCommand command = new CompoundCommand();
        
        // If this use case is in the "included" list, remove it
        UseCase containingUseCase = EMFModelUtil.getRootContainerOfType(flow, UcPackage.Literals.USE_CASE);
        if (containingUseCase.getIncludedUseCases() != null && containingUseCase.getIncludedUseCases().contains(useCase)) {
            command.append(
                    RemoveCommand.create(editingDomain, containingUseCase, UcPackage.Literals.USE_CASE__INCLUDED_USE_CASES, useCase));
        }
        
        UseCaseReferenceStep step = UcFactory.eINSTANCE.createUseCaseReferenceStep();
        step.setUsecase(useCase);
        command.append(AddCommand.create(editingDomain, flow, UcPackage.Literals.FLOW__STEPS, step));       
        
        doExecute(editingDomain, command);
    }
    
    public void setStepPosition(Step step, int index) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(step);
        
        Command command = new MoveCommand(
                editingDomain, step.eContainer(), UcPackage.Literals.FLOW__STEPS, 
                step, index);
        
        doExecute(editingDomain, command);
    }
    
    public void deleteStep(Flow flow, Step step) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(flow);
        
        CompoundCommand command = new CompoundCommand();
        
        // If any alternate flows are referencing this step, we need to update them as well
        List<Flow> flowsToChange = UcModelUtil.getFlowsReferencingStep(flow, step);
        for (Flow flowToChange : flowsToChange) {
            if (flowToChange instanceof BoundedFlow) {
                command.append(RemoveCommand.create(
                        editingDomain, flowToChange, UcPackage.Literals.BOUNDED_FLOW__REFERENCED_STEPS, step));
            } else {
                command.append(RemoveCommand.create(
                        editingDomain, flow, UcPackage.Literals.FLOW__ALTERNATE_FLOWS, flowToChange));
            }
        }
        
        List<Flow> conclusionsToChange = UcModelUtil.getConclusionsReferencingStep(flow, step);
        for (Flow flowToChange : conclusionsToChange) {
            command.append(
                    SetCommand.create(editingDomain, flowToChange, UcPackage.Literals.FLOW__CONCLUSION_TYPE, ConclusionType.SUCCESS));
            command.append(
                    SetCommand.create(editingDomain, flowToChange, UcPackage.Literals.FLOW__CONCLUSION_STEP, null));
        }      
        
        command.append(RemoveCommand.create(
                editingDomain, flow, UcPackage.Literals.FLOW__STEPS, step));
        
        doExecute(editingDomain, command);
    }
    
    
    /**
     * Creates an alternate flow in the use case.
     * @param parentFlow The parent flow.
     * @param linkedSteps The list of steps associated with the alternate flow.
     */
    public void createAlternateFlow(Flow parentFlow, List<Step> linkedSteps) {        
        Flow newFlow;
        if (linkedSteps == null || linkedSteps.size() == 0) {
            newFlow = UcFactory.eINSTANCE.createBasicFlow();
        } else if (linkedSteps.size() == 1) {
            newFlow = UcFactory.eINSTANCE.createSpecificFlow();
            ((SpecificFlow) newFlow).setReferencedStep(linkedSteps.get(0));
        } else {
            newFlow = UcFactory.eINSTANCE.createBoundedFlow();
            ((BoundedFlow) newFlow).getReferencedSteps().addAll(linkedSteps);
        }
        
        List<Flow> sortedAlternateFlows = new ArrayList<Flow>(parentFlow.getAlternateFlows());
        sortedAlternateFlows.add(newFlow);
        sortedAlternateFlows.sort(new AlternateFlowComparator(parentFlow));
        
        doAdd(parentFlow, UcPackage.Literals.FLOW__ALTERNATE_FLOWS, newFlow, sortedAlternateFlows.indexOf(newFlow));
    }

    /**
     * Deletes an alternate flow from a use case.
     * @param parentFlow The parent flow.
     * @param flowToRemove The alternate flow.
     */
    public void removeAlternativeFlow(Flow parentFlow, Flow flowToRemove) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(parentFlow);
        
        Command command = RemoveCommand.create(
                editingDomain, parentFlow, UcPackage.Literals.FLOW__ALTERNATE_FLOWS, flowToRemove);
        
        doExecute(editingDomain, command);
    }
    
    /**
     * Sets the position of an alternate flow in the list.
     * @param flow The parent flow
     * @param flowToMove The flow to move
     * @param index the new index
     */
    public void setAlternateFlowPosition(Flow flow, Flow flowToMove, int index) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(flow);
        
        Command command = new MoveCommand(
                editingDomain, flow, UcPackage.Literals.FLOW__ALTERNATE_FLOWS, 
                flowToMove, index);
        
        doExecute(editingDomain, command);
    }
    
    /**
     * Adds a new postcondition, and sets it to the flow.
     * @param flow The flow.
     */
    public void createPostCondition(Flow flow) {
        UseCase useCase = EMFModelUtil.getRootContainerOfType(flow, UcPackage.Literals.USE_CASE);
        
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(flow);
        CompoundCommand command = new CompoundCommand();
        
        Condition postCondition = UcFactory.eINSTANCE.createCondition();
        postCondition.setText("");
        
        command.append(AddCommand.create(editingDomain, useCase, UcPackage.Literals.USE_CASE__CONDITIONS, postCondition));
        command.append(SetCommand.create(editingDomain, flow, UcPackage.Literals.FLOW__POST_CONDITION, postCondition));
        
        doExecute(editingDomain, command);
    }
    
    /**
     * Sets the conclusion type of a flow.
     * @param flow The flow.
     * @param conclusionType The conclusion type.
     */
    public void setConclusionType(Flow flow, ConclusionType conclusionType) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(flow);
        CompoundCommand command = new CompoundCommand();
        
        command.append(
                SetCommand.create(editingDomain, flow, UcPackage.Literals.FLOW__CONCLUSION_TYPE, conclusionType));
        command.append(
                SetCommand.create(editingDomain, flow, UcPackage.Literals.FLOW__CONCLUSION_STEP, null));
        
        doExecute(editingDomain, command);
    }
    
    /**
     * Sets the conclusion step of a flow.
     * @param flow The flow.
     * @param conclusionStep The conclusion step.
     */
    public void setConclusionStep(Flow flow, Step conclusionStep) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(flow);
        CompoundCommand command = new CompoundCommand();
        
        command.append(
                SetCommand.create(editingDomain, flow, UcPackage.Literals.FLOW__CONCLUSION_TYPE, ConclusionType.STEP));
        command.append(
                SetCommand.create(editingDomain, flow, UcPackage.Literals.FLOW__CONCLUSION_STEP, conclusionStep));
        
        doExecute(editingDomain, command);
    }
    
    /*
    private void reorderAlternateFlows(Flow flow, EditingDomain editingDomain, CompoundCommand command) {
        List<Flow> sortedAlternateFlows = new ArrayList<Flow>(flow.getAlternateFlows());
        sortedAlternateFlows.sort(new AlternateFlowComparator(flow));
        for (int i = 0; i < sortedAlternateFlows.size(); i++) {
            Flow alternateFlow = sortedAlternateFlows.get(i);
            command.append(new MoveCommand(
                    editingDomain, flow, UcPackage.Literals.FLOW__ALTERNATE_FLOWS, alternateFlow, i));
        }
    }
    */
}
