package ca.mcgill.sel.usecases.language.controller;

import org.eclipse.emf.common.command.CompoundCommand;
import org.eclipse.emf.edit.command.RemoveCommand;
import org.eclipse.emf.edit.command.SetCommand;
import org.eclipse.emf.edit.domain.EditingDomain;

import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.commons.emf.util.EMFModelUtil;
import ca.mcgill.sel.usecases.UcPackage;
import ca.mcgill.sel.usecases.UseCase;
import ca.mcgill.sel.usecases.UseCaseReferenceStep;

/**
 * Controller for operations on steps.
 * @author rlanguay
 *
 */
public class StepController extends BaseController {
    
    /**
     * Updates the use case references by a step.
     * @param step The step. 
     * @param useCase The use case.
     */
    public void setUseCase(UseCaseReferenceStep step, UseCase useCase) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(step);
        
        CompoundCommand command = new CompoundCommand();
        
        // If this use case is in the "included" list, remove it
        UseCase containingUseCase = EMFModelUtil.getRootContainerOfType(step, UcPackage.Literals.USE_CASE);
        if (containingUseCase.getIncludedUseCases() != null && containingUseCase.getIncludedUseCases().contains(useCase)) {
            command.append(
                    RemoveCommand.create(editingDomain, containingUseCase, UcPackage.Literals.USE_CASE__INCLUDED_USE_CASES, useCase));
        }
        
        command.append(SetCommand.create(
                editingDomain, step, UcPackage.Literals.USE_CASE_REFERENCE_STEP__USECASE, useCase));       
        
        doExecute(editingDomain, command);
    }
}
