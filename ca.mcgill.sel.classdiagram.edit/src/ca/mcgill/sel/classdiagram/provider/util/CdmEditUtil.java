package ca.mcgill.sel.classdiagram.provider.util;

import java.util.Collection;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.ecore.EObject;

import ca.mcgill.sel.classdiagram.NamedElement;
import ca.mcgill.sel.classdiagram.Operation;
import ca.mcgill.sel.classdiagram.Parameter;
import ca.mcgill.sel.classdiagram.provider.ClassdiagramEditPlugin;
import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.core.COREMapping;
import ca.mcgill.sel.core.util.COREArtefactUtil;


/**
 * Helper class with convenient static methods for working with EMF objects.
 * 
 * @author joerg
 */
public final class CdmEditUtil {

    /**
     * Creates a new instance of {@link RAMEditUtil}.
     */
    private CdmEditUtil() {
        // suppress default constructor
    }

    /**
     * Returns the complete signature of the given {@link Operation} including parameters.
     * 
     * @param adapterFactory the {@link AdapterFactory} to use
     * @param operation the {@link Operation} a signature should be returned for
     * @param includeClassName whether the class name should be included in the signature (at the beginning)
     * @param includeReturnType whether the return type of the operation should be appended
     * @return the complete signature of the given {@link Operation}
     */
    public static String getOperationSignature(AdapterFactory adapterFactory, Operation operation,
            boolean includeClassName, boolean includeReturnType) {
        StringBuffer stringBuffer = new StringBuffer();

        stringBuffer.append(ClassdiagramEditPlugin.INSTANCE.getString("_UI_Operation_type"));
        stringBuffer.append(" ");

        if (includeClassName) {
            stringBuffer.append(EMFEditUtil.getTextFor(adapterFactory, operation.eContainer()));
            stringBuffer.append(".");
        }

        String operationName = EMFEditUtil.getTextFor(adapterFactory, operation);
        stringBuffer.append(operationName);
        stringBuffer.append("(");

        for (Parameter parameter : operation.getParameters()) {
            // if it is not the first one, add a ","
            if (operation.getParameters().indexOf(parameter) > 0) {
                stringBuffer.append(", ");
            }

            stringBuffer.append(EMFEditUtil.getTextFor(adapterFactory, parameter.getType()));
            stringBuffer.append(" ");
            stringBuffer.append(EMFEditUtil.getTextFor(adapterFactory, parameter));
        }

        stringBuffer.append(")");

        if (includeReturnType) {
            stringBuffer.append(" : ");
            stringBuffer.append(EMFEditUtil.getTextFor(adapterFactory, operation.getReturnType()));
        }

        return stringBuffer.toString();
    }

    /**
     * Return the expected text if the mapping have referenced mappings or the next expected one to display.
     * Will return a String of this form &lt;A,B&gt;
     * If there is no referenced mappings, just return an empty string
     * 
     * @param mapping The mapping we're looking at
     * @param element The element that will be displayed
     * @return A string containing the referenced mappings
     */
    public static String getReferencedMappingsText(COREMapping<? extends EObject> mapping, EObject element) {
        StringBuilder result = new StringBuilder();
        if (mapping.getReferencedMappings().size() >= 1 
                && (mapping.getFrom() == null || mapping.getFrom() == element)) {
            result.append("<");
            for (COREMapping<?> refMapping : mapping.getReferencedMappings()) {
                if (refMapping.getTo() != null) {
                    NamedElement namedElement = (NamedElement) refMapping.getTo();
                    result.append(namedElement.getName());
                    result.append(",");
                }
            }
            result.deleteCharAt(result.length() - 1);
            result.append(">");
        } else {
            Collection<COREMapping<? extends EObject>> mappings 
                = COREArtefactUtil.getNextReferencedMappings(element);
            
            if (mappings != null) {
                result.append("<");
                for (COREMapping<? extends EObject> refMapping : mappings) {
                    NamedElement namedElement = (NamedElement) refMapping.getTo();
                    result.append(namedElement.getName());
                    result.append(",");
                }
                result.deleteCharAt(result.length() - 1);
                result.append(">");
            }
        }
        return result.toString();
    }

}
