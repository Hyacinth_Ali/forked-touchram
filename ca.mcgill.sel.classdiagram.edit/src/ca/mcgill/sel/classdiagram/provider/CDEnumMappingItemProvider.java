/**
 */
package ca.mcgill.sel.classdiagram.provider;


import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;

import ca.mcgill.sel.classdiagram.CDEnum;
import ca.mcgill.sel.classdiagram.CDEnumMapping;
import ca.mcgill.sel.classdiagram.CdmFactory;
import ca.mcgill.sel.classdiagram.util.CdmInterfaceUtil;
import ca.mcgill.sel.core.COREExternalArtefact;
import ca.mcgill.sel.core.COREMapping;
import ca.mcgill.sel.core.COREModelExtension;
import ca.mcgill.sel.core.CorePackage;
import ca.mcgill.sel.core.provider.COREMappingItemProvider;

/**
 * This is the item provider adapter for a {@link ca.mcgill.sel.classdiagram.CDEnumMapping} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class CDEnumMappingItemProvider extends COREMappingItemProvider {
    /**
     * This constructs an instance from a factory and a notifier.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public CDEnumMappingItemProvider(AdapterFactory adapterFactory) {
        super(adapterFactory);
    }

    /**
     * This returns the property descriptors for the adapted class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
        if (itemPropertyDescriptors == null) {
            super.getPropertyDescriptors(object);

        }
        return itemPropertyDescriptors;
    }

    /**
     * This returns CDEnumMapping.gif.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Object getImage(Object object) {
        return overlayImage(object, getResourceLocator().getImage("full/obj16/CDEnumMapping"));
    }

    /**
     * This returns the label text for the adapted class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public String getText(Object object) {
        return getString("_UI_CDEnumMapping_type");
    }


    /**
     * This handles model notifications by calling {@link #updateChildren} to update any cached
     * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void notifyChanged(Notification notification) {
        updateChildren(notification);
        super.notifyChanged(notification);
    }

    /**
     * This adds a property descriptor for the To Element feature.
     * <!-- begin-user-doc -->
     * @param object the object to add a property descriptor for
     * <!-- end-user-doc -->
     * @generated NOT
     */
    protected void addToPropertyDescriptor(Object object) {
        itemPropertyDescriptors.add(
            new ItemPropertyDescriptor(
                ((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
                 getResourceLocator(),
                 getString("_UI_CORELink_to_feature"),
                 getString("_UI_PropertyDescriptor_description", 
                             "_UI_CORELink_to_feature", "_UI_CORELink_type"),
                 CorePackage.Literals.CORE_LINK__TO,
                 true,
                 false,
                 true,
                 null,
                 null,
                 null) {
                @Override
                public Collection<?> getChoiceOfValues(Object object) {
                    COREMapping<CDEnum> enumMapping = (CDEnumMapping) object;
                    
                 // Gather the COREModelExtension of the enumMapping to gather its COREExternalArtefact
                    COREModelExtension modelExtension = (COREModelExtension) enumMapping.eContainer();
                    COREExternalArtefact artefact = (COREExternalArtefact) modelExtension.getSource();
                    
                    // For every artefact, gather the Enums
                    Collection<CDEnum> enumsResult = new ArrayList<>();
                    enumsResult.addAll(CdmInterfaceUtil.getAvailableEnums(artefact));
                    
                    return enumsResult;
                }
                                
//                @Override
//                public void setPropertyValue(Object object, Object value) {
//                    RAMReferenceUtil.setLocalizedPropertyValue(getEditingDomain(object), object, feature, value);
//                }
                
            });
    }

    /**
     * This adds a property descriptor for the To Element feature.
     * <!-- begin-user-doc -->
     * @param object the object to add a property descriptor for
     * <!-- end-user-doc -->
     * @generated NOT
     */
    protected void addFromPropertyDescriptor(Object object) {
        itemPropertyDescriptors.add(
            new ItemPropertyDescriptor(
                ((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
                 getResourceLocator(),
                 getString("_UI_CORELink_from_feature"),
                 getString("_UI_PropertyDescriptor_description", 
                             "_UI_CORELink_from_feature", "_UI_CORELink_type"),
                 CorePackage.Literals.CORE_LINK__FROM,
                 true,
                 false,
                 true,
                 null,
                 null,
                 null) {
                @Override
                public Collection<?> getChoiceOfValues(Object object) {
                    COREMapping<CDEnum> enumMapping = (CDEnumMapping) object;
                    
                    // Gather the current COREExternalArtefact
                    COREExternalArtefact currentArtefact = 
                            (COREExternalArtefact) enumMapping.eContainer().eContainer();
                    
                    // For every artefact, gather the Enums
                    Collection<CDEnum> enumsResult = new ArrayList<>();
                    enumsResult.addAll(CdmInterfaceUtil.getAvailableExternalEnums(currentArtefact));
                    
                    return enumsResult;
                }
                                
//                @Override
//                public void setPropertyValue(Object object, Object value) {
                // TODO: localize
//                    RAMReferenceUtil.setLocalizedPropertyValue(getEditingDomain(object), object, feature, value);
//                }
                
            });
    }
    
    
    /**
     * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
     * that can be created under this object.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
        super.collectNewChildDescriptors(newChildDescriptors, object);

        newChildDescriptors.add
            (createChildParameter
                (CorePackage.Literals.CORE_MAPPING__MAPPINGS,
                 CdmFactory.eINSTANCE.createCDClassifierMapping()));

        newChildDescriptors.add
            (createChildParameter
                (CorePackage.Literals.CORE_MAPPING__MAPPINGS,
                 CdmFactory.eINSTANCE.createCDEnumMapping()));

        newChildDescriptors.add
            (createChildParameter
                (CorePackage.Literals.CORE_MAPPING__MAPPINGS,
                 CdmFactory.eINSTANCE.createCDOperationMapping()));

        newChildDescriptors.add
            (createChildParameter
                (CorePackage.Literals.CORE_MAPPING__MAPPINGS,
                 CdmFactory.eINSTANCE.createCDParameterMapping()));

        newChildDescriptors.add
            (createChildParameter
                (CorePackage.Literals.CORE_MAPPING__MAPPINGS,
                 CdmFactory.eINSTANCE.createCDAttributeMapping()));

        newChildDescriptors.add
            (createChildParameter
                (CorePackage.Literals.CORE_MAPPING__MAPPINGS,
                 CdmFactory.eINSTANCE.createCDEnumLiteralMapping()));
    }

    /**
     * Return the resource locator for this item provider's resources.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public ResourceLocator getResourceLocator() {
        return ClassdiagramEditPlugin.INSTANCE;
    }

}
