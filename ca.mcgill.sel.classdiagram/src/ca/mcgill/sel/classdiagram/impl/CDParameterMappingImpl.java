/**
 */
package ca.mcgill.sel.classdiagram.impl;

import ca.mcgill.sel.classdiagram.CDParameterMapping;
import ca.mcgill.sel.classdiagram.CdmPackage;
import ca.mcgill.sel.classdiagram.Parameter;
import ca.mcgill.sel.core.impl.COREMappingImpl;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>CD Parameter Mapping</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class CDParameterMappingImpl extends COREMappingImpl<Parameter> implements CDParameterMapping {
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected CDParameterMappingImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return CdmPackage.Literals.CD_PARAMETER_MAPPING;
    }


} //CDParameterMappingImpl
