/**
 */
package ca.mcgill.sel.classdiagram.impl;

import ca.mcgill.sel.classdiagram.CDEnumLiteral;
import ca.mcgill.sel.classdiagram.CDEnumLiteralMapping;
import ca.mcgill.sel.classdiagram.CdmPackage;

import ca.mcgill.sel.core.impl.COREMappingImpl;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>CD Enum Literal Mapping</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class CDEnumLiteralMappingImpl extends COREMappingImpl<CDEnumLiteral> implements CDEnumLiteralMapping {
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected CDEnumLiteralMappingImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return CdmPackage.Literals.CD_ENUM_LITERAL_MAPPING;
    }

} //CDEnumLiteralMappingImpl
