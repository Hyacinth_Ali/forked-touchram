/**
 */
package ca.mcgill.sel.classdiagram.impl;

import java.util.Collection;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;

import ca.mcgill.sel.classdiagram.CDAttributeMapping;
import ca.mcgill.sel.classdiagram.CDClassifierMapping;
import ca.mcgill.sel.classdiagram.CDOperationMapping;
import ca.mcgill.sel.classdiagram.CdmPackage;
import ca.mcgill.sel.classdiagram.Classifier;
import ca.mcgill.sel.commons.emf.util.EMFModelUtil;
import ca.mcgill.sel.core.CorePackage;
import ca.mcgill.sel.core.impl.COREMappingImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>CD Classifier Mapping</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class CDClassifierMappingImpl extends COREMappingImpl<Classifier> implements CDClassifierMapping {
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected CDClassifierMappingImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return CdmPackage.Literals.CD_CLASSIFIER_MAPPING;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated NOT
     */
    @Override
    public EList<CDAttributeMapping> getAttributeMappings() {
        Collection<CDAttributeMapping> collection = EMFModelUtil.collectElementsOfType(this, 
                CorePackage.Literals.CORE_MAPPING__MAPPINGS, CdmPackage.Literals.CD_ATTRIBUTE_MAPPING);
         
        return new BasicEList<CDAttributeMapping>(collection);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated NOT
     */
    @Override
    public EList<CDOperationMapping> getOperationMappings() {
        Collection<CDOperationMapping> collection = EMFModelUtil.collectElementsOfType(this, 
                CorePackage.Literals.CORE_MAPPING__MAPPINGS, CdmPackage.Literals.CD_OPERATION_MAPPING);
         
        return new BasicEList<CDOperationMapping>(collection);
    }

} //CDClassifierMappingImpl
