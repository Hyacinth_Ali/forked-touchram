/**
 */
package ca.mcgill.sel.classdiagram;

import ca.mcgill.sel.core.COREMapping;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>CD Parameter Mapping</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see ca.mcgill.sel.classdiagram.CdmPackage#getCDParameterMapping()
 * @model
 * @generated
 */
public interface CDParameterMapping extends COREMapping<Parameter> {
} // CDParameterMapping
