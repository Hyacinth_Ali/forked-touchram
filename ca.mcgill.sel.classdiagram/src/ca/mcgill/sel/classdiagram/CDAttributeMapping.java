/**
 */
package ca.mcgill.sel.classdiagram;

import ca.mcgill.sel.core.COREMapping;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>CD Attribute Mapping</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see ca.mcgill.sel.classdiagram.CdmPackage#getCDAttributeMapping()
 * @model
 * @generated
 */
public interface CDAttributeMapping extends COREMapping<Attribute> {
} // CDAttributeMapping
