/**
 */
package ca.mcgill.sel.classdiagram.util;

import ca.mcgill.sel.classdiagram.Association;
import ca.mcgill.sel.classdiagram.AssociationEnd;
import ca.mcgill.sel.classdiagram.Attribute;
import ca.mcgill.sel.classdiagram.CDAny;
import ca.mcgill.sel.classdiagram.CDArray;
import ca.mcgill.sel.classdiagram.CDAttributeMapping;
import ca.mcgill.sel.classdiagram.CDBoolean;
import ca.mcgill.sel.classdiagram.CDByte;
import ca.mcgill.sel.classdiagram.CDChar;
import ca.mcgill.sel.classdiagram.CDClassifierMapping;
import ca.mcgill.sel.classdiagram.CDCollection;
import ca.mcgill.sel.classdiagram.CDDouble;
import ca.mcgill.sel.classdiagram.CDEnum;
import ca.mcgill.sel.classdiagram.CDEnumLiteral;
import ca.mcgill.sel.classdiagram.CDEnumLiteralMapping;
import ca.mcgill.sel.classdiagram.CDEnumMapping;
import ca.mcgill.sel.classdiagram.CDFloat;
import ca.mcgill.sel.classdiagram.CDInt;
import ca.mcgill.sel.classdiagram.CDLong;
import ca.mcgill.sel.classdiagram.CDMappableElement;
import ca.mcgill.sel.classdiagram.CDOperationMapping;
import ca.mcgill.sel.classdiagram.CDParameterMapping;
import ca.mcgill.sel.classdiagram.CDSequence;
import ca.mcgill.sel.classdiagram.CDSet;
import ca.mcgill.sel.classdiagram.CDString;
import ca.mcgill.sel.classdiagram.CDVoid;
import ca.mcgill.sel.classdiagram.CdmPackage;
import ca.mcgill.sel.classdiagram.ClassDiagram;
import ca.mcgill.sel.classdiagram.Classifier;
import ca.mcgill.sel.classdiagram.ImplementationClass;
import ca.mcgill.sel.classdiagram.Layout;
import ca.mcgill.sel.classdiagram.LayoutElement;
import ca.mcgill.sel.classdiagram.NamedElement;
import ca.mcgill.sel.classdiagram.Note;
import ca.mcgill.sel.classdiagram.ObjectType;
import ca.mcgill.sel.classdiagram.Operation;
import ca.mcgill.sel.classdiagram.Parameter;
import ca.mcgill.sel.classdiagram.PrimitiveType;
import ca.mcgill.sel.classdiagram.StructuralFeature;
import ca.mcgill.sel.classdiagram.Type;
import ca.mcgill.sel.classdiagram.TypeParameter;
import ca.mcgill.sel.classdiagram.TypedElement;

import ca.mcgill.sel.core.CORELink;
import ca.mcgill.sel.core.COREMapping;
import ca.mcgill.sel.core.COREModelElementComposition;

import java.util.Map;

import org.eclipse.emf.common.util.EMap;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see ca.mcgill.sel.classdiagram.CdmPackage
 * @generated
 */
public class CdmSwitch<T1> extends Switch<T1> {
    /**
     * The cached model package
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected static CdmPackage modelPackage;

    /**
     * Creates an instance of the switch.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public CdmSwitch() {
        if (modelPackage == null) {
            modelPackage = CdmPackage.eINSTANCE;
        }
    }

    /**
     * Checks whether this is a switch for the given package.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param ePackage the package in question.
     * @return whether this is a switch for the given package.
     * @generated
     */
    @Override
    protected boolean isSwitchFor(EPackage ePackage) {
        return ePackage == modelPackage;
    }

    /**
     * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the first non-null result returned by a <code>caseXXX</code> call.
     * @generated
     */
    @Override
    protected T1 doSwitch(int classifierID, EObject theEObject) {
        switch (classifierID) {
            case CdmPackage.NAMED_ELEMENT: {
                NamedElement namedElement = (NamedElement)theEObject;
                T1 result = caseNamedElement(namedElement);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case CdmPackage.TYPED_ELEMENT: {
                TypedElement typedElement = (TypedElement)theEObject;
                T1 result = caseTypedElement(typedElement);
                if (result == null) result = caseNamedElement(typedElement);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case CdmPackage.PARAMETER: {
                Parameter parameter = (Parameter)theEObject;
                T1 result = caseParameter(parameter);
                if (result == null) result = caseTypedElement(parameter);
                if (result == null) result = caseCDMappableElement(parameter);
                if (result == null) result = caseNamedElement(parameter);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case CdmPackage.ATTRIBUTE: {
                Attribute attribute = (Attribute)theEObject;
                T1 result = caseAttribute(attribute);
                if (result == null) result = caseStructuralFeature(attribute);
                if (result == null) result = caseCDMappableElement(attribute);
                if (result == null) result = caseTypedElement(attribute);
                if (result == null) result = caseNamedElement(attribute);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case CdmPackage.STRUCTURAL_FEATURE: {
                StructuralFeature structuralFeature = (StructuralFeature)theEObject;
                T1 result = caseStructuralFeature(structuralFeature);
                if (result == null) result = caseTypedElement(structuralFeature);
                if (result == null) result = caseNamedElement(structuralFeature);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case CdmPackage.TYPE: {
                Type type = (Type)theEObject;
                T1 result = caseType(type);
                if (result == null) result = caseNamedElement(type);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case CdmPackage.OBJECT_TYPE: {
                ObjectType objectType = (ObjectType)theEObject;
                T1 result = caseObjectType(objectType);
                if (result == null) result = caseType(objectType);
                if (result == null) result = caseCDMappableElement(objectType);
                if (result == null) result = caseNamedElement(objectType);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case CdmPackage.PRIMITIVE_TYPE: {
                PrimitiveType primitiveType = (PrimitiveType)theEObject;
                T1 result = casePrimitiveType(primitiveType);
                if (result == null) result = caseImplementationClass(primitiveType);
                if (result == null) result = caseType(primitiveType);
                if (result == null) result = caseCDMappableElement(primitiveType);
                if (result == null) result = caseClassifier(primitiveType);
                if (result == null) result = caseObjectType(primitiveType);
                if (result == null) result = caseNamedElement(primitiveType);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case CdmPackage.CLASSIFIER: {
                Classifier classifier = (Classifier)theEObject;
                T1 result = caseClassifier(classifier);
                if (result == null) result = caseObjectType(classifier);
                if (result == null) result = caseType(classifier);
                if (result == null) result = caseCDMappableElement(classifier);
                if (result == null) result = caseNamedElement(classifier);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case CdmPackage.OPERATION: {
                Operation operation = (Operation)theEObject;
                T1 result = caseOperation(operation);
                if (result == null) result = caseCDMappableElement(operation);
                if (result == null) result = caseNamedElement(operation);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case CdmPackage.CLASS: {
                ca.mcgill.sel.classdiagram.Class class_ = (ca.mcgill.sel.classdiagram.Class)theEObject;
                T1 result = caseClass(class_);
                if (result == null) result = caseClassifier(class_);
                if (result == null) result = caseObjectType(class_);
                if (result == null) result = caseType(class_);
                if (result == null) result = caseCDMappableElement(class_);
                if (result == null) result = caseNamedElement(class_);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case CdmPackage.TYPE_PARAMETER: {
                TypeParameter typeParameter = (TypeParameter)theEObject;
                T1 result = caseTypeParameter(typeParameter);
                if (result == null) result = caseType(typeParameter);
                if (result == null) result = caseNamedElement(typeParameter);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case CdmPackage.ASSOCIATION: {
                Association association = (Association)theEObject;
                T1 result = caseAssociation(association);
                if (result == null) result = caseNamedElement(association);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case CdmPackage.ASSOCIATION_END: {
                AssociationEnd associationEnd = (AssociationEnd)theEObject;
                T1 result = caseAssociationEnd(associationEnd);
                if (result == null) result = caseStructuralFeature(associationEnd);
                if (result == null) result = caseCDMappableElement(associationEnd);
                if (result == null) result = caseTypedElement(associationEnd);
                if (result == null) result = caseNamedElement(associationEnd);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case CdmPackage.CLASS_DIAGRAM: {
                ClassDiagram classDiagram = (ClassDiagram)theEObject;
                T1 result = caseClassDiagram(classDiagram);
                if (result == null) result = caseNamedElement(classDiagram);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case CdmPackage.IMPLEMENTATION_CLASS: {
                ImplementationClass implementationClass = (ImplementationClass)theEObject;
                T1 result = caseImplementationClass(implementationClass);
                if (result == null) result = caseClassifier(implementationClass);
                if (result == null) result = caseObjectType(implementationClass);
                if (result == null) result = caseType(implementationClass);
                if (result == null) result = caseCDMappableElement(implementationClass);
                if (result == null) result = caseNamedElement(implementationClass);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case CdmPackage.NOTE: {
                Note note = (Note)theEObject;
                T1 result = caseNote(note);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case CdmPackage.ELEMENT_MAP: {
                @SuppressWarnings("unchecked") Map.Entry<EObject, LayoutElement> elementMap = (Map.Entry<EObject, LayoutElement>)theEObject;
                T1 result = caseElementMap(elementMap);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case CdmPackage.LAYOUT: {
                Layout layout = (Layout)theEObject;
                T1 result = caseLayout(layout);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case CdmPackage.LAYOUT_ELEMENT: {
                LayoutElement layoutElement = (LayoutElement)theEObject;
                T1 result = caseLayoutElement(layoutElement);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case CdmPackage.CONTAINER_MAP: {
                @SuppressWarnings("unchecked") Map.Entry<EObject, EMap<EObject, LayoutElement>> containerMap = (Map.Entry<EObject, EMap<EObject, LayoutElement>>)theEObject;
                T1 result = caseContainerMap(containerMap);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case CdmPackage.CD_BOOLEAN: {
                CDBoolean cdBoolean = (CDBoolean)theEObject;
                T1 result = caseCDBoolean(cdBoolean);
                if (result == null) result = casePrimitiveType(cdBoolean);
                if (result == null) result = caseImplementationClass(cdBoolean);
                if (result == null) result = caseType(cdBoolean);
                if (result == null) result = caseCDMappableElement(cdBoolean);
                if (result == null) result = caseClassifier(cdBoolean);
                if (result == null) result = caseObjectType(cdBoolean);
                if (result == null) result = caseNamedElement(cdBoolean);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case CdmPackage.CD_DOUBLE: {
                CDDouble cdDouble = (CDDouble)theEObject;
                T1 result = caseCDDouble(cdDouble);
                if (result == null) result = casePrimitiveType(cdDouble);
                if (result == null) result = caseImplementationClass(cdDouble);
                if (result == null) result = caseType(cdDouble);
                if (result == null) result = caseCDMappableElement(cdDouble);
                if (result == null) result = caseClassifier(cdDouble);
                if (result == null) result = caseObjectType(cdDouble);
                if (result == null) result = caseNamedElement(cdDouble);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case CdmPackage.CD_INT: {
                CDInt cdInt = (CDInt)theEObject;
                T1 result = caseCDInt(cdInt);
                if (result == null) result = casePrimitiveType(cdInt);
                if (result == null) result = caseImplementationClass(cdInt);
                if (result == null) result = caseType(cdInt);
                if (result == null) result = caseCDMappableElement(cdInt);
                if (result == null) result = caseClassifier(cdInt);
                if (result == null) result = caseObjectType(cdInt);
                if (result == null) result = caseNamedElement(cdInt);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case CdmPackage.CD_LONG: {
                CDLong cdLong = (CDLong)theEObject;
                T1 result = caseCDLong(cdLong);
                if (result == null) result = casePrimitiveType(cdLong);
                if (result == null) result = caseImplementationClass(cdLong);
                if (result == null) result = caseType(cdLong);
                if (result == null) result = caseCDMappableElement(cdLong);
                if (result == null) result = caseClassifier(cdLong);
                if (result == null) result = caseObjectType(cdLong);
                if (result == null) result = caseNamedElement(cdLong);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case CdmPackage.CD_STRING: {
                CDString cdString = (CDString)theEObject;
                T1 result = caseCDString(cdString);
                if (result == null) result = casePrimitiveType(cdString);
                if (result == null) result = caseImplementationClass(cdString);
                if (result == null) result = caseType(cdString);
                if (result == null) result = caseCDMappableElement(cdString);
                if (result == null) result = caseClassifier(cdString);
                if (result == null) result = caseObjectType(cdString);
                if (result == null) result = caseNamedElement(cdString);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case CdmPackage.CD_BYTE: {
                CDByte cdByte = (CDByte)theEObject;
                T1 result = caseCDByte(cdByte);
                if (result == null) result = casePrimitiveType(cdByte);
                if (result == null) result = caseImplementationClass(cdByte);
                if (result == null) result = caseType(cdByte);
                if (result == null) result = caseCDMappableElement(cdByte);
                if (result == null) result = caseClassifier(cdByte);
                if (result == null) result = caseObjectType(cdByte);
                if (result == null) result = caseNamedElement(cdByte);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case CdmPackage.CD_FLOAT: {
                CDFloat cdFloat = (CDFloat)theEObject;
                T1 result = caseCDFloat(cdFloat);
                if (result == null) result = casePrimitiveType(cdFloat);
                if (result == null) result = caseImplementationClass(cdFloat);
                if (result == null) result = caseType(cdFloat);
                if (result == null) result = caseCDMappableElement(cdFloat);
                if (result == null) result = caseClassifier(cdFloat);
                if (result == null) result = caseObjectType(cdFloat);
                if (result == null) result = caseNamedElement(cdFloat);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case CdmPackage.CD_ARRAY: {
                CDArray cdArray = (CDArray)theEObject;
                T1 result = caseCDArray(cdArray);
                if (result == null) result = casePrimitiveType(cdArray);
                if (result == null) result = caseImplementationClass(cdArray);
                if (result == null) result = caseType(cdArray);
                if (result == null) result = caseCDMappableElement(cdArray);
                if (result == null) result = caseClassifier(cdArray);
                if (result == null) result = caseObjectType(cdArray);
                if (result == null) result = caseNamedElement(cdArray);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case CdmPackage.CD_CHAR: {
                CDChar cdChar = (CDChar)theEObject;
                T1 result = caseCDChar(cdChar);
                if (result == null) result = casePrimitiveType(cdChar);
                if (result == null) result = caseImplementationClass(cdChar);
                if (result == null) result = caseType(cdChar);
                if (result == null) result = caseCDMappableElement(cdChar);
                if (result == null) result = caseClassifier(cdChar);
                if (result == null) result = caseObjectType(cdChar);
                if (result == null) result = caseNamedElement(cdChar);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case CdmPackage.CD_ENUM: {
                CDEnum cdEnum = (CDEnum)theEObject;
                T1 result = caseCDEnum(cdEnum);
                if (result == null) result = casePrimitiveType(cdEnum);
                if (result == null) result = caseImplementationClass(cdEnum);
                if (result == null) result = caseType(cdEnum);
                if (result == null) result = caseCDMappableElement(cdEnum);
                if (result == null) result = caseClassifier(cdEnum);
                if (result == null) result = caseObjectType(cdEnum);
                if (result == null) result = caseNamedElement(cdEnum);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case CdmPackage.CD_ENUM_LITERAL: {
                CDEnumLiteral cdEnumLiteral = (CDEnumLiteral)theEObject;
                T1 result = caseCDEnumLiteral(cdEnumLiteral);
                if (result == null) result = caseNamedElement(cdEnumLiteral);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case CdmPackage.CD_ANY: {
                CDAny cdAny = (CDAny)theEObject;
                T1 result = caseCDAny(cdAny);
                if (result == null) result = caseObjectType(cdAny);
                if (result == null) result = caseType(cdAny);
                if (result == null) result = caseCDMappableElement(cdAny);
                if (result == null) result = caseNamedElement(cdAny);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case CdmPackage.CD_VOID: {
                CDVoid cdVoid = (CDVoid)theEObject;
                T1 result = caseCDVoid(cdVoid);
                if (result == null) result = caseObjectType(cdVoid);
                if (result == null) result = caseType(cdVoid);
                if (result == null) result = caseCDMappableElement(cdVoid);
                if (result == null) result = caseNamedElement(cdVoid);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case CdmPackage.CD_COLLECTION: {
                CDCollection cdCollection = (CDCollection)theEObject;
                T1 result = caseCDCollection(cdCollection);
                if (result == null) result = caseImplementationClass(cdCollection);
                if (result == null) result = caseClassifier(cdCollection);
                if (result == null) result = caseObjectType(cdCollection);
                if (result == null) result = caseType(cdCollection);
                if (result == null) result = caseCDMappableElement(cdCollection);
                if (result == null) result = caseNamedElement(cdCollection);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case CdmPackage.CD_SET: {
                CDSet cdSet = (CDSet)theEObject;
                T1 result = caseCDSet(cdSet);
                if (result == null) result = caseCDCollection(cdSet);
                if (result == null) result = caseImplementationClass(cdSet);
                if (result == null) result = caseClassifier(cdSet);
                if (result == null) result = caseObjectType(cdSet);
                if (result == null) result = caseType(cdSet);
                if (result == null) result = caseCDMappableElement(cdSet);
                if (result == null) result = caseNamedElement(cdSet);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case CdmPackage.CD_SEQUENCE: {
                CDSequence cdSequence = (CDSequence)theEObject;
                T1 result = caseCDSequence(cdSequence);
                if (result == null) result = caseCDCollection(cdSequence);
                if (result == null) result = caseImplementationClass(cdSequence);
                if (result == null) result = caseClassifier(cdSequence);
                if (result == null) result = caseObjectType(cdSequence);
                if (result == null) result = caseType(cdSequence);
                if (result == null) result = caseCDMappableElement(cdSequence);
                if (result == null) result = caseNamedElement(cdSequence);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case CdmPackage.CD_CLASSIFIER_MAPPING: {
                CDClassifierMapping cdClassifierMapping = (CDClassifierMapping)theEObject;
                T1 result = caseCDClassifierMapping(cdClassifierMapping);
                if (result == null) result = caseCOREMapping(cdClassifierMapping);
                if (result == null) result = caseCORELink(cdClassifierMapping);
                if (result == null) result = caseCOREModelElementComposition(cdClassifierMapping);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case CdmPackage.CD_ENUM_MAPPING: {
                CDEnumMapping cdEnumMapping = (CDEnumMapping)theEObject;
                T1 result = caseCDEnumMapping(cdEnumMapping);
                if (result == null) result = caseCOREMapping(cdEnumMapping);
                if (result == null) result = caseCORELink(cdEnumMapping);
                if (result == null) result = caseCOREModelElementComposition(cdEnumMapping);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case CdmPackage.CD_OPERATION_MAPPING: {
                CDOperationMapping cdOperationMapping = (CDOperationMapping)theEObject;
                T1 result = caseCDOperationMapping(cdOperationMapping);
                if (result == null) result = caseCOREMapping(cdOperationMapping);
                if (result == null) result = caseCORELink(cdOperationMapping);
                if (result == null) result = caseCOREModelElementComposition(cdOperationMapping);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case CdmPackage.CD_PARAMETER_MAPPING: {
                CDParameterMapping cdParameterMapping = (CDParameterMapping)theEObject;
                T1 result = caseCDParameterMapping(cdParameterMapping);
                if (result == null) result = caseCOREMapping(cdParameterMapping);
                if (result == null) result = caseCORELink(cdParameterMapping);
                if (result == null) result = caseCOREModelElementComposition(cdParameterMapping);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case CdmPackage.CD_ATTRIBUTE_MAPPING: {
                CDAttributeMapping cdAttributeMapping = (CDAttributeMapping)theEObject;
                T1 result = caseCDAttributeMapping(cdAttributeMapping);
                if (result == null) result = caseCOREMapping(cdAttributeMapping);
                if (result == null) result = caseCORELink(cdAttributeMapping);
                if (result == null) result = caseCOREModelElementComposition(cdAttributeMapping);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case CdmPackage.CD_ENUM_LITERAL_MAPPING: {
                CDEnumLiteralMapping cdEnumLiteralMapping = (CDEnumLiteralMapping)theEObject;
                T1 result = caseCDEnumLiteralMapping(cdEnumLiteralMapping);
                if (result == null) result = caseCOREMapping(cdEnumLiteralMapping);
                if (result == null) result = caseCORELink(cdEnumLiteralMapping);
                if (result == null) result = caseCOREModelElementComposition(cdEnumLiteralMapping);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case CdmPackage.CD_MAPPABLE_ELEMENT: {
                CDMappableElement cdMappableElement = (CDMappableElement)theEObject;
                T1 result = caseCDMappableElement(cdMappableElement);
                if (result == null) result = caseNamedElement(cdMappableElement);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            default: return defaultCase(theEObject);
        }
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Named Element</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Named Element</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T1 caseNamedElement(NamedElement object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Typed Element</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Typed Element</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T1 caseTypedElement(TypedElement object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Parameter</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Parameter</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T1 caseParameter(Parameter object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Attribute</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Attribute</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T1 caseAttribute(Attribute object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Structural Feature</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Structural Feature</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T1 caseStructuralFeature(StructuralFeature object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Type</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Type</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T1 caseType(Type object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Object Type</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Object Type</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T1 caseObjectType(ObjectType object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Primitive Type</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Primitive Type</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T1 casePrimitiveType(PrimitiveType object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Classifier</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Classifier</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T1 caseClassifier(Classifier object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Operation</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Operation</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T1 caseOperation(Operation object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Class</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Class</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T1 caseClass(ca.mcgill.sel.classdiagram.Class object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Type Parameter</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Type Parameter</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T1 caseTypeParameter(TypeParameter object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Association</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Association</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T1 caseAssociation(Association object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Association End</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Association End</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T1 caseAssociationEnd(AssociationEnd object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Class Diagram</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Class Diagram</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T1 caseClassDiagram(ClassDiagram object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Implementation Class</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Implementation Class</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T1 caseImplementationClass(ImplementationClass object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Note</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Note</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T1 caseNote(Note object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Element Map</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Element Map</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T1 caseElementMap(Map.Entry<EObject, LayoutElement> object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Layout</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Layout</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T1 caseLayout(Layout object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Layout Element</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Layout Element</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T1 caseLayoutElement(LayoutElement object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Container Map</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Container Map</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T1 caseContainerMap(Map.Entry<EObject, EMap<EObject, LayoutElement>> object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>CD Boolean</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>CD Boolean</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T1 caseCDBoolean(CDBoolean object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>CD Double</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>CD Double</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T1 caseCDDouble(CDDouble object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>CD Int</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>CD Int</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T1 caseCDInt(CDInt object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>CD Long</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>CD Long</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T1 caseCDLong(CDLong object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>CD String</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>CD String</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T1 caseCDString(CDString object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>CD Byte</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>CD Byte</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T1 caseCDByte(CDByte object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>CD Float</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>CD Float</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T1 caseCDFloat(CDFloat object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>CD Array</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>CD Array</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T1 caseCDArray(CDArray object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>CD Char</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>CD Char</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T1 caseCDChar(CDChar object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>CD Enum</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>CD Enum</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T1 caseCDEnum(CDEnum object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>CD Enum Literal</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>CD Enum Literal</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T1 caseCDEnumLiteral(CDEnumLiteral object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>CD Any</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>CD Any</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T1 caseCDAny(CDAny object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>CD Void</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>CD Void</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T1 caseCDVoid(CDVoid object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>CD Collection</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>CD Collection</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T1 caseCDCollection(CDCollection object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>CD Set</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>CD Set</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T1 caseCDSet(CDSet object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>CD Sequence</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>CD Sequence</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T1 caseCDSequence(CDSequence object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>CD Classifier Mapping</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>CD Classifier Mapping</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T1 caseCDClassifierMapping(CDClassifierMapping object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>CD Enum Mapping</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>CD Enum Mapping</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T1 caseCDEnumMapping(CDEnumMapping object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>CD Operation Mapping</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>CD Operation Mapping</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T1 caseCDOperationMapping(CDOperationMapping object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>CD Parameter Mapping</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>CD Parameter Mapping</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T1 caseCDParameterMapping(CDParameterMapping object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>CD Attribute Mapping</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>CD Attribute Mapping</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T1 caseCDAttributeMapping(CDAttributeMapping object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>CD Enum Literal Mapping</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>CD Enum Literal Mapping</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T1 caseCDEnumLiteralMapping(CDEnumLiteralMapping object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>CD Mappable Element</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>CD Mappable Element</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T1 caseCDMappableElement(CDMappableElement object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>CORE Model Element Composition</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>CORE Model Element Composition</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public <T> T1 caseCOREModelElementComposition(COREModelElementComposition<T> object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>CORE Link</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>CORE Link</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public <T> T1 caseCORELink(CORELink<T> object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>CORE Mapping</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>CORE Mapping</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public <T> T1 caseCOREMapping(COREMapping<T> object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch, but this is the last case anyway.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject)
     * @generated
     */
    @Override
    public T1 defaultCase(EObject object) {
        return null;
    }

} //CdmSwitch
