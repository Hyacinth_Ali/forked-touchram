/**
 */
package ca.mcgill.sel.classdiagram;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>CD Int</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see ca.mcgill.sel.classdiagram.CdmPackage#getCDInt()
 * @model
 * @generated
 */
public interface CDInt extends PrimitiveType {
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @model kind="operation" required="true"
     *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot body='\'int\''"
     * @generated
     */
    String getName();

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @model kind="operation" required="true"
     *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot body='\'java.lang.Integer\''"
     * @generated
     */
    String getInstanceClassName();

} // CDInt
