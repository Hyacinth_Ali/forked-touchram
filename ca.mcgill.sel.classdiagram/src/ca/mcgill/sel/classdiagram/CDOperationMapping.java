/**
 */
package ca.mcgill.sel.classdiagram;

import ca.mcgill.sel.core.COREMapping;
import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>CD Operation Mapping</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see ca.mcgill.sel.classdiagram.CdmPackage#getCDOperationMapping()
 * @model
 * @generated
 */
public interface CDOperationMapping extends COREMapping<Operation> {

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @model kind="operation"
     * @generated
     */
    EList<CDParameterMapping> getParameterMappings();
} // CDOperationMapping
