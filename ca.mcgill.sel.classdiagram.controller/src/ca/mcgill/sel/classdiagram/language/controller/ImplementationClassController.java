package ca.mcgill.sel.classdiagram.language.controller;

import java.util.List;
import java.util.Set;

import org.eclipse.emf.common.command.CompoundCommand;
import org.eclipse.emf.edit.command.AddCommand;
import org.eclipse.emf.edit.command.SetCommand;
import org.eclipse.emf.edit.domain.EditingDomain;

import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.classdiagram.ImplementationClass;
import ca.mcgill.sel.classdiagram.ObjectType;
import ca.mcgill.sel.classdiagram.Operation;
import ca.mcgill.sel.classdiagram.OperationType;
import ca.mcgill.sel.classdiagram.Parameter;
import ca.mcgill.sel.classdiagram.VisibilityType;
import ca.mcgill.sel.classdiagram.CdmPackage;
import ca.mcgill.sel.classdiagram.ClassDiagram;
import ca.mcgill.sel.classdiagram.Type;
import ca.mcgill.sel.classdiagram.TypeParameter;
import ca.mcgill.sel.classdiagram.util.CdmModelUtil;

/**
 * Controller for implementation classes.
 * 
 * @author Franz
 * @author yhattab
 */
public class ImplementationClassController extends BaseController {
    
    /**
     * Creates a new instance of {@link ImplementationClassController}.
     */
    protected ImplementationClassController() {
        // prevent anyone outside this package to instantiate
    }

    /**
     * Adds an operation to an existing implementation class.
     * 
     * @param owner the implementation class where the operation will be added
     * @param index the index where the operation should be added
     * @param name the name of the operation
     * @param visibility the {@link VisibilityType} of the operation
     * @param returnType the return type of the operation
     * @param parameters the parameters of the operation
     * @param isStatic is the operation static or not
     */
    public void addOperation(ImplementationClass owner, int index, String name, VisibilityType visibility,
            Type returnType, List<Parameter> parameters, boolean isStatic) {
        Operation operation = CdmModelUtil.createOperation(owner, name, returnType, parameters);
        if (operation != null) {
            operation.setStatic(isStatic);
            operation.setOperationType(OperationType.NORMAL);

            EditingDomain domain = EMFEditUtil.getEditingDomain(owner);

            CompoundCommand compundCommand = new CompoundCommand();

            // Update visibility for the operation and classifier if necessary
            compundCommand.append(ControllerFactory.INSTANCE.getClassController()
                    .changeOperationAndClassVisibilityCommand(
                            domain, owner, operation, visibility));
            // Add the operation
            compundCommand.append(AddCommand.create(domain, owner, CdmPackage.Literals.CLASSIFIER__OPERATIONS,
                    operation,
                    index));
            doExecute(domain, compundCommand);
        }
    }

    /**
     * Adds a constructor to an existing implementation class.
     * 
     * @param owner the implementation class where the constructor will be added
     * @param name the name of the constructor
     * @param visibility the {@link VisibilityType} of the operation
     * @param parameters the parameters of the constructor
     */
    public void addConstructor(ImplementationClass owner, String name, VisibilityType visibility,
            List<Parameter> parameters) {
        Operation operation = CdmModelUtil.createOperation(owner, name, owner, parameters);
        if (operation != null) {
            operation.setOperationType(OperationType.CONSTRUCTOR);
            int index = CdmModelUtil.findConstructorIndexFor(owner, OperationType.CONSTRUCTOR);
            EditingDomain domain = EMFEditUtil.getEditingDomain(owner);

            CompoundCommand compundCommand = new CompoundCommand();

            // Update visibility for the operation and classifier if necessary
            compundCommand.append(ControllerFactory.INSTANCE.getClassController()
                    .changeOperationAndClassVisibilityCommand(
                            domain, owner, operation, visibility));

            // Add the operation
            compundCommand.append(AddCommand.create(domain, owner, CdmPackage.Literals.CLASSIFIER__OPERATIONS,
                    operation,
                    index));
            doExecute(domain, compundCommand);
        }
    }

    /**
     * Sets the type parameter's generic type.
     * In addition, if this way existing implementation classes match in type parameters and are a super or sub type,
     * the inheritance link is set accordingly.
     * 
     * @param owner the {@link ImplementationClass} containing the type parameter
     * @param typeParameter the {@link TypeParameter} to update
     * @param genericType the {@link ObjectType} to set
     * @param superTypes the set of super types the class to create has
     * @param subTypes the set of sub types (existing in the structural view) the class to create has
     */
    public void setTypeParameterType(ImplementationClass owner, TypeParameter typeParameter, ObjectType genericType,
            Set<String> superTypes, Set<ImplementationClass> subTypes) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(owner);
        CompoundCommand command = new CompoundCommand();

        command.append(SetCommand.create(editingDomain, typeParameter,
                CdmPackage.Literals.TYPE_PARAMETER__GENERIC_TYPE, genericType));

        // Temporarily set the type in order that typeParametersMatch works properly.
        ObjectType previousType = typeParameter.getGenericType();
        typeParameter.setGenericType(genericType);

        if (superTypes != null) {
            ClassDiagram cd = (ClassDiagram) owner.eContainer();
            for (String className : superTypes) {
                ImplementationClass superType = CdmModelUtil.getImplementationClassByName(cd, className);
                if (superType != null && CdmModelUtil.typeParametersMatch(owner, superType)) {
                    command.append(AddCommand.create(editingDomain, owner,
                            CdmPackage.Literals.CLASSIFIER__SUPER_TYPES, superType));
                }
            }
        }

        if (subTypes != null) {
            for (ImplementationClass subType : subTypes) {
                if (CdmModelUtil.typeParametersMatch(owner, subType)) {
                    command.append(AddCommand.create(editingDomain, subType,
                            CdmPackage.Literals.CLASSIFIER__SUPER_TYPES, owner));
                }
            }
        }

        // Undo temporary change.
        typeParameter.setGenericType(previousType);

        doExecute(editingDomain, command);
    }

}
