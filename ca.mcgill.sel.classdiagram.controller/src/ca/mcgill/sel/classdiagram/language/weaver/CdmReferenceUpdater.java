package ca.mcgill.sel.classdiagram.language.weaver;

import java.util.List;

import org.eclipse.emf.ecore.util.Switch;

import ca.mcgill.sel.classdiagram.AssociationEnd;
import ca.mcgill.sel.classdiagram.Attribute;
import ca.mcgill.sel.classdiagram.CDCollection;
import ca.mcgill.sel.classdiagram.Classifier;
import ca.mcgill.sel.classdiagram.Operation;
import ca.mcgill.sel.classdiagram.Parameter;
import ca.mcgill.sel.classdiagram.Type;
import ca.mcgill.sel.classdiagram.TypeParameter;
import ca.mcgill.sel.classdiagram.util.CdmSwitch;
import ca.mcgill.sel.core.weaver.util.COREReferenceUpdater;

/**
 * Takes care of updating the references of elements of any {@link org.eclipse.emf.ecore.EObject} of Cdm class diagrams
 * based on the weaving information provided by the class diagram weaver.
 * This class is a singleton and it's instance can be retrieved via {@link #getInstance()}.
 * 
 * @author joerg
 */

public final class CdmReferenceUpdater extends COREReferenceUpdater {
    
    /**
     * Switch for each class that needs to be updated according to the weaving information.
     * 
     * @author joerg
     */
    private class Switcher extends CdmSwitch<Object> {

        @Override
        public Object caseAttribute(Attribute attribute) {
            // Right now this will only be called for attributes that are temporary properties of a lifeline.
            // Therefore it is enough to replace the type of the attribute.
            Type type = attribute.getType();

            if (currentWeavingInformation.wasWoven(type)) {
                attribute.setType(currentWeavingInformation.getFirstToElement(type));
            }

            return Boolean.TRUE;
        }    

        @Override
        public Object caseOperation(Operation operation) {
            Type type = operation.getReturnType();

            if (currentWeavingInformation.wasWoven(type)) {
                operation.setReturnType(currentWeavingInformation.getFirstToElement(type));
            }

            return Boolean.TRUE;
        }

        @Override
        public Object caseParameter(Parameter parameter) {
            Type type = parameter.getType();
    
            if (currentWeavingInformation.wasWoven(type)) {
                parameter.setType(currentWeavingInformation.getFirstToElement(type));
            }
    
            return Boolean.TRUE;
        }
        
        @Override
        public Object caseCDCollection(CDCollection collection) {
            Type type = collection.getType();

            if (currentWeavingInformation.wasWoven(type)) {
                collection.setType(currentWeavingInformation.getFirstToElement(type));
            }

            return Boolean.TRUE;
        }

        @Override
        public Object caseTypeParameter(TypeParameter typeParam) {
            if (currentWeavingInformation.wasWoven(typeParam.getGenericType())) {
                typeParam.setGenericType(currentWeavingInformation.getFirstToElement(typeParam.getGenericType()));
            }
            return Boolean.TRUE;
        }

        @Override
        public Object caseClassifier(Classifier object) {
            List<Classifier> superTypes = object.getSuperTypes();

            for (int index = 0; index < superTypes.size(); index++) {
                Classifier classifier = superTypes.get(index);

                if (currentWeavingInformation.wasWoven(classifier)) {
                    Classifier newClassifier = currentWeavingInformation.getFirstToElement(classifier);
                    superTypes.remove(index);
                    if (!superTypes.contains(newClassifier)) {
                        superTypes.add(index, newClassifier);
                    }
                }
            }
            return Boolean.TRUE;
        }

        @Override
        public Object caseAssociationEnd(AssociationEnd assocEnd) {
            Classifier classifier = assocEnd.getClassifier();

            if (currentWeavingInformation.wasWoven(classifier)) {
                assocEnd.setClassifier(currentWeavingInformation.getFirstToElement(classifier));
            }
            return Boolean.TRUE;
        }        
    }
    
    /**
     * The singleton instance.
     */
    private static CdmReferenceUpdater instance;

    /**
     * Returns the singleton instance of the {@link CdmReferenceUpdater}.
     * 
     * @return the singleton instance
     */
    public static CdmReferenceUpdater getInstance() {
        if (instance == null) {
            instance = new CdmReferenceUpdater();
        }

        return instance;
    }

    @Override
    protected Switch<?> createSwitcher() {
        return new Switcher();
    }
    
}

