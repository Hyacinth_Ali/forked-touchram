/**
 */
package ca.mcgill.sel.usecases;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Specific Flow</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.usecases.SpecificFlow#getReferencedStep <em>Referenced Step</em>}</li>
 * </ul>
 *
 * @see ca.mcgill.sel.usecases.UcPackage#getSpecificFlow()
 * @model
 * @generated
 */
public interface SpecificFlow extends Flow {
    /**
     * Returns the value of the '<em><b>Referenced Step</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>Referenced Step</em>' reference.
     * @see #setReferencedStep(Step)
     * @see ca.mcgill.sel.usecases.UcPackage#getSpecificFlow_ReferencedStep()
     * @model required="true"
     * @generated
     */
    Step getReferencedStep();

    /**
     * Sets the value of the '{@link ca.mcgill.sel.usecases.SpecificFlow#getReferencedStep <em>Referenced Step</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Referenced Step</em>' reference.
     * @see #getReferencedStep()
     * @generated
     */
    void setReferencedStep(Step value);

} // SpecificFlow
