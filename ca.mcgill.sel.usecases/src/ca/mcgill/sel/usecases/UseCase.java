/**
 */
package ca.mcgill.sel.usecases;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Use Case</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.usecases.UseCase#getPrimaryActor <em>Primary Actor</em>}</li>
 *   <li>{@link ca.mcgill.sel.usecases.UseCase#getSecondaryActors <em>Secondary Actors</em>}</li>
 *   <li>{@link ca.mcgill.sel.usecases.UseCase#getIntention <em>Intention</em>}</li>
 *   <li>{@link ca.mcgill.sel.usecases.UseCase#getMultiplicity <em>Multiplicity</em>}</li>
 *   <li>{@link ca.mcgill.sel.usecases.UseCase#getLevel <em>Level</em>}</li>
 *   <li>{@link ca.mcgill.sel.usecases.UseCase#getMainSuccessScenario <em>Main Success Scenario</em>}</li>
 *   <li>{@link ca.mcgill.sel.usecases.UseCase#getGeneralization <em>Generalization</em>}</li>
 *   <li>{@link ca.mcgill.sel.usecases.UseCase#getIncludedUseCases <em>Included Use Cases</em>}</li>
 *   <li>{@link ca.mcgill.sel.usecases.UseCase#getExtendedUseCase <em>Extended Use Case</em>}</li>
 *   <li>{@link ca.mcgill.sel.usecases.UseCase#getConditions <em>Conditions</em>}</li>
 * </ul>
 *
 * @see ca.mcgill.sel.usecases.UcPackage#getUseCase()
 * @model
 * @generated
 */
public interface UseCase extends NamedElement {
    /**
     * Returns the value of the '<em><b>Primary Actor</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>Primary Actor</em>' reference.
     * @see #setPrimaryActor(Actor)
     * @see ca.mcgill.sel.usecases.UcPackage#getUseCase_PrimaryActor()
     * @model
     * @generated
     */
    Actor getPrimaryActor();

    /**
     * Sets the value of the '{@link ca.mcgill.sel.usecases.UseCase#getPrimaryActor <em>Primary Actor</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Primary Actor</em>' reference.
     * @see #getPrimaryActor()
     * @generated
     */
    void setPrimaryActor(Actor value);

    /**
     * Returns the value of the '<em><b>Secondary Actors</b></em>' reference list.
     * The list contents are of type {@link ca.mcgill.sel.usecases.Actor}.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>Secondary Actors</em>' reference list.
     * @see ca.mcgill.sel.usecases.UcPackage#getUseCase_SecondaryActors()
     * @model
     * @generated
     */
    EList<Actor> getSecondaryActors();

    /**
     * Returns the value of the '<em><b>Intention</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>Intention</em>' attribute.
     * @see #setIntention(String)
     * @see ca.mcgill.sel.usecases.UcPackage#getUseCase_Intention()
     * @model
     * @generated
     */
    String getIntention();

    /**
     * Sets the value of the '{@link ca.mcgill.sel.usecases.UseCase#getIntention <em>Intention</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Intention</em>' attribute.
     * @see #getIntention()
     * @generated
     */
    void setIntention(String value);

    /**
     * Returns the value of the '<em><b>Multiplicity</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>Multiplicity</em>' attribute.
     * @see #setMultiplicity(String)
     * @see ca.mcgill.sel.usecases.UcPackage#getUseCase_Multiplicity()
     * @model
     * @generated
     */
    String getMultiplicity();

    /**
     * Sets the value of the '{@link ca.mcgill.sel.usecases.UseCase#getMultiplicity <em>Multiplicity</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Multiplicity</em>' attribute.
     * @see #getMultiplicity()
     * @generated
     */
    void setMultiplicity(String value);

    /**
     * Returns the value of the '<em><b>Level</b></em>' attribute.
     * The default value is <code>"UserGoal"</code>.
     * The literals are from the enumeration {@link ca.mcgill.sel.usecases.Level}.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>Level</em>' attribute.
     * @see ca.mcgill.sel.usecases.Level
     * @see #setLevel(Level)
     * @see ca.mcgill.sel.usecases.UcPackage#getUseCase_Level()
     * @model default="UserGoal"
     * @generated
     */
    Level getLevel();

    /**
     * Sets the value of the '{@link ca.mcgill.sel.usecases.UseCase#getLevel <em>Level</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Level</em>' attribute.
     * @see ca.mcgill.sel.usecases.Level
     * @see #getLevel()
     * @generated
     */
    void setLevel(Level value);

    /**
     * Returns the value of the '<em><b>Main Success Scenario</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>Main Success Scenario</em>' containment reference.
     * @see #setMainSuccessScenario(BasicFlow)
     * @see ca.mcgill.sel.usecases.UcPackage#getUseCase_MainSuccessScenario()
     * @model containment="true" required="true"
     * @generated
     */
    BasicFlow getMainSuccessScenario();

    /**
     * Sets the value of the '{@link ca.mcgill.sel.usecases.UseCase#getMainSuccessScenario <em>Main Success Scenario</em>}' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Main Success Scenario</em>' containment reference.
     * @see #getMainSuccessScenario()
     * @generated
     */
    void setMainSuccessScenario(BasicFlow value);

    /**
     * Returns the value of the '<em><b>Generalization</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>Generalization</em>' reference.
     * @see #setGeneralization(UseCase)
     * @see ca.mcgill.sel.usecases.UcPackage#getUseCase_Generalization()
     * @model
     * @generated
     */
    UseCase getGeneralization();

    /**
     * Sets the value of the '{@link ca.mcgill.sel.usecases.UseCase#getGeneralization <em>Generalization</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Generalization</em>' reference.
     * @see #getGeneralization()
     * @generated
     */
    void setGeneralization(UseCase value);

    /**
     * Returns the value of the '<em><b>Included Use Cases</b></em>' reference list.
     * The list contents are of type {@link ca.mcgill.sel.usecases.UseCase}.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>Included Use Cases</em>' reference list.
     * @see ca.mcgill.sel.usecases.UcPackage#getUseCase_IncludedUseCases()
     * @model
     * @generated
     */
    EList<UseCase> getIncludedUseCases();

    /**
     * Returns the value of the '<em><b>Extended Use Case</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>Extended Use Case</em>' reference.
     * @see #setExtendedUseCase(UseCase)
     * @see ca.mcgill.sel.usecases.UcPackage#getUseCase_ExtendedUseCase()
     * @model
     * @generated
     */
    UseCase getExtendedUseCase();

    /**
     * Sets the value of the '{@link ca.mcgill.sel.usecases.UseCase#getExtendedUseCase <em>Extended Use Case</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Extended Use Case</em>' reference.
     * @see #getExtendedUseCase()
     * @generated
     */
    void setExtendedUseCase(UseCase value);

    /**
     * Returns the value of the '<em><b>Conditions</b></em>' containment reference list.
     * The list contents are of type {@link ca.mcgill.sel.usecases.Condition}.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>Conditions</em>' containment reference list.
     * @see ca.mcgill.sel.usecases.UcPackage#getUseCase_Conditions()
     * @model containment="true"
     * @generated
     */
    EList<Condition> getConditions();

} // UseCase
