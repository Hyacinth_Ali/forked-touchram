/**
 */
package ca.mcgill.sel.usecases;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Step</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.usecases.Step#getStepText <em>Step Text</em>}</li>
 *   <li>{@link ca.mcgill.sel.usecases.Step#getActor <em>Actor</em>}</li>
 * </ul>
 *
 * @see ca.mcgill.sel.usecases.UcPackage#getStep()
 * @model abstract="true"
 * @generated
 */
public interface Step extends EObject {

    /**
     * Returns the value of the '<em><b>Step Text</b></em>' attribute.
     * The default value is <code>""</code>.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>Step Text</em>' attribute.
     * @see #setStepText(String)
     * @see ca.mcgill.sel.usecases.UcPackage#getStep_StepText()
     * @model default=""
     * @generated
     */
    String getStepText();

    /**
     * Sets the value of the '{@link ca.mcgill.sel.usecases.Step#getStepText <em>Step Text</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Step Text</em>' attribute.
     * @see #getStepText()
     * @generated
     */
    void setStepText(String value);

    /**
     * Returns the value of the '<em><b>Actor</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>Actor</em>' reference.
     * @see #setActor(Actor)
     * @see ca.mcgill.sel.usecases.UcPackage#getStep_Actor()
     * @model
     * @generated
     */
    Actor getActor();

    /**
     * Sets the value of the '{@link ca.mcgill.sel.usecases.Step#getActor <em>Actor</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Actor</em>' reference.
     * @see #getActor()
     * @generated
     */
    void setActor(Actor value);
} // Step
