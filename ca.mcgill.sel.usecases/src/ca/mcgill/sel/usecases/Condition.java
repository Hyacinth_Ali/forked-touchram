/**
 */
package ca.mcgill.sel.usecases;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Condition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.usecases.Condition#getText <em>Text</em>}</li>
 * </ul>
 *
 * @see ca.mcgill.sel.usecases.UcPackage#getCondition()
 * @model
 * @generated
 */
public interface Condition extends EObject {
    /**
     * Returns the value of the '<em><b>Text</b></em>' attribute.
     * The default value is <code>""</code>.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>Text</em>' attribute.
     * @see #setText(String)
     * @see ca.mcgill.sel.usecases.UcPackage#getCondition_Text()
     * @model default=""
     * @generated
     */
    String getText();

    /**
     * Sets the value of the '{@link ca.mcgill.sel.usecases.Condition#getText <em>Text</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Text</em>' attribute.
     * @see #getText()
     * @generated
     */
    void setText(String value);

} // Condition
