/**
 */
package ca.mcgill.sel.usecases.impl;

import ca.mcgill.sel.usecases.BasicFlow;
import ca.mcgill.sel.usecases.UcPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Basic Flow</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class BasicFlowImpl extends FlowImpl implements BasicFlow {
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected BasicFlowImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return UcPackage.Literals.BASIC_FLOW;
    }

} //BasicFlowImpl
