/**
 */
package ca.mcgill.sel.usecases.impl;

import ca.mcgill.sel.usecases.BoundedFlow;
import ca.mcgill.sel.usecases.Step;
import ca.mcgill.sel.usecases.UcPackage;

import java.util.Collection;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Bounded Flow</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.usecases.impl.BoundedFlowImpl#getReferencedSteps <em>Referenced Steps</em>}</li>
 * </ul>
 *
 * @generated
 */
public class BoundedFlowImpl extends FlowImpl implements BoundedFlow {
    /**
     * The cached value of the '{@link #getReferencedSteps() <em>Referenced Steps</em>}' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getReferencedSteps()
     * @generated
     * @ordered
     */
    protected EList<Step> referencedSteps;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected BoundedFlowImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return UcPackage.Literals.BOUNDED_FLOW;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EList<Step> getReferencedSteps() {
        if (referencedSteps == null) {
            referencedSteps = new EObjectResolvingEList<Step>(Step.class, this, UcPackage.BOUNDED_FLOW__REFERENCED_STEPS);
        }
        return referencedSteps;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case UcPackage.BOUNDED_FLOW__REFERENCED_STEPS:
                return getReferencedSteps();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @SuppressWarnings("unchecked")
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case UcPackage.BOUNDED_FLOW__REFERENCED_STEPS:
                getReferencedSteps().clear();
                getReferencedSteps().addAll((Collection<? extends Step>)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case UcPackage.BOUNDED_FLOW__REFERENCED_STEPS:
                getReferencedSteps().clear();
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case UcPackage.BOUNDED_FLOW__REFERENCED_STEPS:
                return referencedSteps != null && !referencedSteps.isEmpty();
        }
        return super.eIsSet(featureID);
    }

} //BoundedFlowImpl
