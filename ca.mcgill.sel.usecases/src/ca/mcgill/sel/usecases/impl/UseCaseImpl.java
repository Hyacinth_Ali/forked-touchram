/**
 */
package ca.mcgill.sel.usecases.impl;

import ca.mcgill.sel.usecases.Actor;
import ca.mcgill.sel.usecases.BasicFlow;
import ca.mcgill.sel.usecases.Condition;
import ca.mcgill.sel.usecases.Level;
import ca.mcgill.sel.usecases.UcPackage;
import ca.mcgill.sel.usecases.UseCase;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Use Case</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.usecases.impl.UseCaseImpl#getPrimaryActor <em>Primary Actor</em>}</li>
 *   <li>{@link ca.mcgill.sel.usecases.impl.UseCaseImpl#getSecondaryActors <em>Secondary Actors</em>}</li>
 *   <li>{@link ca.mcgill.sel.usecases.impl.UseCaseImpl#getIntention <em>Intention</em>}</li>
 *   <li>{@link ca.mcgill.sel.usecases.impl.UseCaseImpl#getMultiplicity <em>Multiplicity</em>}</li>
 *   <li>{@link ca.mcgill.sel.usecases.impl.UseCaseImpl#getLevel <em>Level</em>}</li>
 *   <li>{@link ca.mcgill.sel.usecases.impl.UseCaseImpl#getMainSuccessScenario <em>Main Success Scenario</em>}</li>
 *   <li>{@link ca.mcgill.sel.usecases.impl.UseCaseImpl#getGeneralization <em>Generalization</em>}</li>
 *   <li>{@link ca.mcgill.sel.usecases.impl.UseCaseImpl#getIncludedUseCases <em>Included Use Cases</em>}</li>
 *   <li>{@link ca.mcgill.sel.usecases.impl.UseCaseImpl#getExtendedUseCase <em>Extended Use Case</em>}</li>
 *   <li>{@link ca.mcgill.sel.usecases.impl.UseCaseImpl#getConditions <em>Conditions</em>}</li>
 * </ul>
 *
 * @generated
 */
public class UseCaseImpl extends NamedElementImpl implements UseCase {
    /**
     * The cached value of the '{@link #getPrimaryActor() <em>Primary Actor</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getPrimaryActor()
     * @generated
     * @ordered
     */
    protected Actor primaryActor;

    /**
     * The cached value of the '{@link #getSecondaryActors() <em>Secondary Actors</em>}' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getSecondaryActors()
     * @generated
     * @ordered
     */
    protected EList<Actor> secondaryActors;

    /**
     * The default value of the '{@link #getIntention() <em>Intention</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getIntention()
     * @generated
     * @ordered
     */
    protected static final String INTENTION_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getIntention() <em>Intention</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getIntention()
     * @generated
     * @ordered
     */
    protected String intention = INTENTION_EDEFAULT;

    /**
     * The default value of the '{@link #getMultiplicity() <em>Multiplicity</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getMultiplicity()
     * @generated
     * @ordered
     */
    protected static final String MULTIPLICITY_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getMultiplicity() <em>Multiplicity</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getMultiplicity()
     * @generated
     * @ordered
     */
    protected String multiplicity = MULTIPLICITY_EDEFAULT;

    /**
     * The default value of the '{@link #getLevel() <em>Level</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getLevel()
     * @generated
     * @ordered
     */
    protected static final Level LEVEL_EDEFAULT = Level.USER_GOAL;

    /**
     * The cached value of the '{@link #getLevel() <em>Level</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getLevel()
     * @generated
     * @ordered
     */
    protected Level level = LEVEL_EDEFAULT;

    /**
     * The cached value of the '{@link #getMainSuccessScenario() <em>Main Success Scenario</em>}' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getMainSuccessScenario()
     * @generated
     * @ordered
     */
    protected BasicFlow mainSuccessScenario;

    /**
     * The cached value of the '{@link #getGeneralization() <em>Generalization</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getGeneralization()
     * @generated
     * @ordered
     */
    protected UseCase generalization;

    /**
     * The cached value of the '{@link #getIncludedUseCases() <em>Included Use Cases</em>}' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getIncludedUseCases()
     * @generated
     * @ordered
     */
    protected EList<UseCase> includedUseCases;

    /**
     * The cached value of the '{@link #getExtendedUseCase() <em>Extended Use Case</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getExtendedUseCase()
     * @generated
     * @ordered
     */
    protected UseCase extendedUseCase;

    /**
     * The cached value of the '{@link #getConditions() <em>Conditions</em>}' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getConditions()
     * @generated
     * @ordered
     */
    protected EList<Condition> conditions;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected UseCaseImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return UcPackage.Literals.USE_CASE;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Actor getPrimaryActor() {
        if (primaryActor != null && primaryActor.eIsProxy()) {
            InternalEObject oldPrimaryActor = (InternalEObject)primaryActor;
            primaryActor = (Actor)eResolveProxy(oldPrimaryActor);
            if (primaryActor != oldPrimaryActor) {
                if (eNotificationRequired())
                    eNotify(new ENotificationImpl(this, Notification.RESOLVE, UcPackage.USE_CASE__PRIMARY_ACTOR, oldPrimaryActor, primaryActor));
            }
        }
        return primaryActor;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public Actor basicGetPrimaryActor() {
        return primaryActor;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void setPrimaryActor(Actor newPrimaryActor) {
        Actor oldPrimaryActor = primaryActor;
        primaryActor = newPrimaryActor;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, UcPackage.USE_CASE__PRIMARY_ACTOR, oldPrimaryActor, primaryActor));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EList<Actor> getSecondaryActors() {
        if (secondaryActors == null) {
            secondaryActors = new EObjectResolvingEList<Actor>(Actor.class, this, UcPackage.USE_CASE__SECONDARY_ACTORS);
        }
        return secondaryActors;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public String getIntention() {
        return intention;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void setIntention(String newIntention) {
        String oldIntention = intention;
        intention = newIntention;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, UcPackage.USE_CASE__INTENTION, oldIntention, intention));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public String getMultiplicity() {
        return multiplicity;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void setMultiplicity(String newMultiplicity) {
        String oldMultiplicity = multiplicity;
        multiplicity = newMultiplicity;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, UcPackage.USE_CASE__MULTIPLICITY, oldMultiplicity, multiplicity));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Level getLevel() {
        return level;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void setLevel(Level newLevel) {
        Level oldLevel = level;
        level = newLevel == null ? LEVEL_EDEFAULT : newLevel;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, UcPackage.USE_CASE__LEVEL, oldLevel, level));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public BasicFlow getMainSuccessScenario() {
        return mainSuccessScenario;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public NotificationChain basicSetMainSuccessScenario(BasicFlow newMainSuccessScenario, NotificationChain msgs) {
        BasicFlow oldMainSuccessScenario = mainSuccessScenario;
        mainSuccessScenario = newMainSuccessScenario;
        if (eNotificationRequired()) {
            ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, UcPackage.USE_CASE__MAIN_SUCCESS_SCENARIO, oldMainSuccessScenario, newMainSuccessScenario);
            if (msgs == null) msgs = notification; else msgs.add(notification);
        }
        return msgs;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void setMainSuccessScenario(BasicFlow newMainSuccessScenario) {
        if (newMainSuccessScenario != mainSuccessScenario) {
            NotificationChain msgs = null;
            if (mainSuccessScenario != null)
                msgs = ((InternalEObject)mainSuccessScenario).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - UcPackage.USE_CASE__MAIN_SUCCESS_SCENARIO, null, msgs);
            if (newMainSuccessScenario != null)
                msgs = ((InternalEObject)newMainSuccessScenario).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - UcPackage.USE_CASE__MAIN_SUCCESS_SCENARIO, null, msgs);
            msgs = basicSetMainSuccessScenario(newMainSuccessScenario, msgs);
            if (msgs != null) msgs.dispatch();
        }
        else if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, UcPackage.USE_CASE__MAIN_SUCCESS_SCENARIO, newMainSuccessScenario, newMainSuccessScenario));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public UseCase getGeneralization() {
        if (generalization != null && generalization.eIsProxy()) {
            InternalEObject oldGeneralization = (InternalEObject)generalization;
            generalization = (UseCase)eResolveProxy(oldGeneralization);
            if (generalization != oldGeneralization) {
                if (eNotificationRequired())
                    eNotify(new ENotificationImpl(this, Notification.RESOLVE, UcPackage.USE_CASE__GENERALIZATION, oldGeneralization, generalization));
            }
        }
        return generalization;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public UseCase basicGetGeneralization() {
        return generalization;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void setGeneralization(UseCase newGeneralization) {
        UseCase oldGeneralization = generalization;
        generalization = newGeneralization;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, UcPackage.USE_CASE__GENERALIZATION, oldGeneralization, generalization));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EList<UseCase> getIncludedUseCases() {
        if (includedUseCases == null) {
            includedUseCases = new EObjectResolvingEList<UseCase>(UseCase.class, this, UcPackage.USE_CASE__INCLUDED_USE_CASES);
        }
        return includedUseCases;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public UseCase getExtendedUseCase() {
        if (extendedUseCase != null && extendedUseCase.eIsProxy()) {
            InternalEObject oldExtendedUseCase = (InternalEObject)extendedUseCase;
            extendedUseCase = (UseCase)eResolveProxy(oldExtendedUseCase);
            if (extendedUseCase != oldExtendedUseCase) {
                if (eNotificationRequired())
                    eNotify(new ENotificationImpl(this, Notification.RESOLVE, UcPackage.USE_CASE__EXTENDED_USE_CASE, oldExtendedUseCase, extendedUseCase));
            }
        }
        return extendedUseCase;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public UseCase basicGetExtendedUseCase() {
        return extendedUseCase;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void setExtendedUseCase(UseCase newExtendedUseCase) {
        UseCase oldExtendedUseCase = extendedUseCase;
        extendedUseCase = newExtendedUseCase;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, UcPackage.USE_CASE__EXTENDED_USE_CASE, oldExtendedUseCase, extendedUseCase));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EList<Condition> getConditions() {
        if (conditions == null) {
            conditions = new EObjectContainmentEList<Condition>(Condition.class, this, UcPackage.USE_CASE__CONDITIONS);
        }
        return conditions;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case UcPackage.USE_CASE__MAIN_SUCCESS_SCENARIO:
                return basicSetMainSuccessScenario(null, msgs);
            case UcPackage.USE_CASE__CONDITIONS:
                return ((InternalEList<?>)getConditions()).basicRemove(otherEnd, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case UcPackage.USE_CASE__PRIMARY_ACTOR:
                if (resolve) return getPrimaryActor();
                return basicGetPrimaryActor();
            case UcPackage.USE_CASE__SECONDARY_ACTORS:
                return getSecondaryActors();
            case UcPackage.USE_CASE__INTENTION:
                return getIntention();
            case UcPackage.USE_CASE__MULTIPLICITY:
                return getMultiplicity();
            case UcPackage.USE_CASE__LEVEL:
                return getLevel();
            case UcPackage.USE_CASE__MAIN_SUCCESS_SCENARIO:
                return getMainSuccessScenario();
            case UcPackage.USE_CASE__GENERALIZATION:
                if (resolve) return getGeneralization();
                return basicGetGeneralization();
            case UcPackage.USE_CASE__INCLUDED_USE_CASES:
                return getIncludedUseCases();
            case UcPackage.USE_CASE__EXTENDED_USE_CASE:
                if (resolve) return getExtendedUseCase();
                return basicGetExtendedUseCase();
            case UcPackage.USE_CASE__CONDITIONS:
                return getConditions();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @SuppressWarnings("unchecked")
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case UcPackage.USE_CASE__PRIMARY_ACTOR:
                setPrimaryActor((Actor)newValue);
                return;
            case UcPackage.USE_CASE__SECONDARY_ACTORS:
                getSecondaryActors().clear();
                getSecondaryActors().addAll((Collection<? extends Actor>)newValue);
                return;
            case UcPackage.USE_CASE__INTENTION:
                setIntention((String)newValue);
                return;
            case UcPackage.USE_CASE__MULTIPLICITY:
                setMultiplicity((String)newValue);
                return;
            case UcPackage.USE_CASE__LEVEL:
                setLevel((Level)newValue);
                return;
            case UcPackage.USE_CASE__MAIN_SUCCESS_SCENARIO:
                setMainSuccessScenario((BasicFlow)newValue);
                return;
            case UcPackage.USE_CASE__GENERALIZATION:
                setGeneralization((UseCase)newValue);
                return;
            case UcPackage.USE_CASE__INCLUDED_USE_CASES:
                getIncludedUseCases().clear();
                getIncludedUseCases().addAll((Collection<? extends UseCase>)newValue);
                return;
            case UcPackage.USE_CASE__EXTENDED_USE_CASE:
                setExtendedUseCase((UseCase)newValue);
                return;
            case UcPackage.USE_CASE__CONDITIONS:
                getConditions().clear();
                getConditions().addAll((Collection<? extends Condition>)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case UcPackage.USE_CASE__PRIMARY_ACTOR:
                setPrimaryActor((Actor)null);
                return;
            case UcPackage.USE_CASE__SECONDARY_ACTORS:
                getSecondaryActors().clear();
                return;
            case UcPackage.USE_CASE__INTENTION:
                setIntention(INTENTION_EDEFAULT);
                return;
            case UcPackage.USE_CASE__MULTIPLICITY:
                setMultiplicity(MULTIPLICITY_EDEFAULT);
                return;
            case UcPackage.USE_CASE__LEVEL:
                setLevel(LEVEL_EDEFAULT);
                return;
            case UcPackage.USE_CASE__MAIN_SUCCESS_SCENARIO:
                setMainSuccessScenario((BasicFlow)null);
                return;
            case UcPackage.USE_CASE__GENERALIZATION:
                setGeneralization((UseCase)null);
                return;
            case UcPackage.USE_CASE__INCLUDED_USE_CASES:
                getIncludedUseCases().clear();
                return;
            case UcPackage.USE_CASE__EXTENDED_USE_CASE:
                setExtendedUseCase((UseCase)null);
                return;
            case UcPackage.USE_CASE__CONDITIONS:
                getConditions().clear();
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case UcPackage.USE_CASE__PRIMARY_ACTOR:
                return primaryActor != null;
            case UcPackage.USE_CASE__SECONDARY_ACTORS:
                return secondaryActors != null && !secondaryActors.isEmpty();
            case UcPackage.USE_CASE__INTENTION:
                return INTENTION_EDEFAULT == null ? intention != null : !INTENTION_EDEFAULT.equals(intention);
            case UcPackage.USE_CASE__MULTIPLICITY:
                return MULTIPLICITY_EDEFAULT == null ? multiplicity != null : !MULTIPLICITY_EDEFAULT.equals(multiplicity);
            case UcPackage.USE_CASE__LEVEL:
                return level != LEVEL_EDEFAULT;
            case UcPackage.USE_CASE__MAIN_SUCCESS_SCENARIO:
                return mainSuccessScenario != null;
            case UcPackage.USE_CASE__GENERALIZATION:
                return generalization != null;
            case UcPackage.USE_CASE__INCLUDED_USE_CASES:
                return includedUseCases != null && !includedUseCases.isEmpty();
            case UcPackage.USE_CASE__EXTENDED_USE_CASE:
                return extendedUseCase != null;
            case UcPackage.USE_CASE__CONDITIONS:
                return conditions != null && !conditions.isEmpty();
        }
        return super.eIsSet(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public String toString() {
        if (eIsProxy()) return super.toString();

        StringBuilder result = new StringBuilder(super.toString());
        result.append(" (intention: ");
        result.append(intention);
        result.append(", multiplicity: ");
        result.append(multiplicity);
        result.append(", level: ");
        result.append(level);
        result.append(')');
        return result.toString();
    }

} //UseCaseImpl
