/**
 */
package ca.mcgill.sel.usecases.impl;

import ca.mcgill.sel.usecases.Actor;
import ca.mcgill.sel.usecases.Layout;
import ca.mcgill.sel.usecases.Note;
import ca.mcgill.sel.usecases.UcPackage;
import ca.mcgill.sel.usecases.UseCase;
import ca.mcgill.sel.usecases.UseCaseModel;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Use Case Model</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.usecases.impl.UseCaseModelImpl#getLayout <em>Layout</em>}</li>
 *   <li>{@link ca.mcgill.sel.usecases.impl.UseCaseModelImpl#getActors <em>Actors</em>}</li>
 *   <li>{@link ca.mcgill.sel.usecases.impl.UseCaseModelImpl#getUseCases <em>Use Cases</em>}</li>
 *   <li>{@link ca.mcgill.sel.usecases.impl.UseCaseModelImpl#getNotes <em>Notes</em>}</li>
 *   <li>{@link ca.mcgill.sel.usecases.impl.UseCaseModelImpl#getSystemName <em>System Name</em>}</li>
 * </ul>
 *
 * @generated
 */
public class UseCaseModelImpl extends NamedElementImpl implements UseCaseModel {
    /**
     * The cached value of the '{@link #getLayout() <em>Layout</em>}' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getLayout()
     * @generated
     * @ordered
     */
    protected Layout layout;

    /**
     * The cached value of the '{@link #getActors() <em>Actors</em>}' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getActors()
     * @generated
     * @ordered
     */
    protected EList<Actor> actors;

    /**
     * The cached value of the '{@link #getUseCases() <em>Use Cases</em>}' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getUseCases()
     * @generated
     * @ordered
     */
    protected EList<UseCase> useCases;

    /**
     * The cached value of the '{@link #getNotes() <em>Notes</em>}' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getNotes()
     * @generated
     * @ordered
     */
    protected EList<Note> notes;

    /**
     * The default value of the '{@link #getSystemName() <em>System Name</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getSystemName()
     * @generated
     * @ordered
     */
    protected static final String SYSTEM_NAME_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getSystemName() <em>System Name</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getSystemName()
     * @generated
     * @ordered
     */
    protected String systemName = SYSTEM_NAME_EDEFAULT;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected UseCaseModelImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return UcPackage.Literals.USE_CASE_MODEL;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Layout getLayout() {
        return layout;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public NotificationChain basicSetLayout(Layout newLayout, NotificationChain msgs) {
        Layout oldLayout = layout;
        layout = newLayout;
        if (eNotificationRequired()) {
            ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, UcPackage.USE_CASE_MODEL__LAYOUT, oldLayout, newLayout);
            if (msgs == null) msgs = notification; else msgs.add(notification);
        }
        return msgs;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void setLayout(Layout newLayout) {
        if (newLayout != layout) {
            NotificationChain msgs = null;
            if (layout != null)
                msgs = ((InternalEObject)layout).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - UcPackage.USE_CASE_MODEL__LAYOUT, null, msgs);
            if (newLayout != null)
                msgs = ((InternalEObject)newLayout).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - UcPackage.USE_CASE_MODEL__LAYOUT, null, msgs);
            msgs = basicSetLayout(newLayout, msgs);
            if (msgs != null) msgs.dispatch();
        }
        else if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, UcPackage.USE_CASE_MODEL__LAYOUT, newLayout, newLayout));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EList<Actor> getActors() {
        if (actors == null) {
            actors = new EObjectContainmentEList<Actor>(Actor.class, this, UcPackage.USE_CASE_MODEL__ACTORS);
        }
        return actors;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EList<UseCase> getUseCases() {
        if (useCases == null) {
            useCases = new EObjectContainmentEList<UseCase>(UseCase.class, this, UcPackage.USE_CASE_MODEL__USE_CASES);
        }
        return useCases;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EList<Note> getNotes() {
        if (notes == null) {
            notes = new EObjectContainmentEList<Note>(Note.class, this, UcPackage.USE_CASE_MODEL__NOTES);
        }
        return notes;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public String getSystemName() {
        return systemName;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void setSystemName(String newSystemName) {
        String oldSystemName = systemName;
        systemName = newSystemName;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, UcPackage.USE_CASE_MODEL__SYSTEM_NAME, oldSystemName, systemName));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case UcPackage.USE_CASE_MODEL__LAYOUT:
                return basicSetLayout(null, msgs);
            case UcPackage.USE_CASE_MODEL__ACTORS:
                return ((InternalEList<?>)getActors()).basicRemove(otherEnd, msgs);
            case UcPackage.USE_CASE_MODEL__USE_CASES:
                return ((InternalEList<?>)getUseCases()).basicRemove(otherEnd, msgs);
            case UcPackage.USE_CASE_MODEL__NOTES:
                return ((InternalEList<?>)getNotes()).basicRemove(otherEnd, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case UcPackage.USE_CASE_MODEL__LAYOUT:
                return getLayout();
            case UcPackage.USE_CASE_MODEL__ACTORS:
                return getActors();
            case UcPackage.USE_CASE_MODEL__USE_CASES:
                return getUseCases();
            case UcPackage.USE_CASE_MODEL__NOTES:
                return getNotes();
            case UcPackage.USE_CASE_MODEL__SYSTEM_NAME:
                return getSystemName();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @SuppressWarnings("unchecked")
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case UcPackage.USE_CASE_MODEL__LAYOUT:
                setLayout((Layout)newValue);
                return;
            case UcPackage.USE_CASE_MODEL__ACTORS:
                getActors().clear();
                getActors().addAll((Collection<? extends Actor>)newValue);
                return;
            case UcPackage.USE_CASE_MODEL__USE_CASES:
                getUseCases().clear();
                getUseCases().addAll((Collection<? extends UseCase>)newValue);
                return;
            case UcPackage.USE_CASE_MODEL__NOTES:
                getNotes().clear();
                getNotes().addAll((Collection<? extends Note>)newValue);
                return;
            case UcPackage.USE_CASE_MODEL__SYSTEM_NAME:
                setSystemName((String)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case UcPackage.USE_CASE_MODEL__LAYOUT:
                setLayout((Layout)null);
                return;
            case UcPackage.USE_CASE_MODEL__ACTORS:
                getActors().clear();
                return;
            case UcPackage.USE_CASE_MODEL__USE_CASES:
                getUseCases().clear();
                return;
            case UcPackage.USE_CASE_MODEL__NOTES:
                getNotes().clear();
                return;
            case UcPackage.USE_CASE_MODEL__SYSTEM_NAME:
                setSystemName(SYSTEM_NAME_EDEFAULT);
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case UcPackage.USE_CASE_MODEL__LAYOUT:
                return layout != null;
            case UcPackage.USE_CASE_MODEL__ACTORS:
                return actors != null && !actors.isEmpty();
            case UcPackage.USE_CASE_MODEL__USE_CASES:
                return useCases != null && !useCases.isEmpty();
            case UcPackage.USE_CASE_MODEL__NOTES:
                return notes != null && !notes.isEmpty();
            case UcPackage.USE_CASE_MODEL__SYSTEM_NAME:
                return SYSTEM_NAME_EDEFAULT == null ? systemName != null : !SYSTEM_NAME_EDEFAULT.equals(systemName);
        }
        return super.eIsSet(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public String toString() {
        if (eIsProxy()) return super.toString();

        StringBuilder result = new StringBuilder(super.toString());
        result.append(" (systemName: ");
        result.append(systemName);
        result.append(')');
        return result.toString();
    }

} //UseCaseModelImpl
