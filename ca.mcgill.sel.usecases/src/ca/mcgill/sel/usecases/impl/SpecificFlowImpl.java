/**
 */
package ca.mcgill.sel.usecases.impl;

import ca.mcgill.sel.usecases.SpecificFlow;
import ca.mcgill.sel.usecases.Step;
import ca.mcgill.sel.usecases.UcPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Specific Flow</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.usecases.impl.SpecificFlowImpl#getReferencedStep <em>Referenced Step</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SpecificFlowImpl extends FlowImpl implements SpecificFlow {
    /**
     * The cached value of the '{@link #getReferencedStep() <em>Referenced Step</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getReferencedStep()
     * @generated
     * @ordered
     */
    protected Step referencedStep;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected SpecificFlowImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return UcPackage.Literals.SPECIFIC_FLOW;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Step getReferencedStep() {
        if (referencedStep != null && referencedStep.eIsProxy()) {
            InternalEObject oldReferencedStep = (InternalEObject)referencedStep;
            referencedStep = (Step)eResolveProxy(oldReferencedStep);
            if (referencedStep != oldReferencedStep) {
                if (eNotificationRequired())
                    eNotify(new ENotificationImpl(this, Notification.RESOLVE, UcPackage.SPECIFIC_FLOW__REFERENCED_STEP, oldReferencedStep, referencedStep));
            }
        }
        return referencedStep;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public Step basicGetReferencedStep() {
        return referencedStep;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void setReferencedStep(Step newReferencedStep) {
        Step oldReferencedStep = referencedStep;
        referencedStep = newReferencedStep;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, UcPackage.SPECIFIC_FLOW__REFERENCED_STEP, oldReferencedStep, referencedStep));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case UcPackage.SPECIFIC_FLOW__REFERENCED_STEP:
                if (resolve) return getReferencedStep();
                return basicGetReferencedStep();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case UcPackage.SPECIFIC_FLOW__REFERENCED_STEP:
                setReferencedStep((Step)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case UcPackage.SPECIFIC_FLOW__REFERENCED_STEP:
                setReferencedStep((Step)null);
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case UcPackage.SPECIFIC_FLOW__REFERENCED_STEP:
                return referencedStep != null;
        }
        return super.eIsSet(featureID);
    }

} //SpecificFlowImpl
