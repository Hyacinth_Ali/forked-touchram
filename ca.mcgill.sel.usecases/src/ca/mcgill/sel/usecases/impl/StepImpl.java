/**
 */
package ca.mcgill.sel.usecases.impl;

import ca.mcgill.sel.usecases.Actor;
import ca.mcgill.sel.usecases.Step;
import ca.mcgill.sel.usecases.UcPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Step</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.usecases.impl.StepImpl#getStepText <em>Step Text</em>}</li>
 *   <li>{@link ca.mcgill.sel.usecases.impl.StepImpl#getActor <em>Actor</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class StepImpl extends MinimalEObjectImpl.Container implements Step {
    /**
     * The default value of the '{@link #getStepText() <em>Step Text</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getStepText()
     * @generated
     * @ordered
     */
    protected static final String STEP_TEXT_EDEFAULT = "";
    /**
     * The cached value of the '{@link #getStepText() <em>Step Text</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getStepText()
     * @generated
     * @ordered
     */
    protected String stepText = STEP_TEXT_EDEFAULT;

    /**
     * The cached value of the '{@link #getActor() <em>Actor</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getActor()
     * @generated
     * @ordered
     */
    protected Actor actor;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected StepImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return UcPackage.Literals.STEP;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public String getStepText() {
        return stepText;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void setStepText(String newStepText) {
        String oldStepText = stepText;
        stepText = newStepText;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, UcPackage.STEP__STEP_TEXT, oldStepText, stepText));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Actor getActor() {
        if (actor != null && actor.eIsProxy()) {
            InternalEObject oldActor = (InternalEObject)actor;
            actor = (Actor)eResolveProxy(oldActor);
            if (actor != oldActor) {
                if (eNotificationRequired())
                    eNotify(new ENotificationImpl(this, Notification.RESOLVE, UcPackage.STEP__ACTOR, oldActor, actor));
            }
        }
        return actor;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public Actor basicGetActor() {
        return actor;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void setActor(Actor newActor) {
        Actor oldActor = actor;
        actor = newActor;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, UcPackage.STEP__ACTOR, oldActor, actor));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case UcPackage.STEP__STEP_TEXT:
                return getStepText();
            case UcPackage.STEP__ACTOR:
                if (resolve) return getActor();
                return basicGetActor();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case UcPackage.STEP__STEP_TEXT:
                setStepText((String)newValue);
                return;
            case UcPackage.STEP__ACTOR:
                setActor((Actor)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case UcPackage.STEP__STEP_TEXT:
                setStepText(STEP_TEXT_EDEFAULT);
                return;
            case UcPackage.STEP__ACTOR:
                setActor((Actor)null);
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case UcPackage.STEP__STEP_TEXT:
                return STEP_TEXT_EDEFAULT == null ? stepText != null : !STEP_TEXT_EDEFAULT.equals(stepText);
            case UcPackage.STEP__ACTOR:
                return actor != null;
        }
        return super.eIsSet(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public String toString() {
        if (eIsProxy()) return super.toString();

        StringBuilder result = new StringBuilder(super.toString());
        result.append(" (stepText: ");
        result.append(stepText);
        result.append(')');
        return result.toString();
    }

} //StepImpl
