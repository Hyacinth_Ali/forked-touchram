/**
 */
package ca.mcgill.sel.usecases;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see ca.mcgill.sel.usecases.UcFactory
 * @model kind="package"
 *        annotation="http://www.eclipse.org/OCL/Import"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore invocationDelegates='http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot' settingDelegates='http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot' validationDelegates='http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot'"
 * @generated
 */
public interface UcPackage extends EPackage {
    /**
     * The package name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    String eNAME = "usecases";

    /**
     * The package namespace URI.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    String eNS_URI = "http://cs.mcgill.ca/sel/uc/1.0";

    /**
     * The package namespace name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    String eNS_PREFIX = "usecases";

    /**
     * The singleton instance of the package.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    UcPackage eINSTANCE = ca.mcgill.sel.usecases.impl.UcPackageImpl.init();

    /**
     * The meta object id for the '{@link ca.mcgill.sel.usecases.impl.NamedElementImpl <em>Named Element</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see ca.mcgill.sel.usecases.impl.NamedElementImpl
     * @see ca.mcgill.sel.usecases.impl.UcPackageImpl#getNamedElement()
     * @generated
     */
    int NAMED_ELEMENT = 1;

    /**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int NAMED_ELEMENT__NAME = 0;

    /**
     * The number of structural features of the '<em>Named Element</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int NAMED_ELEMENT_FEATURE_COUNT = 1;

    /**
     * The number of operations of the '<em>Named Element</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int NAMED_ELEMENT_OPERATION_COUNT = 0;

    /**
     * The meta object id for the '{@link ca.mcgill.sel.usecases.impl.ActorImpl <em>Actor</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see ca.mcgill.sel.usecases.impl.ActorImpl
     * @see ca.mcgill.sel.usecases.impl.UcPackageImpl#getActor()
     * @generated
     */
    int ACTOR = 0;

    /**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int ACTOR__NAME = NAMED_ELEMENT__NAME;

    /**
     * The feature id for the '<em><b>Lower Bound</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int ACTOR__LOWER_BOUND = NAMED_ELEMENT_FEATURE_COUNT + 0;

    /**
     * The feature id for the '<em><b>Upper Bound</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int ACTOR__UPPER_BOUND = NAMED_ELEMENT_FEATURE_COUNT + 1;

    /**
     * The feature id for the '<em><b>Generalization</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int ACTOR__GENERALIZATION = NAMED_ELEMENT_FEATURE_COUNT + 2;

    /**
     * The number of structural features of the '<em>Actor</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int ACTOR_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 3;

    /**
     * The number of operations of the '<em>Actor</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int ACTOR_OPERATION_COUNT = NAMED_ELEMENT_OPERATION_COUNT + 0;

    /**
     * The meta object id for the '{@link ca.mcgill.sel.usecases.impl.UseCaseImpl <em>Use Case</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see ca.mcgill.sel.usecases.impl.UseCaseImpl
     * @see ca.mcgill.sel.usecases.impl.UcPackageImpl#getUseCase()
     * @generated
     */
    int USE_CASE = 2;

    /**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int USE_CASE__NAME = NAMED_ELEMENT__NAME;

    /**
     * The feature id for the '<em><b>Primary Actor</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int USE_CASE__PRIMARY_ACTOR = NAMED_ELEMENT_FEATURE_COUNT + 0;

    /**
     * The feature id for the '<em><b>Secondary Actors</b></em>' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int USE_CASE__SECONDARY_ACTORS = NAMED_ELEMENT_FEATURE_COUNT + 1;

    /**
     * The feature id for the '<em><b>Intention</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int USE_CASE__INTENTION = NAMED_ELEMENT_FEATURE_COUNT + 2;

    /**
     * The feature id for the '<em><b>Multiplicity</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int USE_CASE__MULTIPLICITY = NAMED_ELEMENT_FEATURE_COUNT + 3;

    /**
     * The feature id for the '<em><b>Level</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int USE_CASE__LEVEL = NAMED_ELEMENT_FEATURE_COUNT + 4;

    /**
     * The feature id for the '<em><b>Main Success Scenario</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int USE_CASE__MAIN_SUCCESS_SCENARIO = NAMED_ELEMENT_FEATURE_COUNT + 5;

    /**
     * The feature id for the '<em><b>Generalization</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int USE_CASE__GENERALIZATION = NAMED_ELEMENT_FEATURE_COUNT + 6;

    /**
     * The feature id for the '<em><b>Included Use Cases</b></em>' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int USE_CASE__INCLUDED_USE_CASES = NAMED_ELEMENT_FEATURE_COUNT + 7;

    /**
     * The feature id for the '<em><b>Extended Use Case</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int USE_CASE__EXTENDED_USE_CASE = NAMED_ELEMENT_FEATURE_COUNT + 8;

    /**
     * The feature id for the '<em><b>Conditions</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int USE_CASE__CONDITIONS = NAMED_ELEMENT_FEATURE_COUNT + 9;

    /**
     * The number of structural features of the '<em>Use Case</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int USE_CASE_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 10;

    /**
     * The number of operations of the '<em>Use Case</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int USE_CASE_OPERATION_COUNT = NAMED_ELEMENT_OPERATION_COUNT + 0;

    /**
     * The meta object id for the '{@link ca.mcgill.sel.usecases.impl.LayoutImpl <em>Layout</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see ca.mcgill.sel.usecases.impl.LayoutImpl
     * @see ca.mcgill.sel.usecases.impl.UcPackageImpl#getLayout()
     * @generated
     */
    int LAYOUT = 3;

    /**
     * The feature id for the '<em><b>Containers</b></em>' map.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int LAYOUT__CONTAINERS = 0;

    /**
     * The number of structural features of the '<em>Layout</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int LAYOUT_FEATURE_COUNT = 1;

    /**
     * The number of operations of the '<em>Layout</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int LAYOUT_OPERATION_COUNT = 0;

    /**
     * The meta object id for the '{@link ca.mcgill.sel.usecases.impl.ContainerMapImpl <em>Container Map</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see ca.mcgill.sel.usecases.impl.ContainerMapImpl
     * @see ca.mcgill.sel.usecases.impl.UcPackageImpl#getContainerMap()
     * @generated
     */
    int CONTAINER_MAP = 4;

    /**
     * The feature id for the '<em><b>Key</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int CONTAINER_MAP__KEY = 0;

    /**
     * The feature id for the '<em><b>Value</b></em>' map.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int CONTAINER_MAP__VALUE = 1;

    /**
     * The number of structural features of the '<em>Container Map</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int CONTAINER_MAP_FEATURE_COUNT = 2;

    /**
     * The number of operations of the '<em>Container Map</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int CONTAINER_MAP_OPERATION_COUNT = 0;

    /**
     * The meta object id for the '{@link ca.mcgill.sel.usecases.impl.ElementMapImpl <em>Element Map</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see ca.mcgill.sel.usecases.impl.ElementMapImpl
     * @see ca.mcgill.sel.usecases.impl.UcPackageImpl#getElementMap()
     * @generated
     */
    int ELEMENT_MAP = 5;

    /**
     * The feature id for the '<em><b>Key</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int ELEMENT_MAP__KEY = 0;

    /**
     * The feature id for the '<em><b>Value</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int ELEMENT_MAP__VALUE = 1;

    /**
     * The number of structural features of the '<em>Element Map</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int ELEMENT_MAP_FEATURE_COUNT = 2;

    /**
     * The number of operations of the '<em>Element Map</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int ELEMENT_MAP_OPERATION_COUNT = 0;

    /**
     * The meta object id for the '{@link ca.mcgill.sel.usecases.impl.LayoutElementImpl <em>Layout Element</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see ca.mcgill.sel.usecases.impl.LayoutElementImpl
     * @see ca.mcgill.sel.usecases.impl.UcPackageImpl#getLayoutElement()
     * @generated
     */
    int LAYOUT_ELEMENT = 6;

    /**
     * The feature id for the '<em><b>X</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int LAYOUT_ELEMENT__X = 0;

    /**
     * The feature id for the '<em><b>Y</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int LAYOUT_ELEMENT__Y = 1;

    /**
     * The number of structural features of the '<em>Layout Element</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int LAYOUT_ELEMENT_FEATURE_COUNT = 2;

    /**
     * The number of operations of the '<em>Layout Element</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int LAYOUT_ELEMENT_OPERATION_COUNT = 0;

    /**
     * The meta object id for the '{@link ca.mcgill.sel.usecases.impl.UseCaseModelImpl <em>Use Case Model</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see ca.mcgill.sel.usecases.impl.UseCaseModelImpl
     * @see ca.mcgill.sel.usecases.impl.UcPackageImpl#getUseCaseModel()
     * @generated
     */
    int USE_CASE_MODEL = 7;

    /**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int USE_CASE_MODEL__NAME = NAMED_ELEMENT__NAME;

    /**
     * The feature id for the '<em><b>Layout</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int USE_CASE_MODEL__LAYOUT = NAMED_ELEMENT_FEATURE_COUNT + 0;

    /**
     * The feature id for the '<em><b>Actors</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int USE_CASE_MODEL__ACTORS = NAMED_ELEMENT_FEATURE_COUNT + 1;

    /**
     * The feature id for the '<em><b>Use Cases</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int USE_CASE_MODEL__USE_CASES = NAMED_ELEMENT_FEATURE_COUNT + 2;

    /**
     * The feature id for the '<em><b>Notes</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int USE_CASE_MODEL__NOTES = NAMED_ELEMENT_FEATURE_COUNT + 3;

    /**
     * The feature id for the '<em><b>System Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int USE_CASE_MODEL__SYSTEM_NAME = NAMED_ELEMENT_FEATURE_COUNT + 4;

    /**
     * The number of structural features of the '<em>Use Case Model</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int USE_CASE_MODEL_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 5;

    /**
     * The number of operations of the '<em>Use Case Model</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int USE_CASE_MODEL_OPERATION_COUNT = NAMED_ELEMENT_OPERATION_COUNT + 0;

    /**
     * The meta object id for the '{@link ca.mcgill.sel.usecases.impl.NoteImpl <em>Note</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see ca.mcgill.sel.usecases.impl.NoteImpl
     * @see ca.mcgill.sel.usecases.impl.UcPackageImpl#getNote()
     * @generated
     */
    int NOTE = 8;

    /**
     * The feature id for the '<em><b>Noted Element</b></em>' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int NOTE__NOTED_ELEMENT = 0;

    /**
     * The feature id for the '<em><b>Content</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int NOTE__CONTENT = 1;

    /**
     * The number of structural features of the '<em>Note</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int NOTE_FEATURE_COUNT = 2;

    /**
     * The number of operations of the '<em>Note</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int NOTE_OPERATION_COUNT = 0;


    /**
     * The meta object id for the '{@link ca.mcgill.sel.usecases.impl.StepImpl <em>Step</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see ca.mcgill.sel.usecases.impl.StepImpl
     * @see ca.mcgill.sel.usecases.impl.UcPackageImpl#getStep()
     * @generated
     */
    int STEP = 9;

    /**
     * The feature id for the '<em><b>Step Text</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int STEP__STEP_TEXT = 0;

    /**
     * The feature id for the '<em><b>Actor</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int STEP__ACTOR = 1;

    /**
     * The number of structural features of the '<em>Step</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int STEP_FEATURE_COUNT = 2;

    /**
     * The number of operations of the '<em>Step</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int STEP_OPERATION_COUNT = 0;

    /**
     * The meta object id for the '{@link ca.mcgill.sel.usecases.impl.FlowImpl <em>Flow</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see ca.mcgill.sel.usecases.impl.FlowImpl
     * @see ca.mcgill.sel.usecases.impl.UcPackageImpl#getFlow()
     * @generated
     */
    int FLOW = 10;

    /**
     * The feature id for the '<em><b>Steps</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int FLOW__STEPS = 0;

    /**
     * The feature id for the '<em><b>Precondition</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int FLOW__PRECONDITION = 1;

    /**
     * The feature id for the '<em><b>Alternate Flows</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int FLOW__ALTERNATE_FLOWS = 2;

    /**
     * The feature id for the '<em><b>Conclusion Type</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int FLOW__CONCLUSION_TYPE = 3;

    /**
     * The feature id for the '<em><b>Conclusion Step</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int FLOW__CONCLUSION_STEP = 4;

    /**
     * The feature id for the '<em><b>Post Condition</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int FLOW__POST_CONDITION = 5;

    /**
     * The number of structural features of the '<em>Flow</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int FLOW_FEATURE_COUNT = 6;

    /**
     * The number of operations of the '<em>Flow</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int FLOW_OPERATION_COUNT = 0;

    /**
     * The meta object id for the '{@link ca.mcgill.sel.usecases.impl.BoundedFlowImpl <em>Bounded Flow</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see ca.mcgill.sel.usecases.impl.BoundedFlowImpl
     * @see ca.mcgill.sel.usecases.impl.UcPackageImpl#getBoundedFlow()
     * @generated
     */
    int BOUNDED_FLOW = 11;

    /**
     * The feature id for the '<em><b>Steps</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int BOUNDED_FLOW__STEPS = FLOW__STEPS;

    /**
     * The feature id for the '<em><b>Precondition</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int BOUNDED_FLOW__PRECONDITION = FLOW__PRECONDITION;

    /**
     * The feature id for the '<em><b>Alternate Flows</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int BOUNDED_FLOW__ALTERNATE_FLOWS = FLOW__ALTERNATE_FLOWS;

    /**
     * The feature id for the '<em><b>Conclusion Type</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int BOUNDED_FLOW__CONCLUSION_TYPE = FLOW__CONCLUSION_TYPE;

    /**
     * The feature id for the '<em><b>Conclusion Step</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int BOUNDED_FLOW__CONCLUSION_STEP = FLOW__CONCLUSION_STEP;

    /**
     * The feature id for the '<em><b>Post Condition</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int BOUNDED_FLOW__POST_CONDITION = FLOW__POST_CONDITION;

    /**
     * The feature id for the '<em><b>Referenced Steps</b></em>' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int BOUNDED_FLOW__REFERENCED_STEPS = FLOW_FEATURE_COUNT + 0;

    /**
     * The number of structural features of the '<em>Bounded Flow</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int BOUNDED_FLOW_FEATURE_COUNT = FLOW_FEATURE_COUNT + 1;

    /**
     * The number of operations of the '<em>Bounded Flow</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int BOUNDED_FLOW_OPERATION_COUNT = FLOW_OPERATION_COUNT + 0;

    /**
     * The meta object id for the '{@link ca.mcgill.sel.usecases.impl.SpecificFlowImpl <em>Specific Flow</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see ca.mcgill.sel.usecases.impl.SpecificFlowImpl
     * @see ca.mcgill.sel.usecases.impl.UcPackageImpl#getSpecificFlow()
     * @generated
     */
    int SPECIFIC_FLOW = 12;

    /**
     * The feature id for the '<em><b>Steps</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int SPECIFIC_FLOW__STEPS = FLOW__STEPS;

    /**
     * The feature id for the '<em><b>Precondition</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int SPECIFIC_FLOW__PRECONDITION = FLOW__PRECONDITION;

    /**
     * The feature id for the '<em><b>Alternate Flows</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int SPECIFIC_FLOW__ALTERNATE_FLOWS = FLOW__ALTERNATE_FLOWS;

    /**
     * The feature id for the '<em><b>Conclusion Type</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int SPECIFIC_FLOW__CONCLUSION_TYPE = FLOW__CONCLUSION_TYPE;

    /**
     * The feature id for the '<em><b>Conclusion Step</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int SPECIFIC_FLOW__CONCLUSION_STEP = FLOW__CONCLUSION_STEP;

    /**
     * The feature id for the '<em><b>Post Condition</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int SPECIFIC_FLOW__POST_CONDITION = FLOW__POST_CONDITION;

    /**
     * The feature id for the '<em><b>Referenced Step</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int SPECIFIC_FLOW__REFERENCED_STEP = FLOW_FEATURE_COUNT + 0;

    /**
     * The number of structural features of the '<em>Specific Flow</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int SPECIFIC_FLOW_FEATURE_COUNT = FLOW_FEATURE_COUNT + 1;

    /**
     * The number of operations of the '<em>Specific Flow</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int SPECIFIC_FLOW_OPERATION_COUNT = FLOW_OPERATION_COUNT + 0;

    /**
     * The meta object id for the '{@link ca.mcgill.sel.usecases.impl.BasicFlowImpl <em>Basic Flow</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see ca.mcgill.sel.usecases.impl.BasicFlowImpl
     * @see ca.mcgill.sel.usecases.impl.UcPackageImpl#getBasicFlow()
     * @generated
     */
    int BASIC_FLOW = 13;

    /**
     * The feature id for the '<em><b>Steps</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int BASIC_FLOW__STEPS = FLOW__STEPS;

    /**
     * The feature id for the '<em><b>Precondition</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int BASIC_FLOW__PRECONDITION = FLOW__PRECONDITION;

    /**
     * The feature id for the '<em><b>Alternate Flows</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int BASIC_FLOW__ALTERNATE_FLOWS = FLOW__ALTERNATE_FLOWS;

    /**
     * The feature id for the '<em><b>Conclusion Type</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int BASIC_FLOW__CONCLUSION_TYPE = FLOW__CONCLUSION_TYPE;

    /**
     * The feature id for the '<em><b>Conclusion Step</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int BASIC_FLOW__CONCLUSION_STEP = FLOW__CONCLUSION_STEP;

    /**
     * The feature id for the '<em><b>Post Condition</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int BASIC_FLOW__POST_CONDITION = FLOW__POST_CONDITION;

    /**
     * The number of structural features of the '<em>Basic Flow</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int BASIC_FLOW_FEATURE_COUNT = FLOW_FEATURE_COUNT + 0;

    /**
     * The number of operations of the '<em>Basic Flow</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int BASIC_FLOW_OPERATION_COUNT = FLOW_OPERATION_COUNT + 0;

    /**
     * The meta object id for the '{@link ca.mcgill.sel.usecases.impl.CommunicationStepImpl <em>Communication Step</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see ca.mcgill.sel.usecases.impl.CommunicationStepImpl
     * @see ca.mcgill.sel.usecases.impl.UcPackageImpl#getCommunicationStep()
     * @generated
     */
    int COMMUNICATION_STEP = 14;

    /**
     * The feature id for the '<em><b>Step Text</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int COMMUNICATION_STEP__STEP_TEXT = STEP__STEP_TEXT;

    /**
     * The feature id for the '<em><b>Actor</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int COMMUNICATION_STEP__ACTOR = STEP__ACTOR;

    /**
     * The feature id for the '<em><b>Direction</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int COMMUNICATION_STEP__DIRECTION = STEP_FEATURE_COUNT + 0;

    /**
     * The number of structural features of the '<em>Communication Step</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int COMMUNICATION_STEP_FEATURE_COUNT = STEP_FEATURE_COUNT + 1;

    /**
     * The number of operations of the '<em>Communication Step</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int COMMUNICATION_STEP_OPERATION_COUNT = STEP_OPERATION_COUNT + 0;

    /**
     * The meta object id for the '{@link ca.mcgill.sel.usecases.impl.UseCaseReferenceStepImpl <em>Use Case Reference Step</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see ca.mcgill.sel.usecases.impl.UseCaseReferenceStepImpl
     * @see ca.mcgill.sel.usecases.impl.UcPackageImpl#getUseCaseReferenceStep()
     * @generated
     */
    int USE_CASE_REFERENCE_STEP = 15;

    /**
     * The feature id for the '<em><b>Step Text</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int USE_CASE_REFERENCE_STEP__STEP_TEXT = STEP__STEP_TEXT;

    /**
     * The feature id for the '<em><b>Actor</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int USE_CASE_REFERENCE_STEP__ACTOR = STEP__ACTOR;

    /**
     * The feature id for the '<em><b>Usecase</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int USE_CASE_REFERENCE_STEP__USECASE = STEP_FEATURE_COUNT + 0;

    /**
     * The number of structural features of the '<em>Use Case Reference Step</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int USE_CASE_REFERENCE_STEP_FEATURE_COUNT = STEP_FEATURE_COUNT + 1;

    /**
     * The number of operations of the '<em>Use Case Reference Step</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int USE_CASE_REFERENCE_STEP_OPERATION_COUNT = STEP_OPERATION_COUNT + 0;

    /**
     * The meta object id for the '{@link ca.mcgill.sel.usecases.impl.ContextStepImpl <em>Context Step</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see ca.mcgill.sel.usecases.impl.ContextStepImpl
     * @see ca.mcgill.sel.usecases.impl.UcPackageImpl#getContextStep()
     * @generated
     */
    int CONTEXT_STEP = 16;

    /**
     * The feature id for the '<em><b>Step Text</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int CONTEXT_STEP__STEP_TEXT = STEP__STEP_TEXT;

    /**
     * The feature id for the '<em><b>Actor</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int CONTEXT_STEP__ACTOR = STEP__ACTOR;

    /**
     * The feature id for the '<em><b>Type</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int CONTEXT_STEP__TYPE = STEP_FEATURE_COUNT + 0;

    /**
     * The number of structural features of the '<em>Context Step</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int CONTEXT_STEP_FEATURE_COUNT = STEP_FEATURE_COUNT + 1;

    /**
     * The number of operations of the '<em>Context Step</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int CONTEXT_STEP_OPERATION_COUNT = STEP_OPERATION_COUNT + 0;

    /**
     * The meta object id for the '{@link ca.mcgill.sel.usecases.impl.ConditionImpl <em>Condition</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see ca.mcgill.sel.usecases.impl.ConditionImpl
     * @see ca.mcgill.sel.usecases.impl.UcPackageImpl#getCondition()
     * @generated
     */
    int CONDITION = 17;

    /**
     * The feature id for the '<em><b>Text</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int CONDITION__TEXT = 0;

    /**
     * The number of structural features of the '<em>Condition</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int CONDITION_FEATURE_COUNT = 1;

    /**
     * The number of operations of the '<em>Condition</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int CONDITION_OPERATION_COUNT = 0;

    /**
     * The meta object id for the '{@link ca.mcgill.sel.usecases.impl.ExtensionPointImpl <em>Extension Point</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see ca.mcgill.sel.usecases.impl.ExtensionPointImpl
     * @see ca.mcgill.sel.usecases.impl.UcPackageImpl#getExtensionPoint()
     * @generated
     */
    int EXTENSION_POINT = 18;

    /**
     * The feature id for the '<em><b>Step Text</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int EXTENSION_POINT__STEP_TEXT = STEP__STEP_TEXT;

    /**
     * The feature id for the '<em><b>Actor</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int EXTENSION_POINT__ACTOR = STEP__ACTOR;

    /**
     * The number of structural features of the '<em>Extension Point</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int EXTENSION_POINT_FEATURE_COUNT = STEP_FEATURE_COUNT + 0;

    /**
     * The number of operations of the '<em>Extension Point</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int EXTENSION_POINT_OPERATION_COUNT = STEP_OPERATION_COUNT + 0;

    /**
     * The meta object id for the '{@link ca.mcgill.sel.usecases.Level <em>Level</em>}' enum.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see ca.mcgill.sel.usecases.Level
     * @see ca.mcgill.sel.usecases.impl.UcPackageImpl#getLevel()
     * @generated
     */
    int LEVEL = 19;

    /**
     * The meta object id for the '{@link ca.mcgill.sel.usecases.Direction <em>Direction</em>}' enum.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see ca.mcgill.sel.usecases.Direction
     * @see ca.mcgill.sel.usecases.impl.UcPackageImpl#getDirection()
     * @generated
     */
    int DIRECTION = 20;

    /**
     * The meta object id for the '{@link ca.mcgill.sel.usecases.ContextType <em>Context Type</em>}' enum.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see ca.mcgill.sel.usecases.ContextType
     * @see ca.mcgill.sel.usecases.impl.UcPackageImpl#getContextType()
     * @generated
     */
    int CONTEXT_TYPE = 21;


    /**
     * The meta object id for the '{@link ca.mcgill.sel.usecases.ConclusionType <em>Conclusion Type</em>}' enum.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see ca.mcgill.sel.usecases.ConclusionType
     * @see ca.mcgill.sel.usecases.impl.UcPackageImpl#getConclusionType()
     * @generated
     */
    int CONCLUSION_TYPE = 22;


    /**
     * Returns the meta object for class '{@link ca.mcgill.sel.usecases.Actor <em>Actor</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>Actor</em>'.
     * @see ca.mcgill.sel.usecases.Actor
     * @generated
     */
    EClass getActor();

    /**
     * Returns the meta object for the attribute '{@link ca.mcgill.sel.usecases.Actor#getLowerBound <em>Lower Bound</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Lower Bound</em>'.
     * @see ca.mcgill.sel.usecases.Actor#getLowerBound()
     * @see #getActor()
     * @generated
     */
    EAttribute getActor_LowerBound();

    /**
     * Returns the meta object for the attribute '{@link ca.mcgill.sel.usecases.Actor#getUpperBound <em>Upper Bound</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Upper Bound</em>'.
     * @see ca.mcgill.sel.usecases.Actor#getUpperBound()
     * @see #getActor()
     * @generated
     */
    EAttribute getActor_UpperBound();

    /**
     * Returns the meta object for the reference '{@link ca.mcgill.sel.usecases.Actor#getGeneralization <em>Generalization</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the reference '<em>Generalization</em>'.
     * @see ca.mcgill.sel.usecases.Actor#getGeneralization()
     * @see #getActor()
     * @generated
     */
    EReference getActor_Generalization();

    /**
     * Returns the meta object for class '{@link ca.mcgill.sel.usecases.NamedElement <em>Named Element</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>Named Element</em>'.
     * @see ca.mcgill.sel.usecases.NamedElement
     * @generated
     */
    EClass getNamedElement();

    /**
     * Returns the meta object for the attribute '{@link ca.mcgill.sel.usecases.NamedElement#getName <em>Name</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Name</em>'.
     * @see ca.mcgill.sel.usecases.NamedElement#getName()
     * @see #getNamedElement()
     * @generated
     */
    EAttribute getNamedElement_Name();

    /**
     * Returns the meta object for class '{@link ca.mcgill.sel.usecases.UseCase <em>Use Case</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>Use Case</em>'.
     * @see ca.mcgill.sel.usecases.UseCase
     * @generated
     */
    EClass getUseCase();

    /**
     * Returns the meta object for the reference '{@link ca.mcgill.sel.usecases.UseCase#getPrimaryActor <em>Primary Actor</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the reference '<em>Primary Actor</em>'.
     * @see ca.mcgill.sel.usecases.UseCase#getPrimaryActor()
     * @see #getUseCase()
     * @generated
     */
    EReference getUseCase_PrimaryActor();

    /**
     * Returns the meta object for the reference list '{@link ca.mcgill.sel.usecases.UseCase#getSecondaryActors <em>Secondary Actors</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the reference list '<em>Secondary Actors</em>'.
     * @see ca.mcgill.sel.usecases.UseCase#getSecondaryActors()
     * @see #getUseCase()
     * @generated
     */
    EReference getUseCase_SecondaryActors();

    /**
     * Returns the meta object for the attribute '{@link ca.mcgill.sel.usecases.UseCase#getIntention <em>Intention</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Intention</em>'.
     * @see ca.mcgill.sel.usecases.UseCase#getIntention()
     * @see #getUseCase()
     * @generated
     */
    EAttribute getUseCase_Intention();

    /**
     * Returns the meta object for the attribute '{@link ca.mcgill.sel.usecases.UseCase#getMultiplicity <em>Multiplicity</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Multiplicity</em>'.
     * @see ca.mcgill.sel.usecases.UseCase#getMultiplicity()
     * @see #getUseCase()
     * @generated
     */
    EAttribute getUseCase_Multiplicity();

    /**
     * Returns the meta object for the attribute '{@link ca.mcgill.sel.usecases.UseCase#getLevel <em>Level</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Level</em>'.
     * @see ca.mcgill.sel.usecases.UseCase#getLevel()
     * @see #getUseCase()
     * @generated
     */
    EAttribute getUseCase_Level();

    /**
     * Returns the meta object for the containment reference '{@link ca.mcgill.sel.usecases.UseCase#getMainSuccessScenario <em>Main Success Scenario</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the containment reference '<em>Main Success Scenario</em>'.
     * @see ca.mcgill.sel.usecases.UseCase#getMainSuccessScenario()
     * @see #getUseCase()
     * @generated
     */
    EReference getUseCase_MainSuccessScenario();

    /**
     * Returns the meta object for the reference '{@link ca.mcgill.sel.usecases.UseCase#getGeneralization <em>Generalization</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the reference '<em>Generalization</em>'.
     * @see ca.mcgill.sel.usecases.UseCase#getGeneralization()
     * @see #getUseCase()
     * @generated
     */
    EReference getUseCase_Generalization();

    /**
     * Returns the meta object for the reference list '{@link ca.mcgill.sel.usecases.UseCase#getIncludedUseCases <em>Included Use Cases</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the reference list '<em>Included Use Cases</em>'.
     * @see ca.mcgill.sel.usecases.UseCase#getIncludedUseCases()
     * @see #getUseCase()
     * @generated
     */
    EReference getUseCase_IncludedUseCases();

    /**
     * Returns the meta object for the reference '{@link ca.mcgill.sel.usecases.UseCase#getExtendedUseCase <em>Extended Use Case</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the reference '<em>Extended Use Case</em>'.
     * @see ca.mcgill.sel.usecases.UseCase#getExtendedUseCase()
     * @see #getUseCase()
     * @generated
     */
    EReference getUseCase_ExtendedUseCase();

    /**
     * Returns the meta object for the containment reference list '{@link ca.mcgill.sel.usecases.UseCase#getConditions <em>Conditions</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the containment reference list '<em>Conditions</em>'.
     * @see ca.mcgill.sel.usecases.UseCase#getConditions()
     * @see #getUseCase()
     * @generated
     */
    EReference getUseCase_Conditions();

    /**
     * Returns the meta object for class '{@link ca.mcgill.sel.usecases.Layout <em>Layout</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>Layout</em>'.
     * @see ca.mcgill.sel.usecases.Layout
     * @generated
     */
    EClass getLayout();

    /**
     * Returns the meta object for the map '{@link ca.mcgill.sel.usecases.Layout#getContainers <em>Containers</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the map '<em>Containers</em>'.
     * @see ca.mcgill.sel.usecases.Layout#getContainers()
     * @see #getLayout()
     * @generated
     */
    EReference getLayout_Containers();

    /**
     * Returns the meta object for class '{@link java.util.Map.Entry <em>Container Map</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>Container Map</em>'.
     * @see java.util.Map.Entry
     * @model keyType="org.eclipse.emf.ecore.EObject" keyRequired="true"
     *        valueMapType="ca.mcgill.sel.usecases.ElementMap&lt;org.eclipse.emf.ecore.EObject, ca.mcgill.sel.usecases.LayoutElement&gt;"
     * @generated
     */
    EClass getContainerMap();

    /**
     * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Key</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the reference '<em>Key</em>'.
     * @see java.util.Map.Entry
     * @see #getContainerMap()
     * @generated
     */
    EReference getContainerMap_Key();

    /**
     * Returns the meta object for the map '{@link java.util.Map.Entry <em>Value</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the map '<em>Value</em>'.
     * @see java.util.Map.Entry
     * @see #getContainerMap()
     * @generated
     */
    EReference getContainerMap_Value();

    /**
     * Returns the meta object for class '{@link java.util.Map.Entry <em>Element Map</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>Element Map</em>'.
     * @see java.util.Map.Entry
     * @model keyType="org.eclipse.emf.ecore.EObject" keyRequired="true"
     *        valueType="ca.mcgill.sel.usecases.LayoutElement" valueContainment="true" valueRequired="true"
     * @generated
     */
    EClass getElementMap();

    /**
     * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Key</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the reference '<em>Key</em>'.
     * @see java.util.Map.Entry
     * @see #getElementMap()
     * @generated
     */
    EReference getElementMap_Key();

    /**
     * Returns the meta object for the containment reference '{@link java.util.Map.Entry <em>Value</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the containment reference '<em>Value</em>'.
     * @see java.util.Map.Entry
     * @see #getElementMap()
     * @generated
     */
    EReference getElementMap_Value();

    /**
     * Returns the meta object for class '{@link ca.mcgill.sel.usecases.LayoutElement <em>Layout Element</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>Layout Element</em>'.
     * @see ca.mcgill.sel.usecases.LayoutElement
     * @generated
     */
    EClass getLayoutElement();

    /**
     * Returns the meta object for the attribute '{@link ca.mcgill.sel.usecases.LayoutElement#getX <em>X</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>X</em>'.
     * @see ca.mcgill.sel.usecases.LayoutElement#getX()
     * @see #getLayoutElement()
     * @generated
     */
    EAttribute getLayoutElement_X();

    /**
     * Returns the meta object for the attribute '{@link ca.mcgill.sel.usecases.LayoutElement#getY <em>Y</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Y</em>'.
     * @see ca.mcgill.sel.usecases.LayoutElement#getY()
     * @see #getLayoutElement()
     * @generated
     */
    EAttribute getLayoutElement_Y();

    /**
     * Returns the meta object for class '{@link ca.mcgill.sel.usecases.UseCaseModel <em>Use Case Model</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>Use Case Model</em>'.
     * @see ca.mcgill.sel.usecases.UseCaseModel
     * @generated
     */
    EClass getUseCaseModel();

    /**
     * Returns the meta object for the containment reference '{@link ca.mcgill.sel.usecases.UseCaseModel#getLayout <em>Layout</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the containment reference '<em>Layout</em>'.
     * @see ca.mcgill.sel.usecases.UseCaseModel#getLayout()
     * @see #getUseCaseModel()
     * @generated
     */
    EReference getUseCaseModel_Layout();

    /**
     * Returns the meta object for the containment reference list '{@link ca.mcgill.sel.usecases.UseCaseModel#getActors <em>Actors</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the containment reference list '<em>Actors</em>'.
     * @see ca.mcgill.sel.usecases.UseCaseModel#getActors()
     * @see #getUseCaseModel()
     * @generated
     */
    EReference getUseCaseModel_Actors();

    /**
     * Returns the meta object for the containment reference list '{@link ca.mcgill.sel.usecases.UseCaseModel#getUseCases <em>Use Cases</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the containment reference list '<em>Use Cases</em>'.
     * @see ca.mcgill.sel.usecases.UseCaseModel#getUseCases()
     * @see #getUseCaseModel()
     * @generated
     */
    EReference getUseCaseModel_UseCases();

    /**
     * Returns the meta object for the containment reference list '{@link ca.mcgill.sel.usecases.UseCaseModel#getNotes <em>Notes</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the containment reference list '<em>Notes</em>'.
     * @see ca.mcgill.sel.usecases.UseCaseModel#getNotes()
     * @see #getUseCaseModel()
     * @generated
     */
    EReference getUseCaseModel_Notes();

    /**
     * Returns the meta object for the attribute '{@link ca.mcgill.sel.usecases.UseCaseModel#getSystemName <em>System Name</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>System Name</em>'.
     * @see ca.mcgill.sel.usecases.UseCaseModel#getSystemName()
     * @see #getUseCaseModel()
     * @generated
     */
    EAttribute getUseCaseModel_SystemName();

    /**
     * Returns the meta object for class '{@link ca.mcgill.sel.usecases.Note <em>Note</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>Note</em>'.
     * @see ca.mcgill.sel.usecases.Note
     * @generated
     */
    EClass getNote();

    /**
     * Returns the meta object for the reference list '{@link ca.mcgill.sel.usecases.Note#getNotedElement <em>Noted Element</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the reference list '<em>Noted Element</em>'.
     * @see ca.mcgill.sel.usecases.Note#getNotedElement()
     * @see #getNote()
     * @generated
     */
    EReference getNote_NotedElement();

    /**
     * Returns the meta object for the attribute '{@link ca.mcgill.sel.usecases.Note#getContent <em>Content</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Content</em>'.
     * @see ca.mcgill.sel.usecases.Note#getContent()
     * @see #getNote()
     * @generated
     */
    EAttribute getNote_Content();

    /**
     * Returns the meta object for class '{@link ca.mcgill.sel.usecases.Step <em>Step</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>Step</em>'.
     * @see ca.mcgill.sel.usecases.Step
     * @generated
     */
    EClass getStep();

    /**
     * Returns the meta object for the attribute '{@link ca.mcgill.sel.usecases.Step#getStepText <em>Step Text</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Step Text</em>'.
     * @see ca.mcgill.sel.usecases.Step#getStepText()
     * @see #getStep()
     * @generated
     */
    EAttribute getStep_StepText();

    /**
     * Returns the meta object for the reference '{@link ca.mcgill.sel.usecases.Step#getActor <em>Actor</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the reference '<em>Actor</em>'.
     * @see ca.mcgill.sel.usecases.Step#getActor()
     * @see #getStep()
     * @generated
     */
    EReference getStep_Actor();

    /**
     * Returns the meta object for class '{@link ca.mcgill.sel.usecases.Flow <em>Flow</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>Flow</em>'.
     * @see ca.mcgill.sel.usecases.Flow
     * @generated
     */
    EClass getFlow();

    /**
     * Returns the meta object for the containment reference list '{@link ca.mcgill.sel.usecases.Flow#getSteps <em>Steps</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the containment reference list '<em>Steps</em>'.
     * @see ca.mcgill.sel.usecases.Flow#getSteps()
     * @see #getFlow()
     * @generated
     */
    EReference getFlow_Steps();

    /**
     * Returns the meta object for the attribute '{@link ca.mcgill.sel.usecases.Flow#getPrecondition <em>Precondition</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Precondition</em>'.
     * @see ca.mcgill.sel.usecases.Flow#getPrecondition()
     * @see #getFlow()
     * @generated
     */
    EAttribute getFlow_Precondition();

    /**
     * Returns the meta object for the containment reference list '{@link ca.mcgill.sel.usecases.Flow#getAlternateFlows <em>Alternate Flows</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the containment reference list '<em>Alternate Flows</em>'.
     * @see ca.mcgill.sel.usecases.Flow#getAlternateFlows()
     * @see #getFlow()
     * @generated
     */
    EReference getFlow_AlternateFlows();

    /**
     * Returns the meta object for the attribute '{@link ca.mcgill.sel.usecases.Flow#getConclusionType <em>Conclusion Type</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Conclusion Type</em>'.
     * @see ca.mcgill.sel.usecases.Flow#getConclusionType()
     * @see #getFlow()
     * @generated
     */
    EAttribute getFlow_ConclusionType();

    /**
     * Returns the meta object for the reference '{@link ca.mcgill.sel.usecases.Flow#getConclusionStep <em>Conclusion Step</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the reference '<em>Conclusion Step</em>'.
     * @see ca.mcgill.sel.usecases.Flow#getConclusionStep()
     * @see #getFlow()
     * @generated
     */
    EReference getFlow_ConclusionStep();

    /**
     * Returns the meta object for the reference '{@link ca.mcgill.sel.usecases.Flow#getPostCondition <em>Post Condition</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the reference '<em>Post Condition</em>'.
     * @see ca.mcgill.sel.usecases.Flow#getPostCondition()
     * @see #getFlow()
     * @generated
     */
    EReference getFlow_PostCondition();

    /**
     * Returns the meta object for class '{@link ca.mcgill.sel.usecases.BoundedFlow <em>Bounded Flow</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>Bounded Flow</em>'.
     * @see ca.mcgill.sel.usecases.BoundedFlow
     * @generated
     */
    EClass getBoundedFlow();

    /**
     * Returns the meta object for the reference list '{@link ca.mcgill.sel.usecases.BoundedFlow#getReferencedSteps <em>Referenced Steps</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the reference list '<em>Referenced Steps</em>'.
     * @see ca.mcgill.sel.usecases.BoundedFlow#getReferencedSteps()
     * @see #getBoundedFlow()
     * @generated
     */
    EReference getBoundedFlow_ReferencedSteps();

    /**
     * Returns the meta object for class '{@link ca.mcgill.sel.usecases.SpecificFlow <em>Specific Flow</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>Specific Flow</em>'.
     * @see ca.mcgill.sel.usecases.SpecificFlow
     * @generated
     */
    EClass getSpecificFlow();

    /**
     * Returns the meta object for the reference '{@link ca.mcgill.sel.usecases.SpecificFlow#getReferencedStep <em>Referenced Step</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the reference '<em>Referenced Step</em>'.
     * @see ca.mcgill.sel.usecases.SpecificFlow#getReferencedStep()
     * @see #getSpecificFlow()
     * @generated
     */
    EReference getSpecificFlow_ReferencedStep();

    /**
     * Returns the meta object for class '{@link ca.mcgill.sel.usecases.BasicFlow <em>Basic Flow</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>Basic Flow</em>'.
     * @see ca.mcgill.sel.usecases.BasicFlow
     * @generated
     */
    EClass getBasicFlow();

    /**
     * Returns the meta object for class '{@link ca.mcgill.sel.usecases.CommunicationStep <em>Communication Step</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>Communication Step</em>'.
     * @see ca.mcgill.sel.usecases.CommunicationStep
     * @generated
     */
    EClass getCommunicationStep();

    /**
     * Returns the meta object for the attribute '{@link ca.mcgill.sel.usecases.CommunicationStep#getDirection <em>Direction</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Direction</em>'.
     * @see ca.mcgill.sel.usecases.CommunicationStep#getDirection()
     * @see #getCommunicationStep()
     * @generated
     */
    EAttribute getCommunicationStep_Direction();

    /**
     * Returns the meta object for class '{@link ca.mcgill.sel.usecases.UseCaseReferenceStep <em>Use Case Reference Step</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>Use Case Reference Step</em>'.
     * @see ca.mcgill.sel.usecases.UseCaseReferenceStep
     * @generated
     */
    EClass getUseCaseReferenceStep();

    /**
     * Returns the meta object for the reference '{@link ca.mcgill.sel.usecases.UseCaseReferenceStep#getUsecase <em>Usecase</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the reference '<em>Usecase</em>'.
     * @see ca.mcgill.sel.usecases.UseCaseReferenceStep#getUsecase()
     * @see #getUseCaseReferenceStep()
     * @generated
     */
    EReference getUseCaseReferenceStep_Usecase();

    /**
     * Returns the meta object for class '{@link ca.mcgill.sel.usecases.ContextStep <em>Context Step</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>Context Step</em>'.
     * @see ca.mcgill.sel.usecases.ContextStep
     * @generated
     */
    EClass getContextStep();

    /**
     * Returns the meta object for the attribute '{@link ca.mcgill.sel.usecases.ContextStep#getType <em>Type</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Type</em>'.
     * @see ca.mcgill.sel.usecases.ContextStep#getType()
     * @see #getContextStep()
     * @generated
     */
    EAttribute getContextStep_Type();

    /**
     * Returns the meta object for class '{@link ca.mcgill.sel.usecases.Condition <em>Condition</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>Condition</em>'.
     * @see ca.mcgill.sel.usecases.Condition
     * @generated
     */
    EClass getCondition();

    /**
     * Returns the meta object for the attribute '{@link ca.mcgill.sel.usecases.Condition#getText <em>Text</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Text</em>'.
     * @see ca.mcgill.sel.usecases.Condition#getText()
     * @see #getCondition()
     * @generated
     */
    EAttribute getCondition_Text();

    /**
     * Returns the meta object for class '{@link ca.mcgill.sel.usecases.ExtensionPoint <em>Extension Point</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>Extension Point</em>'.
     * @see ca.mcgill.sel.usecases.ExtensionPoint
     * @generated
     */
    EClass getExtensionPoint();

    /**
     * Returns the meta object for enum '{@link ca.mcgill.sel.usecases.Level <em>Level</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for enum '<em>Level</em>'.
     * @see ca.mcgill.sel.usecases.Level
     * @generated
     */
    EEnum getLevel();

    /**
     * Returns the meta object for enum '{@link ca.mcgill.sel.usecases.Direction <em>Direction</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for enum '<em>Direction</em>'.
     * @see ca.mcgill.sel.usecases.Direction
     * @generated
     */
    EEnum getDirection();

    /**
     * Returns the meta object for enum '{@link ca.mcgill.sel.usecases.ContextType <em>Context Type</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for enum '<em>Context Type</em>'.
     * @see ca.mcgill.sel.usecases.ContextType
     * @generated
     */
    EEnum getContextType();

    /**
     * Returns the meta object for enum '{@link ca.mcgill.sel.usecases.ConclusionType <em>Conclusion Type</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for enum '<em>Conclusion Type</em>'.
     * @see ca.mcgill.sel.usecases.ConclusionType
     * @generated
     */
    EEnum getConclusionType();

    /**
     * Returns the factory that creates the instances of the model.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the factory that creates the instances of the model.
     * @generated
     */
    UcFactory getUcFactory();

    /**
     * <!-- begin-user-doc -->
     * Defines literals for the meta objects that represent
     * <ul>
     *   <li>each class,</li>
     *   <li>each feature of each class,</li>
     *   <li>each operation of each class,</li>
     *   <li>each enum,</li>
     *   <li>and each data type</li>
     * </ul>
     * <!-- end-user-doc -->
     * @generated
     */
    interface Literals {
        /**
         * The meta object literal for the '{@link ca.mcgill.sel.usecases.impl.ActorImpl <em>Actor</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see ca.mcgill.sel.usecases.impl.ActorImpl
         * @see ca.mcgill.sel.usecases.impl.UcPackageImpl#getActor()
         * @generated
         */
        EClass ACTOR = eINSTANCE.getActor();

        /**
         * The meta object literal for the '<em><b>Lower Bound</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EAttribute ACTOR__LOWER_BOUND = eINSTANCE.getActor_LowerBound();

        /**
         * The meta object literal for the '<em><b>Upper Bound</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EAttribute ACTOR__UPPER_BOUND = eINSTANCE.getActor_UpperBound();

        /**
         * The meta object literal for the '<em><b>Generalization</b></em>' reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference ACTOR__GENERALIZATION = eINSTANCE.getActor_Generalization();

        /**
         * The meta object literal for the '{@link ca.mcgill.sel.usecases.impl.NamedElementImpl <em>Named Element</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see ca.mcgill.sel.usecases.impl.NamedElementImpl
         * @see ca.mcgill.sel.usecases.impl.UcPackageImpl#getNamedElement()
         * @generated
         */
        EClass NAMED_ELEMENT = eINSTANCE.getNamedElement();

        /**
         * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EAttribute NAMED_ELEMENT__NAME = eINSTANCE.getNamedElement_Name();

        /**
         * The meta object literal for the '{@link ca.mcgill.sel.usecases.impl.UseCaseImpl <em>Use Case</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see ca.mcgill.sel.usecases.impl.UseCaseImpl
         * @see ca.mcgill.sel.usecases.impl.UcPackageImpl#getUseCase()
         * @generated
         */
        EClass USE_CASE = eINSTANCE.getUseCase();

        /**
         * The meta object literal for the '<em><b>Primary Actor</b></em>' reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference USE_CASE__PRIMARY_ACTOR = eINSTANCE.getUseCase_PrimaryActor();

        /**
         * The meta object literal for the '<em><b>Secondary Actors</b></em>' reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference USE_CASE__SECONDARY_ACTORS = eINSTANCE.getUseCase_SecondaryActors();

        /**
         * The meta object literal for the '<em><b>Intention</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EAttribute USE_CASE__INTENTION = eINSTANCE.getUseCase_Intention();

        /**
         * The meta object literal for the '<em><b>Multiplicity</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EAttribute USE_CASE__MULTIPLICITY = eINSTANCE.getUseCase_Multiplicity();

        /**
         * The meta object literal for the '<em><b>Level</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EAttribute USE_CASE__LEVEL = eINSTANCE.getUseCase_Level();

        /**
         * The meta object literal for the '<em><b>Main Success Scenario</b></em>' containment reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference USE_CASE__MAIN_SUCCESS_SCENARIO = eINSTANCE.getUseCase_MainSuccessScenario();

        /**
         * The meta object literal for the '<em><b>Generalization</b></em>' reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference USE_CASE__GENERALIZATION = eINSTANCE.getUseCase_Generalization();

        /**
         * The meta object literal for the '<em><b>Included Use Cases</b></em>' reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference USE_CASE__INCLUDED_USE_CASES = eINSTANCE.getUseCase_IncludedUseCases();

        /**
         * The meta object literal for the '<em><b>Extended Use Case</b></em>' reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference USE_CASE__EXTENDED_USE_CASE = eINSTANCE.getUseCase_ExtendedUseCase();

        /**
         * The meta object literal for the '<em><b>Conditions</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference USE_CASE__CONDITIONS = eINSTANCE.getUseCase_Conditions();

        /**
         * The meta object literal for the '{@link ca.mcgill.sel.usecases.impl.LayoutImpl <em>Layout</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see ca.mcgill.sel.usecases.impl.LayoutImpl
         * @see ca.mcgill.sel.usecases.impl.UcPackageImpl#getLayout()
         * @generated
         */
        EClass LAYOUT = eINSTANCE.getLayout();

        /**
         * The meta object literal for the '<em><b>Containers</b></em>' map feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference LAYOUT__CONTAINERS = eINSTANCE.getLayout_Containers();

        /**
         * The meta object literal for the '{@link ca.mcgill.sel.usecases.impl.ContainerMapImpl <em>Container Map</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see ca.mcgill.sel.usecases.impl.ContainerMapImpl
         * @see ca.mcgill.sel.usecases.impl.UcPackageImpl#getContainerMap()
         * @generated
         */
        EClass CONTAINER_MAP = eINSTANCE.getContainerMap();

        /**
         * The meta object literal for the '<em><b>Key</b></em>' reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference CONTAINER_MAP__KEY = eINSTANCE.getContainerMap_Key();

        /**
         * The meta object literal for the '<em><b>Value</b></em>' map feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference CONTAINER_MAP__VALUE = eINSTANCE.getContainerMap_Value();

        /**
         * The meta object literal for the '{@link ca.mcgill.sel.usecases.impl.ElementMapImpl <em>Element Map</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see ca.mcgill.sel.usecases.impl.ElementMapImpl
         * @see ca.mcgill.sel.usecases.impl.UcPackageImpl#getElementMap()
         * @generated
         */
        EClass ELEMENT_MAP = eINSTANCE.getElementMap();

        /**
         * The meta object literal for the '<em><b>Key</b></em>' reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference ELEMENT_MAP__KEY = eINSTANCE.getElementMap_Key();

        /**
         * The meta object literal for the '<em><b>Value</b></em>' containment reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference ELEMENT_MAP__VALUE = eINSTANCE.getElementMap_Value();

        /**
         * The meta object literal for the '{@link ca.mcgill.sel.usecases.impl.LayoutElementImpl <em>Layout Element</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see ca.mcgill.sel.usecases.impl.LayoutElementImpl
         * @see ca.mcgill.sel.usecases.impl.UcPackageImpl#getLayoutElement()
         * @generated
         */
        EClass LAYOUT_ELEMENT = eINSTANCE.getLayoutElement();

        /**
         * The meta object literal for the '<em><b>X</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EAttribute LAYOUT_ELEMENT__X = eINSTANCE.getLayoutElement_X();

        /**
         * The meta object literal for the '<em><b>Y</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EAttribute LAYOUT_ELEMENT__Y = eINSTANCE.getLayoutElement_Y();

        /**
         * The meta object literal for the '{@link ca.mcgill.sel.usecases.impl.UseCaseModelImpl <em>Use Case Model</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see ca.mcgill.sel.usecases.impl.UseCaseModelImpl
         * @see ca.mcgill.sel.usecases.impl.UcPackageImpl#getUseCaseModel()
         * @generated
         */
        EClass USE_CASE_MODEL = eINSTANCE.getUseCaseModel();

        /**
         * The meta object literal for the '<em><b>Layout</b></em>' containment reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference USE_CASE_MODEL__LAYOUT = eINSTANCE.getUseCaseModel_Layout();

        /**
         * The meta object literal for the '<em><b>Actors</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference USE_CASE_MODEL__ACTORS = eINSTANCE.getUseCaseModel_Actors();

        /**
         * The meta object literal for the '<em><b>Use Cases</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference USE_CASE_MODEL__USE_CASES = eINSTANCE.getUseCaseModel_UseCases();

        /**
         * The meta object literal for the '<em><b>Notes</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference USE_CASE_MODEL__NOTES = eINSTANCE.getUseCaseModel_Notes();

        /**
         * The meta object literal for the '<em><b>System Name</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EAttribute USE_CASE_MODEL__SYSTEM_NAME = eINSTANCE.getUseCaseModel_SystemName();

        /**
         * The meta object literal for the '{@link ca.mcgill.sel.usecases.impl.NoteImpl <em>Note</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see ca.mcgill.sel.usecases.impl.NoteImpl
         * @see ca.mcgill.sel.usecases.impl.UcPackageImpl#getNote()
         * @generated
         */
        EClass NOTE = eINSTANCE.getNote();

        /**
         * The meta object literal for the '<em><b>Noted Element</b></em>' reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference NOTE__NOTED_ELEMENT = eINSTANCE.getNote_NotedElement();

        /**
         * The meta object literal for the '<em><b>Content</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EAttribute NOTE__CONTENT = eINSTANCE.getNote_Content();

        /**
         * The meta object literal for the '{@link ca.mcgill.sel.usecases.impl.StepImpl <em>Step</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see ca.mcgill.sel.usecases.impl.StepImpl
         * @see ca.mcgill.sel.usecases.impl.UcPackageImpl#getStep()
         * @generated
         */
        EClass STEP = eINSTANCE.getStep();

        /**
         * The meta object literal for the '<em><b>Step Text</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EAttribute STEP__STEP_TEXT = eINSTANCE.getStep_StepText();

        /**
         * The meta object literal for the '<em><b>Actor</b></em>' reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference STEP__ACTOR = eINSTANCE.getStep_Actor();

        /**
         * The meta object literal for the '{@link ca.mcgill.sel.usecases.impl.FlowImpl <em>Flow</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see ca.mcgill.sel.usecases.impl.FlowImpl
         * @see ca.mcgill.sel.usecases.impl.UcPackageImpl#getFlow()
         * @generated
         */
        EClass FLOW = eINSTANCE.getFlow();

        /**
         * The meta object literal for the '<em><b>Steps</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference FLOW__STEPS = eINSTANCE.getFlow_Steps();

        /**
         * The meta object literal for the '<em><b>Precondition</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EAttribute FLOW__PRECONDITION = eINSTANCE.getFlow_Precondition();

        /**
         * The meta object literal for the '<em><b>Alternate Flows</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference FLOW__ALTERNATE_FLOWS = eINSTANCE.getFlow_AlternateFlows();

        /**
         * The meta object literal for the '<em><b>Conclusion Type</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EAttribute FLOW__CONCLUSION_TYPE = eINSTANCE.getFlow_ConclusionType();

        /**
         * The meta object literal for the '<em><b>Conclusion Step</b></em>' reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference FLOW__CONCLUSION_STEP = eINSTANCE.getFlow_ConclusionStep();

        /**
         * The meta object literal for the '<em><b>Post Condition</b></em>' reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference FLOW__POST_CONDITION = eINSTANCE.getFlow_PostCondition();

        /**
         * The meta object literal for the '{@link ca.mcgill.sel.usecases.impl.BoundedFlowImpl <em>Bounded Flow</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see ca.mcgill.sel.usecases.impl.BoundedFlowImpl
         * @see ca.mcgill.sel.usecases.impl.UcPackageImpl#getBoundedFlow()
         * @generated
         */
        EClass BOUNDED_FLOW = eINSTANCE.getBoundedFlow();

        /**
         * The meta object literal for the '<em><b>Referenced Steps</b></em>' reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference BOUNDED_FLOW__REFERENCED_STEPS = eINSTANCE.getBoundedFlow_ReferencedSteps();

        /**
         * The meta object literal for the '{@link ca.mcgill.sel.usecases.impl.SpecificFlowImpl <em>Specific Flow</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see ca.mcgill.sel.usecases.impl.SpecificFlowImpl
         * @see ca.mcgill.sel.usecases.impl.UcPackageImpl#getSpecificFlow()
         * @generated
         */
        EClass SPECIFIC_FLOW = eINSTANCE.getSpecificFlow();

        /**
         * The meta object literal for the '<em><b>Referenced Step</b></em>' reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference SPECIFIC_FLOW__REFERENCED_STEP = eINSTANCE.getSpecificFlow_ReferencedStep();

        /**
         * The meta object literal for the '{@link ca.mcgill.sel.usecases.impl.BasicFlowImpl <em>Basic Flow</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see ca.mcgill.sel.usecases.impl.BasicFlowImpl
         * @see ca.mcgill.sel.usecases.impl.UcPackageImpl#getBasicFlow()
         * @generated
         */
        EClass BASIC_FLOW = eINSTANCE.getBasicFlow();

        /**
         * The meta object literal for the '{@link ca.mcgill.sel.usecases.impl.CommunicationStepImpl <em>Communication Step</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see ca.mcgill.sel.usecases.impl.CommunicationStepImpl
         * @see ca.mcgill.sel.usecases.impl.UcPackageImpl#getCommunicationStep()
         * @generated
         */
        EClass COMMUNICATION_STEP = eINSTANCE.getCommunicationStep();

        /**
         * The meta object literal for the '<em><b>Direction</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EAttribute COMMUNICATION_STEP__DIRECTION = eINSTANCE.getCommunicationStep_Direction();

        /**
         * The meta object literal for the '{@link ca.mcgill.sel.usecases.impl.UseCaseReferenceStepImpl <em>Use Case Reference Step</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see ca.mcgill.sel.usecases.impl.UseCaseReferenceStepImpl
         * @see ca.mcgill.sel.usecases.impl.UcPackageImpl#getUseCaseReferenceStep()
         * @generated
         */
        EClass USE_CASE_REFERENCE_STEP = eINSTANCE.getUseCaseReferenceStep();

        /**
         * The meta object literal for the '<em><b>Usecase</b></em>' reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference USE_CASE_REFERENCE_STEP__USECASE = eINSTANCE.getUseCaseReferenceStep_Usecase();

        /**
         * The meta object literal for the '{@link ca.mcgill.sel.usecases.impl.ContextStepImpl <em>Context Step</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see ca.mcgill.sel.usecases.impl.ContextStepImpl
         * @see ca.mcgill.sel.usecases.impl.UcPackageImpl#getContextStep()
         * @generated
         */
        EClass CONTEXT_STEP = eINSTANCE.getContextStep();

        /**
         * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EAttribute CONTEXT_STEP__TYPE = eINSTANCE.getContextStep_Type();

        /**
         * The meta object literal for the '{@link ca.mcgill.sel.usecases.impl.ConditionImpl <em>Condition</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see ca.mcgill.sel.usecases.impl.ConditionImpl
         * @see ca.mcgill.sel.usecases.impl.UcPackageImpl#getCondition()
         * @generated
         */
        EClass CONDITION = eINSTANCE.getCondition();

        /**
         * The meta object literal for the '<em><b>Text</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EAttribute CONDITION__TEXT = eINSTANCE.getCondition_Text();

        /**
         * The meta object literal for the '{@link ca.mcgill.sel.usecases.impl.ExtensionPointImpl <em>Extension Point</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see ca.mcgill.sel.usecases.impl.ExtensionPointImpl
         * @see ca.mcgill.sel.usecases.impl.UcPackageImpl#getExtensionPoint()
         * @generated
         */
        EClass EXTENSION_POINT = eINSTANCE.getExtensionPoint();

        /**
         * The meta object literal for the '{@link ca.mcgill.sel.usecases.Level <em>Level</em>}' enum.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see ca.mcgill.sel.usecases.Level
         * @see ca.mcgill.sel.usecases.impl.UcPackageImpl#getLevel()
         * @generated
         */
        EEnum LEVEL = eINSTANCE.getLevel();

        /**
         * The meta object literal for the '{@link ca.mcgill.sel.usecases.Direction <em>Direction</em>}' enum.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see ca.mcgill.sel.usecases.Direction
         * @see ca.mcgill.sel.usecases.impl.UcPackageImpl#getDirection()
         * @generated
         */
        EEnum DIRECTION = eINSTANCE.getDirection();

        /**
         * The meta object literal for the '{@link ca.mcgill.sel.usecases.ContextType <em>Context Type</em>}' enum.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see ca.mcgill.sel.usecases.ContextType
         * @see ca.mcgill.sel.usecases.impl.UcPackageImpl#getContextType()
         * @generated
         */
        EEnum CONTEXT_TYPE = eINSTANCE.getContextType();

        /**
         * The meta object literal for the '{@link ca.mcgill.sel.usecases.ConclusionType <em>Conclusion Type</em>}' enum.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see ca.mcgill.sel.usecases.ConclusionType
         * @see ca.mcgill.sel.usecases.impl.UcPackageImpl#getConclusionType()
         * @generated
         */
        EEnum CONCLUSION_TYPE = eINSTANCE.getConclusionType();

    }

} //UcPackage
