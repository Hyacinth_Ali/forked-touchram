/**
 */
package ca.mcgill.sel.usecases;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Flow</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.usecases.Flow#getSteps <em>Steps</em>}</li>
 *   <li>{@link ca.mcgill.sel.usecases.Flow#getPrecondition <em>Precondition</em>}</li>
 *   <li>{@link ca.mcgill.sel.usecases.Flow#getAlternateFlows <em>Alternate Flows</em>}</li>
 *   <li>{@link ca.mcgill.sel.usecases.Flow#getConclusionType <em>Conclusion Type</em>}</li>
 *   <li>{@link ca.mcgill.sel.usecases.Flow#getConclusionStep <em>Conclusion Step</em>}</li>
 *   <li>{@link ca.mcgill.sel.usecases.Flow#getPostCondition <em>Post Condition</em>}</li>
 * </ul>
 *
 * @see ca.mcgill.sel.usecases.UcPackage#getFlow()
 * @model abstract="true"
 * @generated
 */
public interface Flow extends EObject {
    /**
     * Returns the value of the '<em><b>Steps</b></em>' containment reference list.
     * The list contents are of type {@link ca.mcgill.sel.usecases.Step}.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>Steps</em>' containment reference list.
     * @see ca.mcgill.sel.usecases.UcPackage#getFlow_Steps()
     * @model containment="true"
     * @generated
     */
    EList<Step> getSteps();

    /**
     * Returns the value of the '<em><b>Precondition</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>Precondition</em>' attribute.
     * @see #setPrecondition(String)
     * @see ca.mcgill.sel.usecases.UcPackage#getFlow_Precondition()
     * @model
     * @generated
     */
    String getPrecondition();

    /**
     * Sets the value of the '{@link ca.mcgill.sel.usecases.Flow#getPrecondition <em>Precondition</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Precondition</em>' attribute.
     * @see #getPrecondition()
     * @generated
     */
    void setPrecondition(String value);

    /**
     * Returns the value of the '<em><b>Alternate Flows</b></em>' containment reference list.
     * The list contents are of type {@link ca.mcgill.sel.usecases.Flow}.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>Alternate Flows</em>' containment reference list.
     * @see ca.mcgill.sel.usecases.UcPackage#getFlow_AlternateFlows()
     * @model containment="true"
     * @generated
     */
    EList<Flow> getAlternateFlows();

    /**
     * Returns the value of the '<em><b>Conclusion Type</b></em>' attribute.
     * The literals are from the enumeration {@link ca.mcgill.sel.usecases.ConclusionType}.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>Conclusion Type</em>' attribute.
     * @see ca.mcgill.sel.usecases.ConclusionType
     * @see #setConclusionType(ConclusionType)
     * @see ca.mcgill.sel.usecases.UcPackage#getFlow_ConclusionType()
     * @model
     * @generated
     */
    ConclusionType getConclusionType();

    /**
     * Sets the value of the '{@link ca.mcgill.sel.usecases.Flow#getConclusionType <em>Conclusion Type</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Conclusion Type</em>' attribute.
     * @see ca.mcgill.sel.usecases.ConclusionType
     * @see #getConclusionType()
     * @generated
     */
    void setConclusionType(ConclusionType value);

    /**
     * Returns the value of the '<em><b>Conclusion Step</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>Conclusion Step</em>' reference.
     * @see #setConclusionStep(Step)
     * @see ca.mcgill.sel.usecases.UcPackage#getFlow_ConclusionStep()
     * @model
     * @generated
     */
    Step getConclusionStep();

    /**
     * Sets the value of the '{@link ca.mcgill.sel.usecases.Flow#getConclusionStep <em>Conclusion Step</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Conclusion Step</em>' reference.
     * @see #getConclusionStep()
     * @generated
     */
    void setConclusionStep(Step value);

    /**
     * Returns the value of the '<em><b>Post Condition</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>Post Condition</em>' reference.
     * @see #setPostCondition(Condition)
     * @see ca.mcgill.sel.usecases.UcPackage#getFlow_PostCondition()
     * @model
     * @generated
     */
    Condition getPostCondition();

    /**
     * Sets the value of the '{@link ca.mcgill.sel.usecases.Flow#getPostCondition <em>Post Condition</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Post Condition</em>' reference.
     * @see #getPostCondition()
     * @generated
     */
    void setPostCondition(Condition value);

} // Flow
