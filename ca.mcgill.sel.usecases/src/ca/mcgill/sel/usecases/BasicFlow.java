/**
 */
package ca.mcgill.sel.usecases;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Basic Flow</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see ca.mcgill.sel.usecases.UcPackage#getBasicFlow()
 * @model
 * @generated
 */
public interface BasicFlow extends Flow {
} // BasicFlow
