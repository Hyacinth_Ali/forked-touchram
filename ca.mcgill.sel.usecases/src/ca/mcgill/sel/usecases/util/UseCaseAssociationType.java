package ca.mcgill.sel.usecases.util;

/**
 * The possible types of use case associations.
 * @author rlanguay
 *
 */
public enum UseCaseAssociationType {
    /**
     * An inclusion relationship (in the main success scenario).
     */
    INCLUDE,
    
    /**
     * An extension relationship (in an extension flow).
     */
    EXTEND
}
