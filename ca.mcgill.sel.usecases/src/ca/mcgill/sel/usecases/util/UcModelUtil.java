package ca.mcgill.sel.usecases.util;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.eclipse.emf.common.util.BasicEMap;
import org.eclipse.emf.ecore.EObject;
import ca.mcgill.sel.core.controller.ModelUtil;
import ca.mcgill.sel.usecases.Actor;
import ca.mcgill.sel.usecases.BoundedFlow;
import ca.mcgill.sel.usecases.CommunicationStep;
import ca.mcgill.sel.usecases.ContextStep;
import ca.mcgill.sel.usecases.ContextType;
import ca.mcgill.sel.usecases.Flow;
import ca.mcgill.sel.usecases.Layout;
import ca.mcgill.sel.usecases.LayoutElement;
import ca.mcgill.sel.usecases.Level;
import ca.mcgill.sel.usecases.SpecificFlow;
import ca.mcgill.sel.usecases.Step;
import ca.mcgill.sel.usecases.UcFactory;
import ca.mcgill.sel.usecases.UseCase;
import ca.mcgill.sel.usecases.UseCaseModel;
import ca.mcgill.sel.usecases.UseCaseReferenceStep;

public final class UcModelUtil implements ModelUtil {
    
    /**
     * Creates a new instance of {@link UcdmModelUtil}.
     */
    public UcModelUtil() {
    }    

    /**
     * Creates a new use case diagram and associated entities
     * @param name The name of the diagram
     * @return The new diagram
     */
    public static UseCaseModel createUseCaseDiagram(String name) {
        UseCaseModel ucd = UcFactory.eINSTANCE.createUseCaseModel();       

        ucd.setName(name);
        ucd.setSystemName(name);

        // create an empty layout
        createLayout(ucd);       

        return ucd;
    }
    
    /**
     * Gets the set of use cases included by a use case.
     * @param useCase The use case
     * @return the included use cases
     */
    public static List<UseCase> getIncludedUseCases(UseCase useCase) {
        List<UseCase> includedUseCases = new ArrayList<UseCase>();
        includedUseCases.addAll(useCase.getIncludedUseCases());
        List<Flow> flows = new ArrayList<Flow>();
        flows.add(useCase.getMainSuccessScenario());
        flows.addAll(useCase.getMainSuccessScenario().getAlternateFlows());
        
        for (Flow flow : flows) {
            for (Step step : flow.getSteps()) {
                if (step instanceof UseCaseReferenceStep) {
                    UseCaseReferenceStep useCaseStep = (UseCaseReferenceStep) step;
                    includedUseCases.add(useCaseStep.getUsecase());
                }
            }    
        }        
        
        return includedUseCases;
    }
    
    /**
     * Gets the set of use cases that include this use case.
     * @param useCase The use case.
     * @return The use cases including this use case.
     */
    public static List<UseCase> getUseCasesIncluding(UseCase useCase) {
        List<UseCase> useCases = new ArrayList<UseCase>();
        UseCaseModel ucm = (UseCaseModel) useCase.eContainer();
        
        for (UseCase uc : ucm.getUseCases()) {
            if (getIncludedUseCases(uc).contains(useCase)) {
                useCases.add(uc);
            }
        }
        
        return useCases;
    }
    
    /**
     * Gets all use cases generalized by this use case.
     * @param useCase The use case.
     * @return The use cases.
     */
    public static List<UseCase> getUseCasesGeneralizedBy(UseCase useCase) {
        List<UseCase> useCases = new ArrayList<UseCase>();
        UseCaseModel ucm = (UseCaseModel) useCase.eContainer();
        
        for (UseCase uc : ucm.getUseCases()) {
            if (uc.getGeneralization() == useCase) {
                useCases.add(uc);
            }
        }
        
        return useCases;
    }
    
    
    /**
     * Gets the list of flows in a use case that reference a particular step.
     * @param useCase The use case
     * @param step The step.
     * @return The flows
     */
    public static List<Flow> getFlowsReferencingStep(Flow flow, Step step) {
        List<Flow> flows = new ArrayList<Flow>();
        
        for (Flow f : flow.getAlternateFlows()) {
            if (f instanceof BoundedFlow) {
                BoundedFlow boundedFlow = (BoundedFlow) f;
                if (boundedFlow.getReferencedSteps().contains(step)) {
                    flows.add(f);
                    continue;
                }
            } else if (f instanceof SpecificFlow) {
                SpecificFlow specificFlow = (SpecificFlow) f;
                if (specificFlow.getReferencedStep() == step) {
                    flows.add(f);
                    continue;
                }
            }
        }
        
        return flows;
    }
    
    /**
     * Gets the list of flows in a use case that reference a particular step in its conclusion.
     * @param useCase The use case
     * @param step The step.
     * @return The flows
     */
    public static List<Flow> getConclusionsReferencingStep(Flow flow, Step step) {
        List<Flow> flows = new ArrayList<Flow>();
        
        for (Flow f : flow.getAlternateFlows()) {            
            if (f.getConclusionStep() == step) {
                flows.add(f);
            }
        }
        
        return flows;
    }
    
    /**
     * Gets the index of a step within its parent flow.
     * @param step The step.
     * @return The index of the step in its flow.
     */
    public static int getStepIndex(Step step) {
        Flow parentFlow = (Flow) step.eContainer();
        return parentFlow.getSteps().indexOf(step);
    }
    
    /**
     * Checks if a link of a particular type exists between two use cases.
     * @param source The source use case.
     * @param dest The destination use case.
     * @param type The association type.
     * @return True if an association exists, false otherwise.
     */
    public static boolean useCaseLinkExists(UseCase source, UseCase dest, UseCaseAssociationType type) {
        List<UseCase> linkedUseCases = getIncludedUseCases(source);        
        return linkedUseCases.contains(dest);
    }
    
    /**
     * Checks if a flow is the main success scenario flow of a use case.
     * Throws an exception if the flow and the use case are not connected in any way.
     * @param flow The flow.
     * @return True if the flow is the main success scenario of its use case.
     */
    public static boolean isMainSuccessFlow(Flow flow) {
        UseCase useCase = (UseCase) flow.eContainer();
        if (useCase.getMainSuccessScenario() == flow) {
            return true;
        } else if (useCase.getMainSuccessScenario().getAlternateFlows().contains(flow)) {
            return false;
        } else {
            throw new IndexOutOfBoundsException();
        }
    }
    
    /**
     * Gets all use cases in the model associated with an actor.
     * @param actor The actor.
     * @return The use cases.
     */
    public static List<UseCase> getUseCasesAssociatedToActor(Actor actor) {
        List<UseCase> useCases = new ArrayList<UseCase>();
        UseCaseModel ucm = (UseCaseModel) actor.eContainer();
        for (UseCase uc : ucm.getUseCases()) {
            if (uc.getPrimaryActor() == actor || uc.getSecondaryActors().contains(actor)) {
                useCases.add(uc);
            }
        }
        
        return useCases;
    }
    
    /**
     * Gets all steps in a flow that can be extended
     * (either by an alternate flow or an extension use case).
     * @param flow The flow.
     * @return The extendable steps.
     */
    public static List<Step> getExtendableSteps(Flow flow) {
        return flow.getSteps().stream()
                .filter(s -> isExtendableStep(s))
                .collect(Collectors.toList());
    }
    
    /**
     * Gets the list of actors available for reference in a use case step.
     * @param useCase The use case.
     * @return The list of actors.
     */
    public static Set<Actor> getAvailableActors(UseCase useCase) {
        return getAvailableActors(useCase, new ArrayList<UseCase>());
    }
    
    /**
     * Gets the list of flows that can be compared to the current flow.
     * This means they are at the same extension level, with the same step index.
     * @param flow The flow
     * @return The comparable flows.
     */
    public static List<Flow> getComparableFlows(Flow flow) {
        if (!(flow.eContainer() instanceof Flow)) {
            return new ArrayList<Flow>();
        }
        
        Flow parentFlow = (Flow) flow.eContainer();
        String flowStepIndex = UseCaseTextUtils.getFlowStepIndexText(flow, parentFlow);
        return parentFlow.getAlternateFlows()
                .stream()
                .filter(f -> UseCaseTextUtils.getFlowStepIndexText(f, parentFlow).equals(flowStepIndex))
                .collect(Collectors.toList());
    }

    /**
     * Creates a new layout for a given {@link UseCaseDiagram}.
     * The layout is the {@link ca.mcgill.sel.ram.impl.ContainerMapImpl} specifically
     * that holds all {@link LayoutElement} for children of the given {@link UseCaseDiagram}.
     *
     * @param cd the {@link UseCaseDiagram} holding the {@link LayoutElement} for its children
     */
    private static void createLayout(UseCaseModel ucd) {
        Layout layout = UcFactory.eINSTANCE.createLayout();

        // workaround used here since creating the map, adding the values and then putting it doesn't work
        // EMF somehow does some magic with the passed map instance
        layout.getContainers().put(ucd, new BasicEMap<EObject, LayoutElement>());

        ucd.setLayout(layout);
    }
    
    private static boolean isExtendableStep(Step step) {
        if (step instanceof CommunicationStep || step instanceof UseCaseReferenceStep) {
            return true;
        } else if (step instanceof ContextStep) {
            ContextStep contextStep = (ContextStep) step;
            return contextStep.getType() == ContextType.INTERNAL;
        }
        
        return false;
    }
    
    /**
     * Used to limit the search depth (since currently no logic prevents infinite loops).
     * @param useCase The use case.
     * @param The previously searched use cases.
     * @return The actors.
     */
    private static Set<Actor> getAvailableActors(UseCase useCase, List<UseCase> seenUseCases) {
        // Don't search the same use case twice
        if (seenUseCases.contains(useCase)) {
            return new HashSet<Actor>();
        }
        
        seenUseCases.add(useCase);
        
        Set<Actor> actors = new HashSet<Actor>(useCase.getSecondaryActors());
        if (useCase.getPrimaryActor() != null) {
            actors.add(useCase.getPrimaryActor());
        }
        
        if (useCase.getGeneralization() != null) {
            actors.addAll(getAvailableActors(useCase.getGeneralization(), seenUseCases));
        }
        
        if (useCase.getExtendedUseCase() != null) {
            actors.addAll(getAvailableActors(useCase.getExtendedUseCase(), seenUseCases));
        }
        
        // If the use case is a subfunction or user-goal, we can also reference any actors in any use case
        // that INCLUDES this use case (to avoid having excessive links in the diagram).
        // TODO: review
        if (useCase.getLevel() != Level.SUMMARY) {
            for (UseCase includingUseCase : getUseCasesIncluding(useCase)) {
                actors.addAll(getAvailableActors(includingUseCase, seenUseCases));
            }
        }
        
        return actors;
    }

    @Override
    public EObject createNewEmptyModel(String name) {
        return createUseCaseDiagram(name);
    }
}
