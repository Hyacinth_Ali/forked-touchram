package ca.mcgill.sel.usecases.util;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.eclipse.emf.ecore.EObject;

import ca.mcgill.sel.commons.emf.util.EMFModelUtil;
import ca.mcgill.sel.usecases.Actor;
import ca.mcgill.sel.usecases.BoundedFlow;
import ca.mcgill.sel.usecases.ConclusionType;
import ca.mcgill.sel.usecases.ContextStep;
import ca.mcgill.sel.usecases.ContextType;
import ca.mcgill.sel.usecases.Flow;
import ca.mcgill.sel.usecases.SpecificFlow;
import ca.mcgill.sel.usecases.Step;
import ca.mcgill.sel.usecases.UcPackage;
import ca.mcgill.sel.usecases.UseCase;
import ca.mcgill.sel.usecases.UseCaseModel;

/**
 * Utility methods for getting the texts associated with use case artifacts.
 * @author rlanguay
 *
 */
public class UseCaseTextUtils {
    /**
     * The token to replace with the actor's name.
     */
    public static final String ACTOR_TOKEN = "__ACTOR__";
    
    private static final String FAILURE_TEXT = "Use case ends in failure.";
    private static final String SUCCESS_TEXT = "Use case ends in success.";
    private static final String STEP_TEXT = "Use case continues at step %s";
    private static final String STEP_NUMBER_SUFFIX = ".";
    private static final String ELLIPSES = "...";
    /**
     * Gets the display index of a step, based on its position in the flow.
     * @param flow The flow.
     * @param step The step.
     * @return The step number to display.
     */
    public static String getStepDisplayIndex(Flow flow, Step step) {
        if (step instanceof ContextStep && ((ContextStep) step).getType() != ContextType.INTERNAL) {
            return "";
        }
        
        int index = 1;
        for (Step s : flow.getSteps()) {
            if (s == step) {
                return Integer.toString(index);
            } else if (s instanceof ContextStep) {
                ContextStep contextStep = (ContextStep) s;
                if (contextStep.getType() != ContextType.INTERNAL) {
                    continue;
                } else {
                    index++;
                }
            } else {
                index++;
            }
        }
        
        throw new IndexOutOfBoundsException();
    }

    /**
     * Gets the text to display for a particular step, depending on the type.
     * @param flow The flow.
     * @param step The step.
     * @return The text to display in the step's view.
     */
    public static String getStepText(Flow flow, Step step) {
        String stepIndex = getStepDisplayIndex(flow, step);
        String result = step.getStepText() == null ? "" : step.getStepText();
        result = replaceActorIds(result, step);
        
        return String.format("%s. %s", stepIndex, result);
    }
    
    /**
     * Gets the text used to sort steps in a flow.
     * This essentially prepends chars to ensure that numerical ordering is respected.
     * The text itself is never seen.
     * @param flow The flow.
     * @param step The step.
     * @return The sorting text.
     */
    public static String getSortingText(Flow flow, Step step) {
        int stepIndex = flow.getSteps().indexOf(step) + 1;
        String stepIndexString = Integer.toString(stepIndex);
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < stepIndexString.length(); i++) {
            sb.append("z");
        }
        
        sb.append(stepIndexString);
        
        return sb.toString();
    }

    /**
     * Gets the text to display to show the steps associated to an extension flow.
     * @param flow The extension flow.
     * @return The step index text to display.
     */
    public static String getFlowStepNumberText(Flow flow) {
        if (flow.eContainer() instanceof Flow) {
            Flow parentFlow = (Flow) flow.eContainer();
            String flowText = String.format("%s%s", 
                    getFlowStepIndexText(flow, parentFlow), getExtensionFlowLetter(flow, parentFlow));
            if (!"".equals(flowText)) {
                flowText += STEP_NUMBER_SUFFIX;
            }
            
            return String.format("%s%s", getFlowStepNumberText((Flow) flow.eContainer()), flowText);
        } else {
            return "";
        } 
    }
    
    /**
     * Gets the text to display to show the steps associated to an extension flow.
     * Use this overload when the parent is NOT KNOWN (i.e. a flow hasn't been added yet).
     * @param flow The extension flow.
     * @return The step index text to display.
     */
    public static String getFlowStepNumberText(Flow flow, Flow parentFlow) {
        String flowText = String.format("%s%s", 
                getFlowStepIndexText(flow, parentFlow), getExtensionFlowLetter(flow, parentFlow));
        if (!"".equals(flowText)) {
            flowText += STEP_NUMBER_SUFFIX;
        }
        
        return String.format("%s%s", getFlowStepNumberText(parentFlow), flowText);
    }
    
    /**
     * Gets the complete step number for a step.
     * @param step The step.
     * @return The step number.
     */
    public static String getStepNumber(Step step) {
        Flow containingFlow = (Flow) step.eContainer();
        String flowNumber = getFlowStepNumberText(containingFlow);
        String stepNumber = getStepDisplayIndex(containingFlow, step); 
        
        // If the step doesn't have a display index, don't show anything
        if ("".equals(stepNumber)) {
            return "";
        } else {
            return flowNumber + stepNumber + STEP_NUMBER_SUFFIX;
        }
    }
    
    public static String getFlowStepIndexText(Flow flow, Flow parentFlow) {        
        if (flow instanceof SpecificFlow) {
            return getStepDisplayIndex(parentFlow, ((SpecificFlow) flow).getReferencedStep());
        } else if (flow instanceof BoundedFlow) {
            Flow f = parentFlow;
            List<Integer> stepIndexes = ((BoundedFlow) flow).getReferencedSteps()
                    .stream()
                    .map(step -> Integer.parseInt(getStepDisplayIndex(f, step)))
                    .sorted()
                    .collect(Collectors.toList());
            
            List<String> results = new ArrayList<String>();
            int i = 0;
            int rangeStart = 0;
            int rangeEnd = 0;
            
            while (i < stepIndexes.size()) {
                int currentStepIndex = stepIndexes.get(i);
                if (rangeStart == 0) {
                    // Starting a new potential range
                    rangeStart = currentStepIndex;
                } else if ((currentStepIndex == rangeStart + 1 && rangeEnd == 0)
                        || currentStepIndex == rangeEnd + 1) {
                    // Continuing an existing range
                    rangeEnd = currentStepIndex;
                } else {
                    // Current range has ended
                    if (rangeEnd == 0) {
                        results.add(Integer.toString(rangeStart));
                    } else {
                        results.add(String.format("%d-%d", rangeStart, rangeEnd));
                    }
                    
                    rangeStart = currentStepIndex;
                    rangeEnd = 0;
                }
                
                i++;
            }
            
            if (rangeStart != 0) {
                if (rangeEnd == 0) {
                    results.add(Integer.toString(rangeStart));
                } else {
                    results.add(String.format("%d-%d", rangeStart, rangeEnd));
                }
            }
            
            return String.format("(%s)", String.join(", ", results));
        } else {
            // Must be a global flow; put an artificial range of all steps
            String firstDisplayIndex = null;          
            String lastDisplayIndex = null;
            for (Step step : parentFlow.getSteps()) {
                String stepDisplayIndex = getStepDisplayIndex(parentFlow, step);
                if (firstDisplayIndex == null && stepDisplayIndex != "") {
                    firstDisplayIndex = stepDisplayIndex;
                }
                
                if (stepDisplayIndex != "") {
                    lastDisplayIndex = stepDisplayIndex;
                }
            }
            
            if (firstDisplayIndex == null && lastDisplayIndex == null) {
                return "";
            } else if (firstDisplayIndex == lastDisplayIndex) {
                return String.format("(%s)", firstDisplayIndex);
            } else {
                return String.format("(%s-%s)", firstDisplayIndex, lastDisplayIndex);    
            }            
        }
    }
    
    private static String getExtensionFlowLetter(Flow flow, Flow parentFlow) {
        Map<Flow, String> flowToStepExpressionMap = parentFlow.getAlternateFlows()
                .stream()
                .collect(Collectors.toMap(f -> f, 
                    f -> getFlowStepIndexText(f, parentFlow)));
        
        // SPECIAL CASE: this can be called when a flow is not yet part of the alternates
        // In this case, we should return the next letter in the alphabet
        if (!parentFlow.getAlternateFlows().contains(flow)) {
            String newFlowStepExpression = getFlowStepIndexText(flow, parentFlow);
            long numMatches = flowToStepExpressionMap.values()
                    .stream()
                    .filter(c -> c.equals(newFlowStepExpression))
                    .count();
            return getLetterForInteger((int)numMatches);
        }
        
        String currentFlowExpression = flowToStepExpressionMap.get(flow);
        int letter = 1;
        for (Flow f : parentFlow.getAlternateFlows()) {
            if (f == flow) {
                break;
            } else if (flowToStepExpressionMap.get(f).equals(currentFlowExpression)) {
                letter++;
            }
        }
        
        return getLetterForInteger(letter);
    }

    private static String getLetterForInteger(int letter) {
        if (letter > 0 && letter < 27) {
            return String.valueOf((char) (letter + 96));
        } else if (letter >= 27) {
            // In this case we should be doubling up
            int repetitions = letter / 26;
            int singleLetter = (letter % 26) + 1;
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < repetitions; i++) {
                sb.append(getLetterForInteger(singleLetter));
            }
            
            return sb.toString();
        } else {
            return "";
        }
    }
    
    public static String getConclusionTypeText(ConclusionType conclusionType) {
        String result = "";
        switch (conclusionType) {
            case FAILURE:
                result = FAILURE_TEXT;
                break;
            case STEP:
                result = String.format(STEP_TEXT, ELLIPSES);
                break;
            case SUCCESS:
                result = SUCCESS_TEXT;
                break;
        }
        
        return result;
    }

    public static String getConclusionTypeText(Flow flow) {
        String result = "";
        switch (flow.getConclusionType()) {
            case FAILURE:
                result = FAILURE_TEXT;
                break;
            case STEP:
                if (flow.getConclusionStep() != null) {
                    result = String.format(STEP_TEXT, getStepNumber(flow.getConclusionStep()));
                } else {
                    result = String.format(STEP_TEXT, ELLIPSES);
                }
                break;                
            case SUCCESS:
                result = SUCCESS_TEXT;
                break;
        }
        
        return result;
    }
    
    public static String parseActorNames(String text, UseCase useCase) {
        String textLower = text.toLowerCase();
        List<Actor> actors = new ArrayList<Actor>(UcModelUtil.getAvailableActors(useCase));
        
        // Order actors by name length, to find longer matches first
        actors.sort(new Comparator<Actor>() {
            @Override
            public int compare(Actor a, Actor b) {
                return Integer.compare(b.getName().length(), a.getName().length());
            }
        });
        
        for (Actor actor : actors) {
            if (textLower.contains(actor.getName().toLowerCase())) {
                String actorId = EMFModelUtil.getObjectEId(actor);
                Pattern pattern = Pattern.compile(actor.getName(), Pattern.CASE_INSENSITIVE);
                text = pattern.matcher(text).replaceAll(actorId);
            }
        }
        
        return text;
    }
    
    public static String replaceActorIds(String text, EObject ref) {
        UseCaseModel model = EMFModelUtil.getRootContainerOfType(ref, UcPackage.Literals.USE_CASE_MODEL);        
        for (Actor actor : model.getActors()) {
            String actorId = EMFModelUtil.getObjectEId(actor);
            text = text.replace(actorId, actor.getName());
        }
        
        
        return text;
    }
}
