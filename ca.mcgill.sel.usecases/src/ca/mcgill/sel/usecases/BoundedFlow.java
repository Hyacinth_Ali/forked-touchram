/**
 */
package ca.mcgill.sel.usecases;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Bounded Flow</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.usecases.BoundedFlow#getReferencedSteps <em>Referenced Steps</em>}</li>
 * </ul>
 *
 * @see ca.mcgill.sel.usecases.UcPackage#getBoundedFlow()
 * @model
 * @generated
 */
public interface BoundedFlow extends Flow {
    /**
     * Returns the value of the '<em><b>Referenced Steps</b></em>' reference list.
     * The list contents are of type {@link ca.mcgill.sel.usecases.Step}.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>Referenced Steps</em>' reference list.
     * @see ca.mcgill.sel.usecases.UcPackage#getBoundedFlow_ReferencedSteps()
     * @model required="true"
     * @generated
     */
    EList<Step> getReferencedSteps();

} // BoundedFlow
