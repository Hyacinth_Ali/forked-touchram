/**
 */
package ca.mcgill.sel.usecases;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Extension Point</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see ca.mcgill.sel.usecases.UcPackage#getExtensionPoint()
 * @model
 * @generated
 */
public interface ExtensionPoint extends Step {
} // ExtensionPoint
