/**
 */
package ca.mcgill.sel.restif;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Path Fragment</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.restif.PathFragment#getChild <em>Child</em>}</li>
 * </ul>
 *
 * @see ca.mcgill.sel.restif.RestifPackage#getPathFragment()
 * @model abstract="true"
 * @generated
 */
public interface PathFragment extends EObject {
    /**
	 * Returns the value of the '<em><b>Child</b></em>' containment reference list.
	 * The list contents are of type {@link ca.mcgill.sel.restif.PathFragment}.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @return the value of the '<em>Child</em>' containment reference list.
	 * @see ca.mcgill.sel.restif.RestifPackage#getPathFragment_Child()
	 * @model containment="true"
	 * @generated
	 */
    EList<PathFragment> getChild();

} // PathFragment
