/**
 */
package ca.mcgill.sel.restif.impl;

import ca.mcgill.sel.restif.AccessMethod;
import ca.mcgill.sel.restif.PathFragment;
import ca.mcgill.sel.restif.Resource;
import ca.mcgill.sel.restif.RestifPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Resource</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.restif.impl.ResourceImpl#getAccessmethod <em>Accessmethod</em>}</li>
 *   <li>{@link ca.mcgill.sel.restif.impl.ResourceImpl#getEndpoint <em>Endpoint</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ResourceImpl extends MinimalEObjectImpl.Container implements Resource {
    /**
	 * The cached value of the '{@link #getAccessmethod() <em>Accessmethod</em>}' containment reference list.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @see #getAccessmethod()
	 * @generated
	 * @ordered
	 */
    protected EList<AccessMethod> accessmethod;

    /**
	 * The cached value of the '{@link #getEndpoint() <em>Endpoint</em>}' reference.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @see #getEndpoint()
	 * @generated
	 * @ordered
	 */
    protected PathFragment endpoint;

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    protected ResourceImpl() {
		super();
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    protected EClass eStaticClass() {
		return RestifPackage.Literals.RESOURCE;
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public EList<AccessMethod> getAccessmethod() {
		if (accessmethod == null) {
			accessmethod = new EObjectContainmentEList<AccessMethod>(AccessMethod.class, this, RestifPackage.RESOURCE__ACCESSMETHOD);
		}
		return accessmethod;
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public PathFragment getEndpoint() {
		if (endpoint != null && endpoint.eIsProxy()) {
			InternalEObject oldEndpoint = (InternalEObject)endpoint;
			endpoint = (PathFragment)eResolveProxy(oldEndpoint);
			if (endpoint != oldEndpoint) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, RestifPackage.RESOURCE__ENDPOINT, oldEndpoint, endpoint));
			}
		}
		return endpoint;
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    public PathFragment basicGetEndpoint() {
		return endpoint;
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public void setEndpoint(PathFragment newEndpoint) {
		PathFragment oldEndpoint = endpoint;
		endpoint = newEndpoint;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RestifPackage.RESOURCE__ENDPOINT, oldEndpoint, endpoint));
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case RestifPackage.RESOURCE__ACCESSMETHOD:
				return ((InternalEList<?>)getAccessmethod()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case RestifPackage.RESOURCE__ACCESSMETHOD:
				return getAccessmethod();
			case RestifPackage.RESOURCE__ENDPOINT:
				if (resolve) return getEndpoint();
				return basicGetEndpoint();
		}
		return super.eGet(featureID, resolve, coreType);
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @SuppressWarnings("unchecked")
    @Override
    public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case RestifPackage.RESOURCE__ACCESSMETHOD:
				getAccessmethod().clear();
				getAccessmethod().addAll((Collection<? extends AccessMethod>)newValue);
				return;
			case RestifPackage.RESOURCE__ENDPOINT:
				setEndpoint((PathFragment)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public void eUnset(int featureID) {
		switch (featureID) {
			case RestifPackage.RESOURCE__ACCESSMETHOD:
				getAccessmethod().clear();
				return;
			case RestifPackage.RESOURCE__ENDPOINT:
				setEndpoint((PathFragment)null);
				return;
		}
		super.eUnset(featureID);
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public boolean eIsSet(int featureID) {
		switch (featureID) {
			case RestifPackage.RESOURCE__ACCESSMETHOD:
				return accessmethod != null && !accessmethod.isEmpty();
			case RestifPackage.RESOURCE__ENDPOINT:
				return endpoint != null;
		}
		return super.eIsSet(featureID);
	}

} //ResourceImpl
