/**
 */
package ca.mcgill.sel.restif.impl;

import ca.mcgill.sel.restif.HeaderField;
import ca.mcgill.sel.restif.HeaderParameter;
import ca.mcgill.sel.restif.RestifPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Header Parameter</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.restif.impl.HeaderParameterImpl#getHeaderField <em>Header Field</em>}</li>
 * </ul>
 *
 * @generated
 */
public class HeaderParameterImpl extends ParameterImpl implements HeaderParameter {
    /**
	 * The default value of the '{@link #getHeaderField() <em>Header Field</em>}' attribute.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @see #getHeaderField()
	 * @generated
	 * @ordered
	 */
    protected static final HeaderField HEADER_FIELD_EDEFAULT = HeaderField.USERAGENT;

    /**
	 * The cached value of the '{@link #getHeaderField() <em>Header Field</em>}' attribute.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @see #getHeaderField()
	 * @generated
	 * @ordered
	 */
    protected HeaderField headerField = HEADER_FIELD_EDEFAULT;

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    protected HeaderParameterImpl() {
		super();
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    protected EClass eStaticClass() {
		return RestifPackage.Literals.HEADER_PARAMETER;
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public HeaderField getHeaderField() {
		return headerField;
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public void setHeaderField(HeaderField newHeaderField) {
		HeaderField oldHeaderField = headerField;
		headerField = newHeaderField == null ? HEADER_FIELD_EDEFAULT : newHeaderField;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RestifPackage.HEADER_PARAMETER__HEADER_FIELD, oldHeaderField, headerField));
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case RestifPackage.HEADER_PARAMETER__HEADER_FIELD:
				return getHeaderField();
		}
		return super.eGet(featureID, resolve, coreType);
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case RestifPackage.HEADER_PARAMETER__HEADER_FIELD:
				setHeaderField((HeaderField)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public void eUnset(int featureID) {
		switch (featureID) {
			case RestifPackage.HEADER_PARAMETER__HEADER_FIELD:
				setHeaderField(HEADER_FIELD_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public boolean eIsSet(int featureID) {
		switch (featureID) {
			case RestifPackage.HEADER_PARAMETER__HEADER_FIELD:
				return headerField != HEADER_FIELD_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (headerField: ");
		result.append(headerField);
		result.append(')');
		return result.toString();
	}

} //HeaderParameterImpl
