/**
 */
package ca.mcgill.sel.restif;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Resource</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.restif.Resource#getAccessmethod <em>Accessmethod</em>}</li>
 *   <li>{@link ca.mcgill.sel.restif.Resource#getEndpoint <em>Endpoint</em>}</li>
 * </ul>
 *
 * @see ca.mcgill.sel.restif.RestifPackage#getResource()
 * @model
 * @generated
 */
public interface Resource extends EObject {
    /**
	 * Returns the value of the '<em><b>Accessmethod</b></em>' containment reference list.
	 * The list contents are of type {@link ca.mcgill.sel.restif.AccessMethod}.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @return the value of the '<em>Accessmethod</em>' containment reference list.
	 * @see ca.mcgill.sel.restif.RestifPackage#getResource_Accessmethod()
	 * @model containment="true" required="true" upper="4"
	 * @generated
	 */
    EList<AccessMethod> getAccessmethod();

    /**
	 * Returns the value of the '<em><b>Endpoint</b></em>' reference.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @return the value of the '<em>Endpoint</em>' reference.
	 * @see #setEndpoint(PathFragment)
	 * @see ca.mcgill.sel.restif.RestifPackage#getResource_Endpoint()
	 * @model
	 * @generated
	 */
    PathFragment getEndpoint();

    /**
	 * Sets the value of the '{@link ca.mcgill.sel.restif.Resource#getEndpoint <em>Endpoint</em>}' reference.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Endpoint</em>' reference.
	 * @see #getEndpoint()
	 * @generated
	 */
    void setEndpoint(PathFragment value);

} // Resource
