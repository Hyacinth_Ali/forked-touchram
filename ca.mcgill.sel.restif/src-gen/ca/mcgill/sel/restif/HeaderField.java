/**
 */
package ca.mcgill.sel.restif;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Header Field</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see ca.mcgill.sel.restif.RestifPackage#getHeaderField()
 * @model
 * @generated
 */
public enum HeaderField implements Enumerator {

	/**
	 * The '<em><b>ENCODING</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENCODING_VALUE
	 * @generated
	 * @ordered
	 */
	ENCODING(0, "ENCODING", "ENCODING"),

	/**
	 * The '<em><b>AUTHORIZATION</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #AUTHORIZATION_VALUE
	 * @generated
	 * @ordered
	 */
	AUTHORIZATION(1, "AUTHORIZATION", "AUTHORIZATION"),

	/**
	 * The '<em><b>USERAGENT</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #USERAGENT_VALUE
	 * @generated
	 * @ordered
	 */
	USERAGENT(2, "USERAGENT", "USERAGENT");

	/**
	 * The '<em><b>ENCODING</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENCODING
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int ENCODING_VALUE = 0;

	/**
	 * The '<em><b>AUTHORIZATION</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #AUTHORIZATION
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int AUTHORIZATION_VALUE = 1;

	/**
	 * The '<em><b>USERAGENT</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #USERAGENT
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int USERAGENT_VALUE = 2;

	/**
	 * An array of all the '<em><b>Header Field</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final HeaderField[] VALUES_ARRAY =
		new HeaderField[] {
			ENCODING,
			AUTHORIZATION,
			USERAGENT,
		};

	/**
	 * A public read-only list of all the '<em><b>Header Field</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<HeaderField> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Header Field</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static HeaderField get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			HeaderField result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Header Field</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static HeaderField getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			HeaderField result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Header Field</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static HeaderField get(int value) {
		switch (value) {
			case ENCODING_VALUE: return ENCODING;
			case AUTHORIZATION_VALUE: return AUTHORIZATION;
			case USERAGENT_VALUE: return USERAGENT;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private HeaderField(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //HeaderField
