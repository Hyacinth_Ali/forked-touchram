/**
 */
package ca.mcgill.sel.restif;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see ca.mcgill.sel.restif.RestifFactory
 * @model kind="package"
 * @generated
 */
public interface RestifPackage extends EPackage {
    /**
	 * The package name.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    String eNAME = "restif";

    /**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    String eNS_URI = "http://www.example.org/restif";

    /**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    String eNS_PREFIX = "restif";

    /**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    RestifPackage eINSTANCE = ca.mcgill.sel.restif.impl.RestifPackageImpl.init();

    /**
	 * The meta object id for the '{@link ca.mcgill.sel.restif.impl.NamedElementImpl <em>Named Element</em>}' class.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @see ca.mcgill.sel.restif.impl.NamedElementImpl
	 * @see ca.mcgill.sel.restif.impl.RestifPackageImpl#getNamedElement()
	 * @generated
	 */
    int NAMED_ELEMENT = 9;

    /**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
    int NAMED_ELEMENT__NAME = 0;

    /**
	 * The number of structural features of the '<em>Named Element</em>' class.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
    int NAMED_ELEMENT_FEATURE_COUNT = 1;

    /**
	 * The number of operations of the '<em>Named Element</em>' class.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
    int NAMED_ELEMENT_OPERATION_COUNT = 0;

    /**
	 * The meta object id for the '{@link ca.mcgill.sel.restif.impl.RestIFImpl <em>Rest IF</em>}' class.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @see ca.mcgill.sel.restif.impl.RestIFImpl
	 * @see ca.mcgill.sel.restif.impl.RestifPackageImpl#getRestIF()
	 * @generated
	 */
    int REST_IF = 0;

    /**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
    int REST_IF__NAME = NAMED_ELEMENT__NAME;

    /**
	 * The feature id for the '<em><b>Root</b></em>' containment reference.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
    int REST_IF__ROOT = NAMED_ELEMENT_FEATURE_COUNT + 0;

    /**
	 * The feature id for the '<em><b>Resource</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
    int REST_IF__RESOURCE = NAMED_ELEMENT_FEATURE_COUNT + 1;

    /**
	 * The number of structural features of the '<em>Rest IF</em>' class.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
    int REST_IF_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 2;

    /**
	 * The number of operations of the '<em>Rest IF</em>' class.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
    int REST_IF_OPERATION_COUNT = NAMED_ELEMENT_OPERATION_COUNT + 0;

    /**
	 * The meta object id for the '{@link ca.mcgill.sel.restif.impl.PathFragmentImpl <em>Path Fragment</em>}' class.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @see ca.mcgill.sel.restif.impl.PathFragmentImpl
	 * @see ca.mcgill.sel.restif.impl.RestifPackageImpl#getPathFragment()
	 * @generated
	 */
    int PATH_FRAGMENT = 1;

    /**
	 * The feature id for the '<em><b>Child</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
    int PATH_FRAGMENT__CHILD = 0;

    /**
	 * The number of structural features of the '<em>Path Fragment</em>' class.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
    int PATH_FRAGMENT_FEATURE_COUNT = 1;

    /**
	 * The number of operations of the '<em>Path Fragment</em>' class.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
    int PATH_FRAGMENT_OPERATION_COUNT = 0;

    /**
	 * The meta object id for the '{@link ca.mcgill.sel.restif.impl.AccessMethodImpl <em>Access Method</em>}' class.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @see ca.mcgill.sel.restif.impl.AccessMethodImpl
	 * @see ca.mcgill.sel.restif.impl.RestifPackageImpl#getAccessMethod()
	 * @generated
	 */
    int ACCESS_METHOD = 2;

    /**
	 * The feature id for the '<em><b>Input Parameter</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
    int ACCESS_METHOD__INPUT_PARAMETER = 0;

    /**
	 * The feature id for the '<em><b>Result</b></em>' containment reference.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
    int ACCESS_METHOD__RESULT = 1;

    /**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
    int ACCESS_METHOD__TYPE = 2;

    /**
	 * The feature id for the '<em><b>Result Meta Parameter</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
    int ACCESS_METHOD__RESULT_META_PARAMETER = 3;

    /**
	 * The number of structural features of the '<em>Access Method</em>' class.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
    int ACCESS_METHOD_FEATURE_COUNT = 4;

    /**
	 * The number of operations of the '<em>Access Method</em>' class.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
    int ACCESS_METHOD_OPERATION_COUNT = 0;

    /**
	 * The meta object id for the '{@link ca.mcgill.sel.restif.impl.StaticFragmentImpl <em>Static Fragment</em>}' class.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @see ca.mcgill.sel.restif.impl.StaticFragmentImpl
	 * @see ca.mcgill.sel.restif.impl.RestifPackageImpl#getStaticFragment()
	 * @generated
	 */
    int STATIC_FRAGMENT = 3;

    /**
	 * The feature id for the '<em><b>Child</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
    int STATIC_FRAGMENT__CHILD = PATH_FRAGMENT__CHILD;

    /**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
    int STATIC_FRAGMENT__NAME = PATH_FRAGMENT_FEATURE_COUNT + 0;

    /**
	 * The number of structural features of the '<em>Static Fragment</em>' class.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
    int STATIC_FRAGMENT_FEATURE_COUNT = PATH_FRAGMENT_FEATURE_COUNT + 1;

    /**
	 * The number of operations of the '<em>Static Fragment</em>' class.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
    int STATIC_FRAGMENT_OPERATION_COUNT = PATH_FRAGMENT_OPERATION_COUNT + 0;

    /**
	 * The meta object id for the '{@link ca.mcgill.sel.restif.impl.DynamicFragmentImpl <em>Dynamic Fragment</em>}' class.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @see ca.mcgill.sel.restif.impl.DynamicFragmentImpl
	 * @see ca.mcgill.sel.restif.impl.RestifPackageImpl#getDynamicFragment()
	 * @generated
	 */
    int DYNAMIC_FRAGMENT = 4;

    /**
	 * The feature id for the '<em><b>Child</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
    int DYNAMIC_FRAGMENT__CHILD = PATH_FRAGMENT__CHILD;

    /**
	 * The feature id for the '<em><b>Placeholder</b></em>' attribute.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
    int DYNAMIC_FRAGMENT__PLACEHOLDER = PATH_FRAGMENT_FEATURE_COUNT + 0;

    /**
	 * The number of structural features of the '<em>Dynamic Fragment</em>' class.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
    int DYNAMIC_FRAGMENT_FEATURE_COUNT = PATH_FRAGMENT_FEATURE_COUNT + 1;

    /**
	 * The number of operations of the '<em>Dynamic Fragment</em>' class.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
    int DYNAMIC_FRAGMENT_OPERATION_COUNT = PATH_FRAGMENT_OPERATION_COUNT + 0;

    /**
	 * The meta object id for the '{@link ca.mcgill.sel.restif.impl.ResourceImpl <em>Resource</em>}' class.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @see ca.mcgill.sel.restif.impl.ResourceImpl
	 * @see ca.mcgill.sel.restif.impl.RestifPackageImpl#getResource()
	 * @generated
	 */
    int RESOURCE = 5;

    /**
	 * The feature id for the '<em><b>Accessmethod</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
    int RESOURCE__ACCESSMETHOD = 0;

    /**
	 * The feature id for the '<em><b>Endpoint</b></em>' reference.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
    int RESOURCE__ENDPOINT = 1;

    /**
	 * The number of structural features of the '<em>Resource</em>' class.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
    int RESOURCE_FEATURE_COUNT = 2;

    /**
	 * The number of operations of the '<em>Resource</em>' class.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
    int RESOURCE_OPERATION_COUNT = 0;

    /**
	 * The meta object id for the '{@link ca.mcgill.sel.restif.impl.ParameterImpl <em>Parameter</em>}' class.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @see ca.mcgill.sel.restif.impl.ParameterImpl
	 * @see ca.mcgill.sel.restif.impl.RestifPackageImpl#getParameter()
	 * @generated
	 */
    int PARAMETER = 6;

    /**
	 * The number of structural features of the '<em>Parameter</em>' class.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
    int PARAMETER_FEATURE_COUNT = 0;

    /**
	 * The number of operations of the '<em>Parameter</em>' class.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
    int PARAMETER_OPERATION_COUNT = 0;

    /**
	 * The meta object id for the '{@link ca.mcgill.sel.restif.impl.RequestParameterImpl <em>Request Parameter</em>}' class.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @see ca.mcgill.sel.restif.impl.RequestParameterImpl
	 * @see ca.mcgill.sel.restif.impl.RestifPackageImpl#getRequestParameter()
	 * @generated
	 */
    int REQUEST_PARAMETER = 7;

    /**
	 * The feature id for the '<em><b>Default Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
    int REQUEST_PARAMETER__DEFAULT_VALUE = PARAMETER_FEATURE_COUNT + 0;

    /**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
    int REQUEST_PARAMETER__NAME = PARAMETER_FEATURE_COUNT + 1;

    /**
	 * The feature id for the '<em><b>Mandatory</b></em>' attribute.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
    int REQUEST_PARAMETER__MANDATORY = PARAMETER_FEATURE_COUNT + 2;

    /**
	 * The number of structural features of the '<em>Request Parameter</em>' class.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
    int REQUEST_PARAMETER_FEATURE_COUNT = PARAMETER_FEATURE_COUNT + 3;

    /**
	 * The number of operations of the '<em>Request Parameter</em>' class.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
    int REQUEST_PARAMETER_OPERATION_COUNT = PARAMETER_OPERATION_COUNT + 0;

    /**
	 * The meta object id for the '{@link ca.mcgill.sel.restif.impl.HeaderParameterImpl <em>Header Parameter</em>}' class.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @see ca.mcgill.sel.restif.impl.HeaderParameterImpl
	 * @see ca.mcgill.sel.restif.impl.RestifPackageImpl#getHeaderParameter()
	 * @generated
	 */
    int HEADER_PARAMETER = 10;

    /**
	 * The meta object id for the '{@link ca.mcgill.sel.restif.impl.BodyImpl <em>Body</em>}' class.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @see ca.mcgill.sel.restif.impl.BodyImpl
	 * @see ca.mcgill.sel.restif.impl.RestifPackageImpl#getBody()
	 * @generated
	 */
    int BODY = 8;

    /**
	 * The number of structural features of the '<em>Body</em>' class.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
    int BODY_FEATURE_COUNT = PARAMETER_FEATURE_COUNT + 0;

    /**
	 * The number of operations of the '<em>Body</em>' class.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
    int BODY_OPERATION_COUNT = PARAMETER_OPERATION_COUNT + 0;

    /**
	 * The feature id for the '<em><b>Header Field</b></em>' attribute.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
    int HEADER_PARAMETER__HEADER_FIELD = PARAMETER_FEATURE_COUNT + 0;

    /**
	 * The number of structural features of the '<em>Header Parameter</em>' class.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
    int HEADER_PARAMETER_FEATURE_COUNT = PARAMETER_FEATURE_COUNT + 1;

    /**
	 * The number of operations of the '<em>Header Parameter</em>' class.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
    int HEADER_PARAMETER_OPERATION_COUNT = PARAMETER_OPERATION_COUNT + 0;

    /**
	 * The meta object id for the '{@link ca.mcgill.sel.restif.MethodType <em>Method Type</em>}' enum.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @see ca.mcgill.sel.restif.MethodType
	 * @see ca.mcgill.sel.restif.impl.RestifPackageImpl#getMethodType()
	 * @generated
	 */
    int METHOD_TYPE = 11;

    /**
	 * The meta object id for the '{@link ca.mcgill.sel.restif.HeaderField <em>Header Field</em>}' enum.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @see ca.mcgill.sel.restif.HeaderField
	 * @see ca.mcgill.sel.restif.impl.RestifPackageImpl#getHeaderField()
	 * @generated
	 */
    int HEADER_FIELD = 12;


    /**
	 * Returns the meta object for class '{@link ca.mcgill.sel.restif.RestIF <em>Rest IF</em>}'.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Rest IF</em>'.
	 * @see ca.mcgill.sel.restif.RestIF
	 * @generated
	 */
    EClass getRestIF();

    /**
	 * Returns the meta object for the containment reference '{@link ca.mcgill.sel.restif.RestIF#getRoot <em>Root</em>}'.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Root</em>'.
	 * @see ca.mcgill.sel.restif.RestIF#getRoot()
	 * @see #getRestIF()
	 * @generated
	 */
    EReference getRestIF_Root();

    /**
	 * Returns the meta object for the containment reference list '{@link ca.mcgill.sel.restif.RestIF#getResource <em>Resource</em>}'.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Resource</em>'.
	 * @see ca.mcgill.sel.restif.RestIF#getResource()
	 * @see #getRestIF()
	 * @generated
	 */
    EReference getRestIF_Resource();

    /**
	 * Returns the meta object for class '{@link ca.mcgill.sel.restif.PathFragment <em>Path Fragment</em>}'.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Path Fragment</em>'.
	 * @see ca.mcgill.sel.restif.PathFragment
	 * @generated
	 */
    EClass getPathFragment();

    /**
	 * Returns the meta object for the containment reference list '{@link ca.mcgill.sel.restif.PathFragment#getChild <em>Child</em>}'.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Child</em>'.
	 * @see ca.mcgill.sel.restif.PathFragment#getChild()
	 * @see #getPathFragment()
	 * @generated
	 */
    EReference getPathFragment_Child();

    /**
	 * Returns the meta object for class '{@link ca.mcgill.sel.restif.AccessMethod <em>Access Method</em>}'.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Access Method</em>'.
	 * @see ca.mcgill.sel.restif.AccessMethod
	 * @generated
	 */
    EClass getAccessMethod();

    /**
	 * Returns the meta object for the containment reference list '{@link ca.mcgill.sel.restif.AccessMethod#getInputParameter <em>Input Parameter</em>}'.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Input Parameter</em>'.
	 * @see ca.mcgill.sel.restif.AccessMethod#getInputParameter()
	 * @see #getAccessMethod()
	 * @generated
	 */
    EReference getAccessMethod_InputParameter();

    /**
	 * Returns the meta object for the containment reference '{@link ca.mcgill.sel.restif.AccessMethod#getResult <em>Result</em>}'.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Result</em>'.
	 * @see ca.mcgill.sel.restif.AccessMethod#getResult()
	 * @see #getAccessMethod()
	 * @generated
	 */
    EReference getAccessMethod_Result();

    /**
	 * Returns the meta object for the attribute '{@link ca.mcgill.sel.restif.AccessMethod#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see ca.mcgill.sel.restif.AccessMethod#getType()
	 * @see #getAccessMethod()
	 * @generated
	 */
    EAttribute getAccessMethod_Type();

    /**
	 * Returns the meta object for the containment reference list '{@link ca.mcgill.sel.restif.AccessMethod#getResultMetaParameter <em>Result Meta Parameter</em>}'.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Result Meta Parameter</em>'.
	 * @see ca.mcgill.sel.restif.AccessMethod#getResultMetaParameter()
	 * @see #getAccessMethod()
	 * @generated
	 */
    EReference getAccessMethod_ResultMetaParameter();

    /**
	 * Returns the meta object for class '{@link ca.mcgill.sel.restif.StaticFragment <em>Static Fragment</em>}'.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Static Fragment</em>'.
	 * @see ca.mcgill.sel.restif.StaticFragment
	 * @generated
	 */
    EClass getStaticFragment();

    /**
	 * Returns the meta object for the attribute '{@link ca.mcgill.sel.restif.StaticFragment#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see ca.mcgill.sel.restif.StaticFragment#getName()
	 * @see #getStaticFragment()
	 * @generated
	 */
    EAttribute getStaticFragment_Name();

    /**
	 * Returns the meta object for class '{@link ca.mcgill.sel.restif.DynamicFragment <em>Dynamic Fragment</em>}'.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dynamic Fragment</em>'.
	 * @see ca.mcgill.sel.restif.DynamicFragment
	 * @generated
	 */
    EClass getDynamicFragment();

    /**
	 * Returns the meta object for the attribute '{@link ca.mcgill.sel.restif.DynamicFragment#getPlaceholder <em>Placeholder</em>}'.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Placeholder</em>'.
	 * @see ca.mcgill.sel.restif.DynamicFragment#getPlaceholder()
	 * @see #getDynamicFragment()
	 * @generated
	 */
    EAttribute getDynamicFragment_Placeholder();

    /**
	 * Returns the meta object for class '{@link ca.mcgill.sel.restif.Resource <em>Resource</em>}'.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Resource</em>'.
	 * @see ca.mcgill.sel.restif.Resource
	 * @generated
	 */
    EClass getResource();

    /**
	 * Returns the meta object for the containment reference list '{@link ca.mcgill.sel.restif.Resource#getAccessmethod <em>Accessmethod</em>}'.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Accessmethod</em>'.
	 * @see ca.mcgill.sel.restif.Resource#getAccessmethod()
	 * @see #getResource()
	 * @generated
	 */
    EReference getResource_Accessmethod();

    /**
	 * Returns the meta object for the reference '{@link ca.mcgill.sel.restif.Resource#getEndpoint <em>Endpoint</em>}'.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Endpoint</em>'.
	 * @see ca.mcgill.sel.restif.Resource#getEndpoint()
	 * @see #getResource()
	 * @generated
	 */
    EReference getResource_Endpoint();

    /**
	 * Returns the meta object for class '{@link ca.mcgill.sel.restif.Parameter <em>Parameter</em>}'.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Parameter</em>'.
	 * @see ca.mcgill.sel.restif.Parameter
	 * @generated
	 */
    EClass getParameter();

    /**
	 * Returns the meta object for class '{@link ca.mcgill.sel.restif.RequestParameter <em>Request Parameter</em>}'.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Request Parameter</em>'.
	 * @see ca.mcgill.sel.restif.RequestParameter
	 * @generated
	 */
    EClass getRequestParameter();

    /**
	 * Returns the meta object for the attribute '{@link ca.mcgill.sel.restif.RequestParameter#getDefaultValue <em>Default Value</em>}'.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Default Value</em>'.
	 * @see ca.mcgill.sel.restif.RequestParameter#getDefaultValue()
	 * @see #getRequestParameter()
	 * @generated
	 */
    EAttribute getRequestParameter_DefaultValue();

    /**
	 * Returns the meta object for the attribute '{@link ca.mcgill.sel.restif.RequestParameter#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see ca.mcgill.sel.restif.RequestParameter#getName()
	 * @see #getRequestParameter()
	 * @generated
	 */
    EAttribute getRequestParameter_Name();

    /**
	 * Returns the meta object for the attribute '{@link ca.mcgill.sel.restif.RequestParameter#isMandatory <em>Mandatory</em>}'.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Mandatory</em>'.
	 * @see ca.mcgill.sel.restif.RequestParameter#isMandatory()
	 * @see #getRequestParameter()
	 * @generated
	 */
    EAttribute getRequestParameter_Mandatory();

    /**
	 * Returns the meta object for class '{@link ca.mcgill.sel.restif.HeaderParameter <em>Header Parameter</em>}'.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Header Parameter</em>'.
	 * @see ca.mcgill.sel.restif.HeaderParameter
	 * @generated
	 */
    EClass getHeaderParameter();

    /**
	 * Returns the meta object for the attribute '{@link ca.mcgill.sel.restif.HeaderParameter#getHeaderField <em>Header Field</em>}'.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Header Field</em>'.
	 * @see ca.mcgill.sel.restif.HeaderParameter#getHeaderField()
	 * @see #getHeaderParameter()
	 * @generated
	 */
    EAttribute getHeaderParameter_HeaderField();

    /**
	 * Returns the meta object for class '{@link ca.mcgill.sel.restif.Body <em>Body</em>}'.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Body</em>'.
	 * @see ca.mcgill.sel.restif.Body
	 * @generated
	 */
    EClass getBody();

    /**
	 * Returns the meta object for class '{@link ca.mcgill.sel.restif.NamedElement <em>Named Element</em>}'.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Named Element</em>'.
	 * @see ca.mcgill.sel.restif.NamedElement
	 * @generated
	 */
    EClass getNamedElement();

    /**
	 * Returns the meta object for the attribute '{@link ca.mcgill.sel.restif.NamedElement#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see ca.mcgill.sel.restif.NamedElement#getName()
	 * @see #getNamedElement()
	 * @generated
	 */
    EAttribute getNamedElement_Name();

    /**
	 * Returns the meta object for enum '{@link ca.mcgill.sel.restif.MethodType <em>Method Type</em>}'.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Method Type</em>'.
	 * @see ca.mcgill.sel.restif.MethodType
	 * @generated
	 */
    EEnum getMethodType();

    /**
	 * Returns the meta object for enum '{@link ca.mcgill.sel.restif.HeaderField <em>Header Field</em>}'.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Header Field</em>'.
	 * @see ca.mcgill.sel.restif.HeaderField
	 * @generated
	 */
    EEnum getHeaderField();

    /**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
    RestifFactory getRestifFactory();

    /**
	 * <!-- begin-user-doc -->
     * Defines literals for the meta objects that represent
     * <ul>
     *   <li>each class,</li>
     *   <li>each feature of each class,</li>
     *   <li>each operation of each class,</li>
     *   <li>each enum,</li>
     *   <li>and each data type</li>
     * </ul>
     * <!-- end-user-doc -->
	 * @generated
	 */
    interface Literals {
        /**
		 * The meta object literal for the '{@link ca.mcgill.sel.restif.impl.RestIFImpl <em>Rest IF</em>}' class.
		 * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
		 * @see ca.mcgill.sel.restif.impl.RestIFImpl
		 * @see ca.mcgill.sel.restif.impl.RestifPackageImpl#getRestIF()
		 * @generated
		 */
        EClass REST_IF = eINSTANCE.getRestIF();

        /**
		 * The meta object literal for the '<em><b>Root</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
		 * @generated
		 */
        EReference REST_IF__ROOT = eINSTANCE.getRestIF_Root();

        /**
		 * The meta object literal for the '<em><b>Resource</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
		 * @generated
		 */
        EReference REST_IF__RESOURCE = eINSTANCE.getRestIF_Resource();

        /**
		 * The meta object literal for the '{@link ca.mcgill.sel.restif.impl.PathFragmentImpl <em>Path Fragment</em>}' class.
		 * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
		 * @see ca.mcgill.sel.restif.impl.PathFragmentImpl
		 * @see ca.mcgill.sel.restif.impl.RestifPackageImpl#getPathFragment()
		 * @generated
		 */
        EClass PATH_FRAGMENT = eINSTANCE.getPathFragment();

        /**
		 * The meta object literal for the '<em><b>Child</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
		 * @generated
		 */
        EReference PATH_FRAGMENT__CHILD = eINSTANCE.getPathFragment_Child();

        /**
		 * The meta object literal for the '{@link ca.mcgill.sel.restif.impl.AccessMethodImpl <em>Access Method</em>}' class.
		 * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
		 * @see ca.mcgill.sel.restif.impl.AccessMethodImpl
		 * @see ca.mcgill.sel.restif.impl.RestifPackageImpl#getAccessMethod()
		 * @generated
		 */
        EClass ACCESS_METHOD = eINSTANCE.getAccessMethod();

        /**
		 * The meta object literal for the '<em><b>Input Parameter</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
		 * @generated
		 */
        EReference ACCESS_METHOD__INPUT_PARAMETER = eINSTANCE.getAccessMethod_InputParameter();

        /**
		 * The meta object literal for the '<em><b>Result</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
		 * @generated
		 */
        EReference ACCESS_METHOD__RESULT = eINSTANCE.getAccessMethod_Result();

        /**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
		 * @generated
		 */
        EAttribute ACCESS_METHOD__TYPE = eINSTANCE.getAccessMethod_Type();

        /**
		 * The meta object literal for the '<em><b>Result Meta Parameter</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
		 * @generated
		 */
        EReference ACCESS_METHOD__RESULT_META_PARAMETER = eINSTANCE.getAccessMethod_ResultMetaParameter();

        /**
		 * The meta object literal for the '{@link ca.mcgill.sel.restif.impl.StaticFragmentImpl <em>Static Fragment</em>}' class.
		 * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
		 * @see ca.mcgill.sel.restif.impl.StaticFragmentImpl
		 * @see ca.mcgill.sel.restif.impl.RestifPackageImpl#getStaticFragment()
		 * @generated
		 */
        EClass STATIC_FRAGMENT = eINSTANCE.getStaticFragment();

        /**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
		 * @generated
		 */
        EAttribute STATIC_FRAGMENT__NAME = eINSTANCE.getStaticFragment_Name();

        /**
		 * The meta object literal for the '{@link ca.mcgill.sel.restif.impl.DynamicFragmentImpl <em>Dynamic Fragment</em>}' class.
		 * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
		 * @see ca.mcgill.sel.restif.impl.DynamicFragmentImpl
		 * @see ca.mcgill.sel.restif.impl.RestifPackageImpl#getDynamicFragment()
		 * @generated
		 */
        EClass DYNAMIC_FRAGMENT = eINSTANCE.getDynamicFragment();

        /**
		 * The meta object literal for the '<em><b>Placeholder</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
		 * @generated
		 */
        EAttribute DYNAMIC_FRAGMENT__PLACEHOLDER = eINSTANCE.getDynamicFragment_Placeholder();

        /**
		 * The meta object literal for the '{@link ca.mcgill.sel.restif.impl.ResourceImpl <em>Resource</em>}' class.
		 * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
		 * @see ca.mcgill.sel.restif.impl.ResourceImpl
		 * @see ca.mcgill.sel.restif.impl.RestifPackageImpl#getResource()
		 * @generated
		 */
        EClass RESOURCE = eINSTANCE.getResource();

        /**
		 * The meta object literal for the '<em><b>Accessmethod</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
		 * @generated
		 */
        EReference RESOURCE__ACCESSMETHOD = eINSTANCE.getResource_Accessmethod();

        /**
		 * The meta object literal for the '<em><b>Endpoint</b></em>' reference feature.
		 * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
		 * @generated
		 */
        EReference RESOURCE__ENDPOINT = eINSTANCE.getResource_Endpoint();

        /**
		 * The meta object literal for the '{@link ca.mcgill.sel.restif.impl.ParameterImpl <em>Parameter</em>}' class.
		 * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
		 * @see ca.mcgill.sel.restif.impl.ParameterImpl
		 * @see ca.mcgill.sel.restif.impl.RestifPackageImpl#getParameter()
		 * @generated
		 */
        EClass PARAMETER = eINSTANCE.getParameter();

        /**
		 * The meta object literal for the '{@link ca.mcgill.sel.restif.impl.RequestParameterImpl <em>Request Parameter</em>}' class.
		 * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
		 * @see ca.mcgill.sel.restif.impl.RequestParameterImpl
		 * @see ca.mcgill.sel.restif.impl.RestifPackageImpl#getRequestParameter()
		 * @generated
		 */
        EClass REQUEST_PARAMETER = eINSTANCE.getRequestParameter();

        /**
		 * The meta object literal for the '<em><b>Default Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
		 * @generated
		 */
        EAttribute REQUEST_PARAMETER__DEFAULT_VALUE = eINSTANCE.getRequestParameter_DefaultValue();

        /**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
		 * @generated
		 */
        EAttribute REQUEST_PARAMETER__NAME = eINSTANCE.getRequestParameter_Name();

        /**
		 * The meta object literal for the '<em><b>Mandatory</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
		 * @generated
		 */
        EAttribute REQUEST_PARAMETER__MANDATORY = eINSTANCE.getRequestParameter_Mandatory();

        /**
		 * The meta object literal for the '{@link ca.mcgill.sel.restif.impl.HeaderParameterImpl <em>Header Parameter</em>}' class.
		 * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
		 * @see ca.mcgill.sel.restif.impl.HeaderParameterImpl
		 * @see ca.mcgill.sel.restif.impl.RestifPackageImpl#getHeaderParameter()
		 * @generated
		 */
        EClass HEADER_PARAMETER = eINSTANCE.getHeaderParameter();

        /**
		 * The meta object literal for the '<em><b>Header Field</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
		 * @generated
		 */
        EAttribute HEADER_PARAMETER__HEADER_FIELD = eINSTANCE.getHeaderParameter_HeaderField();

        /**
		 * The meta object literal for the '{@link ca.mcgill.sel.restif.impl.BodyImpl <em>Body</em>}' class.
		 * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
		 * @see ca.mcgill.sel.restif.impl.BodyImpl
		 * @see ca.mcgill.sel.restif.impl.RestifPackageImpl#getBody()
		 * @generated
		 */
        EClass BODY = eINSTANCE.getBody();

        /**
		 * The meta object literal for the '{@link ca.mcgill.sel.restif.impl.NamedElementImpl <em>Named Element</em>}' class.
		 * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
		 * @see ca.mcgill.sel.restif.impl.NamedElementImpl
		 * @see ca.mcgill.sel.restif.impl.RestifPackageImpl#getNamedElement()
		 * @generated
		 */
        EClass NAMED_ELEMENT = eINSTANCE.getNamedElement();

        /**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
		 * @generated
		 */
        EAttribute NAMED_ELEMENT__NAME = eINSTANCE.getNamedElement_Name();

        /**
		 * The meta object literal for the '{@link ca.mcgill.sel.restif.MethodType <em>Method Type</em>}' enum.
		 * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
		 * @see ca.mcgill.sel.restif.MethodType
		 * @see ca.mcgill.sel.restif.impl.RestifPackageImpl#getMethodType()
		 * @generated
		 */
        EEnum METHOD_TYPE = eINSTANCE.getMethodType();

        /**
		 * The meta object literal for the '{@link ca.mcgill.sel.restif.HeaderField <em>Header Field</em>}' enum.
		 * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
		 * @see ca.mcgill.sel.restif.HeaderField
		 * @see ca.mcgill.sel.restif.impl.RestifPackageImpl#getHeaderField()
		 * @generated
		 */
        EEnum HEADER_FIELD = eINSTANCE.getHeaderField();

    }

} //RestifPackage
