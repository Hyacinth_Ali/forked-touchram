package ca.mcgill.sel.environmentmodel.language.controller;

/**
 * A factory to obtain controllers for environment model
 * 
 * @author hyacinthali
 *
 */
public class EnvironmentModelControllerFactory {
	
	/**
     * The singleton instance of this factory.
     */
    public static final EnvironmentModelControllerFactory INSTANCE = new EnvironmentModelControllerFactory();
    
    private EnvironmentModelController environmentModelController ;
        
    /**
     * Creates a new instance of {@link EnvironmentModelControllerFactory}.
     */
    private EnvironmentModelControllerFactory() {
        
    }

	/**
	 * Returns the controller for {@link EnvironmentModelController}.
	 * 
	 * @return the environmentModelController
	 */
	public EnvironmentModelController getEnvironmentModelController() {
		if (environmentModelController == null) {
			environmentModelController = new EnvironmentModelController();
        }
		return environmentModelController;
	}
    
    

}
