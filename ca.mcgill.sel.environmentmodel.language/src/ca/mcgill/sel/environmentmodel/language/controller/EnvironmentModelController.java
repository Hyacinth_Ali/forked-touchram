package ca.mcgill.sel.environmentmodel.language.controller;

import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.command.CompoundCommand;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.edit.command.AddCommand;
import org.eclipse.emf.edit.command.RemoveCommand;
import org.eclipse.emf.edit.domain.EditingDomain;

import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.environmentmodel.Actor;
import ca.mcgill.sel.environmentmodel.EmFactory;
import ca.mcgill.sel.environmentmodel.EmPackage;
import ca.mcgill.sel.environmentmodel.EnvironmentModel;
import ca.mcgill.sel.environmentmodel.Message;
import ca.mcgill.sel.environmentmodel.MessageDirection;
import ca.mcgill.sel.environmentmodel.MessageType;

/**
 * The controller for {@link EnvironmentModel}
 * @author hyacinthali
 *
 */
public class EnvironmentModelController extends BaseController{
	
	 /**
     * Creates a new instance of {@link EnvironmentModelController}.
     */
    protected EnvironmentModelController() {
        // Prevent anyone outside this package to instantiate.
    }
    
    /**
     * Creates new actor in environment model.
     * @param owner - the container {@link EnvironmentModel} of the actor
     * @param name - name of the actor
     * @param lowerBound - the lowest multiplicity of the actor
     * @param upperBound - the maximum multiplicity of the actor
     * @param communicationLowerBound - the maximum number of the corresponding message(s)
     * @param communicationUpperBound - the lowest number of the corresponding message(s)
     */
    public void createActor(EnvironmentModel owner, String name, int lowerBound, int upperBound, 
    		int communicationLowerBound, int communicationUpperBound) {
    	
    	Actor newActor = EmFactory.eINSTANCE.createActor();
        newActor.setName(name);
        newActor.setActorLowerBound(lowerBound);
        newActor.setActorUpperBound(upperBound);

        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(owner);

        // Create commands.
        Command addActorCommand = AddCommand.create(editingDomain, owner, EmPackage.Literals.ENVIRONMENT_MODEL__ACTORS,
                newActor);
        
        // Create compound command.
        CompoundCommand compoundCommand = new CompoundCommand();
        compoundCommand.append(addActorCommand);
        
        doExecute(editingDomain, compoundCommand);
    	
    }
    

    /**
     * Creates either input or output message in an environment model.
     * @param em - the environment model
     * @param actor - the actor that contains the message
     * @param name - name of the message
     * @param messageDirection - either input message or output message.
     */
    public void createMessage(EnvironmentModel em, Actor actor, String name, MessageDirection messageDirection) {
    	
    	// create message
    	Message message = EmFactory.eINSTANCE.createMessage();
    	message.setMessageDirection(messageDirection);
    	
    	// check if the type of the message exist
    	MessageType type = messageTypeExist(em, name);
    	if (type == null) {
    		// create new message type
    		type = createMessageType(em, name);
    	}
    	
    	message.setMessageType(type);
    	
    	EditingDomain editingDomain = EMFEditUtil.getEditingDomain(em);
    	
    	// Create commands.
        Command addMessageCommand = AddCommand.create(editingDomain, actor, 
        		EmPackage.Literals.ACTOR__MESSAGES, message);
        
        // Create compound command.
        CompoundCommand compoundCommand = new CompoundCommand();
        compoundCommand.append(addMessageCommand);
        
        doExecute(editingDomain, compoundCommand);
    	
    }
    
    /**
     * Create new message type.
     * @param em - the environment model
     * @param name - name of the message type
     * @return - the message type.
     */
    private MessageType createMessageType(EnvironmentModel em, String name) {
    	
    	  
        // create communication
        MessageType type = EmFactory.eINSTANCE.createMessageType();
        type.setName(name);
        
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(em);

        // Create commands.
        Command addMessageTypeCommand = AddCommand.create(editingDomain, em, 
        		EmPackage.Literals.ENVIRONMENT_MODEL__MESSAGE_TYPES, type);
        
        // Create compound command.
        CompoundCommand compoundCommand = new CompoundCommand();
        compoundCommand.append(addMessageTypeCommand);
        
        // execute commands
        doExecute(editingDomain, compoundCommand);
        
        return type;
		
	}

	/**
     * Return an existing message type.
     * @param em - environment model
     * @param name - name of the message type
     * @return the message type
     */
    private MessageType messageTypeExist(EnvironmentModel em, String name) {
    	MessageType type = null;
		
		for (MessageType t : em.getMessageTypes()) {
			if (t.getName().equals(name)) {
				type = t;
			}
		}
		return type;
	}
    
    /**
     * Removes the given actor and its composed messages
     * @param actor - the actor to be removed
     */
    public void removeActor(Actor actor) {
    	EnvironmentModel em = (EnvironmentModel) actor.eContainer();
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(em);
        
        CompoundCommand compoundCommand = new CompoundCommand();
       

        // create commands to remove composed messages
        for (Message message : actor.getMessages()) {
        	compoundCommand.append(RemoveCommand.create(editingDomain, message));
        }

        // Create command for removing ElementMap (includes the layout element).
//        Command removeElementMapCommand = createRemoveLayoutElementCommand(editingDomain, em, actor);
//        compoundCommand.append(removeElementMapCommand);

        // Create command for removing actor.
        Command removeActorCommand = RemoveCommand.create(editingDomain, actor);
        compoundCommand.append(removeActorCommand);

        doExecute(editingDomain, compoundCommand);
    }
    
    /**
     * Removes the given message.
     *
     * @param message - the message to be removed
     */
    public void removeMessage(Message message) {
        EnvironmentModel em = (EnvironmentModel) EcoreUtil.getRootContainer(message);
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(em);
        CompoundCommand compoundCommand = new CompoundCommand();

        compoundCommand.append(RemoveCommand.create(editingDomain, message));

        doExecute(editingDomain, compoundCommand);
    }

}
