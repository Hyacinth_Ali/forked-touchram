package ca.mcgill.sel.environmentmodel.language.controller;

import ca.mcgill.sel.core.controller.CoreBaseController;

/**
 * The controller with basic functionality that can be used by any sub-class in environment controller.
 * @author hyacinthali
 *
 */
public class BaseController extends CoreBaseController {

}
