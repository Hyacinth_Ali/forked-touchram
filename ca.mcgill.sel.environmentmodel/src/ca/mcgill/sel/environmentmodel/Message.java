/**
 */
package ca.mcgill.sel.environmentmodel;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Message</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.environmentmodel.Message#getMessageType <em>Message Type</em>}</li>
 *   <li>{@link ca.mcgill.sel.environmentmodel.Message#getMessageDirection <em>Message Direction</em>}</li>
 * </ul>
 *
 * @see ca.mcgill.sel.environmentmodel.EmPackage#getMessage()
 * @model
 * @generated
 */
public interface Message extends EObject {
	/**
     * Returns the value of the '<em><b>Message Type</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Message Type</em>' reference.
     * @see #setMessageType(MessageType)
     * @see ca.mcgill.sel.environmentmodel.EmPackage#getMessage_MessageType()
     * @model required="true"
     * @generated
     */
	MessageType getMessageType();

	/**
     * Sets the value of the '{@link ca.mcgill.sel.environmentmodel.Message#getMessageType <em>Message Type</em>}' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Message Type</em>' reference.
     * @see #getMessageType()
     * @generated
     */
	void setMessageType(MessageType value);

	/**
     * Returns the value of the '<em><b>Message Direction</b></em>' attribute.
     * The literals are from the enumeration {@link ca.mcgill.sel.environmentmodel.MessageDirection}.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Message Direction</em>' attribute.
     * @see ca.mcgill.sel.environmentmodel.MessageDirection
     * @see #setMessageDirection(MessageDirection)
     * @see ca.mcgill.sel.environmentmodel.EmPackage#getMessage_MessageDirection()
     * @model
     * @generated
     */
	MessageDirection getMessageDirection();

	/**
     * Sets the value of the '{@link ca.mcgill.sel.environmentmodel.Message#getMessageDirection <em>Message Direction</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Message Direction</em>' attribute.
     * @see ca.mcgill.sel.environmentmodel.MessageDirection
     * @see #getMessageDirection()
     * @generated
     */
	void setMessageDirection(MessageDirection value);

} // Message
