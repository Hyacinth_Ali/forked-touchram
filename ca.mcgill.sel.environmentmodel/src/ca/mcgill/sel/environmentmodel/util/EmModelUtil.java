package ca.mcgill.sel.environmentmodel.util;

import org.eclipse.emf.ecore.EObject;

import ca.mcgill.sel.core.controller.ModelUtil;
import ca.mcgill.sel.environmentmodel.EmFactory;
import ca.mcgill.sel.environmentmodel.EnvironmentModel;

/**
 * Helper class with convenient static methods for working with evironment model objects.
 * @author hyacinthali
 *
 */
public class EmModelUtil implements ModelUtil {
	
	/**
     * Creates a new use case diagram and associated entities
     * @param name The name of the diagram
     * @return The new diagram
     */
    public static EnvironmentModel createEnvironmentModel(String name) {
    	
    	EnvironmentModel em = EmFactory.eINSTANCE.createEnvironmentModel();       
    	em.setName(name);
    	em.setSystemName(name);
        return em;
    }

	@Override
	public EObject createNewEmptyModel(String name) {
		return createEnvironmentModel(name);
	}

}
