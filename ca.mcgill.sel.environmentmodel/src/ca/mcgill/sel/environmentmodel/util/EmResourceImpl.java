/**
 */
package ca.mcgill.sel.environmentmodel.util;

import org.eclipse.emf.common.util.URI;

import org.eclipse.emf.ecore.xmi.impl.XMLResourceImpl;

/**
 * <!-- begin-user-doc -->
 * The <b>Resource </b> associated with the package.
 * <!-- end-user-doc -->
 * @see ca.mcgill.sel.environmentmodel.util.EmResourceFactoryImpl
 * @generated
 */
public class EmResourceImpl extends XMLResourceImpl {
	/**
     * Creates an instance of the resource.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param uri the URI of the new resource.
     * @generated
     */
	public EmResourceImpl(URI uri) {
        super(uri);
    }

} //EmResourceImpl
