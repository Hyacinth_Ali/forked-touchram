package ca.mcgill.sel.ram.loaders;

import java.util.ArrayList;
import java.util.List;

import ca.mcgill.sel.ram.loaders.interfaces.IRamClassSearcher;

/**
 * Searches through a list of class names for the class name that is the best match for the search string provided as
 * input.
 * 
 * @author Franz
 * @author arthurls
 */
public final class RamClassSearcher implements IRamClassSearcher {

    /**
     * Singleton.
     */
    public static final RamClassSearcher INSTANCE = new RamClassSearcher();
    
    // private static final int MIN_NUM_RESULTS = 10;

    /**
     * Constructor.
     */
    public RamClassSearcher() {
        
    }

    /**
     * Returns a list of loadable classes. 
     * The list consists of a subset of classes from the given class list that match the given class name.
     * 
     * @param className the part of a class name that is searched
     * @param classList the list of available classes to load
     * @return a list of recommended classes that can be loaded
     */
    public List<String> getRecommendedLoadable(String className, List<String> classList) {
        List<String> result = new ArrayList<String>();
        String lowerCase = className.toLowerCase();
        for (String curVal : classList) {
            if (curVal.toLowerCase().contains(lowerCase)) {
                result.add(curVal);
            }
        }
        return result;
    }

}
